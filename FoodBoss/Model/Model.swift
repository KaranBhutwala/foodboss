//
//  Model.swift
//  FoodBoss
//
//  Created by Raj Shukla on 06/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import Foundation

struct LanguageListModel{
    var message : String?
    var success : Int?
    var languages : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.languages = (dictionary["languages"] as! NSArray)
    }
}

struct LoginModel{
    var message : String?
    var success : Int?
    var data : NSDictionary?
    
    init?(dictionary : NSDictionary){
        
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        if (dictionary != nil)  {
            if let json = dictionary as? [String: Any?] {
                let result = json.compactMapValues { $0 }
                print(result)
                
                if let DictUser = result as? NSDictionary{
                    data = DictUser

                    NSUSERDEFAULT.set(data, forKey: USER_DATA)
                    NSUSERDEFAULT.set("Yes", forKey: LOGIN)
                }
            }
            
        }
    }
}

struct CountryModel{
    var message : String?
    var success : Int?
    var countries : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.countries = (dictionary["countries"] as! NSArray)
    }
}

struct AddressModel{
    var message : String?
    var success : Int?
    var user_addresses : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.user_addresses = (dictionary["user_addresses"] as! NSArray)
    }
}


struct CouponListModel{
    var message : String?
    var success : Int?
    var coupons : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.coupons = (dictionary["coupons"] as! NSArray)
    }
}


struct CategoriesModel{
    var message : String?
    var success : Int?
    var categories : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.categories = (dictionary["categories"] as! NSArray)
    }
}


struct RestaurantModel{
    var message : String?
    var success : Int?
    var restaurants : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.restaurants = (dictionary["restaurants"] as! NSArray)
    }
}

struct FavouriteRestaurantModel{
    var message : String?
    var success : Int?
    var favourite_restaurants : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.favourite_restaurants = (dictionary["favourite_restaurants"] as! NSArray)
    }
}

struct NotificationModel{
    var message : String?
    var success : Int?
    var notifications : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.notifications = (dictionary["notifications"] as! NSArray)
    }
}

struct SearchModel{
    var message : String?
    var success : Int?
    var search : NSDictionary?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.search = (dictionary["search"] as! NSDictionary)
    }
}

struct CuisineListModel{
    var message : String?
    var success : Int?
    var cuisine : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.cuisine = (dictionary["cuisines"] as! NSArray)
    }
}

struct CartListModel{
    var message : String?
    var success : Int?
    var cart : NSDictionary?
    var restaurant : NSDictionary?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.cart = dictionary["cart"] as? NSDictionary
        self.restaurant = dictionary["restaurant"] as? NSDictionary
    }
}

struct OrderListModel{
    var message : String?
    var success : Int?
    var orders : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.orders = (dictionary["orders"] as! NSArray)
    }
}

struct PaymentListModel{
    var message : String?
    var success : Int?
    var payments : NSArray?
    
    init?(dictionary : NSDictionary){
        self.message = dictionary["message"] as? String
        self.success = dictionary["success"] as? Int
        self.payments = (dictionary["payments"] as! NSArray)
    }
}

struct PaymentMethodModel {
    let id: String
    let name: String
    let details: String
    let shippingCharges: Int
    let paymentCharges: Int
    
    init?(data: Dictionary<String, Any>) {
        guard
            let id = data["id"] as? String,
            !id.isEmpty,
            let name = data["name"] as? String,
            !name.isEmpty,
            let details = data["details"] as? String,
            let shippingCharges = data["shipping_charges"] as? Int,
            let paymentCharges = data["payment_charges"] as? Int else { return nil }
        
        self.id = id
        self.name = name
        self.details = details
        self.shippingCharges = shippingCharges
        self.paymentCharges = paymentCharges
        
    }
}

struct PayPalPaymentMethod {
    enum PayPalPaymentType: String {
        case card, paypal
    }
    
    let token: String
    var isDefault: Bool
    let expirationDate: String?
    let maskedNumber: String?
    let imageUrl: String
    let email: String?
    let cardType: String?
    let paymentType: PayPalPaymentType
    
    init?(data: Dictionary<String, Any>) {
        guard
            let token = data["token"] as? String,
            let imageUrl = data["image_url"] as? String,
            let isDefault = data["default"] as? Bool else { return nil }
        
        self.token = token
        self.isDefault = isDefault
        self.imageUrl = imageUrl
        self.expirationDate = data["expirationDate"] as? String
        self.maskedNumber = data["maskedNumber"] as? String
        self.email = data["email"] as? String
        self.cardType = data["card_type"] as? String
        if let _ = data["maskedNumber"] as? String {
            self.paymentType = .card
        } else {
            self.paymentType = .paypal
        }
    }
}

struct RestaurantReview {
    let reviews: [UserReview]
    let finalReview: String
    let finalReviewCount: String
    
    static func getEmptyReview() -> RestaurantReview {
        return RestaurantReview(reviews: [], finalReview: "0", finalReviewCount: "0")
    }
}

struct UserReview: Codable {
    let userName: String
    let userId: String
    let mobile: String
    let email: String
    let profilePicString: String
    let rating: String
    let review: String
    
    enum CodingKeys: String, CodingKey {
        case rating
        case review
        case userId = "user_id"
        case mobile
        case email
        case profilePicString = "profile_pic"
        case userName = "user_name"
    }
}

struct RestaurantTime: Codable {
    let today: Int
    let openClose: String
    let day: String
    let startTime: String
    let closeTime: String
    
    enum CodingKeys: String, CodingKey {
        case today
        case openClose = "open_close"
        case day
        case startTime = "start_time"
        case closeTime = "close_time"
    }
}

struct RestaurantInfo {
    let timing: [RestaurantTime]
    let restaurantName: String
    let address: String
    let description: String
    
    static func getEmptyInfo() -> RestaurantInfo {
        return RestaurantInfo(timing: [], restaurantName: "", address: "", description: "")
    }
}


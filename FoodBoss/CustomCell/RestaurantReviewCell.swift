//
//  RestaurantReviewCell.swift
//  FoodBoss
//
//  Created by Apple on 01/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class RestaurantReviewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

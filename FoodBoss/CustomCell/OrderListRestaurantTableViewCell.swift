//
//  OrderListRestaurantTableViewCell.swift
//  FoodBoss
//
//  Created by Jay on 04/07/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol ReOrderActionDelegate : class {
    func ReorderButtonPressed(_ index: IndexPath)
}

class OrderListRestaurantTableViewCell: UITableViewCell {
    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOrderPlaced: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblOrderItems: UILabel!
    
    var index = IndexPath()
    var delegate:ReOrderActionDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblOrderPlaced.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary, DictItem : NSDictionary) {
        if let str = Dict.value(forKey: "restaurant_name") as? String{
            lblRestaurantName.text = str
        }
        if let str = Dict.value(forKey: "location") as? String{
            lblArea.text = str
        }
        if let str = Dict.value(forKey: "sub_total") as? String{
            lblPrice.text = CURRANCY + " " + str
        }
        if let str = Dict.value(forKey: "order_date") as? String{
            lblDate.text = str
        }
        if let str = Dict.value(forKey: "status") as? String{
            lblOrderPlaced.text = "  " + str.capitalized + "  "
        }
        
        let pMode = Dict["payment_type_name"] as? String ?? ""
        self.lblPaymentMode.text = "Payment mode".localized + " : " + pMode
        
        imgType.image = #imageLiteral(resourceName: "Veg")
        if let str = DictItem.value(forKey: "veg_nonveg") as? String,!str.isEmpty{
            imgType.image = #imageLiteral(resourceName: "NonVeg")
        }
        let ArrDesc = NSMutableArray()
        if let str = DictItem.value(forKey: "item_name") as? String{
            lblTitle.text = str
        }
        
        // Manthan 23-9-2020
        // Showing Variation and Add ons
        /*if let Arr = DictItem.value(forKey: "variation") as? NSArray{
            for Dict in Arr {
                if let DictInner = Dict as? NSDictionary{
                    var string = ""
                    if let str = DictInner.value(forKey: "title") as? String{
                        string = str
                    }
                    if let str = DictInner.value(forKey: "value") as? String{
                        string = string + " : " + str
                    }
                    ArrDesc.add(string)
                }
            }
        }
        if let Arr = DictItem.value(forKey: "addon_string") as? NSArray{
            for Dict in Arr {
                if let DictInner = Dict as? NSDictionary{
                    var string = ""
                    if let str = DictInner.value(forKey: "title") as? String{
                        string = str
                    }
                    if let str = DictInner.value(forKey: "value") as? String{
                        string = string + " : " + str
                    }
                    ArrDesc.add(string)
                }
            }
        }
        lblDesc.text = ""
        if ArrDesc.count != 0 {
            lblDesc.text = ArrDesc.componentsJoined(by: "\n")
        }*/
        let variationList = DictItem["variation"] as? [[String: Any]] ?? []
        var variationString = ""
        let allVariations = variationList.compactMap { (variation) -> String? in
            if let title = variation["title"] as? String,
                let value = variation["value"] as? String {
                return "\(title.capitalized) : \(value.capitalized)"
            } else {
                return nil
            }
        }
        
        if !allVariations.isEmpty {
            variationString = allVariations.joined(separator: "\n")
        }
        
        let addonList = DictItem["addon_string"] as? [[String: Any]] ?? []
        var addonString = ""
        let allValues = addonList.compactMap { (addon) -> String? in addon["value"] as? String }
        if !allValues.isEmpty {
            addonString = "\("Add on".localized) : \(allValues.joined(separator: ", "))"
        }
        
        let selectedIngredients = DictItem["selected_ingredients"] as? [String] ?? []
        var selectedIngredientString = ""
        for (index, item) in selectedIngredients.enumerated() {
            if index == 0 {
                selectedIngredientString = item
            } else {
                selectedIngredientString += ", \(item)"
            }
        }
        if !selectedIngredientString.isEmpty {
            selectedIngredientString = "\("Ingredients".localized) : \(selectedIngredientString)"
        }
        
        var comments = DictItem["comments"] as? String ?? ""
        if !comments.isEmpty {
            comments = "\("Comments".localized) : \(comments)"
        }
        
        self.lblDesc.text = "\(variationString)\(variationString.isEmpty ? addonString : addonString.isEmpty ? "" : "\n\(addonString)")\((!variationString.isEmpty || !addonString.isEmpty) ?  (selectedIngredientString.isEmpty ? "" : "\n\(selectedIngredientString)") : selectedIngredientString)\((!variationString.isEmpty || !addonString.isEmpty || !selectedIngredientString.isEmpty) ? (comments.isEmpty ? "" : "\n\(comments)") : comments)"
        
        /*if !variationString.isEmpty && !addonString.isEmpty {
            lblDesc.text = "\(variationString)\n\(addonString)"
        } else if variationString.isEmpty {
            lblDesc.text = "\(addonString)"
        } else if addonString.isEmpty {
            lblDesc.text = "\(variationString)"
        } else {
            lblDesc.text = ""
        }*/
    }
    
    @IBAction func ActionRepeat(_ sender: Any) {
        delegate.ReorderButtonPressed(index)
    }
    
}

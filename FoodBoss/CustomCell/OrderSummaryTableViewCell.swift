//
//  OrderSummaryTableViewCell.swift
//  FoodBoss
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class OrderSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgVegNonveg: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblitemDesc: UILabel!
    @IBOutlet weak var lblQtywithPrice: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    @IBOutlet weak var lblRate: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        GlobalMethod.applyShadowCardView(imgView, andCorner: 5)
        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary) {
        print(Dict)
        imgVegNonveg.image = #imageLiteral(resourceName: "Veg")
        if let str = Dict.value(forKey: "veg_nonveg") as? String,!str.isEmpty{
            imgVegNonveg.image = #imageLiteral(resourceName: "NonVeg")
        }
        if let str = Dict.value(forKey: "item_name") as? String{
            self.lblItemName.text = str
        }
        if let str = Dict.value(forKey: "quantity") as? String{
            if let strSubTotal = Dict.value(forKey: "item_sub_total") as? String{
                self.lblQtywithPrice.text = str + " X " + strSubTotal + " " + CURRANCY
            }
        }
        
        if let str = Dict.value(forKey: "image") as? String{
            var imgUrl:String = str
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            imgView.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
        }
        
        // Manthan 23-9-2020
        // Showing Variation and Add ons
        /*let ArrVariation = NSMutableArray()
        if let Array = Dict.value(forKey: "variation") as? NSArray{
            for Dict in Array{
                var strVariation = ""
                if let str = (Dict as! NSDictionary).value(forKey: "title") as? String{
                    strVariation = str
                }
                if let str = (Dict as! NSDictionary).value(forKey: "value") as? String{
                    strVariation = strVariation + " : " + str
                }
                ArrVariation.add(strVariation)
            }
        }
        //
        let variationString = ArrVariation.componentsJoined(by: "\n")*/
        let variationList = Dict["variation"] as? [[String: Any]] ?? []
        var variationString = ""
        let allVariations = variationList.compactMap { (variation) -> String? in
            if let title = variation["title"] as? String,
                let value = variation["value"] as? String {
                return "\(title.capitalized) : \(value.capitalized)"
            } else {
                return nil
            }
        }
        
        if !allVariations.isEmpty {
            variationString = allVariations.joined(separator: "\n")
        }
        
        let addonList = Dict["addon_string"] as? [[String: Any]] ?? []
        var addonString = ""
        let allValues = addonList.compactMap { (addon) -> String? in addon["value"] as? String }
        if !allValues.isEmpty {
            addonString = "\("Add on".localized) : \(allValues.joined(separator: ", "))"
        }
        
        let selectedIngredients = Dict["selected_ingredients"] as? [String] ?? []
        var selectedIngredientString = ""
        for (index, item) in selectedIngredients.enumerated() {
            if index == 0 {
                selectedIngredientString = item
            } else {
                selectedIngredientString += ", \(item)"
            }
        }
        if !selectedIngredientString.isEmpty {
            selectedIngredientString = "\("Ingredients".localized) : \(selectedIngredientString)"
        }
        
        /*if !variationString.isEmpty && !addonString.isEmpty {
            self.lblitemDesc.text = "\(variationString)\n\(addonString)"
        } else if variationString.isEmpty {
            self.lblitemDesc.text = "\(addonString)"
        } else if addonString.isEmpty {
            self.lblitemDesc.text = "\(variationString)"
        } else {
            self.lblitemDesc.text = ""
        }*/
        var comments = Dict["comments"] as? String ?? ""
        if !comments.isEmpty {
            comments = "\("Comments".localized) : \(comments)"
        }
        
        self.lblitemDesc.text = "\(variationString)\(variationString.isEmpty ? addonString : addonString.isEmpty ? "" : "\n\(addonString)")\((!variationString.isEmpty || !addonString.isEmpty) ?  (selectedIngredientString.isEmpty ? "" : "\n\(selectedIngredientString)") : selectedIngredientString)\((!variationString.isEmpty || !addonString.isEmpty || !selectedIngredientString.isEmpty) ? (comments.isEmpty ? "" : "\n\(comments)") : comments)"

//        self.lblitemDesc.text = ArrVariation.componentsJoined(by: "\n")
        //
        if let str = Dict.value(forKey: "rate") as? String,!str.isEmpty{
            imgStar.isHidden = false
            lblRate.isHidden = false
            lblRate.text = str
            constraintBottom.constant = 30
        }
        else{
            constraintBottom.constant = 15
            imgStar.isHidden = true
            lblRate.isHidden = true
            lblRate.text = ""
        }
        
    }
}

//
//  CartBillDetailsTableViewCell.swift
//  FoodBoss
//
//  Created by Apple on 24/09/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class CartBillDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var kItemTotalLabel: UILabel!
    @IBOutlet weak var itemTotalLabel: UILabel!
    @IBOutlet weak var kAddonTotalLabel: UILabel!
    @IBOutlet weak var addonTotalLabel: UILabel!
    @IBOutlet weak var kTipAmountLabel: UILabel!
    @IBOutlet weak var tipAmountLabel: UILabel!
    @IBOutlet weak var kTaxLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var kDeliveryChargeLabel: UILabel!
    @IBOutlet weak var deliveryChargeLabel: UILabel!
    @IBOutlet weak var kDiscountLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var kGrandTotalLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var clearTipButton: UIButton!
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//
//  ItemCell.swift
//  Wishtry
//
//  Created by IOS Development on 15/09/20.
//  Copyright © 2020 Tausif Rana. All rights reserved.
//

import UIKit
import SDWebImage

class ItemCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(text: String, img: String) {
        
        self.img.layer.cornerRadius = 5
        self.img.layer.borderColor = UIColor.lightText.cgColor
        self.img.layer.borderWidth = 0.5
        self.lbl.text = text
        DispatchQueue.main.async {
            self.img.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "RestaurantPlaceholder"))
        }
    }
}

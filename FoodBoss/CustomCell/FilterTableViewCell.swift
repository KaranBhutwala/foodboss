//
//  FilterTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 09/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    public func configure(with Dict: NSDictionary) {
       // lblPrice.text = ""
        
        if let str = Dict.value(forKey: "cuisine") as? String{
            self.lbl.text = str
        }
        
        if let str = Dict.value(forKey: "addon_title") as? String{
            self.lbl.textColor = GRAY
            self.lbl.text = str
        }
        
        if let str = Dict.value(forKey: "addon_price") as? Int{
          //  self.lblPrice.textColor = GRAY
            // self.lblPrice.text = CURRANCY + " " + str
            self.lbl.text = self.lbl.text! + " (+\(CURRANCY + " " + String (describing: str)))"
        }
        
        if let str = Dict.value(forKey: "addon_price") as? String{
            //self.lblPrice.textColor = UIColor.black
           // self.lblPrice.text = CURRANCY + " " + str
            self.lbl.text = self.lbl.text! + " (+\(CURRANCY + " " + String (describing: str)))"
        }
    }
}

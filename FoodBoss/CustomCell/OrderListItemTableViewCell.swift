//
//  OrderListItemTableViewCell.swift
//  FoodBoss
//
//  Created by Jay on 04/07/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class OrderListItemTableViewCell: UITableViewCell {

    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var constraintViewInnerTop: NSLayoutConstraint!
    @IBOutlet weak var constraintViewInnerBottom: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    public func configure(with Dict: NSDictionary) {
        print(Dict)
        imgType.image = #imageLiteral(resourceName: "Veg")
        if let str = Dict.value(forKey: "veg_nonveg") as? String,!str.isEmpty{
            imgType.image = #imageLiteral(resourceName: "NonVeg")
        }
        let ArrDesc = NSMutableArray()
        if let str = Dict.value(forKey: "item_name") as? String{
            lblTitle.text = str
        }
        if let Arr = Dict.value(forKey: "variation") as? NSArray{
            for Dict in Arr {
                if let DictInner = Dict as? NSDictionary{
                    var string = ""
                    if let str = DictInner.value(forKey: "title") as? String{
                        string = str
                    }
                    if let str = DictInner.value(forKey: "value") as? String{
                        string = string + " : " + str
                    }
                    ArrDesc.add(string)
                }
            }
        }
        if let Arr = Dict.value(forKey: "addon_string") as? NSArray{
            for Dict in Arr {
                if let DictInner = Dict as? NSDictionary{
                    var string = ""
                    if let str = DictInner.value(forKey: "title") as? String{
                        string = str
                    }
                    if let str = DictInner.value(forKey: "value") as? String{
                        string = string + " : " + str
                    }
                    ArrDesc.add(string)
                }
            }
        }
        lblDesc.text = ""
        if ArrDesc.count != 0 {
            lblDesc.text = ArrDesc.componentsJoined(by: "\n")
        }
    }
}

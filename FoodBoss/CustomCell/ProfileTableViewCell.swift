//
//  ProfileTableViewCell.swift
//  FoodBoss
//
//  Created by Apple on 24/09/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileTitleLabel: UILabel!
    @IBOutlet weak var profileDetailsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        GlobalMethod.applyShadowCardView(self.containerView, andCorner: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

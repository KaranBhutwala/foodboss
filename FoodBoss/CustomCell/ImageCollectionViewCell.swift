//
//  ImageCollectionViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 12/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

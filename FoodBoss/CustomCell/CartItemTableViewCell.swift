//
//  CartItemTableViewCell.swift
//  FoodBoss
//
//  Created by Jay on 20/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol QuantityActionDelegate : class {
    func PlusButtonPressed(_ index: IndexPath)
    func MinusButtonPressed(_ index: IndexPath)
}

protocol QuantityDetailCartActionDelegate : class {
    func PlusDetailButtonPressed(_ index: IndexPath)
    func MinusDetailButtonPressed(_ index: IndexPath)
}

class CartItemTableViewCell: UITableViewCell {

    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var imgVegNonveg: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOtherPrice: UILabel!
    @IBOutlet weak var lblitemDesc: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblQtywithPrice: UILabel!
    @IBOutlet weak var constraintImgBottom: NSLayoutConstraint!
    @IBOutlet weak var viewQty: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var ConstraintOne: NSLayoutConstraint!
    @IBOutlet weak var ConstraintTwo: NSLayoutConstraint!
    @IBOutlet weak var ConstraintThree: NSLayoutConstraint!
    
    var delegateDetailQty:QuantityDetailCartActionDelegate!
    var delegate:QuantityActionDelegate!
    var IsUsedinDetail = false
    var index = IndexPath()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewQty.layer.cornerRadius = viewQty.frame.height / 2
        self.viewQty.layer.borderColor = UIColor.black.cgColor
        self.viewQty.layer.borderWidth = 0.2
        GlobalMethod.applyShadowCardView(imgView, andCorner: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func configure(with Dict: NSDictionary) {
       // imgVegNonveg.image = #imageLiteral(resourceName: "Veg")
       // if let str = Dict.value(forKey: "veg_nonveg") as? String,!str.isEmpty{
       //     imgVegNonveg.image = #imageLiteral(resourceName: "NonVeg")
       // }
        
        imgView.layer.cornerRadius = 5
        imgView.layer.borderWidth = 0.5
        imgView.layer.borderColor = UIColor.darkGray.cgColor

        if let str = Dict.value(forKey: "item_name") as? String{
            self.lblItemName.text = str
        }
        if let str = Dict.value(forKey: "item_price") as? String{
            self.lblPrice.text = CURRANCY + " " +  str
        }
      
        if let str = Dict.value(forKey: "item_sub_total") as? String{
            self.lblOtherPrice.text = CURRANCY + " " +  str
        }
        if let str = Dict.value(forKey: "quantity") as? String{
            self.lblQty.text = str
            if let strSubTotal = Dict.value(forKey: "item_sub_total") as? String{
                self.lblQtywithPrice.text = str + " X " + strSubTotal
            }
        }
        
        if let str = Dict.value(forKey: "image") as? String{
            
            if str == ""
            {
                imgView.image = UIImage.init(named: "placeholder.png")
            }
            else
            {
                var imgUrl:String = str
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                imgView.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)

            }
        }
        
        let ArrVariation = NSMutableArray()
        if let Array = Dict.value(forKey: "variation") as? NSArray{
            for Dict in Array{
                var strVariation = ""
                if let str = (Dict as! NSDictionary).value(forKey: "title") as? String{
                    strVariation = str.capitalized
                }
                if let str = (Dict as! NSDictionary).value(forKey: "value") as? String{
                    strVariation = strVariation + " : " + str.capitalized
                }
                ArrVariation.add(strVariation)
            }
        }
        
        let variationString = ArrVariation.componentsJoined(by: "\n")
        
        let addonList = Dict["addon_string"] as? [[String: Any]] ?? []
        var addonString = ""
        let allValues = addonList.compactMap { (addon) -> String? in addon["value"] as? String }
        if !allValues.isEmpty {
            addonString = "\("Add on".localized) : \(allValues.joined(separator: ", "))"
        }
        
        let selectedIngredients = Dict["selected_ingredients"] as? [String] ?? []
        var selectedIngredientString = ""
        for (index, item) in selectedIngredients.enumerated() {
            if index == 0 {
                selectedIngredientString = item
            } else {
                selectedIngredientString += ", \(item)"
            }
        }
        if !selectedIngredientString.isEmpty {
            selectedIngredientString = "\("Ingredients".localized) : \(selectedIngredientString)"
        }
        
        var comments = Dict["comments"] as? String ?? ""
        if !comments.isEmpty {
            comments = "\("Comments".localized) : \(comments)"
        }
        
        self.lblitemDesc.text = "\(variationString)\(variationString.isEmpty ? addonString : addonString.isEmpty ? "" : "\n\(addonString)")\((!variationString.isEmpty || !addonString.isEmpty) ?  (selectedIngredientString.isEmpty ? "" : "\n\(selectedIngredientString)") : selectedIngredientString)\((!variationString.isEmpty || !addonString.isEmpty || !selectedIngredientString.isEmpty) ? (comments.isEmpty ? "" : "\n\(comments)") : comments)"
        
    }

    @IBAction func PlusAction(_ sender: Any) {
        
        if IsUsedinDetail{
            delegateDetailQty.PlusDetailButtonPressed(index)
        }
        else{
            delegate.PlusButtonPressed(index)
        }
        
    }
    
    @IBAction func MinusAction(_ sender: Any) {
        
        if IsUsedinDetail{
            delegateDetailQty.MinusDetailButtonPressed(index)
        }
        else{
            delegate.MinusButtonPressed(index)
        }
    }
}

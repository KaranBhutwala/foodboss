//
//  FoodItemTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 13/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit


protocol FoodDetailsActionDelegate : class {
    func AddMoreButtonPressed(_ index: IndexPath)
}

class FoodItemTableViewCell: UITableViewCell {

    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var viewQty: UIView!
    @IBOutlet weak var editView: UIView! // For SkeletonView
    var delegate:FoodDetailsActionDelegate!
    var delegateQty:QuantityActionDelegate!
    var index = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblAdd.text = "Add".localized
        self.imgItem.layer.cornerRadius = 5
        self.viewAdd.layer.cornerRadius = viewAdd.frame.height / 2
        self.viewAdd.layer.borderColor = UIColor.black.cgColor
        self.viewAdd.layer.borderWidth = 0.2
        
        self.viewQty.layer.cornerRadius = viewQty.frame.height / 2
        self.viewQty.layer.borderColor = UIColor.black.cgColor
        self.viewQty.layer.borderWidth = 0.2
    }
    
    public func configure(with Dict: NSDictionary) {

        /*imgType.image = #imageLiteral(resourceName: "Veg")
        if let str = Dict.value(forKey: "veg_nonveg") as? String,!str.isEmpty{
            imgType.image = #imageLiteral(resourceName: "NonVeg")
        }*/
        if let str = Dict.value(forKey: "image") as? String{
            var imgUrl:String = str
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            imgItem.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
        }
        if let str = Dict.value(forKey: "item_name") as? String{
            lblName.text = str
        }
        if let str = Dict.value(forKey: "sale_price") as? String{
            lblPrice.text = CURRANCY + " " + str
        }
        
        
        lblDesc.numberOfLines = 2
        if let str = Dict.value(forKey: "item_description") as? String{
            lblDesc.text = str
        }
        

    }
    
    func getLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let lbl = UILabel(frame: .zero)
        lbl.frame.size.width = width
        lbl.font = font
        lbl.numberOfLines = 0
        lbl.text = text
        lbl.sizeToFit()

        return lbl.frame.size.height
    }
    
    @IBAction func ActionAdd(_ sender: UIButton) {
        delegate.AddMoreButtonPressed(index)
    }
    
    @IBAction func PlusAction(_ sender: Any) {
        delegateQty.PlusButtonPressed(index)
    }
    
    @IBAction func MinusAction(_ sender: Any) {
        delegateQty.MinusButtonPressed(index)
    }
}

//
//  PaypalPaymentMethodTableViewCell.swift
//  FoodBoss
//
//  Created by Apple on 25/09/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class PaypalPaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var defaultStateImageView: UIImageView!
    @IBOutlet weak var paymentMethodImageView: UIImageView!
    @IBOutlet weak var paymentMethodDetails: UILabel!
    @IBOutlet weak var paymentMethodTitle: UILabel!
    @IBOutlet weak var setDefaultButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

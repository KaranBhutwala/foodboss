//
//  DetailHeader.swift
//  FoodBoss
//
//  Created by Raj Shukla on 12/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol CustomHeaderDelegate: class {
    func customHeader(_ customHeader: DetailHeader)
    func Fav(_ customHeader: DetailHeader)
}

class DetailHeader: UITableViewHeaderFooterView {
    
    weak var delegate: CustomHeaderDelegate?
    
    static let reuseIdentifier = "DetailHeader"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func ActionSectionTouched(_ sender: Any) {
        delegate?.customHeader(self)
    }
    @IBAction func ActionFavTouched(_ sender: Any) {
        delegate?.Fav(self)
    }
    
}

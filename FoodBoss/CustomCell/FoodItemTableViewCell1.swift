//
//  FoodItemTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 13/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol FoodDetailsActionDelegateSearch : class {
    func AddMoreButtonPressed(_ index: IndexPath)
}

protocol QuantityActionDelegateSearch : class {
    func PlusButtonPressedSearch(_ index: IndexPath)
    func MinusButtonPressedSearch(_ index: IndexPath)
}

class FoodItemTableViewCell1: UITableViewCell {

    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var viewQty: UIView!
    @IBOutlet weak var editView: UIView! // For SkeletonView
    var delegate: FoodDetailsActionDelegateSearch!
    var delegateQty: QuantityActionDelegateSearch!
    var index = IndexPath()

    override func awakeFromNib() {
        super.awakeFromNib()
        lblAdd.text = "Add".localized
        self.imgItem.layer.cornerRadius = 5
        self.viewAdd.layer.cornerRadius = viewAdd.frame.height / 2
        self.viewAdd.layer.borderColor = UIColor.black.cgColor
        self.viewAdd.layer.borderWidth = 0.2
        
        self.viewQty.layer.cornerRadius = viewQty.frame.height / 2
        self.viewQty.layer.borderColor = UIColor.black.cgColor
        self.viewQty.layer.borderWidth = 0.2
    }
    
    @IBAction func ActionAdd(_ sender: UIButton) {
       delegate.AddMoreButtonPressed(index)
        print("ADD MORE BUTTON CLICK")
    }
    
    @IBAction func PlusAction(_ sender: Any) {
        delegate.AddMoreButtonPressed(index)
      // delegateQty.PlusButtonPressedSearch(index)
    }
    
    @IBAction func MinusAction(_ sender: Any) {
        delegate.AddMoreButtonPressed(index)
//        delegateQty.MinusButtonPressedSearch(index)
    }
}

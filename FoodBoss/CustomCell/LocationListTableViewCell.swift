//
//  LocationListTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 18/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol AddressActionDelegate : class {
    func EditButtonPressed(_ index: IndexPath)
}

class LocationListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgAddressType: UIImageView!
    @IBOutlet weak var lblAddressType: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var viewInner: UIView!
    
    var index = IndexPath()
    var delegate:AddressActionDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        GlobalMethod.applyShadowCardView(viewInner, andCorner: 6)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func configure(with Dict: NSDictionary) {
        
        if let str = Dict.value(forKey: "address_type") as? String{
            lblAddressType.text = str.capitalized
            if str == "other"{
                imgAddressType.image = #imageLiteral(resourceName: "OtherSelected")
            }
            else if str == "home"{
                imgAddressType.image = #imageLiteral(resourceName: "HomeSelected")
            }
            else{
                imgAddressType.image = #imageLiteral(resourceName: "OfficeSelected")
            }
        }
        if let strOne = Dict.value(forKey: "area") as? String{
            lblAddress.text = strOne
       }
        if let strOne = Dict.value(forKey: "street") as? String{
            lblAddressTitle.text = strOne
        }
    }
    
    @IBAction func ActionEditAddress(_ sender: Any) {
        delegate.EditButtonPressed(index)
    }
}

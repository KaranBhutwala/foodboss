//
//  MyAccountTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 08/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class MyAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        GlobalMethod.applyShadowCardView(viewInner, andCorner: 10)
        img.setImageColor(color: UIColor.darkGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary) {
        print(Dict)
        if let str = Dict.value(forKey: "Title") as? String{
            self.lblTitle.text = str
        }
        if let str = Dict.value(forKey: "Desc") as? String{
            self.lblDesc.text = str
        }
    }
}

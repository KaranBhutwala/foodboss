//
//  RateTableViewCell.swift
//  FoodBoss
//
//  Created by Apple on 18/08/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol RateDelegate: class {
    func Rate(_ index: IndexPath, Rate : CGFloat)
}

class RateTableViewCell: UITableViewCell {

    @IBOutlet weak var imgVegNonveg: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewRate: HCSStarRatingView!
    
    var delegateRate:RateDelegate!
    var index = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        GlobalMethod.applyShadowCardView(imgView, andCorner: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary) {
        print(Dict)
        imgVegNonveg.image = #imageLiteral(resourceName: "Veg")
        if let str = Dict.value(forKey: "veg_nonveg") as? String,!str.isEmpty{
            imgVegNonveg.image = #imageLiteral(resourceName: "NonVeg")
        }
        if let str = Dict.value(forKey: "item_name") as? String{
            self.lblItemName.text = str
        }
        if let str = Dict.value(forKey: "image") as? String{
            var imgUrl:String = str
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            imgView.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
        }
    }
    @IBAction func ActionItemRating(_ sender: HCSStarRatingView) {
        delegateRate.Rate(index, Rate: sender.value)
    }
    
}

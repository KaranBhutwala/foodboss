//
//  RestaurantTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 19/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {

    @IBOutlet weak var imgRestaurant: UIImageView!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblRestaurantDesc: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var lblRates: UILabel!
    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var lblDeal: UILabel!
    @IBOutlet weak var OfferView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        GlobalMethod.applyShadowCardView(imgRestaurant, andCorner: 5)
            //lblDeal.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary) {
        OfferView.isHidden = true
        if let str = Dict.value(forKey: "logo") as? String{
            var imgUrl:String = str
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            imgRestaurant.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
        }
        if let str = Dict.value(forKey: "delivery_time") as? String, !str.isEmpty{
            lblDeliveryTime.text = str
            if let str = Dict.value(forKey: "min_order_amount") as? String, !str.isEmpty{
                lblDeliveryTime.text = lblDeliveryTime.text! + " • " + "Minimum ".localized + CURRANCY + " " + str
            }
        }
        else{
            if let str = Dict.value(forKey: "min_order_amount") as? String, !str.isEmpty{
                lblDeliveryTime.text = ""//Minimum ".localized + CURRANCY + " " + str
            }
        }
        
        if let str = Dict.value(forKey: "name") as? String{
            lblRestaurantName.text = str
        }
//        if var str = Dict.value(forKey: "price_for_one") as? String{
//            if str.isEmpty { str = "0" }
//            lblInfo.text = CURRANCY + " " + str + " per person"
//            if let strTwo = Dict.value(forKey: "price_for_two") as? String{
//                lblInfo.text = lblInfo.text! + " | " + CURRANCY + " " + strTwo + " for two"
//            }
//        }
        if let arr = Dict.value(forKey: "cuisines") as? NSArray{
            let ArrayName = NSMutableArray()
            for Dict in arr{
                if let dict = Dict as? NSDictionary{
                    if let str = dict.value(forKey: "cuisine") as? String{
                        ArrayName.add(str)
                    }
                }
            }
            if ArrayName.count != 0{
                lblRestaurantDesc.text = ArrayName.componentsJoined(by: ",")
            }else{
                lblRestaurantDesc.text = ""
            }
            
        }
        
        if let str = Dict.value(forKey: "final_review") as? String{
            self.lblRates.text = str
        }
      //  if let str = Dict.value(forKey: "final_review_count") as? String{
       //     self.lblReview.text = "(" + str + ")"
       // }
        
        
        
        if let str = Dict.value(forKey: "is_offer_available") as? Int, str == 1{
            OfferView.isHidden = false
            //OfferView.backgroundColor = UIColor.green
            
            if let arrOffer = Dict.value(forKey: "offers") as? NSArray{

                let myWidth = 25
                var myHeight = 0
                let ArrayName = NSMutableArray()
                
                for Dict in arrOffer{
                    if let dict = Dict as? NSDictionary{
                        if let str = dict.value(forKey: "badge") as? String{
                            ArrayName.add(str)
                        }
                    }
                }
                
                for i in 0...ArrayName.count - 1 {
                    let imageView: UIImageView = UIImageView()
                    imageView.sd_setImage(with: URL(string: ArrayName[i] as! String), placeholderImage: nil)
                    imageView.frame = CGRect(x: myHeight , y: 5 , width: myWidth, height: 25)
                    self.OfferView.addSubview(imageView)
                    myHeight = myHeight + 30
                }
            }
        }
        if let str = Dict.value(forKey: "is_open") as? Int, str == 1{
            self.contentView.backgroundColor = UIColor.white
        }
        else{
            self.contentView.backgroundColor = UIColor.colorFromHex(hexString: "#F2F2F2")
        }
        
    }
    
    
    public func configure1(with Dict: NSDictionary) {

        if let str = Dict.value(forKey: "logo") as? String{
                var imgUrl:String = str
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                imgRestaurant.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
            }
            if let str = Dict.value(forKey: "delivery_time") as? String, !str.isEmpty{
                lblDeliveryTime.text = str
                if let str = Dict.value(forKey: "min_order_amount") as? String, !str.isEmpty{
                    lblDeliveryTime.text = lblDeliveryTime.text! + " • " + "Minimum ".localized + CURRANCY + " " + str
                }
            }
            else{
                if let str = Dict.value(forKey: "min_order_amount") as? String, !str.isEmpty{
                    lblDeliveryTime.text = "Minimum ".localized + CURRANCY + " " + str
                }
            }
            
            if let str = Dict.value(forKey: "name") as? String{
                lblRestaurantName.text = str
            }
    
            if let arr = Dict.value(forKey: "cuisines") as? NSArray{
                let ArrayName = NSMutableArray()
                for Dict in arr{
                    if let dict = Dict as? NSDictionary{
                        if let str = dict.value(forKey: "cuisine") as? String{
                            ArrayName.add(str)
                        }
                    }
                }
                lblRestaurantDesc.text = ArrayName.componentsJoined(by: ",")
            }
            
            if let str = Dict.value(forKey: "final_review") as? String{
                self.lblRates.text = str
            }
         
           
            if let str = Dict.value(forKey: "is_open") as? Int, str == 1{
                self.contentView.backgroundColor = UIColor.white
            }
            else{
                self.contentView.backgroundColor = UIColor.colorFromHex(hexString: "#F2F2F2")
            }
            
        }
}

//
//  CouponListTableViewCell.swift
//  FoodBoss
//
//  Created by Jay on 21/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class CouponListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary) {
//        print(Dict)
        if let str = Dict.value(forKey: "coupon_code") as? String{
            self.lblTitle.text = str
        }
    }
}

//
//  ItemMenuCartHeaderTableViewCell.swift
//  FoodBoss
//
//  Created by Jay on 18/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class ItemMenuCartHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  LanguageSelectionTableViewCell.swift
//  FoodBoss
//
//  Created by Raj Shukla on 06/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class LanguageSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var btnObjCustomize: UIButton!
    @IBOutlet weak var vWObjCustomize: UIView!
    
    @IBOutlet weak var lblObjCustomize: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lbl.numberOfLines = 0
        lblObjCustomize.layer.cornerRadius = 5
        lblObjCustomize.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configure(with Dict: NSDictionary) {
        if let str = Dict.value(forKey: "language") as? String{
            self.lbl.text = str.capitalized
        }
        
        if let str = Dict.value(forKey: "value") as? String{
            self.lbl.textColor = GRAY
            self.lbl.text = str.capitalized
        }
    }
    
    public func configureBaseSelection(with Dict: NSDictionary) {
        if let str = Dict.value(forKey: "title") as? String{
            self.lbl.text = str.capitalized
        }
        
        if let str = Dict.value(forKey: "option_name") as? String{
            self.lbl.textColor = GRAY
            self.lbl.text = str.capitalized
        }
        
        if let str = Dict.value(forKey: "addon_price") as? NSNumber, str != 0{
            let roundedVal = String(format:"%.2f", Double(truncating: str))
            //  self.lblPrice.textColor = GRAY
            // self.lblPrice.text = CURRANCY + " " + str
            self.lbl.text = self.lbl.text! + " (+\(CURRANCY + " " + String (describing: roundedVal)))"
        }
        
        if let str = Dict.value(forKey: "addon_price") as? String, str != "0"{
            let roundedVal = String(format:"%.2f", str)
            //self.lblPrice.textColor = UIColor.black
            // self.lblPrice.text = CURRANCY + " " + str
            self.lbl.text = self.lbl.text! + " (+\(CURRANCY + " " + String (describing: roundedVal)))"
        }
        
    }
}

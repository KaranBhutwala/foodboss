//
//  AlertListView.swift
//  FoodBoss
//
//  Created by Apple on 16/09/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

protocol AlertListViewDeleagate: class {
    func didSelectItem(item: PaymentMethodModel)
}

class AlertListView: UIView, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    weak var delegate: AlertListViewDeleagate?
    var targetController:UIViewController?
    var dataSet = [PaymentMethodModel]()
    
    func set(titleText: String, dataSet: [PaymentMethodModel],targetController: UIViewController) {
        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(didTappedOnSurface(gesture:)))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        self.emptyView.addGestureRecognizer(tapGesture)
        
        self.infoLabel.text = titleText
        self.targetController = targetController
        self.delegate = targetController as! CartViewController
        self.dataSet = dataSet
         
        self.tableView.registerNib(HeaderTableViewCell.self)
        self.tableView.tableFooterView = UIView()
        self.tableView.reloadData()
        
        self.contentViewHeight.constant = CGFloat(dataSet.count * 50) + 100 + max(34, self.tableView.adjustedContentInset.bottom)

        self.contentView.roundCorners([.topLeft, .topRight], radius: 15)
        self.contentView.layer.masksToBounds = true
        
        self.contentView.layoutIfNeeded()
    }
    
    func showPopUpView() {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        if #available(iOS 13.0, *) {
            SCENE?.window?.addSubview(self)
        } else {
            APP?.window?.addSubview(self)
        }
        var frame = self.contentView.frame
        frame.origin.y = self.bounds.height - self.contentView.frame.height
        
        self.contentView.frame.origin.y = self.frame.height
        UIView.animate(withDuration: 0.30,
                       delay: 0,
                       options: .curveEaseOut, animations: {
            self.contentView.frame = frame
        }, completion: nil)
        
    }
    
    func hidePopUp() {
        var frame = self.contentView.frame
        frame.origin.y = self.frame.height
        UIView.animate(withDuration: 0.30, animations: {
            self.contentView.frame = frame
        }) { (flag) in
            self.removeFromSuperview()
        }
        
        self.targetController = nil
    }
    
    @objc func didTappedOnSurface(gesture: UITapGestureRecognizer) {
        hidePopUp()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(HeaderTableViewCell.self)
        /*if T.self == PaymentMethodModel.self {
            cell.lbl.text = (dataSet[indexPath.row] as! PaymentMethodModel).name
        }*/
        cell.lbl.text = dataSet[indexPath.row].name
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*if T.self == PaymentMethodModel.self {
            delegate?.didSelectItem(item: (self.dataSet[indexPath.row] as! PaymentMethodModel))
        }*/
        delegate?.didSelectItem(item: self.dataSet[indexPath.row])
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.tableView) == true {
            return false
        }
        return true
    }
    
}

/*extension AlertListView<T>: UITableViewDataSource {
    
    
}

extension AlertListView: UITableViewDelegate {
    
}

extension AlertListView: UIGestureRecognizerDelegate {
    
}*/

//
//  SceneDelegate.swift
//  FoodBoss
//
//  Created by Raj Shukla on 06/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Braintree
import GoogleSignIn
import FBSDKCoreKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        ISLOGIN = "NO"
       /* if let Login = UserDefaults.standard.string(forKey: LOGIN){
            if Login == "Yes"{
                ISLOGIN = "YES"
                
                if let str = UserDefaults.standard.string(forKey: LAT){
                    LATTITUDE = str
                }
                if let str = UserDefaults.standard.string(forKey: LONG){
                    LONGITUDE = str
                }
                if let str = UserDefaults.standard.string(forKey: ADDRESS_SELECTED){
                    ADDRESS = str
                }
                
                if let User = UserDefaults.standard.string(forKey: USER_ID){
                    USERID = User
                }
                
                var Identifire = "NavLocation"
                
                if !LATTITUDE.isEmpty{
                    Identifire = "NavTab"
                }
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: Identifire)
                self.window?.rootViewController = initialViewController
                
                if let dicUser = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary
                {
                    print(dicUser)
                    if let str = dicUser.value(forKey: "user_id") as? String, str.count != 0
                    {
                        USERID = str
                    }
                    
                    if let str = dicUser.value(forKey: "user_id") as? Int, str != 0
                    {
                        USERID = String (describing: str)
                    }
                    
                    if let str = dicUser.value(forKey: "cart_session") as? String, str.count != 0
                    {
                        CART_SESSION = str
                    }
                }
            }
        }*/
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        URLContexts.forEach { (context) in
            print("\nSCENE DELEGATE OPEN USER:\t\(context.url)\n")
            if context.url.scheme?.localizedCaseInsensitiveCompare("com.FB.FoodBoss.app.payments") == .orderedSame {
                BTAppSwitch.handleOpenURLContext(context)
            }  else if context.url.scheme?.localizedCaseInsensitiveCompare("com.googleusercontent.apps.260716817125-fl9fdumitpn33jb04cu8l375gmtd1n7a") == .orderedSame {
                GIDSignIn.sharedInstance()?.handle(context.url)
            } else {
                guard let url = URLContexts.first?.url else {
                    return
                }

                ApplicationDelegate.shared.application(UIApplication.shared,
                                                       open: url,
                                                       sourceApplication: nil,
                                                       annotation: [UIApplication.OpenURLOptionsKey.annotation])
            }
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}


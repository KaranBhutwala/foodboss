//
//  Mytextfield.swift
//  Alpha Estate Vault
//
//  Created by Raj Shukla on 05/03/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class EdgeRoundRectButton: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
    }
}

class TopEdgeRectButton : UIButton {
    
    override func awakeFromNib() {
        self.layoutIfNeeded()
        self.roundCorners([.topLeft, .topRight], radius: 15)
    }
}

class RoundRectButton: UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height/2
    }
}

class RoundRectView: UIView {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height/2
    }
}

class RoundRectImageView: UIImageView {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height/2
    }
}

class RoundRectVisualView: UIVisualEffectView {
    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.height/2
    }
}

class TopEdgeRoundRectButton: UIButton {
    override func awakeFromNib() {
//        self.layer.cornerRadius = 5
        self.layoutIfNeeded()
        self.roundCorners([.topLeft, .topRight], radius: 10)
//        self.mas = true
        self.clipsToBounds = true
    }
}

class InsetTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

//
//  MyTextField.swift
//  FoodBoss
//
//  Created by Raj Shukla on 13/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import Foundation

class MyTextField: SkyFloatingLabelTextField {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    
    override func awakeFromNib() {
//        self.layer.cornerRadius = 10.0
        self.tintColor = UIColor.black
        self.selectedTitleColor = COLOR_TURQUOISE!
        self.disabledColor = UIColor.black
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
}


class MyLocationTextField: SkyFloatingLabelTextField {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 45);
    
    override func awakeFromNib() {
//        self.layer.cornerRadius = 10.0
        self.tintColor = UIColor.black
        self.selectedTitleColor = COLOR_TURQUOISE!
        self.disabledColor = UIColor.black
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
}

//
//  Header.swift
//  Just One Energy
//
//  Created by Vipul on 26/04/18.
//  Copyright © 2018 Coronation. All rights reserved.
import UIKit
import Foundation

//Local 
//let API_BASE_URL = "http://192.168.50.62/matchday/api/services/"

//Live
////let API_BASE_URL = "http://php1.coronation.in/matchday/beta/api/services/"

let APP = UIApplication.shared.delegate as? AppDelegate

let SCENE = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate

let NSUSERDEFAULT = UserDefaults.standard


var INITIAL_LAUNCH = "INITIAL_LAUNCH"
var SAVED_ADDRESS = "SAVED_ADDRESS"
let ADDRESS_DATA = "ADDRESS_DATA"
var SAVED_MOBILE_NO = "SAVED_MOBILE_NO"
var IS_ADDRESS_AVAILABLE = "IS_ADDRESS_AVAILABLE"






var USERID = ""
var CART_SESSION = ""
var DELIVERY_LOCATION = ""
var COME_FROM_REPEAT_ORDER = "COME_FROM_REPEAT_ORDER"

var LATTITUDE = ""
var LONGITUDE = ""
var ADDRESS = ""

let API_ID = "YzMxYjMyMzY0Y2UxOWNhOGZjZDE1MGE0MTdlY2NlNTg="
let FROMAPP = "true"

var searchLat = "searchLat"
var searchLong = "searchLong"
var searchArea = "searchArea"
var searchZip = "searchZip"
var searchCitys = "searchCity"
var searchStates = "searchState"
var searchCountrys = "searchCountry"

var SELECTED_ITEM_IMAGE = "SELECTED_ITEM_IMAGE"
var SELECTED_RESTAURANT_ID = "SELECTED_RESTAURANT_ID"

var LAT = "LAT"
var LONG = "LONG"
var ADDRESS_SELECTED = "ADDRESS_SELECTED"
let LOGIN = "LOGIN"
let SOCIAL_LOGIN = "SOCIAL_LOGIN"
let USER_EMAIL = "USER_EMAIL"
let USER_PHONE = "USER_PHONE"
let USER_NAME = "USER_NAME"
let USER_ID = "USER_ID"
let USER_IMAGE = "USER_IMAGE"
let FCM_TOKEN_ID = "FCM_TOKEN_ID"
let NOTIFICATION_COUNT = "NOTIFICATION_COUNT"
let USER_DATA = "USER_DATA"
let USER_TYPE = "USER_TYPE"
let IS_SHOW_INTRO = "IS_SHOW_INTRO"

let PREF_KEY_LANGUAGE_CODE = "kUserSelectedLanguageCode"
let GID_CLIENT_ID = "260716817125-fl9fdumitpn33jb04cu8l375gmtd1n7a.apps.googleusercontent.com"

let CURRANCY = "€"
let SESSION_USER_ID  = "SESSION_USER_ID"
let SAVED_CART_SESSION  = "SAVED_CART_SESSION"

let RELOAD_AFTER_PROFILE_UPDATE  = "RELOAD_AFTER_PROFILE_UPDATE"
let RELOAD_AFTER_NOTIFICATION_UPDATE  = "RELOAD_AFTER_NOTIFICATION_UPDATE"

var LeagueID = ""
var ISLOGIN = "NO"
var CURRENT_MESSAGE = ""
var SCREEN = ""
var NEW_MESSAGE = NSMutableDictionary()
var SELECTED_PICTURE = UIImage()
//let RELOAD_AFTER_REMOVE_CHIPS = "RELOAD_AFTER_REMOVE_CHIPS"

let kIphone_4s : Bool =  (UIScreen.main.bounds.size.height == 480)
let kIphone_5 : Bool =  (UIScreen.main.bounds.size.height == 568)
let kIphone_6 : Bool =  (UIScreen.main.bounds.size.height == 667)
let kIphone_6_Plus : Bool =  (UIScreen.main.bounds.size.height == 736)
let kIphone_X : Bool =  (UIScreen.main.bounds.size.height == 812)

//let COLOR_NAVIGATION = UIColor.colorFromHex(hexString: "#149c14")

let COLOR_TURQUOISE = UIColor(named: "Turquoise")
let NEW_GRAY = UIColor(named: "NewGray")
let GRAY = UIColor(named: "Gray")
let COLOR_BLUE = UIColor(named: "Blue")
let COLOR_GREEN = UIColor(named: "Green")
let COLOR_LIGHTGREEN = UIColor(named: "LightGreen")
let COLOR_ORANGE = UIColor(named: "Orange")
let COLOR_RED = UIColor(named: "Red")
let COLOR_YELLOW = UIColor(named: "Yellow")
let COLOR_BACKGROUND = UIColor(named: "Background")
//let COLOR_BACKGROUND = UIColor(named: "Background")


let IS_ADDRESS_ADDED = "IS_ADDRESS_ADDED"


func FONT_NORAML_CUSTOM(xx: CGFloat) -> UIFont
{
    return UIFont(name: "Tahoma", size: xx)!
}

func FONT_BOLD_CUSTOM(xx: CGFloat) -> UIFont
{
    return UIFont(name: "Tahoma-Bold", size: xx)!
}


var INTERNET_MESSAGE:String = "Please check your internet connection, it seems to be disconnected."

func SHOW_INTERNET_ALERT() {
//    AlertUtils().showAlertWithTitleFromVC(vc: (APP!.window?.rootViewController)!, title:"Atlanta", andMessage: INTERNET_MESSAGE, buttons: ["OK"]) { (index) in
//    }
}

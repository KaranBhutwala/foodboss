//
//  ServiceManager.swift
//  Atlanta
//
//  Created by Apple on 07/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

let ServManager = ServiceManager.shared

class ServiceManager: NSObject {
    var isLoderRequire: Bool = true
    var viewController = UIViewController()
    static let shared = ServiceManager()
    
//    var request: Alamofire.Request? {
//        didSet {
//            oldValue?.cancel()
//        }
//    }
    
    // Manthan 2-10-2020
    // Return reference for DataRequest,
    // So we can cancel ongoing request to avoid async response on popped ViewController
    @discardableResult func callGETApi(_ url: String, isLoaderRequired: Bool, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: NSDictionary) -> Void) -> DataRequest? {
        return callApi(true, url: url, paramters: nil, isLoaderRequired: isLoaderRequired, andCompletion: completion)
    }
    
    @discardableResult func callPostAPI(_ url: String, isLoaderRequired: Bool, paramters: [String: Any]?, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: NSDictionary) -> Void) -> DataRequest? {
        return callApi(false, url: url, paramters: paramters, isLoaderRequired: isLoaderRequired, andCompletion: completion)
    }
    
    fileprivate func callApi(_ isGet: Bool, url: String, paramters: [String: Any]?, isLoaderRequired: Bool, andCompletion completion: @escaping (_ isSuccess: Bool, _ statusCode: Int, _ message: String, _ data: NSDictionary) -> Void) -> DataRequest? {
        
        if !GlobalMethod.internet(){
            SHOW_INTERNET_ALERT()
            return nil
        }

        return AF.request(url, method: isGet ? .get : .post, parameters: paramters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in

            switch response.result {
            case .success(let JSON):
//                    GlobalMethod.StopProgress(self.viewController.view)
                let dictJSON = JSON as! NSDictionary
//                print("RESPONSE: ", dictJSON)
                var status_code = 0
                var message = ""
                var data = NSDictionary()
                
                if let success = dictJSON.value(forKey: "success") as? Int {
                    status_code = success
                }
                if let mes = dictJSON.value(forKey: "message") as? String {
                    message = mes
                }
//                    if let Dict = dictJSON.value(forKey: "data") as? NSDictionary {
//                        data = Dict
//                    }
                data = dictJSON
                if status_code == 1 {
                    completion(true, status_code, message, data)
                } else if status_code == 401 {
                    self.unAutorizedTokenLogout()
                } else {
                    completion(false, status_code, message, NSDictionary())
                }
            case .failure( _):
                print(response)
//                    GlobalMethod.StopProgress(self.viewController.view)
                completion(false, 0, "There might be some internal server issue. Please try after sometime.", NSDictionary())
            }
        }
    }

    func unAutorizedTokenLogout(){
        
    }
}

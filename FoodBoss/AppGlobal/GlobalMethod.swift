//
//  GlobalMethod.swift
//  Just One Energy
//
//  Created by Vipul on 26/04/18.
//  Copyright © 2018 Coronation. All rights reserved.

import UIKit
import MobileCoreServices
import Foundation
import CommonCrypto
import SwiftMessages
import Lottie


class GlobalMethod: NSObject
{
    class func applyShadowCardView(_ view: UIView, andCorner radi: Int) {
        view.layer.cornerRadius = CGFloat(radi)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 2.0
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
    
    class func applyWhiteShadowCardView(_ view: UIView, andCorner radi: Int) {
        view.layer.cornerRadius = CGFloat(radi)
        view.layer.shadowColor = UIColor.white.cgColor
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 2.0
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
    
    
    class func applyShadowCardProjectView(_ view: UIView, andCorner radi: Int) {
        view.layer.cornerRadius = CGFloat(radi)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 1.0
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
    
    class func StartAnimatedProgress(_ view: UIView)
    {
        let animationView = AnimationView(name: "horizontal_loader")
        animationView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .loop

        
        animationView.play()
        
        let  HUD = MBProgressHUD.showAdded(to: view, animated: true)
        HUD.bezelView.color = UIColor.clear
        //The key here is to save the GIF file or URL download directly into a NSData instead of making it a UIImage. Bypassing UIImage will let the GIF file keep the animation.
        HUD.customView = animationView
        
        HUD.customView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        var rotation: CABasicAnimation?
        rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation?.fromValue = nil
        // If you want to rotate Gif Image Uncomment
        //  rotation?.toValue = CGFloat.pi * 2
        rotation?.duration = 0.7
        rotation?.isRemovedOnCompletion = false
//        HUD.customView?.layer.add(rotation!, forKey: "Spin")
        HUD.mode = MBProgressHUDMode.customView
        // Change hud bezelview Color and blurr effect
        HUD.bezelView.color = UIColor.clear
        HUD.bezelView.tintColor = UIColor.clear
        HUD.bezelView.style = .solidColor
        HUD.bezelView.blurEffectStyle = .dark
        
        HUD.backgroundColor = UIColor.white
        HUD.contentColor = UIColor.white

        // Speed
        rotation?.repeatCount = .infinity
        HUD.show(animated: true)
    }
    
    class func StartCookingProgress(_ view: UIView)
    {
        let animationView = AnimationView(name: "Cooking")
        animationView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .loop

        
        animationView.play()
        
        let  HUD = MBProgressHUD.showAdded(to: view, animated: true)
        HUD.bezelView.color = UIColor.clear
        //The key here is to save the GIF file or URL download directly into a NSData instead of making it a UIImage. Bypassing UIImage will let the GIF file keep the animation.
        HUD.customView = animationView
        
        HUD.customView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        var rotation: CABasicAnimation?
        rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation?.fromValue = nil
        // If you want to rotate Gif Image Uncomment
        //  rotation?.toValue = CGFloat.pi * 2
        rotation?.duration = 0.7
        rotation?.isRemovedOnCompletion = false
//        HUD.customView?.layer.add(rotation!, forKey: "Spin")
        HUD.mode = MBProgressHUDMode.customView
        // Change hud bezelview Color and blurr effect
        HUD.bezelView.color = UIColor.clear
        HUD.bezelView.tintColor = UIColor.clear
        HUD.bezelView.style = .solidColor
        HUD.bezelView.blurEffectStyle = .dark
        
        HUD.backgroundColor = UIColor.white
        HUD.contentColor = UIColor.white

        // Speed
        rotation?.repeatCount = .infinity
        HUD.show(animated: true)
    }
    
    class func StartLottieProgress(_ view: UIView, Json : String)
    {
        let animationView = AnimationView(name: Json)
        animationView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .loop

        
        animationView.play()
        
        let  HUD = MBProgressHUD.showAdded(to: view, animated: true)
        HUD.bezelView.color = UIColor.clear
        //The key here is to save the GIF file or URL download directly into a NSData instead of making it a UIImage. Bypassing UIImage will let the GIF file keep the animation.
        HUD.customView = animationView
        
        HUD.customView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        var rotation: CABasicAnimation?
        rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation?.fromValue = nil
        // If you want to rotate Gif Image Uncomment
        //  rotation?.toValue = CGFloat.pi * 2
        rotation?.duration = 0.7
        rotation?.isRemovedOnCompletion = false
//        HUD.customView?.layer.add(rotation!, forKey: "Spin")
        HUD.mode = MBProgressHUDMode.customView
        // Change hud bezelview Color and blurr effect
        HUD.bezelView.color = UIColor.clear
        HUD.bezelView.tintColor = UIColor.clear
        HUD.bezelView.style = .solidColor
        HUD.bezelView.blurEffectStyle = .dark
        
        HUD.backgroundColor = UIColor.white
        HUD.contentColor = UIColor.white
        if Json == "Delivered"{
            HUD.backgroundColor = UIColor.black
            HUD.contentColor = UIColor.black
        }

        // Speed
        rotation?.repeatCount = .infinity
        HUD.show(animated: true)
    }
    
    class func StartProgress(_ view: UIView)
    {
        let spinner: RTSpinKitView = RTSpinKitView(style: .styleArc, color: COLOR_ORANGE)
                
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.isSquare = false
        hud.mode = MBProgressHUDMode.customView
        hud.customView = spinner
        hud.backgroundColor = UIColor.white
//        hud.contentColor = UIColor.colorFromHex(hexString: "")
        spinner.startAnimating()
    }
    
    public static func popDialog(controller: UIViewController, title:String, message: String){
        let dialog = UIAlertController(title: title, message: message, preferredStyle:UIAlertController.Style.alert)
        let objAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        dialog.addAction(objAction)
        controller.present(dialog, animated: true, completion: nil)
    }
    class func ShowNoInterNet(_ controller: UIViewController){
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        if let View = mainStoryboard.instantiateViewController(withIdentifier: "NoInternetConnectionViewController") as? NoInternetConnectionViewController
//        {
//            controller.present(View, animated:false, completion: nil)
//        }
    }
    
    class func StopProgress(_ view: UIView){

        MBProgressHUD.hide(for: view, animated: true)
    }
    
    class func ShowPopAlert(Message : String, View : UIViewController){
        let alert = UIAlertController(title: Message, message: nil, preferredStyle: UIAlertController.Style.alert)
        View.present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    class func ShowPop(Message : String){
        let warning = MessageView.viewFromNib(layout: .cardView)
        warning.configureTheme(.warning)
        warning.configureDropShadow()
        
//        let iconText = ["🤔", "😳", "🙄", "😶"].randomElement()!
        let iconText = ["🙄"].randomElement()!
        warning.configureContent(title: "Oops", body: Message, iconText: iconText)
        warning.button?.isHidden = true
        var warningConfig = SwiftMessages.defaultConfig
        warningConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar.rawValue)
        SwiftMessages.show(config: warningConfig, view: warning)
        
        
        let info = MessageView.viewFromNib(layout: .messageView)
        info.configureTheme(.info)
        info.button?.isHidden = true
        info.configureContent(title: "Oops", body: Message)
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.presentationStyle = .bottom
        infoConfig.duration = .seconds(seconds: 0.25)
        
//        SwiftMessages.show(config: infoConfig, view: info)
    }
    
    class func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    class func doStringContainsNumber( _string : String) -> Bool{
        let numberRegEx  = ".*[0-9]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: _string)

        return containsNumber
    }
    class func validateText(Text:String, Regex : String) -> Bool {
        let RegexFormat = Regex
        let RegexPredicate = NSPredicate(format:"SELF MATCHES %@", RegexFormat)
        return RegexPredicate.evaluate(with: Text)
    }
    
    class func Convertin64 (Str : String) -> String{
        let longstring = Str
        let data = (longstring).data(using: String.Encoding.utf8)
        let base64ID = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return base64ID
    }
    
    class func verifyUrl(urlString: String?) -> Bool {
        guard let urlString = urlString else {return false}
        guard let url = NSURL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url as URL) {return false}
        
        //
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: urlString)
        
    }
    
    class func internet() -> Bool {
        let reachability = Reachability()
        if reachability!.connection != .none {
            return true
        } else {
            return false
        }
    }
    
    class func ChangeDateFormate(Fulldate strdate: String, CurrentFormate setFor : String, getFormate getFor : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_GB")
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.dateFormat = setFor
        let formateDate = dateFormatter.date(from:strdate)!
        dateFormatter.dateFormat = getFor
        let dateFromString = "\(dateFormatter.string(from: formateDate))"
        return dateFromString
    }
    
    class func ConvertTimestampTodate(strdate: String, Format : String) -> String {
        let unixTimestamp = Double(strdate)
        let date = Date(timeIntervalSince1970: unixTimestamp!)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_GB")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = Format //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        //        print(strDate)
        return strDate
    }
    
    class func ChangeDateFormat(Fulldate strdate: String, CurrentFormate setFor : String, getFormate getFor : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_GB")
        dateFormatter.dateFormat = setFor
        let formateDate = dateFormatter.date(from:strdate)!
        dateFormatter.dateFormat = getFor
        let dateFromString = "\(dateFormatter.string(from: formateDate))"
        return dateFromString
    }
    
    class func getCurrentDate(Format : String) -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = Format
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let result = formatter.string(from: date)
        return result
    }
    
    class func validatePassword(password: String) -> Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        guard texttest.evaluate(with: password) else { return false }
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        guard texttest1.evaluate(with: password) else { return false }
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/_*+-]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        guard texttest2.evaluate(with: password) else { return false }
        
        return true
    }
    
    class func containsOnlyLetters(input: String) -> Bool {
        for chr in input {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    
    class func ShowToast(Text : String, View : UIView){
        var style = ToastStyle()
//        style.messageFont = UIFont(name: "Zapfino", size: 14.0)!
        style.messageColor = UIColor.white
        style.messageAlignment = .center
        style.backgroundColor = COLOR_TURQUOISE!
        View.makeToast(Text, duration: 1.5, position: .bottom, style: style)
    }
    
    class func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    class func CheckString(String: Any) -> String {
        if let str = String as? String{
            return str
        }
        else{
            let Blank = ""
            return Blank
        }
    }
    
    class func roundDecimals(str : String) -> String{
        if let VAlue = Double(str) {
            let string = String(format: "%.5f", VAlue)
            return string
        }
        else {
            let Blank = "0.0"
            return Blank
        }
    }
    
    class func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    
    class func ConvertInJsonToString(from object:Any) -> String?
    {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    class func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    class func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    class func PrintFonts(){
        var index = 1
        for family in UIFont.familyNames {
            print("\(family)")
            
            
            for name in UIFont.fontNames(forFamilyName: family) {
                print("\(name)")
                print(index)
                index = index + 1
            }
        }
    }
    
    class func NoInternetMessage (){
        let status2 = MessageView.viewFromNib(layout: .statusLine)
        status2.backgroundView.backgroundColor = UIColor.black
        status2.bodyLabel?.textColor = UIColor.white
        status2.configureContent(body: "Please check your internet connection.".localized)
        var status2Config = SwiftMessages.defaultConfig
        status2Config.presentationContext = .window(windowLevel: UIWindow.Level.normal.rawValue)
        status2Config.preferredStatusBarStyle = .lightContent
        
        SwiftMessages.show(config: status2Config, view: status2)
    }
    
    class func fixOrientation(img:UIImage) -> UIImage {
        if (img.imageOrientation == UIImage.Orientation.up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }
    
    class func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    class func displayNoInternet(View : UIViewController) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "NoLoginViewController") as! NoLoginViewController
//
//        View.addChild(controller)
//
//        // Add the child's View as a subview
//        View.view.addSubview(controller.view)
//        controller.view.frame = View.view.bounds
//        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//        // tell the childviewcontroller it's contained in it's parent
//        controller.didMove(toParent: View)
    }
    class func displayNoData(View : UIViewController) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = storyboard.instantiateViewController(withIdentifier: "NoLoginViewController") as! NoLoginViewController
//
//        View.addChild(controller)
//
//        // Add the child's View as a subview
//        View.view.addSubview(controller.view)
//        controller.view.frame = View.view.bounds
//        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//        // tell the childviewcontroller it's contained in it's parent
//        controller.didMove(toParent: View)
    }
    
    class func removeNoData(View : UIViewController) {
        
    }
    
    class func selectedLanguageForUser() -> String {
        return UserDefaults.standard.string(forKey: PREF_KEY_LANGUAGE_CODE) ?? "english"
    }
    
    class func getLanguageCodeFrom(language: String) -> String {
        if language == "greek" {
            return "el"
        } else {
            return "en"
        }
    }
    
    class func deviceLanguage() -> String {
        var languageList = UserDefaults.standard.array(forKey: "AppleLanguages") as? [String] ?? []
        if languageList.isEmpty {
            languageList = ["en"]
            UserDefaults.standard.set(languageList, forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            return "en"
        } else {
            let prefLang = languageList.first
            let subLangs = prefLang!.components(separatedBy: "-")
            if subLangs.first! == "el" {
                // Greek
                return "el"
            } else {
                // English
                return "en"
            }
        }
    }
}

//extension UIColor {
//    public convenience init?(hexString: String) {
//        let r, g, b, a: CGFloat
//
//        if hexString.hasPrefix("#") {
//            let start = hexString.index(hexString.startIndex, offsetBy: 1)
//            let hexColor = String(hexString[start...])
//
//            if hexColor.count == 8 {
//                let scanner = Scanner(string: hexColor)
//                var hexNumber: UInt64 = 0
//
//                if scanner.scanHexInt64(&hexNumber) {
//                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
//                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
//                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
//                    a = CGFloat(hexNumber & 0x000000ff) / 255
//
//                    self.init(red: r, green: g, blue: b, alpha: a)
//                    return
//                }
//            }
//        }
//
//        return nil
//    }
//}

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}


extension UISearchBar {
    
    var textColor:UIColor? {
        get {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                return textField.textColor
            } else {
                return nil
            }
        }
        
        set (newValue) {
            if let textField = self.value(forKey: "searchField") as?
                UITextField  {
                textField.textColor = newValue
            }
        }
    }
}

extension String {
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_")
        return text.filter {okayChars.contains($0) }
    }
    
    var stripped: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return self.filter {okayChars.contains($0) }
    }

}

extension Date {
    
    func adding(months: Int) -> Date? {
        let calendar = Calendar(identifier: .gregorian)
        
        var components = DateComponents()
        components.calendar = calendar
        components.timeZone = TimeZone(secondsFromGMT: 0)
        components.month = months
        
        return calendar.date(byAdding: components, to: self)
    }
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
}

extension UIImage {
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func DropShadow()
    {
        let layer = self.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
    }
    
}
extension String
{
    func Convertmd5() -> String!
    {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLength = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLength)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        
        for i in 0..<digestLength {
            hash.appendFormat("%02x", result[i])
        }
        
        result.deinitialize(count: 0)
        
        return String(format: hash as String)
    }
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

extension UILabel {
    
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font!], context: nil).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
    
    public static func estimatedSize(_ text: String, targetSize: CGSize = .zero) -> CGSize {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.text = text
       // label.font = FONT_BOLD_CUSTOM(xx: 16)
        return label.sizeThatFits(targetSize)
    }
    
    func setHTMLFromString(htmlText: String) {
        let modifiedFont = String(format:"<span style=\"font-family:'Colfax-Medium'; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)
//        let modifiedFont = NSString(format:"<span style=\"font-family: \(self.font!.fontName); font-size: \(self.font!.pointSize)\">%@</span>" as NSString, text)

        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        self.attributedText = attrStr
    }
    
    

    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText

        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }

    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)

        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)

        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
    
}

extension String
{
    var alphanumeric: String
    {
        return self.components(separatedBy: CharacterSet.alphanumerics.inverted).joined().lowercased()
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func makeCustomRound(topLeft: CGFloat = 0, topRight: CGFloat = 0, bottomLeft: CGFloat = 0, bottomRight: CGFloat = 0) {
        let minX = bounds.minX
        let minY = bounds.minY
        let maxX = bounds.maxX
        let maxY = bounds.maxY

        let path = UIBezierPath()
        path.move(to: CGPoint(x: minX + topLeft, y: minY))
        path.addLine(to: CGPoint(x: maxX - topRight, y: minY))
        path.addArc(withCenter: CGPoint(x: maxX - topRight, y: minY + topRight), radius: topRight, startAngle:CGFloat(3 * Double.pi / 2), endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: maxX, y: maxY - bottomRight))
        path.addArc(withCenter: CGPoint(x: maxX - bottomRight, y: maxY - bottomRight), radius: bottomRight, startAngle: 0, endAngle: CGFloat(Double.pi / 2), clockwise: true)
        path.addLine(to: CGPoint(x: minX + bottomLeft, y: maxY))
        path.addArc(withCenter: CGPoint(x: minX + bottomLeft, y: maxY - bottomLeft), radius: bottomLeft, startAngle: CGFloat(Double.pi / 2), endAngle: CGFloat(Double.pi), clockwise: true)
        path.addLine(to: CGPoint(x: minX, y: minY + topLeft))
        path.addArc(withCenter: CGPoint(x: minX + topLeft, y: minY + topLeft), radius: topLeft, startAngle: CGFloat(Double.pi), endAngle: CGFloat(3 * Double.pi / 2), clockwise: true)
        path.close()

        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UITableView {
    
    func scrollToBottom(){
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}


extension FileManager{
    
    func createTemporaryDirectory() throws -> URL {
//        let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(UUID().uuidString)
        let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("ChatArchive")
        try createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        return url
    }
}

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
}

extension UIImage {

    func imageWithSize(scaledToSize newSize: CGSize) -> UIImage {

        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }

}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

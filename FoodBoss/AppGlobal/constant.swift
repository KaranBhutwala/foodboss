//
//  constant.swift
//  MatchDay
//
//  Created by Apple on 30/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
class constant {
    
    public static let API_BASE_URL = "http://www.foodboss.gr/api/"
    
//    public static let API_BASE_URL = "http://192.168.50.62/foodboss/api/"
    
    public static let LANGUAGE_LIST = API_BASE_URL + "services/list_languages"
    
    public static let LOG_IN = API_BASE_URL + "services/request_login_otp"
    
    public static let CLEAR_CART = API_BASE_URL + "services/clear_cart"

    public static let REQUEST_OTP_PLACEORDER = API_BASE_URL + "services/request_otp"

    public static let VERIFY_OTP_PLACEORDER = API_BASE_URL + "services/verify_opt"

    
    public static let EMAIL_SIGN_IN = API_BASE_URL + "services/signin"
    
    public static let EMAIL_SIGN_UP = API_BASE_URL + "services/signup"
    
    public static let FORGOT_PASSWORD = API_BASE_URL + "services/forgot_password"

    
    public static let SOCIAL_LOG_IN = API_BASE_URL + "services/social_login"
    
    public static let ADD_LOCATION = API_BASE_URL + "services/user_addresses/save"
    
    public static let REMOVE_LOCATION = API_BASE_URL + "services/user_addresses/delete"

    public static let COUNTRY = API_BASE_URL + "services/list_country"
    
    public static let VERIFY_OTP = API_BASE_URL + "services/check_login_otp"
    
    public static let ADDRESS = API_BASE_URL + "services/user_addresses/list"
    
    public static let COUPON_LIST = API_BASE_URL + "services/coupons/list"
    
    public static let OFFER_LIST = API_BASE_URL + "services/coupons/offers_list"

    public static let CATEGORY_LIST = API_BASE_URL + "services/categories/list"
    
    public static let RESTAURANT_LIST = API_BASE_URL + "services/restaurants/list"
    
    public static let FAVOURITE_RESTAURANT_LIST = API_BASE_URL + "services/favourite_restaurants/list"
    
    public static let SEARCH_LIST = API_BASE_URL + "services/search"
    
    public static let CART_LIST = API_BASE_URL + "services/cart/list"
    
    public static let RESTAURANT_FAV = API_BASE_URL + "services/favourite_restaurants/add"
    
    public static let PAYMENT_LIST = API_BASE_URL + "services/paymentMethod"
    
    public static let PROFILE = API_BASE_URL + "services/user_profile/details"
    
    public static let UPDATE_PROFILE_PICTURE = API_BASE_URL + "services/update_profile_picture"
    
    public static let UPDATE_PROFILE = API_BASE_URL + "services/user_profile/update"
    
    public static let NOTIFICATION_LIST = API_BASE_URL + "services/users/notifications_list"
    
    public static let CUISINES_LIST = API_BASE_URL + "services/cuisines/list"
    
    public static let ORDER_LIST = API_BASE_URL + "services/orders/list"
    
    public static let ORDER_DETAILS = API_BASE_URL + "services/orders/details"
    
    public static let DETAILS = API_BASE_URL + "services/restaurants/details"
    
    public static let MENU_ITEM_DETAILS = API_BASE_URL + "services/menu_items/details"
    
    public static let ADD_TO_CART = API_BASE_URL + "services/cart/add"
    
    public static let UPDATE_CART = API_BASE_URL + "services/cart/update"
    
    public static let DELETE_CART = API_BASE_URL + "services/cart/delete"
    
    public static let APPY_COUPON = API_BASE_URL + "services/cart/apply_coupon"
    
    public static let PLACE_ORDER = API_BASE_URL + "services/placeorder"
    
    public static let GET_ITEM_PRICE = API_BASE_URL + "services/menu_items/get_item_price"
    
    public static let REORDER = API_BASE_URL + "services/orders/reorder"
    
    public static let REVIEW = API_BASE_URL + "services/reviews/saveOrderReviews"
    
    public static let CLIENT_TOKEN = API_BASE_URL + "services/clientToken"
    
    public static let INITIATE_TRANSACTION = API_BASE_URL + "services/braintree_initiate_payment"
    
    public static let SAVED_PAYMENT_METHOD = API_BASE_URL + "services/user_payment_cards/list"
    
    public static let SET_DEFAULT_PAYMENT_METHOD = API_BASE_URL + "services/user_payment_cards/set_as_default"
    
    public static let DELETE_PAYMENT_METHOD = API_BASE_URL + "services/user_payment_cards/delete"
}

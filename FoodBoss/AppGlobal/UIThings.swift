//
//  UIThings.swift
//  Atlanta
//
//  Created by Apple on 09/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class UIThings: NSObject {

}


//MARK:- ACTION SHEET
func showActionSheetWithTitleFromVC(vc:UIViewController, title:String, andMessage message:String, buttons:[String],canCancel:Bool, completion:((_ index:Int) -> Void)!) -> Void {
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
    
    
    
    for index in 0..<buttons.count    {
        
        let action = UIAlertAction(title: buttons[index], style: .default, handler: {
            (alert: UIAlertAction!) in
            if(completion != nil){
                completion(index)
            }
        })
        
        alertController.addAction(action)
    }
    
    if(canCancel){
        let action = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: {
            (alert: UIAlertAction!) in
            if(completion != nil){
                completion(buttons.count)
            }
        })
        
        alertController.addAction(action)
    }
    
    vc.present(alertController, animated: true, completion: nil)
}

func resizeImage(_ image: UIImage, longEdge: CGFloat) -> UIImage {
    
    var newHeight = image.size.height
    var newWidth = image.size.width
    
    if newHeight < 1000 && newWidth < 1000{
        return image
    }
    
    var scale = longEdge / image.size.height
    
    if newHeight > newWidth{
        scale = longEdge / image.size.height
        newWidth = image.size.width * scale
        newHeight = longEdge
    }else{
        scale = longEdge / image.size.width
        newHeight = image.size.height * scale
        newWidth = longEdge
    }
    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}


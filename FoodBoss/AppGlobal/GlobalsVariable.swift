//
//  GlobalsVariable.swift
//  FoodBoss
//
//  Created by IOS Development on 15/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import Foundation
import UIKit

struct GlobalsVariable
{
    static var sharedInstance = GlobalsVariable()
    
    
    var CustomAlert = "Alert"

    var isComeFromMenu = "false"
    
    var isAppLaunchFirstTime = false
    var ArrayOfSearchMenu = [String]()
    var ItemAddComeFrom = ""
    var isComeFromMenuAddress = "false"
    var SelectedRestaurantType = ""
    var isComeFromCart = "false"

    var SelectedOptionForFilter = "1"
    var SelectedOptionForFilterType = "Delivery"
    var FilterScreenComeFrom = "Dashboard"
}

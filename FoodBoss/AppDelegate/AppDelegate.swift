//
//  AppDelegate.swift
//  FoodBoss
//
//  Created by Raj Shukla on 06/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import CoreData
import Braintree
import GoogleSignIn
import FBSDKCoreKit
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GIDSignIn.sharedInstance()?.clientID = GID_CLIENT_ID
        BTAppSwitch.setReturnURLScheme("com.FB.FoodBoss.app.payments")
        ApplicationDelegate.shared.application(application,
                                               didFinishLaunchingWithOptions: launchOptions)
        
        let languageList = UserDefaults.standard.array(forKey: "AppleLanguages") as? [String] ?? []
        print("\n Available Language List:\t \(languageList) \n")
        
        let langCode = GlobalMethod.deviceLanguage()
        UserDefaults.standard.set(langCode == "el" ? "greek" : "english", forKey: PREF_KEY_LANGUAGE_CODE)
       
        GMSPlacesClient.provideAPIKey("AIzaSyBoNGeJtvqtsD3HT4uX069JFfD0rLuJ1bk")

        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableDebugging = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        
      /*  ISLOGIN = "NO"
        if let Login = UserDefaults.standard.string(forKey: LOGIN){
            if Login == "Yes"{
                ISLOGIN = "YES"
                
                if let str = UserDefaults.standard.string(forKey: LAT){
                    LATTITUDE = str
                }
                if let str = UserDefaults.standard.string(forKey: LONG){
                    LONGITUDE = str
                }
                if let str = UserDefaults.standard.string(forKey: ADDRESS_SELECTED){
                    ADDRESS = str
                }
                
                if let User = UserDefaults.standard.string(forKey: USER_ID){
                    USERID = User
                }
                
                var Identifire = "NavLocation"
                
                if !LATTITUDE.isEmpty{
                    Identifire = "NavTab"
                }
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: Identifire)
                
                
                self.window?.rootViewController = initialViewController
                
                if let dicUser = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary
                {
                    print(dicUser)
                    if let str = dicUser.value(forKey: "user_id") as? String, str.count != 0
                    {
                        USERID = str
                    }
                    
                    if let str = dicUser.value(forKey: "cart_session") as? String, str.count != 0
                    {
                        CART_SESSION = str
                    }
                    
                    if let str = dicUser.value(forKey: "user_id") as? Int, str != 0
                    {
                        USERID = String (describing: str)
                    }
                }
            }
        }*/
        
       // configureSearchPlace()
        
        return true
    }

    func configureSearchPlace()
    {
        let barColor: UIColor =  _ColorLiteralType(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
      let backgroundColor: UIColor =  _ColorLiteralType(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
      let textColor: UIColor =  _ColorLiteralType(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
      // Navigation bar background.
        UINavigationBar.appearance().barTintColor = barColor
        UINavigationBar.appearance().tintColor = UIColor.white
      // Color and font of typed text in the search bar.
        let searchBarTextAttributes = [NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 16)]
      UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes as [NSAttributedString.Key : Any]
      // Color of the placeholder text in the search bar prior to text entry
        let placeholderAttributes = [NSAttributedString.Key.foregroundColor: backgroundColor, NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 15)]
      // Color of the default search text.
        let attributedPlaceholder = NSAttributedString(string: "Search", attributes: placeholderAttributes as [NSAttributedString.Key : Any])
      UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = attributedPlaceholder

    }
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print("\nAPP DELEGATE OPEN USER:\t\(url)\n")
        if url.scheme?.localizedCaseInsensitiveCompare("com.FB.FoodBoss.app.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        } else if url.scheme?.localizedCaseInsensitiveCompare("com.googleusercontent.apps.260716817125-fl9fdumitpn33jb04cu8l375gmtd1n7a") == .orderedSame {
            if GIDSignIn.sharedInstance()?.handle(url) == true {
                return true
            }
        } else {
            ApplicationDelegate.shared.application(app,
                                                   open: url,
                                                   sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                   annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        return false
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "FoodBoss")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


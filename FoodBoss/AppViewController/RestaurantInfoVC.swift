//
//  RestaurantInfoVC.swift
//  FoodBoss
//
//  Created by Apple on 01/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class RestaurantInfoVC: UIViewController {
    let tableHeaderHeight: CGFloat = 250.0
    static func initiateFromStoryBoard() -> RestaurantInfoVC {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            let vc = mainStoryBoard.instantiateViewController(identifier: String(describing: RestaurantInfoVC.self)) as RestaurantInfoVC
            return vc
        } else {
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: RestaurantInfoVC.self)) as! RestaurantInfoVC
            return vc
        }
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var vcTitleLabel: UILabel!
    var collectionView: UICollectionView!
    
    var restaurantInfo = RestaurantInfo.getEmptyInfo()
    var imageList = [String]()
    var timer: Timer? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.setImage(#imageLiteral(resourceName: "Back_White"), for: .normal)
        self.backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: .touchUpInside)
        self.navigationView.backgroundColor = .clear
        self.vcTitleLabel.text = ""
        self.view.bringSubviewToFront(self.navigationView)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: tableHeaderHeight),
                                               collectionViewLayout: flowLayout)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isPagingEnabled = true
        self.collectionView.bounces = false
        self.collectionView.registerNib(ImageCollectionViewCell.self)
        
        self.tableView.estimatedRowHeight = 44
        self.tableView.estimatedSectionHeaderHeight = 295
        
        let tvHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: tableHeaderHeight))
        tvHeaderView.addSubview(self.collectionView)
        self.tableView.tableHeaderView = tvHeaderView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tableView.reloadData()
        self.collectionView.reloadData()
        self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollCollectionView), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
    }
    
    @objc func scrollCollectionView() {
        if self.imageList.count > 1 {
            let offSet = self.collectionView.contentOffset.x
            if offSet == 0.0 {
                self.collectionView.setContentOffset(CGPoint(x: offSet + self.collectionView.frame.width, y: 0), animated: true)
            } else if offSet >= self.collectionView.frame.width {
                let page = offSet / self.collectionView.frame.width
                let pageInt = Int(page)
                if pageInt == self.imageList.count - 1 {
                    self.collectionView.setContentOffset(CGPoint.zero, animated: true)
                } else {
                    self.collectionView.setContentOffset(CGPoint(x: offSet + self.collectionView.frame.width, y: 0), animated: true)
                }
            }
        }
    }
    
    @objc func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func changeController(_ sender: UISegmentedControl) {
        guard let masterVC = self.parent as? RestaurantDetailsVC else { return }
        masterVC.selectController(index: 0)
    }
    
}

extension RestaurantInfoVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "RestaurantInfoHeaderCell") as? RestaurantInfoHeaderCell
        headerView?.headerLabel.text = "Payment Methods".localized
        headerView?.kRestaurantAddressLabel.text = "Restaurant Address".localized
        headerView?.kWorkingHoursLabel.text = "Working Hours".localized
        headerView?.restaurantAddressLabel.text = self.restaurantInfo.address
        headerView?.descriptionLabel.text = self.restaurantInfo.description
        headerView?.segmentController.selectedSegmentIndex = 1
        headerView?.segmentController.setTitle("Reviews".localized, forSegmentAt: 0)
        headerView?.segmentController.setTitle("Info".localized, forSegmentAt: 1)
        headerView?.segmentController.addTarget(self, action: #selector(changeController(_:)), for: .valueChanged)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restaurantInfo.timing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantTimeCell", for: indexPath)
        let dayLabel = cell.viewWithTag(1) as! UILabel
        let timeLabel = cell.viewWithTag(2) as! UILabel
        
        dayLabel.text = self.restaurantInfo.timing[indexPath.row].day.capitalized
        timeLabel.text = "\(self.restaurantInfo.timing[indexPath.row].startTime) - \(self.restaurantInfo.timing[indexPath.row].closeTime)"
        
        return cell
    }
}

extension RestaurantInfoVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            if self.tableView.contentOffset.y > 250 && self.vcTitleLabel.text?.isEmpty == true {
                self.backButton.setImage(#imageLiteral(resourceName: "Back_White_gray"), for: .normal)
                self.vcTitleLabel.text = restaurantInfo.restaurantName
                self.navigationView.backgroundColor = .white
                self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
            } else if self.tableView.contentOffset.y <= 0 && self.vcTitleLabel.text?.isEmpty == false {
                self.backButton.setImage(#imageLiteral(resourceName: "Back_White"), for: .normal)
                self.vcTitleLabel.text = ""
                self.navigationView.backgroundColor = .clear
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
    }
    
}

extension RestaurantInfoVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return max(1, imageList.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(ImageCollectionViewCell.self, indexPath: indexPath)
        if self.imageList.count == 0 {
            cell.img.image = UIImage (named: "RestaurantPlaceholder")
        } else {
            let str = imageList[indexPath.item]
            let availableWidth = collectionView.frame.width
            var imgUrl:String = str + "&w=\(availableWidth)"
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            cell.img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: UIImage (named: "RestaurantPlaceholder"))
        }
        
        return cell
    }
    
}

extension RestaurantInfoVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: tableHeaderHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}


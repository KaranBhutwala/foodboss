//
//  MyAccountViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 08/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class MyAccountViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    private var arrayData = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.registerNib(MyAccountTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.addNavigation()
        arrayData = [["Title":"Manage Accounts".localized,"Desc":"Work, Home & Other...".localized],
                     ["Title":"Payments".localized,"Desc":"Wallet, Saved Cards...".localized],
                     ["Title":"Favourites".localized,"Desc":"Your top liked restaurant & food".localized],
                 ["Title":"Offers".localized,"Desc":"Best deals for you...".localized]]
        
    }
   
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "My account".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - TableView

extension MyAccountViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MyAccountTableViewCell.self)
        if let Dict = arrayData.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        } else if indexPath.row == 1 {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "SavedPaymentMethodsViewController") as? SavedPaymentMethodsViewController {
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "SavedPaymentMethodsViewController") as? SavedPaymentMethodsViewController {
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        } else if indexPath.row == 2 {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "FavRestaurantListViewController") as? FavRestaurantListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "FavRestaurantListViewController") as? FavRestaurantListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
        
    }
}

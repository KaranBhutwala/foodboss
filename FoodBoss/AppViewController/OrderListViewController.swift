//
//  OrderListViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 10/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class OrderListViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    private var ArrayOrderList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        self.tblView.registerNib(OrderListRestaurantTableViewCell.self)
        self.tblView.registerNib(OrderListItemTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 235
        self.FetchOrders()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Past Orders".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK:- --- [TABLEVIEW CALLING] ---

extension OrderListViewController: SkeletonTableViewDelegate, SkeletonTableViewDataSource {
    //return UITableView.automaticDimension
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: OrderListRestaurantTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ArrayOrderList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
//        if let Dict = self.ArrayOrderList.object(at: section) as? NSDictionary{
//            print(Dict)
//            if let Arr = Dict.value(forKey: "order_items") as? NSArray{
//                return Arr.count + 1
//            }
//        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let Dict = self.ArrayOrderList.object(at: indexPath.section) as? NSDictionary{
            if let Arr = Dict.value(forKey: "order_items") as? NSArray{
                if indexPath.row == 0{
                    let cell = tableView.dequeue(OrderListRestaurantTableViewCell.self)
                    cell.selectionStyle = .none
                    GlobalMethod.applyShadowCardView(cell.viewInner, andCorner: 6)
                    cell.lblOrderItems.text = ""
                    if let DictItem = Arr.object(at: 0) as? NSDictionary{
                        cell.configure(with: Dict, DictItem: DictItem)
                    }
                    let items = Arr.count
                    if items != 1{
                        cell.lblOrderItems.text =  "+ " + String (describing: items - 1) + " Items".localized
                    }
                    cell.delegate = self
                    cell.index = indexPath
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = self.ArrayOrderList.object(at: indexPath.section) as? NSDictionary{
            if let Arr = Dict.value(forKey: "order_items") as? NSArray{
                if indexPath.row == 0{
//                    print(Dict)
                    if #available(iOS 13.0, *) {
                        if let View = self.storyboard?.instantiateViewController(identifier: "OrderDetailViewController") as? OrderDetailViewController{
                            if let str = Dict.value(forKey: "order_id") as? String{
                                View.OrderID = str
                            }
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    } else {
                        if let View = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as? OrderDetailViewController{
                            if let str = Dict.value(forKey: "order_id") as? String{
                                View.OrderID = str
                            }
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}
//MARK:- --- [Actions] ---

extension OrderListViewController: ReOrderActionDelegate{
    func ReorderButtonPressed(_ index: IndexPath) {
        if let Dict = self.ArrayOrderList.object(at: index.section) as? NSDictionary{
            print(Dict)
            if let str = Dict.value(forKey: "order_id") as? String{
                self.ReOrder(OrderID: str)
            }
        }
    }
}

//MARK:- --- [API CALLING] ---

extension OrderListViewController{
    
    func FetchOrders(){
        if !GlobalMethod.internet(){
            
        }else{
            
//            GlobalMethod.StartProgress(self.view)
            self.tblView.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                        "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                        "page":"0",
                                        "limit":"20",
                                        "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ORDER_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
//                    print(dataDict)
                    if let Data = OrderListModel.init(dictionary: dataDict){
                        for Dict in Data.orders!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayOrderList.add(dictTemp)
                                }
                            }
                        }
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
//                    GlobalMethod.StopProgress(self.view)
                    self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    func ReOrder(OrderID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                        "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                        "order_id":OrderID,
                                        "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                        "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.REORDER, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
//                    print(dataDict)
                    if #available(iOS 13.0, *) {
                        if let View = self.storyboard?.instantiateViewController(identifier: "CartViewController") as? CartViewController{
                            View.isPushed = true
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    } else {
                        if let View = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController{
                            View.isPushed = true
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

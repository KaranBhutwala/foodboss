//
//  CountryPickerViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 11/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class CountryPickerViewController: UIViewController {

    @IBOutlet var tblView: UITableView!
    
    private var arrayLocation = NSMutableArray()
    private var arrayLocationCopy = NSMutableArray()
    
    var SelectedCountry: ((_ arr: NSDictionary) -> Void)?
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect (x: 0, y: 0, width: 200, height: 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.AddRightBarbuttonItems()
        self.addNavigation()
        self.tblView.tableFooterView = UIView()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = COLOR_TURQUOISE
        self.navigationController?.navigationBar.barTintColor = COLOR_TURQUOISE
        self.navigationController?.navigationBar.backgroundColor = COLOR_TURQUOISE
        
        self.FetchCountry()
    }
    
    func AddRightBarbuttonItems()
    {
        let btnSearch = UIButton.init(type: .custom)
        btnSearch.setImage(UIImage(named: "search_menu"), for: UIControl.State.normal)
        btnSearch.addTarget(self, action: #selector(self.ActionSearch(_:)), for: UIControl.Event.touchUpInside)
        btnSearch.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        btnSearch.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7);
        let barSearch = UIBarButtonItem(customView: btnSearch)
        
        self.navigationItem.rightBarButtonItems = [barSearch]
    }
    
    @IBAction func ActionSearch(_ sender: UIButton)
    {
        self.navigationItem.rightBarButtonItems = nil
        searchBar.placeholder = "Search".localized
        searchBar.searchBarStyle = .minimal
        searchBar.tintColor = COLOR_YELLOW
        searchBar.delegate = self
        searchBar.textColor = COLOR_YELLOW
        searchBar.showsCancelButton = true
        searchBar.becomeFirstResponder()
        
        self.searchBar.tintColor = COLOR_YELLOW
        self.searchBar.delegate = self
        self.searchBar.textColor = COLOR_YELLOW
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        textField.clearButtonMode = .never
        
        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
        glassIconView.tintColor = COLOR_YELLOW
        
        let clearButton = textField.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = COLOR_YELLOW
        
        navigationItem.titleView = UIView()
        navigationItem.titleView = searchBar
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "Back_Arrow"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        lblHeader.text = "Countries".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 16)
        lblHeader.textColor = COLOR_YELLOW
        viewHeader.addSubview(lblHeader)
        
        navigationItem.titleView = viewHeader
        self.navigationItem.leftBarButtonItems = [barBack]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: Tble
extension CountryPickerViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "CellCountry") as! UITableViewCell
        if let Dict = self.arrayLocation.object(at: indexPath.row) as? NSDictionary{
            let lbl = Cell.viewWithTag(1) as! UILabel
            if let strDialCode = Dict.value(forKey: "phonecode") as? String{
                if let strname = Dict.value(forKey: "name") as? String{
                    lbl.text = strDialCode + "   " + strname
                }
            }
        }
        Cell.selectionStyle = .none
        return Cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = self.arrayLocation.object(at: indexPath.row) as? NSDictionary{
            if let block = self.SelectedCountry {
                block(Dict)
//                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


// MARK: - Search
extension CountryPickerViewController: UISearchBarDelegate
{
    func Filtersearch(SearchText searchText: String)
    {
        let predicate = NSPredicate(format: "phonecode CONTAINS[cd] %@ || name CONTAINS[cd] %@",searchText,searchText)
        let arrTemp = arrayLocationCopy.filtered(using: predicate) as NSArray
        arrayLocation = arrTemp.mutableCopy() as! NSMutableArray
        
        if searchText.count == 0
        {
            self.arrayLocation = arrayLocationCopy
        }
        
        tblView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.count == 0
        {
            if arrayLocation.count != arrayLocationCopy.count
            {
                arrayLocation.removeAllObjects()
                arrayLocation.addObjects(from: arrayLocationCopy as! [Any])
                tblView.reloadData()
            }
        }
        else
        {
            self.Filtersearch(SearchText: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        guard let trimmedString = searchBar.text?.trimmingCharacters(in: CharacterSet.whitespaces) else{
            return
        }
        
        self.Filtersearch(SearchText: trimmedString)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        //        self.PageIndex = 0
        self.view.endEditing(true)
        searchBar.text = String()
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        
        self.view.endEditing(true)
        self.navigationItem.rightBarButtonItems = nil
        self.addNavigation()
        self.AddRightBarbuttonItems()
        
        if arrayLocation.count != arrayLocationCopy.count
        {
            arrayLocation.removeAllObjects()
            arrayLocation.addObjects(from: arrayLocationCopy as! [Any])
        }
        tblView.reloadData()
    }
}

//MARK:- --- [API CALLING] ---

extension CountryPickerViewController{
    
    func FetchCountry(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,"apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.COUNTRY, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    if let Data = CountryModel.init(dictionary: dataDict){
                        for Dict in Data.countries!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.arrayLocation.add(dictTemp)
                                    self.arrayLocationCopy.add(dictTemp)
                                }
                            }
                        }
                    }
                    self.tblView.reloadData()
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

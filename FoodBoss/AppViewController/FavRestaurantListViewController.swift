//
//  FavRestaurantListViewController.swift
//  FoodBoss
//
//  Created by Apple on 01/08/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class FavRestaurantListViewController: UIViewController {
    
    private var ArrayRestaurant = NSMutableArray()
    @IBOutlet weak var tblView: UITableView!

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.addNavigation()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        self.tblView.registerNib(RestaurantTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 104
        self.FetchRestaurant()
    }
    
    func addNavigation()
       {
           self.navigationController?.navigationBar.topItem?.title = ""
           self.navigationController?.navigationBar.topItem?.title = ""
           self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
           self.navigationController?.navigationBar.shadowImage = UIImage()
           self.navigationController?.navigationBar.isTranslucent = true
           self.navigationController?.view.backgroundColor = UIColor.white
           
           let button = UIButton.init(type: .custom)
           button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
           button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
           let barBack = UIBarButtonItem(customView: button)
           
           let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
           let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
           
           
           lblHeader.text = "Favourite Restaurants".localized
           lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
           lblHeader.textColor = UIColor.black
           viewHeader.addSubview(lblHeader)
       
           self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
       }

    
    @objc func ActionBack()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - TableView

extension FavRestaurantListViewController: SkeletonTableViewDelegate, SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
//        String(describing: T.self)
        return String(describing: RestaurantTableViewCell.self)
//        return "RestaurantTableViewCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayRestaurant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RestaurantTableViewCell.self)
//        cell.delegate = self
//        cell.index = indexPath
        if let Dict = ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                //return 250
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = self.ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            print(Dict)
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
}
//MARK:- --- [API CALLING] ---

extension FavRestaurantListViewController{
    func FetchRestaurant(){
        if !GlobalMethod.internet(){
            
        }else{
            
//            GlobalMethod.StartProgress(self.view)
            self.tblView.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "type":"",
                                          "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.FAVOURITE_RESTAURANT_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let Data = FavouriteRestaurantModel.init(dictionary: dataDict){
                        for Dict in Data.favourite_restaurants!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayRestaurant.add(dictTemp)
                                }
                            }
                        }
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
//                    GlobalMethod.StopProgress(self.view)
                    self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()
                }
            }
        }
    }
}

//
//  CartViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 19/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Lottie
import PopupKit
import Braintree
import BraintreeDropIn
import SkeletonView
import Alamofire

class CartViewController: UIViewController,UITextViewDelegate {

    
    @IBOutlet weak var View0: UIView!
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    @IBOutlet weak var View4: UIView!
    @IBOutlet weak var View5: UIView!
    var resLat = ""
    var resLong = ""

    
    var isPushed = false
    var LottieJson = "Burger"
    
    @IBOutlet weak var RepeatView: UIView!
    private var ArrayCartItems = NSMutableArray()
    
    // Manthan 14-9-2020
    // Using Model for Payment Option
    //private var ArrayPayment = NSMutableArray()
    private var paymentOptionList = [PaymentMethodModel]()
    enum PaymentType: String {
        case None, COD, Online
    }
    private var selectedPaymentType: PaymentType = .None
    var dataCollector: BTDataCollector!
    var alertListView: AlertListView!
    var deliveryAlertListView: DeliveryListViewAlert!
    var tipAmount = NSNumber(integerLiteral: 0)
    let numberFormatter  = NumberFormatter()
    // TableView info
    let kCellIdentifier = "keyCellIdentifier"
    let kRowHeight = "keyRowHeight"
    var orderUIDetails = [[String: Any]]()
    
    var paymentOptionRequest: DataRequest? = nil
    var cartDetailsRequest: DataRequest? = nil
    //
    
    var isTakeawaySelected = "false"
    
    private var DictRestaurant = NSMutableDictionary()
    private var DictPrice = NSMutableDictionary()
    private var RestaurantRequest = "Any Restaurant Request?".localized
    private var CouponCode = ""
    
    @IBOutlet var viewRestaurant: UIView!
    @IBOutlet weak var imgRestaurant: UIImageView!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblRestaurantArea: UILabel!
    @IBOutlet weak var lblDeliveryTIme: UILabel!
    @IBOutlet weak var btnAddMore: UILabel!
    @IBOutlet weak var addMoreView: UIView!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var imgWC: NSLayoutConstraint!
    
    @IBOutlet weak var constraintTblOptionHeight: NSLayoutConstraint!
    @IBOutlet var viewOptions: UIView!
    @IBOutlet weak var lblOptions: UILabel!
    @IBOutlet weak var tblOptions: UITableView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewEmptyCart: UIView!
    @IBOutlet weak var viewEmptyAnimation: UIView!
    private var popup : PopupView!
    private var SelectedPaymentType = "1"
    private var SelectedOrderType = "0"
    @IBOutlet var viewPopDelivery: UIView!

    var RestaurantPickedTittle = ""
    var RestaurantPickedAddress = ""
    var DeliveryTime = ""
    var ServiceType = ""
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        if isPushed
        {
            self.addNavigation()
            self.AddRightBarButton()
        }
        else {
             self.AddRightBarButton()
        }
        
        self.FetchCartList()
        self.FetchPaymentOptions()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.cartDetailsRequest?.cancel()
        self.paymentOptionRequest?.cancel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GlobalMethod.applyShadowCardProjectView(imgRestaurant, andCorner: 5)
        self.alertListView = (Bundle.main.loadNibNamed("AlertListView",
                                                       owner: self,
                                                       options: nil)![0] as! AlertListView)
        
        self.deliveryAlertListView = (Bundle.main.loadNibNamed("DeliveryListViewAlert",
                                                               owner: self,
                                                               options: nil)![0] as! DeliveryListViewAlert)
        
        // Setup Order details in TableView
        self.setupOrderDetailsForTableView()

        tblView.tableHeaderView = viewRestaurant
        tblView.registerNib(CartItemTableViewCell.self)
        tblOptions.registerNib(HeaderTableViewCell.self)
        self.tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        //numberFormatter.numberStyle = .currency
        self.numberFormatter.minimumFractionDigits = 1
        self.tblView.tableFooterView = UIView()
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
        
        
        let animationView = AnimationView(name: "EmptyBottles")
        animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .loop

        viewEmptyAnimation.addSubview(animationView)
    }
        
    func CustomisedNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "blackMenui.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionSendToADDRESS(_:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        button.tintColor = UIColor.darkGray
        let barButton = UIBarButtonItem(customView: button)
        
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = ADDRESS.capitalized
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.darkGray
        viewHeader.addSubview(lblHeader)
        
        let buttonPlace = UIButton.init(type: .custom)
        buttonPlace.setImage(UIImage(named: "imgBlack.png"), for: UIControl.State.normal)
        buttonPlace.addTarget(self, action: #selector(self.ActionSendToPLACE(_:)), for: UIControl.Event.touchUpInside)
        buttonPlace.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonPlace.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        buttonPlace.tintColor = UIColor.darkGray
        let barButtonPlace = UIBarButtonItem(customView: buttonPlace)
        self.navigationItem.leftBarButtonItems = [barButton, barButtonPlace, UIBarButtonItem (customView: viewHeader)]
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ActionSendToPLACE(_:)))
        viewHeader.addGestureRecognizer(tapGesture1)
        viewHeader.isUserInteractionEnabled = true
        
        let buttonNotification = UIButton(type: .custom)
        let image = UIImage(named: "Notification")?.withRenderingMode(.alwaysTemplate)
        buttonNotification.setImage(image, for: .normal)
        buttonNotification.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        buttonNotification.imageEdgeInsets = UIEdgeInsets (top: 5, left: 5, bottom: 5, right: 5)
        buttonNotification.addTarget(self, action: #selector(self.ActionNotification(_:)), for: UIControl.Event.touchUpInside)
        buttonNotification.tintColor = UIColor.darkGray
        
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem (customView: buttonNotification)]
    }
    
    @IBAction func ActionSendToADDRESS(_ sender: Any) {
        
        
        if let View = self.storyboard?.instantiateViewController(identifier: "MenuVC") as? MenuVC{
            self.navigationController?.pushViewController(View, animated: true)
        }
    }
    
    @IBAction func ActionNotification(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "NotificationViewController") as? NotificationViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func ActionSendToPLACE(_ sender: Any) {
        
        GlobalsVariable.sharedInstance.isComeFromMenuAddress = "true"
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                View.FromDashboard = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                View.FromDashboard = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    func AddLeftBarButton(){
        
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
    }
    
   
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func AddRightBarButton(){
     
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white

        let button = UIButton.init(type: .custom)
        button.setTitle("Delivery >".localized, for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.cornerRadius = 5
        button.titleLabel?.font = FONT_NORAML_CUSTOM(xx: 15)
       // button.layer.borderColor = UIColor.darkGray.cgColor
        button.addTarget(self, action: #selector(self.addTapped), for: .touchUpInside)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)


    }
    @objc func addTapped(sender: AnyObject) {
        
        if ServiceType == "both"
        {
            DispatchQueue.main.async {
                self.deliveryAlertListView.set(titleText: "Select Delivery Option".localized, dataSet: ["Delivery", "Takeaway"], targetController: self)
                self.deliveryAlertListView.showPopUpView()
                
            }
         }
     }
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Cart".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    func setupOrderDetailsForTableView() {
        self.orderUIDetails.removeAll()
        self.orderUIDetails.append([kCellIdentifier:"CellNote", kRowHeight:75])
        self.orderUIDetails.append([kCellIdentifier:"CellTip", kRowHeight:53])
        self.orderUIDetails.append([kCellIdentifier:"CellCoupon", kRowHeight:60])
        self.orderUIDetails.append([kCellIdentifier:"CellBillDetails", kRowHeight:169])
        if self.SelectedOrderType == "0" {
            self.orderUIDetails.append([kCellIdentifier:"CellDeliveryAddress", kRowHeight:200])
        }
    }
    
  
    

    
    @IBAction func ActionDelivery(_ sender: Any) {
        DispatchQueue.main.async {
            self.deliveryAlertListView.set(titleText: "Select Delivery Option".localized, dataSet: ["Delivery", "Takeaway"], targetController: self)
            self.deliveryAlertListView.showPopUpView()
            /*self.viewPopDelivery.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 170)
            self.viewPopDelivery.roundCorners([.topLeft, .topRight], radius: 15)
            self.viewPopDelivery.layer.masksToBounds = true

            self.viewPopDelivery.setNeedsLayout()

            let layout = PopupView.Layout(horizontal: PopupView.HorizontalLayout.center, vertical: PopupView.VerticalLayout.bottom)

            self.popup = PopupView(contentView: self.viewPopDelivery, showType: PopupView.ShowType.slideInFromBottom, dismissType: PopupView.DismissType.slideOutToBottom, maskType: PopupView.MaskType.dimmed, shouldDismissOnBackgroundTouch: true, shouldDismissOnContentTouch: false)
            self.popup.show(with: layout)*/
        }
    }
    
   
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            if newText.isEmpty{
                RestaurantRequest = "Any Restaurant Request?".localized
            }
            else{
                RestaurantRequest = newText
            }
            return true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if RestaurantRequest == "Any Restaurant Request?".localized {
            textView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Any Restaurant Request?".localized
        }
    }
        
    @IBAction func btnAddMoreAction(_ sender: UIButton)
    {
        if let View = self.storyboard?.instantiateViewController(identifier: "FoodDetailsViewController") as? FoodDetailsViewController{
            View.DictRestaurant = DictRestaurant
            self.navigationController?.pushViewController(View, animated: true)
        }
    }
    
    func SetData(){

        self.addMoreView.layer.cornerRadius = 5
        self.addMoreView.layer.borderWidth = 0.5
        self.addMoreView.layer.borderColor = UIColor.darkGray.cgColor
        
        print("RESTAURANT DETAIL ==>>",self.DictRestaurant)
        
        if let str = self.DictRestaurant.value(forKey: "logo") as? String{
            //var imgUrl:String = str + "&w=\(70)"
            //imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            imgRestaurant.sd_setImage(with: URL(string: str), placeholderImage: nil)
            //imgRestaurant.contentMode = .scaleAspectFill
        }
        
        if let str = self.DictRestaurant.value(forKey: "name") as? String{
            self.lblRestaurantName.text = str
            self.RestaurantPickedTittle = str
        }
        
        if let str = self.DictRestaurant.value(forKey: "delivery_time") as? String{
            DeliveryTime = str
        }
        
        if let str = self.DictRestaurant.value(forKey: "address") as? String{
            self.lblRestaurantArea.text = str
            self.RestaurantPickedAddress = str

        }
        
        if let type = self.DictRestaurant.value(forKey: "type") as? String{

            ServiceType = type
            print("SERVICE TYPE ==>>",type)
        }
        
        if let str = self.DictRestaurant.value(forKey: "latitude") as? String{
            resLat = str
        }
        
        if let str = self.DictRestaurant.value(forKey: "longitude") as? String{
            resLong = str
        }
        self.tblView.reloadData()
    }
    
    
    func ActionDeliveryLocation() {
        
        if isTakeawaySelected == "true"
        {
        
        }
        else
        {
            GlobalsVariable.sharedInstance.isComeFromMenuAddress = "true"
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                    View.FromCart = true
                    View.DeliveryLocation = { (Dict : NSDictionary)in
                        NSUSERDEFAULT.set(Dict, forKey: DELIVERY_LOCATION)
                    }
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                    View.FromCart = true
                    View.DeliveryLocation = { (Dict : NSDictionary)in
                        NSUSERDEFAULT.set(Dict, forKey: DELIVERY_LOCATION)
                    }
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
        
        
    }
    
    func ActionCoupon() {
        if self.CouponCode.isEmpty {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "PromoCodeViewController") as? PromoCodeViewController{
                    View.Coupon = { (Promo : String)in
                        print(Promo)
                        self.CouponCode = Promo
                        self.ApplyCoupon(Coupon: Promo)
                    }
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "PromoCodeViewController") as? PromoCodeViewController{
                    View.Coupon = { (Promo : String)in
                        print(Promo)
                        self.CouponCode = Promo
                        self.ApplyCoupon(Coupon: Promo)
                    }
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        } else {
            self.CouponCode = ""
            self.FetchCartList()
        }
    }
    
    func PlaceOrder() {
        switch self.selectedPaymentType {
        case .COD, .Online:
            var orderParams = [String: Any]()
            if RestaurantRequest != "Any Restaurant Request?".localized && !RestaurantRequest.isEmpty{
                orderParams["delivery_note"] = RestaurantRequest
            }
            orderParams["tip"] = self.numberFormatter.string(from: self.tipAmount)!
            orderParams["cart_session"] = UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!
            orderParams["payment_method"] = self.SelectedPaymentType
            orderParams["coupon_code"] = self.CouponCode
            orderParams["order_type"] = self.SelectedOrderType
            orderParams["user_id"] = UserDefaults.standard.string(forKey: SESSION_USER_ID)!
            orderParams["apiId"] = API_ID
            orderParams["from_app"] = FROMAPP
            orderParams["language"] = GlobalMethod.selectedLanguageForUser()
            if let DictLocation = NSUSERDEFAULT.value(forKey: DELIVERY_LOCATION) as? NSDictionary {
                if let str = DictLocation.value(forKey: "address_id") as? String{
                    orderParams["address_id"] = str
                }
            }
            self.PlaceOrder(with: orderParams)
        default:
            print("\n Unable to define Payment Option")
        }
        
       
    }
    
    @IBAction func ActionPay(_ sender: Any) {

        if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
        {
            if self.SelectedOrderType == "0" {
                if let _ = NSUSERDEFAULT.value(forKey: DELIVERY_LOCATION) as? NSDictionary {
                    
                    
                    let mobileNo = NSUSERDEFAULT.value(forKey: SAVED_MOBILE_NO) as? String
                    
                    if mobileNo == ""
                    {
                        print("MOBILE NO ARE EMPTY")
                        
                        
                        if #available(iOS 13.0, *) {
                            if let View = self.storyboard?.instantiateViewController(identifier: "EmptyMobileVC") as? EmptyMobileVC{
                                
                                self.navigationController?.pushViewController(View, animated: true)
                            }
                        } else {
                            if let View = self.storyboard?.instantiateViewController(withIdentifier: "EmptyMobileVC") as? EmptyMobileVC{
                                self.navigationController?.pushViewController(View, animated: true)
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.alertListView.set(titleText: "Select Payment Option".localized, dataSet: self.paymentOptionList, targetController: self)
                            self.alertListView.showPopUpView()
                       }
                    }
                    
                } else {
                    self.ActionDeliveryLocation()
                    return
                }
            } else {
                DispatchQueue.main.async {
                    self.alertListView.set(titleText: "Select Payment Option".localized, dataSet: self.paymentOptionList, targetController: self)
                    self.alertListView.showPopUpView()
                }
            }
        }
        else
        {
            GlobalsVariable.sharedInstance.isComeFromCart = "true"

            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as? LoginViewController{
                    
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
    
    @IBAction func ActionDeliveryOrder(_ sender: Any) {
        self.SelectedOrderType = "0"
        self.popup.dismiss(animated: true)
    }
    
    
    @IBAction func ActionTakeAwayOrder(_ sender: Any) {
        self.SelectedOrderType = "1"
        self.popup.dismiss(animated: true)
    }
    
    func showDropIn(clientToken: String, orderId: String) {
        if let apiClient = BTAPIClient(authorization: clientToken) {
            self.dataCollector = BTDataCollector(apiClient: apiClient)
            self.dataCollector.collectDeviceData { (deviceData) in
                
                // Set the theme before initializing Drop-in
                BTUIKAppearance.sharedInstance().colorScheme = .dynamic
                
                let request = BTDropInRequest()
                request.applePayDisabled = true
                request.paypalDisabled = false
                request.threeDSecureVerification = true
                request.vaultManager = true
                
                let threeDSecureRequest = BTThreeDSecureRequest()
                guard let strTotalAmount = self.DictPrice.value(forKey: "grand_total") as? String else { return }
                guard let grandTotal = Double(strTotalAmount) else { return }
                let totalWithTip = self.tipAmount.doubleValue + grandTotal
                
                
                //let totalWithTip = self.tipAmount.intValue + Int(strTotalAmount)!
//                threeDSecureRequest.amount = NSDecimalNumber(string: strTotalAmount)
                //threeDSecureRequest.amount = NSDecimalNumber(integerLiteral: totalWithTip)
                threeDSecureRequest.amount = NSDecimalNumber(floatLiteral: totalWithTip)
                threeDSecureRequest.versionRequested = .version2
                
                request.threeDSecureRequest = threeDSecureRequest
                
                let dropIn = BTDropInController(authorization: clientToken, request: request) { (dropInController, dropInResult, error) in
                    if error != nil {
                        print("\nDropIn Error:")
                        print("\n \(String(describing: error?.localizedDescription))")
                        dropInController.dismiss(animated: true, completion: nil)
                        //GlobalMethod.ShowToast(Text: String(describing: error?.localizedDescription), View: self.view)
                        GlobalMethod.ShowPopAlert(Message: String(describing: error?.localizedDescription), View: self)
                    } else if dropInResult?.isCancelled == true {
                        print("\nDropIn Cancelled")
                        dropInController.dismiss(animated: true, completion: nil)
                    } else if let result = dropInResult {
                        print("\nDropIn Result:\t\(result.paymentDescription)")
                        print("DropIn Payment Method Description:\t\(String(describing: result.paymentMethod?.localizedDescription))")
                        print("DropIn Nonce:\t\(String(describing: result.paymentMethod?.nonce))")
                        dropInController.dismiss(animated: true, completion: nil)
                        self.initiateTransaction(amount: String(totalWithTip),
                                                 paymentNonce: result.paymentMethod!.nonce,
                                                 deviceData: deviceData,
                                                 orderId: orderId)
                    }
                    
                }
                
                GlobalMethod.StopProgress(self.view)
                
                if dropIn != nil {
                    self.present(dropIn!, animated: true, completion: nil)
                }
            }
        } else {
            GlobalMethod.StopProgress(self.view)
            GlobalMethod.ShowPopAlert(Message: "Unable to initialize BTApiClient", View: self)
        }
    
    }
    
    func moveToPlaceOrderVC(with data: NSDictionary) {
        DispatchQueue.main.async {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "PlaceOrderViewController") as? PlaceOrderViewController{
                    View.DictOrder = data
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderViewController") as? PlaceOrderViewController{
                    View.DictOrder = data
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
    
    func showAlertForTip() {
        let tipAlertVc = UIAlertController(title: "Give Tip".localized, message: "", preferredStyle: .alert)
        tipAlertVc.addTextField { (textField) in
            textField.placeholder = "Plase enter Tip amount".localized
            textField.keyboardType = .decimalPad
//            textField.text = self.tipAmount.doubleValue > 0 ? self.numberFormatter.string(from: self.tipAmount) : ""
        }
        let okAction = UIAlertAction(title: "ADD".localized, style: .default, handler: { action in
            guard let textField = tipAlertVc.textFields?[0] else { return }
            if let tipAmount = textField.text, !tipAmount.isEmpty {
                let amount = NSDecimalNumber(string: tipAmount)
                if amount.doubleValue >= 0 {
                    if let validAmount = self.numberFormatter.string(from: amount) {
                        print("\nTip Amount:\t\(validAmount)\n")
                        self.tipAmount = self.numberFormatter.number(from: validAmount)!
                        self.tblView.reloadRows(at: [IndexPath(row: 3, section: 1)], with: .automatic)
                    }
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .cancel, handler: nil)
        tipAlertVc.addAction(okAction)
        tipAlertVc.addAction(cancelAction)
        
        self.present(tipAlertVc, animated: true, completion: nil)
    }
    
    @objc func clearTipButtonTapped(sender: Any) {
        print("\nTip Clear\n")
        self.tipAmount = 0
        self.tblView.reloadRows(at: [IndexPath(row: 3, section: 1)], with: .automatic)
    }
    
}

// MARK: - AlertListView
extension CartViewController: AlertListViewDeleagate {
    func didSelectItem(item: PaymentMethodModel) {
        guard let paymentType = PaymentType(rawValue: item.name) else {
            self.alertListView.hidePopUp()
            return
        }
        self.selectedPaymentType = paymentType
        self.SelectedPaymentType = item.id
        self.alertListView.hidePopUp()
        self.PlaceOrder()
    }
}

extension CartViewController: DeliveryListAlertDelegate {
    func didSelectDeliveryOption(option: String) {
        self.SelectedOrderType = option
        self.deliveryAlertListView.hidePopUp()

        guard let item = self.navigationItem.rightBarButtonItems?.first else { return }

        let button = item.customView as? UIButton
        button?.setTitle(option == "0" ? "Delivery".localized : "Takeaway".localized, for: .normal)

        if option == "0"
        {
            isTakeawaySelected = "false"
            self.tblView.reloadData()

        }
        else
        {
            
            isTakeawaySelected = "true"
            self.tblView.reloadData()
        }
        
        /*DispatchQueue.main.async {
            self.setupOrderDetailsForTableView()
            if self.orderUIDetails.count != self.tblView.numberOfRows(inSection: 1) {
                if option == "0" {
                    self.tblView.insertRows(at: [IndexPath(row: self.orderUIDetails.count - 1, section: 1)], with: .automatic)
                } else {
                    self.tblView.deleteRows(at: [IndexPath(row: self.orderUIDetails.count, section: 1)], with: .automatic)
                }
            }
        }*/
    }
}

// MARK: - TableView

extension CartViewController: UITableViewDelegate,SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        if skeletonView == self.tblView {
            if indexPath.section == 0 {
                return String(describing: CartItemTableViewCell.self)
            }
            else
            {
                return self.orderUIDetails[indexPath.row][kCellIdentifier] as! String
            }
        } else {
            return ""
        }
    }
    
    func numSections(in collectionSkeletonView: UITableView) -> Int {
        if collectionSkeletonView == tblOptions{ return 1 }
        return 2
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if skeletonView == self.tblOptions {
            return self.paymentOptionList.count
        } else {
            if section == 0 {
                return 2
            } else {
                return self.orderUIDetails.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblOptions{ return 1 }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblOptions {
            print("PAYMENT OPTION COUNT",self.paymentOptionList.count)
            return self.paymentOptionList.count
        } else {
            if section == 0 {
                print("CART ARAY COUNT",self.ArrayCartItems.count)
                return self.ArrayCartItems.count
            } else {
                print("OORDER DETAIL COUNT",self.orderUIDetails.count)

                return self.orderUIDetails.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tblOptions {
            let cell = tableView.dequeue(HeaderTableViewCell.self)
            cell.lbl.text = paymentOptionList[indexPath.row].name
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            if indexPath.section == 0
            {
                let cell = tableView.dequeue(CartItemTableViewCell.self)
               /* if UserDefaults.standard.string(forKey: COME_FROM_REPEAT_ORDER) == "true"
                {
                    cell.isUserInteractionEnabled = false
                }
                else
                {
                    cell.isUserInteractionEnabled = true
                }*/
                if cell.isSkeletonActive {
                    cell.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                }
                cell.delegate = self
                cell.index = indexPath
                if let Dict = ArrayCartItems.object(at: indexPath.row) as? NSDictionary{
                    cell.configure(with: Dict)
                }
                cell.lblQtywithPrice.isHidden = true
                cell.selectionStyle = .none
                return cell
            } else {
                
                switch indexPath.row {
                case 0:
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "CellNote")
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: self.orderUIDetails[indexPath.row][kCellIdentifier] as! String)
                    //
                    
                    
                    
                    if cell!.isSkeletonActive {
                        cell!.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    }
                    let img = cell?.viewWithTag(1) as! UIImageView
                    let txt = cell?.viewWithTag(2) as! UITextView
                    txt.text = RestaurantRequest

                    img.setImageColor(color: UIColor.black)
                    cell?.selectionStyle = .none
                   /* if UserDefaults.standard.string(forKey: COME_FROM_REPEAT_ORDER) == "true"
                    {
                        cell!.isUserInteractionEnabled = false
                        txt.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell!.isUserInteractionEnabled = true
                        txt.isUserInteractionEnabled = true
                    }*/
                    
                    return cell!
                case 1:
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "CellTip")
                    let cell = tableView.dequeueReusableCell(withIdentifier: self.orderUIDetails[indexPath.row][kCellIdentifier] as! String)
                    
                    if cell!.isSkeletonActive {
                        cell!.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    }
                    (cell?.viewWithTag(1) as? UIImageView)?.setImageColor(color: .black)
                    cell?.selectionStyle = .none
                    
                    
                    /*if UserDefaults.standard.string(forKey: COME_FROM_REPEAT_ORDER) == "true"
                    {
                        cell!.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell!.isUserInteractionEnabled = true
                    }
                    */
                    return cell!
                case 2:
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "CellCoupon")
                    let cell = tableView.dequeueReusableCell(withIdentifier: self.orderUIDetails[indexPath.row][kCellIdentifier] as! String)
                   
                    if cell!.isSkeletonActive {
                        cell!.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    }
                    let couponLabel = cell?.viewWithTag(2) as! UILabel
                    let cellImage = cell?.viewWithTag(3) as! UIImageView
                    if self.CouponCode.isEmpty {
                        couponLabel.text = "Apply Coupon".localized
                        cellImage.image = UIImage(named: "Right")
                    } else {
                        couponLabel.text = "Promo code applied".localized
                        cellImage.image = UIImage(named: "cross")
                    }
                   /* if UserDefaults.standard.string(forKey: COME_FROM_REPEAT_ORDER) == "true"
                    {
                        cell!.isUserInteractionEnabled = false
                        couponLabel.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell!.isUserInteractionEnabled = true
                        couponLabel.isUserInteractionEnabled = true
                    }*/
                    
                    cell?.selectionStyle = .none
                    return cell!
                case 3:
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "CellBillDetails")
                    let cell = tableView.dequeueReusableCell(withIdentifier: self.orderUIDetails[indexPath.row][kCellIdentifier] as! String)
                   
                    if cell!.isSkeletonActive {
                        cell!.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    }
                    let lblItemTotal = cell?.viewWithTag(1) as! UILabel
                    let lblAddonTotal = cell?.viewWithTag(2) as! UILabel
                    let lblTaxes = cell?.viewWithTag(3) as! UILabel
                    let lblDeliveryCharge = cell?.viewWithTag(4) as! UILabel
                    let lblDiscount = cell?.viewWithTag(5) as! UILabel
                    let lblTotal = cell?.viewWithTag(6) as! UILabel
                    let clearTipButton = cell?.viewWithTag(7) as! UIButton
                    let tipAmountLabel = cell?.viewWithTag(8) as! UILabel
                    let tipParentView = cell?.viewWithTag(9) as! UIStackView
 
                    clearTipButton.setImage(UIImage(named: "cross")!, for: .normal)
                    if self.tipAmount.doubleValue > 0 {
                        tipParentView.isHidden = false
                        clearTipButton.addTarget(self,
                                                 action: #selector(clearTipButtonTapped(sender:)),
                                                 for: UIControl.Event.touchUpInside)
                    } else {
                        tipParentView.isHidden = true
                    }
                    tipAmountLabel.text = CURRANCY + " " + self.numberFormatter.string(from: self.tipAmount)!
                    
                    if let str = self.DictPrice.value(forKey: "total_price") as? String{
                        lblItemTotal.text = CURRANCY + " " + str
                    }
                    if let str = self.DictPrice.value(forKey: "total_addons") as? String{
                        lblAddonTotal.text = CURRANCY + " " + str
                    }
                    if let str = self.DictPrice.value(forKey: "taxes") as? String{
                        lblTaxes.text = CURRANCY + " " + str
                    }
                    if let str = self.DictPrice.value(forKey: "delivery_charges") as? String, str != "0"{
                        lblDeliveryCharge.text = CURRANCY + " " + str
                        lblDeliveryCharge.textColor = UIColor.black
                    }
                    else{
                        lblDeliveryCharge.text = "Free".localized
                        lblDeliveryCharge.textColor = COLOR_GREEN
                    }
                    if let str = self.DictPrice.value(forKey: "discount_amount") as? String{
                        lblDiscount.text = CURRANCY + " " + str
                    }
                    if let str = self.DictPrice.value(forKey: "grand_total") as? String{
                        if let grandTotal = Double(str) {
                            let totalWithTip = self.tipAmount.doubleValue + grandTotal
                            lblTotal.text = CURRANCY + " " + self.numberFormatter.string(from: NSNumber(value: totalWithTip))!
                        }
                    }
                    //cell?.layoutSubviews()
                   /* if UserDefaults.standard.string(forKey: COME_FROM_REPEAT_ORDER) == "true"
                    {
                        cell!.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell!.isUserInteractionEnabled = true
                    }*/
                    cell?.selectionStyle = .none
                    return cell!
                case 4:
                    //let cell = tableView.dequeueReusableCell(withIdentifier: "CellDeliveryAddress")
                    let cell = tableView.dequeueReusableCell(withIdentifier: self.orderUIDetails[indexPath.row][kCellIdentifier] as! String)
                   
                    if cell!.isSkeletonActive {
                        cell!.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    }
                    let img = cell?.viewWithTag(1) as! UIImageView
                    let lblTitle = cell?.viewWithTag(2) as! UILabel
                    let lblDesc = cell?.viewWithTag(3) as! UILabel
                    let EditImage = cell?.viewWithTag(4) as! UIImageView

                    let directionButton = cell?.viewWithTag(5) as! UIButton

                    if isTakeawaySelected == "true"
                    {
                        directionButton.isHidden = false
                        EditImage.isHidden = true
                        directionButton.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)

                        lblTitle.text = "Takeaway address"
                        lblDesc.numberOfLines = 0
                        lblDesc.sizeToFit()
                        lblDesc.text = self.RestaurantPickedTittle + "\n" + self.RestaurantPickedAddress
                        img.isHidden = true
                    }
                    else
                    {
                        directionButton.isHidden = true
                        EditImage.isHidden = false
                        img.image = UIImage.init(named: "HomeUnSelected.png")
                        img.setImageColor(color: COLOR_ORANGE!)
                        lblTitle.text = "Select Delivery Address".localized
                        lblDesc.text = ""
                        if let DictLocation = NSUSERDEFAULT.value(forKey: DELIVERY_LOCATION) as? NSDictionary
                        {
                            img.isHidden = false
                            if let str = DictLocation.value(forKey: "address_type") as? String{
                                lblTitle.text = str.capitalized
                                if str == "home"{
                                    img.image = #imageLiteral(resourceName: "HomeUnSelected")
                                }
                                else if str == "work"{
                                    img.image = #imageLiteral(resourceName: "OfficeUnSelected")
                                }
                                else{
                                    img.image = #imageLiteral(resourceName: "OtherUnSelected")
                                }
                            }
                            
                            if let strDelivery = DictLocation.value(forKey: "area") as? String{
                                if let strCurrent = DictLocation.value(forKey: "street") as? String{
                                    lblDesc.text = strDelivery + "\n" + strCurrent
                                }
                            }
                        }
                        
                    }
                    
                   /* if UserDefaults.standard.string(forKey: COME_FROM_REPEAT_ORDER) == "true"
                    {
                        cell!.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell!.isUserInteractionEnabled = true
                    }*/
                    cell?.selectionStyle = .none
                    return cell!    
                default:
                    return UITableViewCell()
                }
            }
        }
        
       
        
    }

    @objc func connected(sender: UIButton)
    {
        if isTakeawaySelected == "true"
        {
            print("DIRECTION CLICK")
            if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
                 UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(resLat),\(resLong)&zoom=14&views=traffic&q=\(resLat),\(resLong)")!, options: [:], completionHandler: nil)
             } else {
                 UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(resLat),\(resLong)&zoom=14&views=traffic&q=\(resLat),\(resLong)")!, options: [:], completionHandler: nil)
             }
        }
        else
        {
            self.ActionDeliveryLocation()
        }
    }
    private func tableView(tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblOptions { return 70 }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 130
        default:
    
            return CGFloat(integerLiteral: self.orderUIDetails[indexPath.row][kRowHeight] as! Int)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        if tableView == tblOptions {
            guard let paymentType = PaymentType(rawValue: self.paymentOptionList[indexPath.row].name) else { return }
            self.selectedPaymentType = paymentType
            self.SelectedPaymentType = self.paymentOptionList[indexPath.row].id
            self.view.dismissPresentingPopup()
            self.PlaceOrder()
            //return
        } else {
            if indexPath.section == 0 {
                if let Dict = ArrayCartItems.object(at: indexPath.row) as? NSDictionary{
                    print(Dict)
                }
            }
            
            if indexPath.section == 1 {
              
                switch indexPath.row {
                case 1:
                    self.showAlertForTip()
                case 2:
                    self.ActionCoupon()
                case 4:
                    if isTakeawaySelected == "true"
                    {
                        print("DIRECTION CLICK")
                        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
                             UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(resLat),\(resLong)&zoom=14&views=traffic&q=\(resLat),\(resLong)")!, options: [:], completionHandler: nil)
                         } else {
                             UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(resLat),\(resLong)&zoom=14&views=traffic&q=\(resLat),\(resLong)")!, options: [:], completionHandler: nil)
                         }
                    }
                    else
                    {
                        self.ActionDeliveryLocation()
                    }
                    
                default:
                    print("\nCartVC Section:\(indexPath.section) Row:\(indexPath.row) Selected\n")
                
                }
                //
            }
        }
    }
}

// MARK: - ActionDelegate

extension CartViewController: QuantityActionDelegate{
    func PlusButtonPressed(_ index: IndexPath) {
        print("Plus")
        if let Dict = ArrayCartItems.object(at: index.row) as? NSMutableDictionary{
            print(Dict)
            if var str = Dict.value(forKey: "quantity") as? String{
                str = String (describing: Int(str)! + 1)
                Dict.setValue(str, forKey: "quantity")
                if let strID = Dict.value(forKey: "cart_id") as? String{
                    self.UpdateCart(cart_id: strID, Qty: str)
                }
            }
        }
        self.tblView.reloadRows(at: [index], with: .automatic)
    }
    
    func MinusButtonPressed(_ index: IndexPath) {
        print("Minus")
        if let Dict = ArrayCartItems.object(at: index.row) as? NSMutableDictionary{
            print(Dict)
            if var str = Dict.value(forKey: "quantity") as? String{
                if str == "1"{
                    if let strID = Dict.value(forKey: "cart_id") as? String{
                        self.DeleteFromCart(cart_id: strID)
                    }
                }
                else{
                    str = String (describing: Int(str)! - 1)
                    Dict.setValue(str, forKey: "quantity")
                    if let strID = Dict.value(forKey: "cart_id") as? String{
                        self.UpdateCart(cart_id: strID, Qty: str)
                    }
                }
            }
        }
        self.tblView.reloadRows(at: [index], with: .automatic)
    }
}

//MARK:- --- [API CALLING] ---

extension CartViewController{
    func FetchCartList(){
        if !GlobalMethod.internet(){
            
        }else{
          
            self.view.showAnimatedGradientSkeleton()
            self.viewRestaurant.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                              "apiId":API_ID,
                                              "language": GlobalMethod.selectedLanguageForUser(),
                                              "coupon_code": self.CouponCode,
                                              "restaurant_id": SELECTED_RESTAURANT_ID,
                                              "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!
            ]
            print("CART LIST PARAMETER ==>>",parameter)
            ServManager.viewController = self
            self.cartDetailsRequest = ServManager.callPostAPI(constant.CART_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    print("CART LIST REPONSE ==>>",dataDict)

                    self.viewEmptyCart.isHidden = true
                    self.ArrayCartItems = NSMutableArray()
                    self.DictRestaurant = NSMutableDictionary()
                    self.DictPrice = NSMutableDictionary()
                    
                    if let Data = CartListModel.init(dictionary: dataDict){
                        
                        if let DictCart = Data.cart {
                            if let dictTemp = (DictCart.mutableCopy()) as? NSMutableDictionary{
                                self.DictPrice = dictTemp
                            }
                        }
                        
                        if let DictRestaurant = Data.restaurant {
                            if let dictTemp = (DictRestaurant.mutableCopy()) as? NSMutableDictionary{
                                self.DictRestaurant = dictTemp
                            }
                        }
                        
                        if let ArrItems = Data.cart!.value(forKey: "cart_items") as? NSArray{
                            for Dict in ArrItems{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayCartItems.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                            self.viewRestaurant.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                            self.SetData()
                        }
                        
                        if self.ArrayCartItems.count == 0 {
                            self.viewEmptyCart.isHidden = false
                            self.view.bringSubviewToFront(self.viewEmptyCart)
                        }
                    }
                }else{
                    self.view.bringSubviewToFront(self.viewEmptyCart)
                    self.viewEmptyCart.isHidden = false
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    if #available(iOS 13.0, *) {
                        GlobalMethod.StopProgress((SCENE?.window)!)
                    } else {
                        GlobalMethod.StopProgress((APP?.window)!)
                    }
                }
            }
        }
    }
    
    func UpdateCart(cart_id : String, Qty : String){
        if !GlobalMethod.internet(){
            
        }else{
            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,"quantity":Qty,"apiId":API_ID, "cart_id":cart_id,"cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!, "language": GlobalMethod.selectedLanguageForUser()]
           // print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.UPDATE_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    GlobalMethod.StopProgress(self.view)
                    //print(dataDict)
                    self.viewEmptyCart.isHidden = true
                    self.ArrayCartItems = NSMutableArray()
                    self.DictRestaurant = NSMutableDictionary()
                    self.DictPrice = NSMutableDictionary()
                    
                    if let Data = CartListModel.init(dictionary: dataDict){
//                        print(Data.cart)
                        
                        if let DictCart = Data.cart {
                            if let dictTemp = (DictCart.mutableCopy()) as? NSMutableDictionary{
                                self.DictPrice = dictTemp
                            }
                        }
                        
                        if let DictRestaurant = Data.restaurant {
                            if let dictTemp = (DictRestaurant.mutableCopy()) as? NSMutableDictionary{
                                self.DictRestaurant = dictTemp
                            }
                        }
                        
                        if let ArrItems = Data.cart!.value(forKey: "cart_items") as? NSArray{
                            for Dict in ArrItems{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayCartItems.add(dictTemp)
                                    }
                                }
                            }
                        }
                        self.SetData()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            if #available(iOS 13.0, *) {
                                GlobalMethod.StopProgress((SCENE?.window)!)
                            } else {
                                GlobalMethod.StopProgress((APP?.window)!)
                            }
                        }
                        if self.ArrayCartItems.count == 0 {
                            self.viewEmptyCart.isHidden = false
                            self.view.bringSubviewToFront(self.viewEmptyCart)
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func FetchPaymentOptions(){
        if !GlobalMethod.internet(){
            
        }else{
//            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,"apiId":API_ID,"user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!, "language": GlobalMethod.selectedLanguageForUser()]
            ServManager.viewController = self
            self.paymentOptionRequest = ServManager.callPostAPI(constant.PAYMENT_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    GlobalMethod.StopProgress(self.view)
                    
                    if let Data = PaymentListModel.init(dictionary: dataDict){
                        // Manthan 14-9-2020
                        // Using Model for Payment Option
                        /*for Dict in Data.payments!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayPayment.add(dictTemp)
                                }
                            }
                        }
//                        self.tblView.reloadData()
                        */
                        let methods = Data.payments!.compactMap({ $0 as? Dictionary<String, Any> })
                        self.paymentOptionList = methods.compactMap({ PaymentMethodModel(data: $0) })
                        self.tblView.reloadData()
                        //
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func DeleteFromCart(cart_id : String){
        if !GlobalMethod.internet(){
            
        }else{
            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,"apiId":API_ID, "cart_id":cart_id]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.DELETE_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    self.FetchCartList()
                    GlobalMethod.StopProgress(self.view)
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func ApplyCoupon(Coupon : String){
        if !GlobalMethod.internet(){
            
        }else{
            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,"apiId":API_ID,
                                              "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                              "coupon_code":Coupon,
                                              "language": GlobalMethod.selectedLanguageForUser()]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.APPY_COUPON, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    GlobalMethod.StopProgress(self.view)
                    
                    self.ArrayCartItems = NSMutableArray()
                    self.DictRestaurant = NSMutableDictionary()
                    self.DictPrice = NSMutableDictionary()
                    
                    if let Data = CartListModel.init(dictionary: dataDict){
//                        print(Data.cart)
                        
                        
                        if let DictCart = Data.cart {
                            if let dictTemp = (DictCart.mutableCopy()) as? NSMutableDictionary{
                                self.DictPrice = dictTemp
                            }
                        }
                        
                        if let DictRestaurant = Data.restaurant {
                            if let dictTemp = (DictRestaurant.mutableCopy()) as? NSMutableDictionary{
                                self.DictRestaurant = dictTemp
                            }
                        }
                        
                        if let ArrItems = Data.cart!.value(forKey: "cart_items") as? NSArray{
                            for Dict in ArrItems{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayCartItems.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.SetData()
                        }
                    }
                    
                }else{
                    // Manthan 23-9-2020
                    // Clear `Coupon` code if not applied successfully
                    self.CouponCode = ""
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func PlaceOrder(with orderParams : [String: Any]){
        if !GlobalMethod.internet(){
            
        } else {
            /*if self.selectedPaymentType == .COD {
                if #available(iOS 13.0, *) {
                    GlobalMethod.StartLottieProgress((SCENE?.window)!, Json: "Delivery")
                } else {
                    GlobalMethod.StartLottieProgress((APP?.window)!, Json: "Delivery")
                }
            } else {
                GlobalMethod.StartProgress(self.view)
            }*/
            GlobalMethod.StartProgress(self.view)
            
            print(orderParams)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.PLACE_ORDER,
                                    isLoaderRequired: true,
                                    paramters: orderParams) { (isSuccess, status, msg, dataDict) in
                
                                        // Get Client Token
                if isSuccess{
                    if self.selectedPaymentType == .COD {
                        /*if #available(iOS 13.0, *) {
                            GlobalMethod.StopProgress((SCENE?.window)!)
                        } else {
                            GlobalMethod.StopProgress((APP?.window)!)
                        }*/
                        // Manthan 16-9-2020
                        /*if #available(iOS 13.0, *) {
                            if let View = self.storyboard?.instantiateViewController(identifier: "PlaceOrderViewController") as? PlaceOrderViewController{
                                View.DictOrder = dataDict
                                self.navigationController?.pushViewController(View, animated: true)
                            }
                        } else {
                            if let View = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderViewController") as? PlaceOrderViewController{
                                View.DictOrder = dataDict
                                self.navigationController?.pushViewController(View, animated: true)
                            }
                        }*/
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            GlobalMethod.StopProgress(self.view)
                            self.moveToPlaceOrderVC(with: dataDict)
                        }
                    } else {
                        if let brainTreeToken = dataDict["braintree_token"] as? String {
                            if let orderInt = dataDict["order_id"] as? Int {
                                let orderId = String(orderInt)
                                self.showDropIn(clientToken: brainTreeToken, orderId:orderId)
                            } else {
                                GlobalMethod.StopProgress(self.view)
                                GlobalMethod.ShowPopAlert(Message: "Order id nil", View: self)
                            }
                        } else {
                            GlobalMethod.StopProgress(self.view)
                            GlobalMethod.ShowPopAlert(Message: "BrainTree Token nil", View: self)
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        if self.selectedPaymentType == .COD {
                            GlobalMethod.StopProgress(self.view)
                        } else {
                            GlobalMethod.StopProgress(self.view)
                        }
                        
                    }
                }
            }
        }
    }
    
    func initiateTransaction(amount: String, paymentNonce: String, deviceData: String, orderId: String) {
        if !GlobalMethod.internet(){
            
        } else {
            /*if #available(iOS 13.0, *) {
                GlobalMethod.StartLottieProgress((SCENE?.window)!, Json: "Delivery")
            } else {
                GlobalMethod.StartLottieProgress((APP?.window)!, Json: "Delivery")
            }*/
            GlobalMethod.StartProgress(self.view)
            
            var params = [String: Any]()
            params["amount"] = amount
            params["paymentMethodNonce"] = paymentNonce
            params["deviceData"] = deviceData
            params["from_app"] = FROMAPP
            params["apiId"] = API_ID
            params["language"] = GlobalMethod.selectedLanguageForUser()
            params["order_id"] = orderId
            params["cart_session"] = UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!
            //print("\nParam for Transaction:\t\(params)\n")
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.INITIATE_TRANSACTION,
                                    isLoaderRequired: true,
                                    paramters: params)
            { (isSuccess, status, message, dataDict) in
             //   print("\nTransaction API message:\t\(message)\n")
             //   print("\nTransaction API response:\t\(dataDict)\n")
                
                /*if #available(iOS 13.0, *) {
                    GlobalMethod.StopProgress((SCENE?.window)!)
                } else {
                    GlobalMethod.StopProgress((APP?.window)!)
                }*/
                
                if isSuccess {
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        GlobalMethod.StopProgress(self.view)
                        self.moveToPlaceOrderVC(with: dataDict)
                    }
                } else {
                    GlobalMethod.StopProgress(self.view)
                    GlobalMethod.ShowToast(Text: message, View: self.view)
                }
            }
        }
    }
    
    func fetchClientToken(completion: @escaping (String?) -> Void) {
        completion("sandbox_ktgsqb7c_9bpqx62wnvkftbrx")
        /*let tokenUrl = URL(string: constant.CLIENT_TOKEN)!
        var urlReq = URLRequest(url: tokenUrl)
        urlReq.setValue("text/plain", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: urlReq) { (_data, response, error) in
            if error != nil {
                completion(nil)
            } else {
                if let data = _data {
                    if let clientToken = String(data: data, encoding: .utf8), !clientToken.isEmpty {
                        completion(clientToken)
                    } else {
                        completion(nil)
                    }
                } else {
                    completion(nil)
                }
            }
        }.resume()*/
    }
}

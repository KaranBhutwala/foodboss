//
//  ShowImagesVC.swift
//  FoodBoss
//
//  Created by IOS Development on 08/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Alamofire
import SkeletonView


class ImageCell : UICollectionViewCell
{
    
    @IBOutlet weak var img: UIImageView!
}

class ShowImagesVC: UIViewController {

    var coupons = [NSMutableDictionary]()
     var restaurantDetailsRequest: DataRequest? = nil
     private var ArrayBannerImages = NSMutableArray()
     var ArrayMenuImages = NSMutableArray()

    var DictRestaurant = NSDictionary()
    @IBOutlet weak var lblHeaderTitle: UILabel!
    var RestaurantID = ""
    var RestaurantName = ""
    var comeFrom = ""

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        if comeFrom == "seeFullMenu"
        {
            lblHeaderTitle.text = "Menu \n\(RestaurantName)".localized
            self.collectionView.reloadData()

        }
        else
        {
            lblHeaderTitle.text = "Gallery \n\(RestaurantName)".localized
           FetchDetails()
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)

    }
   
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }

    func FetchCouponList(){
        if !GlobalMethod.internet(){
            
        }else{
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID, "language":GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.COUPON_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                if isSuccess{
                    self.coupons = []
                    if let Data = CouponListModel.init(dictionary: dataDict){
                        for Dict in Data.coupons!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.coupons.append(dictTemp)
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func FetchDetails(){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "restaurant_id": RestaurantID,
                                              "pure_veg":"",
                                              "apiId":API_ID,
                                              "language": GlobalMethod.selectedLanguageForUser()]
             print("IMAGES PARAMETER =====>>",parameter)
            
            ServManager.viewController = self
            self.restaurantDetailsRequest = ServManager.callPostAPI(constant.DETAILS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    if let dictRestaurant = dataDict.value(forKey: "restaurant") as? NSDictionary{
                         print("IMAGES DETAIL RESPONSE =====>>",dataDict)
                        
                        self.ArrayBannerImages = NSMutableArray()
                        
                        if let Arr = dictRestaurant.value(forKey: "images") as? NSArray, Arr.count != 0{
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayBannerImages.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        self.collectionView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }

}

extension ShowImagesVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if comeFrom == "seeFullMenu"
        {
            return self.ArrayMenuImages.count

        }
        return self.ArrayBannerImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        
        if comeFrom == "seeFullMenu"
        {
            if let Dict = ArrayMenuImages.object(at: indexPath.row) as? NSDictionary{
                if let str = Dict.value(forKey: "image") as? String, !str.isEmpty{
                    let availableWidth = collectionView.frame.width
                    var imgUrl:String = str + "&w=\(availableWidth)"
                    imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    cell.img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: UIImage (named: "RestaurantPlaceholder"))
                }
            }
        }
        else
        {
            if let Dict = ArrayBannerImages.object(at: indexPath.row) as? NSDictionary{
                if let str = Dict.value(forKey: "image") as? String, !str.isEmpty{
                    let availableWidth = collectionView.frame.width
                    var imgUrl:String = str + "&w=\(availableWidth)"
                    imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    cell.img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: UIImage (named: "RestaurantPlaceholder"))
                }
            }
        }
        
        

        
        return cell
    }
    
}

extension ShowImagesVC: UICollectionViewDelegate {
    
}

extension ShowImagesVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - collectionView.contentInset.right - collectionView.contentInset.left
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


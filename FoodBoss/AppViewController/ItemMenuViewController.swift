//
//  ItemMenuViewController.swift
//  FoodBoss
//
//  Created by Jay on 14/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Lottie

/*
 {
 "kItemIngredients": [{"kIngredientName": "Chicken", "isSelected": "true/false"}]
 "kIsCustomisable": "true/false"
 }
 */

protocol MyDataSendingDelegateProtocol {
    func sendDataToFirstViewController(arrData: NSMutableArray, arrVar : [NSDictionary], arrfullAdd : [NSDictionary])
}

class HeaderItem : UITableViewCell{
    @IBOutlet weak var imgRight: UIImageView!
    @IBOutlet weak var viewDesc: UIView!
    
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btn: UIButton!
}

class IngredientHeader: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerDiscriptionLabel: UILabel!
    @IBOutlet weak var customiseButton: UIButton!
    @IBOutlet weak var expandButton: UIButton!
}

class ItemCommentCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    
    var textChanged: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commentTextView.delegate = self
        self.commentTextView.layer.borderColor = UIColor.black.cgColor
        self.commentTextView.layer.borderWidth = 0.5
        self.commentTextView.layer.cornerRadius = 5
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("\nTextViewDidBeginEditing\n")
    }
    
    func textChanged(action: @escaping (String) -> Void) {
        self.textChanged = action
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.textChanged?(textView.text)
    }
}

let kItemIngredients = "keyItemIngredients"
let kIsCustomisable = "keyIsCustomisable"
let kIngredientName = "keyIngredientName"
let kIsSelected = "keyIsSelected"

class ItemMenuViewController: UIViewController, UIScrollViewDelegate {
    
    var ingredientDetails: [String: Any] = [String: Any]()
    var itemComments: String = ""
    
    @IBOutlet weak var btnCustomize: EdgeRoundRectButton!
    @IBOutlet var viewCart: UIView!
    @IBOutlet weak var tblCart: UITableView!
    
    @IBOutlet weak var tblBaseHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrolObjTableContainer: UIScrollView!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var viewLottie: UIView!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var constraintLoaderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblObjBaseOptions: UITableView!
    
    @IBOutlet weak var scrolObjHeighConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tblBaseOptionTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var viewIngrediants: UIView!
    @IBOutlet weak var lblIngrediants: UILabel!
    //@IBOutlet weak var constraintsIngrediantsHeight: NSLayoutConstraint!
    
    var tempItemDict = NSDictionary()
    var DictItem = NSDictionary()
    var arrayOption = NSMutableArray()
    var arrayAddOn = NSMutableArray()
    var arrayData = NSMutableArray()
    var indextobeExpand = -1
    var arrayVariations : [NSDictionary] = []
    var arrayFullAddon : [NSDictionary] = []
    var arrayIngredients = NSMutableArray()
    var arraySelectedIngredients = NSMutableArray()
    var finalSendPrice : String = String()
    var arrBaseSelectedOption : [NSArray] = []
    var arrSelection = NSMutableArray()
    var arrSubSelection : [NSArray] = []
    var itemID = ""
    let jsonBase = NSMutableDictionary()
    var isUpdate : Bool = Bool()
    
    private var CustomizeTapped = false
    var isVeg = false
    var Qty = 1
    private var VariationID = ""
    var SearchItemID = ""
    var comeFrom = ""
    var HeaderTittle = ""
    @IBOutlet weak var HeaderImageHC: NSLayoutConstraint!
    var SearchItemName = ""
    var SearchItemDesc = ""
    var SearchItemPrice = ""
    var SearchQtyValue = ""
    var isCustomize : Bool = false
    var originalPrice : Float = Float()
    var finalPrice : Float = Float()
    var salePrice : Float = Float()
    var selectionIndex : Int = 0
    var delegate: MyDataSendingDelegateProtocol? = nil
    var basePrice : Float = Float()
    var addOnPrice : Float = Float()
    // var arrCustomizeTitle : [String] = []
    var arrCustomizeTitle: [(uniqId: String, title: String, price : String, select : Int)] = []
    var isFrom : (title: String, price : String) = (title: "", price : "")
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemDesc: UILabel!
    @IBOutlet weak var lblItemDescHC: NSLayoutConstraint!
    @IBOutlet weak var HeaderImageView: UIView!
    
    @IBOutlet weak var HeaderImage: UIImageView!
    var AddToCart: ((_ SentToCart: Bool, _ Qty : Int,_ Item : NSArray) -> Void)?
    //    var AddToCart: ((_ SentToCart: Bool, _ Qty : Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnCustomize.isHidden = true
        self.btnCustomize.layer.borderColor = COLOR_ORANGE?.cgColor
        self.btnCustomize.layer.borderWidth = 0.3
        
        let imageHeader = UserDefaults.standard.string(forKey: SELECTED_ITEM_IMAGE)
        print("IMAGE URL IN ITEM ==>>",imageHeader)
        if  imageHeader == ""
        {
            print("ITEM NOT HAVE IMAGE")
            HeaderImageHC.constant = 0
            HeaderImageView.isHidden = true
            
        }
        else
        {
            print("ITEM HAVE IMAGE")
            HeaderImageHC.constant = 250
            HeaderImageView.isHidden = false
            // HeaderImage.contentMode = .scaleAspectFill
            // var imgUrl:String = imageHeader!
            // imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HeaderImage.contentMode = .scaleAspectFit
            HeaderImage.sd_setImage(with: URL(string: imageHeader!), placeholderImage: nil)
        }
        self.viewLottie.layoutIfNeeded()
        self.viewLottie.isHidden = false
        self.constraintLoaderHeight.constant = 2
        
        let animationView = AnimationView(name: "horizontal_loader")
        animationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 2)
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .loop
        
        viewLottie.addSubview(animationView)
        
        animationView.play()
        
        if GlobalsVariable.sharedInstance.ItemAddComeFrom == "SearchItem"
        {
            self.FetchItemDetails(ItemID: SearchItemID)
            lblItemDesc.text = SearchItemDesc
            lblPrice.text = CURRANCY + " " + SearchItemPrice
            Qty = Int(SearchQtyValue)!
            
            if Qty == 0
            {
                Qty = 1
            }
            self.lblQty.text = String (describing: Qty)
        }
        else
        {
            if let str = self.DictItem.value(forKey: "sale_price") as? String, str != ""{
                lblPrice.text = CURRANCY + " " + str
                originalPrice = Float(str)!
                finalPrice = Float(str)!
            }
            if let title = isFrom.title as? String, title != ""{
                btn.setTitle(title, for: .normal)
                originalPrice = Float(isFrom.price)!
                lblPrice.text = CURRANCY + " " + "\(originalPrice)"
            }
            
            
            self.lblQty.text = String (describing: Qty)
            //  if arrayData.count == 0{
            
            //            }else{
            //                isCustomize = true
            //            }
            
            if let str = DictItem.value(forKey: "item_name") as? String{
                HeaderTittle = str
            }
            if let str = DictItem.value(forKey: "item_description") as? String{
                lblItemDesc.text = str
            }
            if let Arr = DictItem.value(forKey: "ingredients") as? NSArray,Arr.count == 0{
                self.viewIngrediants.isHidden = true
            }
        }
        
        // self.addNavigation()
        
        self.tblView.registerNib(HeaderTableViewCell.self)
        self.tblView.registerNib(LanguageSelectionTableViewCell.self)
        self.tblView.registerNib(FilterTableViewCell.self)
        
        self.tblObjBaseOptions.registerNib(HeaderTableViewCell.self)
        self.tblObjBaseOptions.registerNib(LanguageSelectionTableViewCell.self)
        self.tblObjBaseOptions.registerNib(FilterTableViewCell.self)
        
        self.tblView.tableFooterView = UIView()
        
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
        
        lblItemName.text = self.HeaderTittle
        
        if isUpdate == false{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if let ItemNewID = self.DictItem.value(forKey: "item_id") as? String{
                    self.itemID = ItemNewID
                    self.FetchItemDetails(ItemID: ItemNewID)
                }
                if let ItemNewID = self.DictItem.value(forKey: "item_id") as? Int{
                    self.itemID = "\(ItemNewID)"
                    self.FetchItemDetails(ItemID: String (describing: ItemNewID))
                }
            }
        }else{
            self.tblObjBaseOptions.reloadData {
                self.tblBaseHeightConstraint.constant = self.tblObjBaseOptions.contentSize.height + 8
                self.scrolObjTableContainer.contentSize.height = self.tblObjBaseOptions.contentSize.height + self.tblView.contentSize.height + 30
            }
            GlobalMethod.StopProgress(self.view)
        }
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let Image = UIImageView()
        Image.image = UIImage(named: "NonVeg")
        
        if isVeg{
            Image.image = UIImage(named: "Veg")
        }
        Image.contentMode = .scaleAspectFit
        Image.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        let barBack = UIBarButtonItem(customView: Image)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = self.HeaderTittle
        
        lblHeader.font = FONT_NORAML_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
    }
    
    @IBAction func ActionPlus(_ sender: Any) {
        if Qty == 0{
            let roundedVal = String(format:"%.2f", originalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
        }else{
            Qty += 1
            self.lblQty.text = String (describing: Qty)
            finalPrice = finalPrice + originalPrice
            let roundedVal = String(format:"%.2f", finalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
            for (index,DictData) in arrayData.enumerated(){
                if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
                    for DictInner in Arr{
                        if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                            if let Value = (DictInner as! NSMutableDictionary).value(forKey: "value_id") as? String{
                                if let Key = (DictData as! NSMutableDictionary).value(forKey: "item_variation_id") as? String{
                                    print(DictInner)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func ActionMinus(_ sender: Any) {
        if Qty == 1{
            let roundedVal = String(format:"%.2f", originalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
            return
        }else{
            Qty -= 1
            self.lblQty.text = String (describing: Qty)
            finalPrice = finalPrice - originalPrice
            let roundedVal = String(format:"%.2f", finalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
        }
        
    }
    
    
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    
    @IBAction func ActionAddToCart(_ sender: Any) {
        var baseVariant : [String] = []
        var indexBase = 0
        
        if arrSelection.count != 0{
            for i in 0...arrSelection.count - 1{
                let dict = arrSelection[i] as! NSDictionary
                let id = dict.value(forKey: "item_variation_id") as! String
                if id == VariationID{
                    print(dict)
                    if let ArrValue = dict.value(forKey: "value") as? NSArray{
                        for DictInner in ArrValue{
                            if let arrItems = arrSubSelection[i] as? [NSDictionary]{
                                print(arrItems)
                                for i in 0...arrItems.count - 1{
                                    if let dict = arrItems[i] as? NSDictionary{
                                        if let Selected = (dict as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                                            if let Value = (dict as! NSMutableDictionary).value(forKey: "value_id") as? String{
                                                baseVariant.append("\(Value)")
                                                indexBase = indexBase + 1
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                if let arrItems = arrSubSelection[i] as? NSArray{
                                    print(arrItems)
                                    for index in 0...arrItems.count - 1{
                                        if let arrsubContent = arrItems[index] as? [NSDictionary]{
                                            for item in arrsubContent{
                                                // let dictnew = item[index] as? NSDictionary
                                                if let Selected = item.value(forKey: "selected") as? Int, Selected == 1{
                                                    if let Value = item.value(forKey: "value_id") as? String{
                                                        baseVariant.append("\(Value)")
                                                        indexBase = indexBase + 1
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        let newVal = uniq(source: baseVariant)
        
        for (index, ID) in newVal.enumerated(){
            let Key = String (describing: index)
            jsonBase.setValue(ID, forKey: Key)
        }
        
        if let title = isFrom.title as? String, title != ""{
            if finalSendPrice == ""{
                finalSendPrice = "\(originalPrice)"
            }
            let sendJson = GlobalMethod.ConvertInJsonToString(from: jsonBase)!
            let userDataDict : [String : Any] = [ "arrData": self.arrayData, "arrVar":self.arrayVariations, "arrfullAdd" : self.arrayFullAddon, "arrcustom" : self.arrCustomizeTitle, "customBool" : isCustomize, "arrSendOption" : arrSubSelection, "price" : finalSendPrice, "itemID" : self.itemID, "arrSelect" : arrBaseSelectedOption, "SendJsonBase" : sendJson, "arrSendSelection" : arrSelection, "arrInGrediants" : self.ingredientDetails]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getNewData"), object: nil, userInfo: userDataDict)
            //self.delegate?.sendDataToFirstViewController(arrData: self.arrayData, arrVar : self.arrayVariations, arrfullAdd : self.arrayFullAddon)
            self.dismiss(animated: true, completion: nil)
            
        }
        else{
            let Json = NSMutableDictionary()
            let ArrayAddon = NSMutableArray()
            for (index,DictData) in arrayData.enumerated(){
                if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
                    print(Arr)
                    var ISSelected = false
                    for DictInner in Arr{
                        if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                            if let Value = (DictInner as! NSMutableDictionary).value(forKey: "value_id") as? String{
                                if let Key = (DictData as! NSMutableDictionary).value(forKey: "item_variation_id") as? String{
                                    Json.setValue(Value, forKey: Key)
                                    ISSelected = true
                                }
                            }
                        }
                    }
                    
                    if !ISSelected{
                        self.indextobeExpand = index
                        if var str = (DictData as! NSDictionary).value(forKey: "title") as? String{
                            str = "Select option from".localized + " \(str.uppercased()) " + "to continue!".localized
                            GlobalMethod.ShowToast(Text: str, View: self.view)
                        }
                        self.tblView.reloadData()
                        return
                    }
                }
                
                if let Arr = (DictData as! NSDictionary).value(forKey: "add_on_options") as? NSArray{
                    for DictInner in Arr{
                        if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                            if let Value = (DictInner as! NSMutableDictionary).value(forKey: "id") as? String{
                                ArrayAddon.add(Value)
                            }
                        }
                    }
                }
            }
            let JsonAddOn = NSMutableDictionary()
            for (index, ID) in ArrayAddon.enumerated(){
                let Key = String (describing: index)
                JsonAddOn.setValue(ID, forKey: Key)
            }
            print("----------")
            //   print(newVal)
            print("----------")
            print(JsonAddOn)
            
            if GlobalsVariable.sharedInstance.ItemAddComeFrom == "SearchItem"
            {
                self.AddToCart(Variation: [jsonBase], AddOn: ArrayAddon, ItemID: SearchItemID)
            }
            else
            {
                self.AddToCart(Variation: [jsonBase], AddOn: ArrayAddon, ItemID: self.itemID)
            }
        }
        //        else{
        //            GlobalMethod.ShowToast(Text: "No data selected", View: self.view)
        //            self.dismiss(animated: true, completion: nil)
        //        }
    }
    
    func GetPrice(){
        let Json = NSMutableDictionary()
        
        let ArrayAddon = NSMutableArray()
        for DictData in arrayData{
            if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "value_id") as? String{
                            if let Key = (DictData as! NSMutableDictionary).value(forKey: "id") as? String{
                                Json.setValue(Value, forKey: Key)
                            }
                        }
                    }
                }
            }
            
            if let Arr = (DictData as! NSDictionary).value(forKey: "add_on_options") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "id") as? String{
                            ArrayAddon.add(Value)
                        }
                    }
                }
            }
        }
        let JsonAddOn = NSMutableDictionary()
        for (index, ID) in ArrayAddon.enumerated(){
            let Key = String (describing: index)
            JsonAddOn.setValue(ID, forKey: Key)
        }
        
        var VariationString = GlobalMethod.ConvertInJsonToString(from: Json)
        if Json.allKeys.count == 0{
            VariationString = ""
        }
        
        if GlobalsVariable.sharedInstance.ItemAddComeFrom == "SearchItem"
        {
            self.GetPrice(Variation: VariationString!, AddOn: ArrayAddon, ItemID: SearchItemID)
        }
        else
        {
            if let ItemID = self.DictItem.value(forKey: "item_id") as? String{
                // self.GetPrice(Variation: VariationString!, AddOn: ArrayAddon, ItemID: ItemID)
            }
            if let ItemID = self.DictItem.value(forKey: "item_id") as? Int{
                //self.GetPrice(Variation: VariationString!, AddOn: ArrayAddon, ItemID: String (describing: ItemID))
            }
        }
    }
    
    @IBAction func ActionCustomize(_ sender: Any) {
        
        if CustomizeTapped{
            CustomizeTapped = false
            self.btnCustomize.backgroundColor = COLOR_ORANGE
            self.btnCustomize.setTitleColor(.white, for: .normal)
        }
        else{
            CustomizeTapped = true
            self.btnCustomize.backgroundColor = .white
            self.btnCustomize.setTitleColor(COLOR_ORANGE, for: .normal)
        }
        self.tblView.reloadData()
    }
    
    @IBAction func ActionExpand(_ sender: UIButton) {
        if CustomizeTapped{
            return
        }
        print(sender.tag)
        if self.indextobeExpand == sender.tag && self.indextobeExpand != -1{
            self.indextobeExpand = -1
        }
        else{
            self.indextobeExpand = sender.tag
        }
        self.tblView.reloadData()
    }
    
    @IBAction func BaseActionExpand(_ sender: UIButton) {
        if CustomizeTapped{
            return
        }
        print(sender.tag)
        if self.indextobeExpand == sender.tag && self.indextobeExpand != -1{
            self.indextobeExpand = -1
        }
        else{
            self.indextobeExpand = sender.tag
        }
        self.tblObjBaseOptions.reloadData {
            self.tblBaseHeightConstraint.constant = self.tblObjBaseOptions.contentSize.height + 8
            self.scrolObjTableContainer.contentSize.height = self.tblObjBaseOptions.contentSize.height + self.tblView.contentSize.height + 30
        }
    }
    
    func checkForIngredients() -> Bool {
        let ingredients = self.ingredientDetails[kItemIngredients] as? [[String: Any]]
        return ingredients != nil && !ingredients!.isEmpty
    }
    
    func getSelectedIngredients() -> [String] {
        let ingredients = (self.ingredientDetails[kItemIngredients] as? [[String: Any]]) ?? []
        let selectedIngredients = ingredients.compactMap({ (ingredient) -> String? in
            if let flag = ingredient[kIsSelected] as? Bool, flag == true {
                return ingredient[kIngredientName] as? String
            } else {
                return nil
            }
        })
        
        return selectedIngredients
    }
    
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //
    //        let y = scrollView.contentOffset.y
    //        if y <= HeaderImageView.frame.size.height && y >= 0 {
    //            tblBaseOptionTopAnchor.constant = -y
    //        } else if tblBaseOptionTopAnchor.constant != -HeaderImageView.frame.size.height && y > HeaderImageView.frame.size.height {
    //            tblBaseOptionTopAnchor.constant = -HeaderImageView.frame.size.height
    //        }
    //
    //    }
    
    
}

//MARK:- --- [TABLEVIEW CALLING] ---

extension ItemMenuViewController: UITableViewDelegate, UITableViewDataSource {
    //return UITableView.automaticDimension Header
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return self.arrayData.count
        //        if self.arrayVariations.count != 0{
        //            if let dictTemp = self.arrayVariations[selectionIndex] as? NSDictionary{
        //                if let ArrInner = dictTemp.value(forKey: "other_data") as? NSArray, ArrInner.count > 1{
        //                    return self.arrayVariations.count + ArrInner.count - 1 + self.arrayFullAddon.count
        //                }else{
        //                    print(self.arrayVariations.count + self.arrayFullAddon.count)
        //                    return self.arrayVariations.count + self.arrayFullAddon.count
        //                }
        //            }
        //        }
        if tableView.tag == 301{
            //            if arrBaseSelectedOption.count != 0{
            //                return arrBaseSelectedOption[selectionIndex].count
            //            }else{
            //                return 0
            //            }
            if self.arrayVariations.count != 0{
                if let dictTemp = self.arrayVariations[selectionIndex] as? NSDictionary{
                    if let ArrInner = dictTemp.value(forKey: "other_data") as? NSArray, ArrInner.count != 0{
                        return ArrInner.count + 1
                    }
                    else{
                        return 0
                    }
                }
            }
            else{
                return 0
            }
        }
        if tableView.tag == 302{
            return self.arrayData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        print("===========================>\(section)")
        if tableView.tag == 301{
            
            let header = tableView.dequeue(HeaderItem.self)
            header.viewDesc.layer.cornerRadius = 5
            header.btn.tag = section
            if section == 0{
            if let Dict = self.arrayVariations[selectionIndex] as? NSDictionary{
                    if let title = Dict.value(forKey: "title") as? String{
                        header.lblTitle.text = title.uppercased()
                        header.lblDesc.textColor = COLOR_GREEN
                        header.lblDesc.text = "Mandatory Selection".localized
                        var SelectedValue = ""
                        if let Arr = Dict.value(forKey: "value") as? NSArray{
                            for DictInner in Arr{
                                if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                                    if let value = (DictInner as! NSMutableDictionary).value(forKey: "value") as? String{
                                        SelectedValue = value
                                    }
                                }
                            }
                        }
                        if !SelectedValue.isEmpty{
                            header.lblDesc.text = SelectedValue.capitalized
                        }
                    }
                    if self.indextobeExpand == section{
                        header.imgRight.transform = header.imgRight.transform.rotated(by: .pi / 2)
                    }
                }
                header.backgroundColor = COLOR_BACKGROUND
                return header
            }
            else{
                if arrBaseSelectedOption[selectionIndex].count >= section{
                    let Dict = self.arrayVariations[selectionIndex]
                    if let DictNew = arrBaseSelectedOption[selectionIndex][section - 1] as? [NSDictionary]{
                        if let title = DictNew[0]["title"] as? String{
                            header.lblTitle.text = title.uppercased()
                            header.lblDesc.textColor = COLOR_GREEN
                            header.lblDesc.text = "Mandatory Selection".localized
                            var SelectedValue = ""
                            if let Arr = Dict.value(forKey: "value") as? NSArray{
                                for DictInner in Arr{
                                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                                        if let value = (DictInner as! NSMutableDictionary).value(forKey: "value") as? String{
                                            SelectedValue = value
                                        }
                                    }
                                }
                            }
                            if !SelectedValue.isEmpty{
                                header.lblDesc.text = SelectedValue.capitalized
                            }
                        }
                    }
                }
                
            }
            if self.indextobeExpand == section{
                header.imgRight.transform = header.imgRight.transform.rotated(by: .pi / 2)
            }
            
            header.backgroundColor = COLOR_BACKGROUND
            return header
        }else{
            let ingredients = self.ingredientDetails[kItemIngredients] as? [[String: Any]]
            
            if section == 0 && self.checkForIngredients() {
                let ingredientHeader = tableView.dequeue(IngredientHeader.self)
                var ingredientStr = ""
                for (index, element) in ingredients!.enumerated() {
                    if index == 0 {
                        ingredientStr = element[kIngredientName] as! String
                    } else {
                        ingredientStr += ", \(element[kIngredientName] as! String)"
                    }
                }
                
                if (self.ingredientDetails[kIsCustomisable] as! Bool) {
                    ingredientHeader.customiseButton.isHidden = false
                } else {
                    ingredientHeader.customiseButton.isHidden = true
                }
                
                ingredientHeader.expandButton.tag = section
                ingredientHeader.expandButton.addTarget(self, action: #selector(ActionExpand(_:)), for: .touchUpInside)
                
                ingredientHeader.headerDiscriptionLabel.text = ingredientStr
                return ingredientHeader
            } else if section == self.arrayData.count - 1 &&
                        (self.arrayData[section] as! [String: Any]).keys.contains("comment") {
                return nil
            } else {
                let header = tableView.dequeue(HeaderItem.self)
                
                header.viewDesc.layer.cornerRadius = 5
                header.btn.tag = section
                if let Dict = self.arrayData.object(at: section) as? NSDictionary{
                    
                    if let Title = Dict.value(forKey: "addon_for") as? String{
                        header.lblTitle.text = Title.uppercased()
                        header.lblDesc.textColor = UIColor.black
                        header.lblDesc.text = "Optional Selection".localized
                        let ARRAY = NSMutableArray()
                        print(Dict)
                        if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                            for DictInner in Arr{
                                if let Selected = (DictInner as! NSDictionary).value(forKey: "selected") as? Int, Selected == 1{
                                    if let value = (DictInner as! NSDictionary).value(forKey: "addon_title") as? String{
                                        ARRAY.add(value)
                                    }
                                }
                            }
                        }
                        if ARRAY.count != 0{
                            header.lblDesc.text = ARRAY.componentsJoined(by: ", ")
                        }
                    }
                }
                if self.indextobeExpand == section{
                    header.imgRight.transform = header.imgRight.transform.rotated(by: .pi / 2)
                }
                
                header.backgroundColor = COLOR_BACKGROUND
                return header
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if tableView.tag == 301{
            return 70
        }else{
            if section == self.arrayData.count - 1 &&
                (self.arrayData[section] as! [String: Any]).keys.contains("comment") {
                return 0
            } else {
                return 70
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 301{
            if section == 0{
                if indextobeExpand == section{
                    return arrCustomizeTitle.count
                }
            }else{
                if indextobeExpand == section{
                    if let Dict = self.arrSelection.object(at: selectionIndex) as? NSDictionary{
                        if let ArrInner = Dict.value(forKey: "other_data") as? NSArray, ArrInner.count != 0{
                            if let ArrItems = ArrInner.value(forKey: "items") as? NSArray, ArrItems.count != 0{
                                let count = (ArrItems[section - 1] as AnyObject).count!
                                return count
                            }
                        }
                    }
                }
            }
            return 0
        }else{
            let isCustomisable = (self.ingredientDetails[kIsCustomisable] as? Bool) ?? false
            let ingredients = (self.ingredientDetails[kItemIngredients] as? [[String: Any]]) ?? []
            if section == 0 && self.checkForIngredients() {
                if isCustomisable && indextobeExpand == section {
                    return ingredients.count
                } else {
                    return 0
                }
            } else if section == self.arrayData.count - 1 && (self.arrayData[section] as! [String: Any]).keys.contains("comment") {
                return 1
            } else {
                if indextobeExpand == section{
                    if let Dict = self.arrayData.object(at: section) as? NSDictionary{
                        if let Arr = Dict.value(forKey: "value") as? NSArray{
                            return Arr.count
                        }
                        if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                            return Arr.count
                        }
                    }
                }
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 301{
            if indexPath.section == 0{
                let cell = tableView.dequeue(LanguageSelectionTableViewCell.self)
                cell.vWObjCustomize.isHidden = true
                if isFrom.title != "Back"{
                    cell.lbl.text = arrCustomizeTitle[indexPath.row].title + " (+\(CURRANCY + " " + arrCustomizeTitle[indexPath.row].price))"
                }else{
                    cell.lbl.text = arrCustomizeTitle[indexPath.row].title
                }
                
                //  let dictInner = arrayOption[indexPath.row] as? NSDictionary
                cell.imgRadio.image = #imageLiteral(resourceName: "radioUnselect")
                cell.imgRadio.setImageColor(color: GRAY!)
                if let strSelected = arrCustomizeTitle[indexPath.row].select as? Int, strSelected == 1{
                    cell.imgRadio.image = #imageLiteral(resourceName: "radioSelect")
                    cell.imgRadio.setImageColor(color: COLOR_TURQUOISE!)
                    VariationID = arrCustomizeTitle[indexPath.row].uniqId
                }
                cell.selectionStyle = .none
                
                return cell
            }
            else{
                //                if let Dict = self.arrSelection.object(at: selectionIndex) as? NSDictionary{
                //                    if let Arr = Dict.value(forKey: "value") as? NSArray{
                //                        if let DictInner = Arr.object(at: indexPath.row) as? NSDictionary{
                //                            print(DictInner)
                //                            let cell = tableView.dequeue(LanguageSelectionTableViewCell.self)
                //                            cell.vWObjCustomize.isHidden = true
                //                            cell.configureBaseSelection(with: DictInner)
                //                            cell.imgRadio.image = #imageLiteral(resourceName: "radioUnselect")
                //                            cell.imgRadio.setImageColor(color: GRAY!)
                //                            if let strSelected = DictInner.value(forKey: "selected") as? Int, strSelected == 1{
                //                                cell.imgRadio.image = #imageLiteral(resourceName: "radioSelect")
                //                                cell.imgRadio.setImageColor(color: COLOR_TURQUOISE!)
                //                            }
                //                            cell.selectionStyle = .none
                //                            return cell
                //                        }
                //                    }
                //                }
                if let arrItems = self.arrSubSelection[selectionIndex] as? [NSDictionary]{
                    if let dictinner = arrItems[indexPath.row] as? NSDictionary{
                        let cell = tableView.dequeue(LanguageSelectionTableViewCell.self)
                        cell.vWObjCustomize.isHidden = true
                        cell.configureBaseSelection(with: dictinner)
                        cell.imgRadio.image = #imageLiteral(resourceName: "radioUnselect")
                        cell.imgRadio.setImageColor(color: GRAY!)
                        if let strSelected = dictinner.value(forKey: "selected") as? Int, strSelected == 1{
                            cell.imgRadio.image = #imageLiteral(resourceName: "radioSelect")
                            cell.imgRadio.setImageColor(color: COLOR_TURQUOISE!)
                        }
                        cell.selectionStyle = .none
                        return cell
                        
                    }
                }else if let arrItems = self.arrSubSelection[selectionIndex] as? NSArray{
                    if let arrTemp = arrItems[indexPath.section - 1] as? [NSDictionary]{
                        let dictinner = arrTemp[indexPath.row]
                        let cell = tableView.dequeue(LanguageSelectionTableViewCell.self)
                        cell.vWObjCustomize.isHidden = true
                        cell.configureBaseSelection(with: dictinner)
                        cell.imgRadio.image = #imageLiteral(resourceName: "radioUnselect")
                        cell.imgRadio.setImageColor(color: GRAY!)
                        if let strSelected = dictinner.value(forKey: "selected") as? Int, strSelected == 1{
                            cell.imgRadio.image = #imageLiteral(resourceName: "radioSelect")
                            cell.imgRadio.setImageColor(color: COLOR_TURQUOISE!)
                        }
                        cell.selectionStyle = .none
                        return cell
                        
                    }
                }
            }
        }else{
            if indexPath.section == 0 && self.checkForIngredients() {
                let cell = tableView.dequeue(FilterTableViewCell.self)
                cell.img.image = #imageLiteral(resourceName: "uncheck")
                cell.img.setImageColor(color: GRAY!)
                let allIngredients = self.ingredientDetails[kItemIngredients] as! [[String: Any]]
                let ingredientInfo = allIngredients[indexPath.row]
                cell.lbl.text = (ingredientInfo[kIngredientName] as! String).capitalized
                if (ingredientInfo[kIsSelected] as! Bool) == true {
                    cell.img.image = #imageLiteral(resourceName: "checked")
                    cell.img.setImageColor(color: COLOR_TURQUOISE!)
                }
                
                cell.lbl.textColor = .black
               // cell.lblPrice.text = ""
                cell.selectionStyle = .none
                return cell
            } else {
                if let Dict = self.arrayData.object(at: indexPath.section) as? NSDictionary{
                    if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                        if let DictInner = Arr.object(at: indexPath.row) as? NSDictionary{
                            let cell = tableView.dequeue(FilterTableViewCell.self)
                            cell.configure(with: DictInner)
                            cell.img.image = #imageLiteral(resourceName: "uncheck")
                            cell.img.setImageColor(color: GRAY!)
                            if let strSelected = DictInner.value(forKey: "selected") as? Int, strSelected == 1{
                                cell.img.image = #imageLiteral(resourceName: "checked")
                                cell.img.setImageColor(color: COLOR_TURQUOISE!)
                            }
                            cell.selectionStyle = .none
                            return cell
                        }
                    }
                    if let _ = Dict["comment"] as? String {
                        let cell = tableView.dequeue(ItemCommentCell.self)
                        cell.commentTextView.text = self.itemComments
                        cell.textChanged { [weak tableView] (newText: String) in
                            DispatchQueue.main.async {
                                self.itemComments = newText
                                tableView?.beginUpdates()
                                tableView?.endUpdates()
                            }
                        }
                        cell.selectionStyle = .none
                        return cell
                    }
                }
            }
        }
        
        
        let cell = UITableViewCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 301{
            if indexPath.section == 0{
                for i in 0...arrCustomizeTitle.count - 1{
                    arrCustomizeTitle[i].select = 0
                }
                VariationID = arrCustomizeTitle[indexPath.row].uniqId
                arrCustomizeTitle[indexPath.row].select = 1
                
                if indexPath.row != selectionIndex{
                    for i in 0...arrSubSelection.count{
                        if let arrItems = self.arrSubSelection[selectionIndex] as? [NSDictionary]{
                            if let dictinner = arrItems[i] as? NSDictionary{
                                if i == 0{
                                    dictinner.setValue(1, forKey: "selected")
                                }else{
                                    dictinner.setValue(0, forKey: "selected")
                                }
                            }
                        }
//                        else if let arrItems = self.arrSubSelection[selectionIndex] as? NSArray{
//                            if let arrTemp = arrItems[i] as? [NSDictionary]{
//                                if arrTemp.count > i{
//                                    if let dictinner = arrTemp[i] as? NSDictionary{
//                                        dictinner.setValue(0, forKey: "selected")
//                                    }
//                                }
//                            }
//                        }
                    }
                }
                
                selectionIndex = indexPath.row
                
            }else{
                if let arrItems = self.arrSubSelection[selectionIndex] as? [NSDictionary]{
                    if let dictinner = arrItems[indexPath.row] as? NSDictionary{
                        self.SetArrayUnSelect(Array: arrItems as NSArray)
                        dictinner.setValue(1, forKey: "selected")
                    }
                }else if let arrItems = self.arrSubSelection[selectionIndex] as? NSArray{
                    if let arrTemp = arrItems[indexPath.section - 1] as? [NSDictionary]{
                        if let dictinner = arrTemp[indexPath.row] as? NSDictionary{
                            self.SetArrayUnSelect(Array: arrTemp as NSArray)
                            dictinner.setValue(1, forKey: "selected")
                        }
                    }
                }
            }
            
            self.tblObjBaseOptions.reloadData {
                self.tblBaseHeightConstraint.constant = self.tblObjBaseOptions.contentSize.height + 8
                self.scrolObjTableContainer.contentSize.height = self.tblObjBaseOptions.contentSize.height + self.tblView.contentSize.height + 30
            }
            self.getLatestPrice()
            
        }else{
            if indexPath.section == 0 && self.checkForIngredients() {
                var allIngredients = self.ingredientDetails[kItemIngredients] as! [[String: Any]]
                var ingredientInfo = allIngredients[indexPath.row]
                ingredientInfo[kIsSelected] = !(ingredientInfo[kIsSelected] as! Bool)
                allIngredients[indexPath.row] = ingredientInfo
                self.ingredientDetails[kItemIngredients] = allIngredients
                self.tblView.reloadData()
            } else {
                
                //            if indexPath.section == 1{
                //                if let Dict = self.arrayData.object(at: selectionIndex) as? NSDictionary{
                //                    print(Dict)
                //                    if let Arr = Dict.value(forKey: "value") as? NSArray{
                //                        if let DictInner = Arr.object(at: indexPath.row) as? NSMutableDictionary{
                //                            print(DictInner)
                //                            self.SetArrayUnSelect(Array: Arr)
                //                            DictInner.setValue(1, forKey: "selected")
                //                        }
                //                    }
                //                }
                //            }
                if let Dict = self.arrayData.object(at: indexPath.section) as? NSDictionary{
                    //                    if let Arr = Dict.value(forKey: "value") as? NSArray{
                    //                        if let DictInner = Arr.object(at: indexPath.row) as? NSMutableDictionary{
                    //                            print(DictInner)
                    //                            self.SetArrayUnSelect(Array: Arr)
                    //                            DictInner.setValue(1, forKey: "selected")
                    //                        }
                    //                    }
                    if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                        if let DictInner = Arr.object(at: indexPath.row) as? NSMutableDictionary{
                            print(DictInner)
                            if let Selected = DictInner.value(forKey: "selected") as? Int, Selected == 0{
                                DictInner.setValue(1, forKey: "selected")
                            }
                            else{
                                DictInner.setValue(0, forKey: "selected")
                            }
                        }
                    }
                }
                // let roundedVal = String(format:"%.2f", originalPrice)
                //lblPrice.text = CURRANCY + " \(roundedVal)"
                self.tblView.reloadData()
                //self.GetPrice()
                self.getLatestPrice()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func SetArrayUnSelect(Array : NSArray){
        for DictInner in Array{
            if let dicInner = DictInner as? NSMutableDictionary{
                dicInner.setValue(0, forKey: "selected")
            }
        }
    }
    
    func getLatestPrice(){
        let Json = NSMutableDictionary()
        let ArrayselectedAddon = NSMutableArray()
        
        var baseP : Float = Float()
        var addonP : Float = Float()
        
        if arrSelection.count != 0{
            for i in 0...arrSubSelection[selectionIndex].count - 1{
                if let arrItems = arrSubSelection[selectionIndex][i] as? [NSDictionary]{
                    for i in 0...arrItems.count - 1{
                        if let dict = arrItems[i] as? NSDictionary{
                            if let Selected = (dict as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                                if let Value = (dict as! NSMutableDictionary).value(forKey: "addon_price") as? NSNumber{
                                    let price : Float = Float(truncating: Value)
                                    baseP = baseP + price
                                    print(baseP)
                                }
                            }
                        }
                    }
                }else{
                    if let dict = arrSubSelection[selectionIndex][i] as? NSDictionary{
                        if let Selected = (dict as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                            if let Value = (dict as! NSMutableDictionary).value(forKey: "addon_price") as? NSNumber{
                                let price : Float = Float(truncating: Value)
                                baseP = baseP + price
                                print(baseP)
                            }
                        }
                    }
                }
            }
        }

        
        
//        for DictData in arrSubSelection[selectionIndex] as! NSArray{
//            for DictInner in DictData{
//                if let arrItems = DictInner as? [NSDictionary]{
//                    print(arrItems)
//                    for i in 0...arrItems.count - 1{
//                        if let dict = arrItems[i] as? NSDictionary{
//                            if let Selected = (dict as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
//                                if let Value = (dict as! NSMutableDictionary).value(forKey: "addon_price") as? NSNumber{
//                                    let price : Float = Float(truncating: Value)
//                                    baseP = baseP + price
//                                    print(baseP)
//                                }
//                            }
//                        }
//                    }
//                }
//                else {
//                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
//                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "addon_price") as? NSNumber{
//                            let price : Float = Float(truncating: Value)
//                            baseP = baseP + price
//                            print(baseP)
//                        }
//                    }
//                }
//
//            }
//
//        }
        
        for DictData in arrayData{
            if let Arr = (DictData as! NSDictionary).value(forKey: "add_on_options") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "addon_price") as? String{
                            let price : Float = Float(Value)!
                            addonP = addonP + price
                            print(addonP)
                            //ArrayselectedAddon.add(Value)
                        }
                    }
                }
            }
            
        }
    
        var newVal = Float()
        var price = String()
        if isFrom.title == "Back"{
            price = "\(originalPrice)"
        }else{
            if arrCustomizeTitle.count != 0{
                price = arrCustomizeTitle[selectionIndex].price
            }else{
                price = "\(originalPrice)"
            }
        }
        let finalVal = baseP + addonP
        if finalVal == 0{
            newVal = Float(price)!
        }else{
            newVal = finalVal + Float(price)!
        }
        
        let roundedVal = String(format:"%.2f", newVal)
        lblPrice.text = CURRANCY + " \(roundedVal)"
        finalSendPrice = "\(newVal)"
        
        print(Json)
        print(ArrayselectedAddon)
    }
    
}

//MARK:- --- [API CALLING] ---

extension ItemMenuViewController
{
    func FetchItemDetails(ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "item_id":ItemID,
                                              "apiId":API_ID]
            print("ADD ITEM PARAMETER ==>> ",parameter)
            
            self.ingredientDetails.removeAll()
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.MENU_ITEM_DETAILS, isLoaderRequired: true, paramters: parameter) { [self] (isSuccess, status, msg, dataDict) in
                self.viewLottie.isHidden = true
                self.constraintLoaderHeight.constant = 0
                if isSuccess{
                    
                    if let dictItems = dataDict.value(forKey: "items") as? NSDictionary{
                        tempItemDict = dictItems
                        if let customisable = dictItems["custom_ingredients"] as? String, customisable == "1" {
                            self.ingredientDetails[kIsCustomisable] = true
                        } else {
                            self.ingredientDetails[kIsCustomisable] = false
                        }
                        
                        if let ingredients = dictItems["ingredients"] as? [String] {
                            self.ingredientDetails[kItemIngredients] = ingredients.map({ [kIngredientName: $0, kIsSelected: false] })
                        }
                        
                        if let ingredients = self.ingredientDetails[kItemIngredients] as? [[String: Any]], !ingredients.isEmpty {
                            self.arrayData.add(self.ingredientDetails)
                        }
                        
                        if let str = dictItems.value(forKey: "is_customization") as? String, str == "1"{
                            self.btnCustomize.isHidden = false
                            self.isCustomize = true
                        }
                        
                        if let ID = dictItems.value(forKey: "variation_id") as? String{
                            self.VariationID = ID
                        }
                        if let ID = dictItems.value(forKey: "variation_id") as? Int{
                            self.VariationID = String (describing: ID)
                        }
                        
                        if let Arr = dictItems.value(forKey: "ingredients") as? NSArray, Arr.count != 0{
                            for str in Arr{
                                self.arrayIngredients.add(str)
                            }
                        }
                        
                        if let Arr = dictItems.value(forKey: "variations") as? NSArray, Arr.count != 0{
                            self.arrayVariations = dictItems.value(forKey: "variations") as! [NSDictionary]
                            var indexTemp = 0
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        if let str = dictTemp.value(forKey: "option_name") as? String{
                                            let id = dictTemp.value(forKey: "item_variation_id") as? String
                                            if indexTemp == 0{
                                                if let str1 = dictTemp.value(forKey: "sale_price") as? NSNumber, str1 != 0, str != ""{
                                                    self.arrCustomizeTitle.append((uniqId: id!, title: str, price : "\(str1)", select : 1))
                                                }else if let str1 = dictTemp.value(forKey: "sale_price") as? String, str1 != "0", str1 != ""{
                                                    self.arrCustomizeTitle.append((uniqId: id!, title: str, price : str1, select : 1))
                                                }else{
                                                    self.arrCustomizeTitle.append((uniqId: id!, title: str, price : "0", select : 1))
                                                }
                                            }else{
                                                if let str1 = dictTemp.value(forKey: "sale_price") as? NSNumber, str1 != 0, str != ""{
                                                    self.arrCustomizeTitle.append((uniqId: id!, title: str, price : "\(str1)", select : 0))
                                                }else if let str1 = dictTemp.value(forKey: "sale_price") as? String, str1 != "0", str1 != ""{
                                                    self.arrCustomizeTitle.append((uniqId: id!, title: str, price : str1, select : 0))
                                                }else{
                                                    self.arrCustomizeTitle.append((uniqId: id!, title: str, price : "0", select : 0))
                                                }
                                            }
                                            
                                            // self.arrayOption.add(str)
                                        }
                                        
                                        let ARR = NSMutableArray()
                                        if let ArrInner = dictTemp.value(forKey: "other_data") as? NSArray, Arr.count != 0{
                                            
                                            if let ArrItems = ArrInner.value(forKey: "items") as? NSArray, Arr.count != 0{
                                                self.arrBaseSelectedOption.append(ArrItems)
                                                var arrnew : [NSDictionary] = []
                                                if ArrItems.count == 1{
                                                    arrnew = ArrItems[0] as! [NSDictionary]
                                                }else{
                                                    for i in 0...ArrItems.count - 1{
                                                        if let dict = ArrItems[i] as? [NSDictionary]{
                                                            arrnew.append(contentsOf: dict)
                                                        }
                                                        
                                                    }
                                                    // arrnew = ArrItems[0] as! [NSDictionary]
                                                }
                                                
                                                var index = 0
                                                for DictInner in arrnew{
                                                    if let dicInner = DictInner as? NSDictionary{
                                                        if let dictInnerTemp = (dicInner.mutableCopy()) as? NSMutableDictionary{
                                                            if index == 0{
                                                                dictInnerTemp.setValue(true, forKey: "selected")
                                                                if self.arrCustomizeTitle.count != 0{
                                                                    self.VariationID = self.arrCustomizeTitle[0].uniqId
                                                                }
                                                                
                                                            }else{
                                                                dictInnerTemp.setValue(false, forKey: "selected")
                                                            }
                                                            self.arrayOption.add(dictInnerTemp)
                                                            ARR.add(dictInnerTemp)
                                                        }
                                                    }
                                                    index = index + 1
                                                }
                                            }
                                        }
                                        dictTemp.setValue(ARR, forKey: "value")
                                        self.arrSelection.add(dictTemp)
                                    }
                                }
                                indexTemp = indexTemp + 1
                            }
                            
                           
                            for item in Arr{
                                if let dict = item as? NSDictionary{
                                    if let ArrInner = dict.value(forKey: "other_data") as? NSArray, Arr.count != 0{
                                        //   print(ArrInner)
                                        var indexItem = 0
                                        if ArrInner.count == 1{
                                            for itemInner in ArrInner{
                                                //   print(item)
                                                if let dictInner = itemInner as? NSDictionary{
                                                    if let ArrItems = dictInner.value(forKey: "items") as? NSArray, Arr.count != 0{
                                                        var arrSubItems : [NSDictionary] = []
                                                        for itemDetail in ArrItems{
                                                            if let dictInnerTemp = ((itemDetail as AnyObject).mutableCopy()) as? NSMutableDictionary{
                                                                print(dictInnerTemp)
                                                                if indexItem == 0{
                                                                    dictInnerTemp.setValue(true, forKey: "selected")
                                                                }else{
                                                                    dictInnerTemp.setValue(false, forKey: "selected")
                                                                }
                                                                arrSubItems.append(dictInnerTemp)
                                                                indexItem = indexItem + 1
                                                            }
                                                        }
                                                        self.arrSubSelection.append(arrSubItems as NSArray)
                                                    }
                                                }
                                                
                                            }
                                        }else{
                                            let arrSubItems = NSMutableArray()
                                            for itemInner in ArrInner{
                                                var indexItem1 = 0
                                                if let dictInner = itemInner as? NSDictionary{
                                                    if let ArrItems = dictInner.value(forKey: "items") as? NSArray, ArrItems.count != 0{
                                                        let arrMerged = NSMutableArray()
                                                        for itemDetail in ArrItems{
                                                            if let dictInnerTemp = ((itemDetail as AnyObject).mutableCopy()) as? NSMutableDictionary{
                                                                print(dictInnerTemp)
                                                                if indexItem1 == 0{
                                                                    dictInnerTemp.setValue(true, forKey: "selected")
                                                                }else{
                                                                    dictInnerTemp.setValue(false, forKey: "selected")
                                                                }
                                                                arrMerged.add(dictInnerTemp)
                                                                indexItem1 = indexItem1 + 1
                                                            }
                                                        }
                                                        arrSubItems.add(arrMerged)
                                                        
                                                    }
                                                }
                                            }
                                            self.arrSubSelection.append(arrSubItems as NSArray)
                                        }
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                        if let Arr = dictItems.value(forKey: "add_ons") as? NSArray, Arr.count != 0{
                            self.arrayFullAddon = Arr as! [NSDictionary]
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        if let str = dictTemp.value(forKey: "addon_for") as? String{
                                            self.arrayAddOn.add(str)
                                        }
                                        
                                        let ARR = NSMutableArray()
                                        if let ArrInner = dictTemp.value(forKey: "add_on_options") as? NSArray, Arr.count != 0{
                                            for DictInner in ArrInner{
                                                if let dicInner = DictInner as? NSDictionary{
                                                    if let dictInnerTemp = (dicInner.mutableCopy()) as? NSMutableDictionary{
                                                        dictInnerTemp.setValue(false, forKey: "selected")
                                                        self.arrayAddOn.add(dictInnerTemp)
                                                        ARR.add(dictInnerTemp)
                                                    }
                                                }
                                            }
                                        }
                                        dictTemp.setValue(ARR, forKey: "add_on_options")
                                        self.arrayData.add(dictTemp)
                                    }
                                }
                            }
                        }
                        //                        print(self.arrayData)
                        print("\nNumber of Sections:\t\(self.arrayData.count)\n")
                        // Manthan 30-9-2020
                        // Check if user can add comments for selected item
                        let canAddComment = dictItems["comments_for_item"] as? String ?? "0"
                        if canAddComment == "1" {
                            self.arrayData.add(["comment": "1"])
                            self.arrayFullAddon.append(["comment": "1"])
                        }
                        self.tblObjBaseOptions.layoutIfNeeded()
                        self.tblView.layoutIfNeeded()
                        self.tblObjBaseOptions.reloadData {
                            self.tblView.reloadData()
                            self.tblBaseHeightConstraint.constant = self.tblObjBaseOptions.contentSize.height
                            self.scrolObjTableContainer.contentSize.height = self.tblObjBaseOptions.contentSize.height + self.tblView.contentSize.height + 30
                            if arrSelection.count != 0{
                                self.getLatestPrice()
                            }
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    
    func resetPrice(){
        if let Arr = tempItemDict.value(forKey: "variations") as? NSArray, Arr.count != 0{
            for item in Arr{
                if let dict = item as? NSDictionary{
                    if let ArrInner = dict.value(forKey: "other_data") as? NSArray, Arr.count != 0{
                        //   print(ArrInner)
                        var indexItem = 0
                        if ArrInner.count == 1{
                            for itemInner in ArrInner{
                                //   print(item)
                                if let dictInner = itemInner as? NSDictionary{
                                    if let ArrItems = dictInner.value(forKey: "items") as? NSArray, Arr.count != 0{
                                        var arrSubItems : [NSDictionary] = []
                                        for itemDetail in ArrItems{
                                            if let dictInnerTemp = ((itemDetail as AnyObject).mutableCopy()) as? NSMutableDictionary{
                                                print(dictInnerTemp)
                                                if indexItem == 0{
                                                    dictInnerTemp.setValue(true, forKey: "selected")
                                                }else{
                                                    dictInnerTemp.setValue(false, forKey: "selected")
                                                }
                                                arrSubItems.append(dictInnerTemp)
                                                indexItem = indexItem + 1
                                            }
                                        }
                                        self.arrSubSelection.append(arrSubItems as NSArray)
                                    }
                                }
                                
                            }
                        }else{
                            let arrSubItems = NSMutableArray()
                            for itemInner in ArrInner{
                                var indexItem1 = 0
                                if let dictInner = itemInner as? NSDictionary{
                                    if let ArrItems = dictInner.value(forKey: "items") as? NSArray, ArrItems.count != 0{
                                        let arrMerged = NSMutableArray()
                                        for itemDetail in ArrItems{
                                            if let dictInnerTemp = ((itemDetail as AnyObject).mutableCopy()) as? NSMutableDictionary{
                                                print(dictInnerTemp)
                                                if indexItem1 == 0{
                                                    dictInnerTemp.setValue(true, forKey: "selected")
                                                }else{
                                                    dictInnerTemp.setValue(false, forKey: "selected")
                                                }
                                                arrMerged.add(dictInnerTemp)
                                                indexItem1 = indexItem1 + 1
                                            }
                                        }
                                        arrSubItems.add(arrMerged)
                                        
                                    }
                                }
                            }
                            self.arrSubSelection.append(arrSubItems as NSArray)
                        }
                        
                    }
                }
            }
        }
    }
    
    
    func UpdateCart(cart_id : String, Qty : String){
        //        if !GlobalMethod.internet(){
        //
        //        }else{
        ////            GlobalMethod.StartProgress(self.view)
        //
        //            let parameter : [String : Any] = ["from_app":FROMAPP,"quantity":Qty,"apiId":API_ID, "cart_id":cart_id,"cart_session":CART_SESSION]
        //            print(parameter)
        //            ServManager.viewController = self
        //            ServManager.callPostAPI(constant.UPDATE_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
        //
        //                if isSuccess{
        //                    print(dataDict)
        //                    GlobalMethod.StopProgress(self.view)
        //
        //                    var Array = NSArray()
        //                    if let Dict = dataDict.value(forKey: "cart") as? NSDictionary{
        //                        if let str = Dict.value(forKey: "total_quantity") as? String{
        //                            if let strTotal = Dict.value(forKey: "grand_total") as? String{
        //                                if str == "0"{
        //
        //                                    self.viewCart.isHidden = true
        //                                    self.constraintViewCartHeight.constant = 0
        //                                }
        //                                else if str == "1"{
        //                                    self.lblCart.text = str + " item | " + CURRANCY + " " + strTotal
        //                                }
        //                                else{
        //                                    self.lblCart.text = str + " items | " + CURRANCY + " " + strTotal
        //                                }
        //                            }
        //                        }
        //
        //
        //                        if let Arr = Dict.value(forKey: "cart_items") as? NSArray{
        //                            Array = Arr
        //                        }
        //                    }
        //
        //                    if let Dictionary = self.ArrayMenuCategory.object(at: self.selectedSection) as? NSMutableDictionary{
        //                        if let Arr = Dictionary.value(forKey: "menu_items") as? NSMutableArray{
        //                            if let Dict = Arr.object(at: self.selectedIndex) as? NSMutableDictionary{
        //                                Dict.setValue(Array, forKey: "cart_items")
        //                            }
        //                        }
        //                    }
        //
        //                    self.viewCart.isHidden = false
        //                    self.constraintViewCartHeight.constant = 50
        //                    self.tblView.reloadData()
        //
        //                }else{
        //                    GlobalMethod.ShowToast(Text: msg, View: self.view)
        //                    GlobalMethod.StopProgress(self.view)
        //                }
        //            }
        //        }
    }
    
    func AddToCart(Variation : NSMutableArray, AddOn : NSMutableArray, ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            //var dictVarOption = [String : Any]()
            //dictVarOption.updateValue(Variation, forKey: VariationID)
            
            // let newVal = uniq(source: Variation)
            
            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              //"user_id":UserDefaults.postandard.string(forKey: SESSION_USER_ID)!,
                                              "item_id":ItemID,
                                              "variation_options": GlobalMethod.ConvertInJsonToString(from: Variation)!,
                                              "auto_added_with":"",
                                              "is_auto_added":"",
                                              "selected_ingredients":GlobalMethod.ConvertInJsonToString(from: self.getSelectedIngredients())!,
                                              "addons":GlobalMethod.ConvertInJsonToString(from: AddOn)!,
                                              "quantity":String (describing: Qty),
                                              "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                              "is_customization": "1",
                                              "variation_id":VariationID,
                                              "apiId":API_ID,
                                              "comments": self.itemComments.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                                              "final_price": finalSendPrice,
                                              "language" : GlobalMethod.selectedLanguageForUser(),
                                              "selected_variation" : ""]
            print("ADD TO CART PARAMETER ==>> ",parameter)
            
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADD_TO_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print("ADD TO CART RESPONSE ==>> ",dataDict)
                    if let DictCart = dataDict.value(forKey: "cart") as? NSDictionary{
                        if let Arr = DictCart.value(forKey: "cart_items") as? NSArray{
                            if let block = self.AddToCart {
                                block(true, self.Qty, Arr)
                                GlobalsVariable.sharedInstance.ItemAddComeFrom = ""
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                        }
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func GetPrice(Variation : String, AddOn : NSMutableArray, ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            //            GlobalMethod.StartProgress(self.view)
            
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "item_id":ItemID,
                                              "selected_variation":Variation,
                                              "addons":GlobalMethod.ConvertInJsonToString(from: AddOn)!,
                                              //                                      "quantity":String (describing: Qty),
                                              //                                      "cart_session":CART_SESSION,
                                              "is_customization": "1",
                                              "variation_id":VariationID,
                                              "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.GET_ITEM_PRICE, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let DictCart = dataDict.value(forKey: "item_price") as? NSDictionary{
                        if let ID = DictCart.value(forKey: "variation_id") as? String{
                            self.VariationID = ID
                        }
                        if let ID = DictCart.value(forKey: "variation_id") as? Int{
                            self.VariationID = String (describing: ID)
                        }
                        if let str = DictCart.value(forKey: "price") as? String{
                            self.lblPrice.text = CURRANCY + " " + str
                        }
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}
extension UITableView {
    func reloadData(completion:@escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
            { _ in completion() }
    }
}
extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}

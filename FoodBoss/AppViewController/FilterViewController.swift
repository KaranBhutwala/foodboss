//
//  FilterViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 09/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class FilterViewController: UIViewController {

    @IBOutlet weak var btnVeg: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var imgRate: UIImageView!
    @IBOutlet weak var lblRate: UILabel!
    
    @IBOutlet weak var imgTime: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var imgLH: UIImageView!
    @IBOutlet weak var lblLH: UILabel!
    
    @IBOutlet weak var imgHL: UIImageView!
    @IBOutlet weak var lblHL: UILabel!
    
    @IBOutlet weak var imgAll: UIImageView!
    @IBOutlet weak var lblAll: UILabel!
    
    @IBOutlet weak var imgDelivery: UIImageView!
    @IBOutlet weak var lblDelivery: UILabel!
    
    @IBOutlet weak var imgPickup: UIImageView!
    @IBOutlet weak var lblPickup: UILabel!
    
    @IBOutlet weak var imgPureVeg: UIImageView!
    @IBOutlet weak var lblPureVeg: UILabel!
    
    @IBOutlet weak var imgOffer: UIImageView!
    @IBOutlet weak var lblOffer: UILabel!
    
    private var ArrayFilter = NSMutableArray()
    private var ArraySelectedFilter = NSMutableArray()
    
    
    var DictFilter = NSMutableDictionary()
    
    @IBOutlet weak var AddressFilterView: UIView!
    var isRate = false
    var isTime = false
    var isLH = false
    var isHL = false
    var isVeg = false
    
    var isAll = false
    var isDelivery = false
    var isPickup = false

    var isOfferDiscount = false
    var isPureVegitatian = false

    @IBOutlet weak var btn: UIButton!
    
    var Filter: ((_ selectedFilters: NSMutableDictionary) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        imgRate.image = #imageLiteral(resourceName: "StarDeselect")
        imgTime.image = #imageLiteral(resourceName: "TimeDeselect")
        imgLH.image = #imageLiteral(resourceName: "LHDeselect")
        imgHL.image = #imageLiteral(resourceName: "HLDeselect")
        
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 44
        self.tblView.registerNib(FilterTableViewCell.self)
        
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
        tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        
        
        if GlobalsVariable.sharedInstance.FilterScreenComeFrom == "OfferClick"
        {
             AddressFilterView.isHidden = false
        }
        else
        {
            AddressFilterView.isHidden = true
        }
        
        if GlobalsVariable.sharedInstance.SelectedOptionForFilter == "0"
        {
            isAll = true
            imgAll.image = #imageLiteral(resourceName: "green_all")
            lblAll.textColor = COLOR_TURQUOISE

        }
        else if GlobalsVariable.sharedInstance.SelectedOptionForFilter == "1"
        {
            isDelivery = true
            imgDelivery.image = #imageLiteral(resourceName: "green_delivery")
            lblDelivery.textColor = COLOR_TURQUOISE

        }
        else if GlobalsVariable.sharedInstance.SelectedOptionForFilter == "2"
        {
            isPickup = true
            imgPickup.image = #imageLiteral(resourceName: "green_pickup")
            lblPickup.textColor = COLOR_TURQUOISE
        }
        
        self.ArraySelectedFilter = NSMutableArray()
        if self.DictFilter.allValues.count != 0{
            if let str = self.DictFilter.value(forKey: "Cuisine") as? String,!str.isEmpty{
                let Arr = str.components(separatedBy: ",")
                for string in Arr{
                    self.ArraySelectedFilter.add(string)
                }
            }
            if let str = self.DictFilter.value(forKey: "Rate") as? String, str == "1"{
                isRate = true
            }
            if let str = self.DictFilter.value(forKey: "Time") as? String, str == "1"{
                isTime = true
            }
            if let str = self.DictFilter.value(forKey: "LH") as? String, str == "1"{
                isLH = true
            }
            if let str = self.DictFilter.value(forKey: "HL") as? String, str == "1"{
                isHL = true
            }
            if let str = self.DictFilter.value(forKey: "Veg") as? String, str == "1"{
                isVeg = true
            }
            SetupData()
        }
        self.FetchCuisines()

    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = COLOR_BACKGROUND
        self.navigationController?.navigationBar.barTintColor = COLOR_BACKGROUND
        

        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = "Sort/Filter".localized
        
        lblHeader.font = FONT_NORAML_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem (customView: viewHeader)]
        
        let buttonClear = UIButton.init(type: .custom)
        buttonClear.addTarget(self, action: #selector(self.ActionClear), for: UIControl.Event.touchUpInside)
        buttonClear.setTitle("Clear".localized, for: .normal)
        buttonClear.titleLabel?.font = FONT_NORAML_CUSTOM(xx: 13)
        buttonClear.backgroundColor = UIColor.white
        buttonClear.setTitleColor(COLOR_RED, for: .normal)
        buttonClear.frame = CGRect(x: 0, y: 0, width: 75, height: 30)
        buttonClear.layer.cornerRadius = buttonClear.frame.height/2
        buttonClear.clipsToBounds = true
        let barClear = UIBarButtonItem(customView: buttonClear)
        
        self.navigationItem.rightBarButtonItems = [barClear]
    }
    
    @objc func ActionClear(){
        DictFilter = NSMutableDictionary()
        DictFilter.setValue("1", forKey: "Clear")
        
        if let block = self.Filter {
            block(DictFilter)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func SetupData(){
        
        if isRate{
            isRate = true
            imgRate.image = #imageLiteral(resourceName: "StarSelect")
            lblRate.textColor = COLOR_TURQUOISE
        }
        if isTime{
            isTime = true
            imgTime.image = #imageLiteral(resourceName: "TimeSelect")
            lblTime.textColor = COLOR_TURQUOISE
        }
        if isLH{
            isLH = true
            imgLH.image = #imageLiteral(resourceName: "LHSelect")
            lblLH.textColor = COLOR_TURQUOISE
        }
        if isHL{
            isHL = true
            imgHL.image = #imageLiteral(resourceName: "HLSelect")
            lblHL.textColor = COLOR_TURQUOISE
        }
        if isVeg{
            isVeg = true
            btnVeg.setTitleColor(COLOR_TURQUOISE, for: .normal)
        }
        
    }
    
    @IBAction func ActionAll(_ sender: Any)
    {
        
        if isAll{
            isAll = false
            imgAll.image = #imageLiteral(resourceName: "black_all")
            lblAll.textColor = GRAY

        }
        else{
            GlobalsVariable.sharedInstance.SelectedOptionForFilterType = "both"
            GlobalsVariable.sharedInstance.SelectedRestaurantType = "both"

            isAll = true
            imgAll.image = #imageLiteral(resourceName: "green_all")
            lblAll.textColor = COLOR_TURQUOISE
            
            isDelivery = false
            imgDelivery.image = #imageLiteral(resourceName: "black_delivery")
            lblDelivery.textColor = GRAY
            
            isPickup = false
            imgPickup.image = #imageLiteral(resourceName: "black_pickup")
            lblPickup.textColor = GRAY


        }
    }
    
    @IBAction func ActionDelivery(_ sender: UIButton)
    {
        if isDelivery{
            isDelivery = false
            imgDelivery.image = #imageLiteral(resourceName: "black_delivery")
            lblDelivery.textColor = GRAY
           // GlobalsVariable.sharedInstance.SelectedOptionForFilterType = "home_delivery"
           // GlobalsVariable.sharedInstance.SelectedRestaurantType = "home_delivery"


        }
        else{
            GlobalsVariable.sharedInstance.SelectedOptionForFilterType = "home_delivery"
            GlobalsVariable.sharedInstance.SelectedRestaurantType = "home_delivery"

            isDelivery = true
            imgDelivery.image = #imageLiteral(resourceName: "green_delivery")
            lblDelivery.textColor = COLOR_TURQUOISE
            
            isAll = false
            imgAll.image = #imageLiteral(resourceName: "black_all")
            lblAll.textColor = GRAY
            
            isPickup = false
            imgPickup.image = #imageLiteral(resourceName: "black_pickup")
            lblPickup.textColor = GRAY


        }
    }
    
    
    @IBAction func ActionPickup(_ sender: UIButton)
    {
        if isPickup{
            isPickup = false
            imgPickup.image = #imageLiteral(resourceName: "black_pickup")
            lblPickup.textColor = GRAY
        }
        else{
            GlobalsVariable.sharedInstance.SelectedOptionForFilterType = "self_service"
            GlobalsVariable.sharedInstance.SelectedRestaurantType = "self_service"

            isPickup = true
            imgPickup.image = #imageLiteral(resourceName: "green_pickup")
            lblPickup.textColor = COLOR_TURQUOISE
            
            isAll = false
            imgAll.image = #imageLiteral(resourceName: "black_all")
            lblAll.textColor = GRAY
            
            isDelivery = false
            imgDelivery.image = #imageLiteral(resourceName: "black_delivery")
            lblDelivery.textColor = GRAY


        }
    }
    
    @IBAction func ActionRating(_ sender: Any) {
        if isRate{
            isRate = false
            imgRate.image = #imageLiteral(resourceName: "StarDeselect")
            lblRate.textColor = GRAY
        }
        else{
            isRate = true
            imgRate.image = #imageLiteral(resourceName: "StarSelect")
            lblRate.textColor = COLOR_TURQUOISE
        }
    }
    
    @IBAction func ActionTime(_ sender: Any) {
        if isTime{
            isTime = false
            imgTime.image = #imageLiteral(resourceName: "TimeDeselect")
            lblTime.textColor = GRAY
        }
        else{
            isTime = true
            imgTime.image = #imageLiteral(resourceName: "TimeSelect")
            lblTime.textColor = COLOR_TURQUOISE
        }
    }
    
    @IBAction func ActionLowtoHigh(_ sender: Any) {
        if isLH{
            isLH = false
            imgLH.image = #imageLiteral(resourceName: "LHDeselect")
            lblLH.textColor = GRAY
        }
        else{
            isLH = true
            imgLH.image = #imageLiteral(resourceName: "LHSelect")
            lblLH.textColor = COLOR_TURQUOISE
            if isHL{
                isHL = false
                imgHL.image = #imageLiteral(resourceName: "HLDeselect")
                lblHL.textColor = GRAY
            }
        }
    }
    
    @IBAction func ActionHightoLow(_ sender: Any) {
        if isHL{
            isHL = false
            imgHL.image = #imageLiteral(resourceName: "HLDeselect")
            lblHL.textColor = GRAY
        }
        else{
            isHL = true
            imgHL.image = #imageLiteral(resourceName: "HLSelect")
            lblHL.textColor = COLOR_TURQUOISE
            if isLH{
                isLH = false
                imgLH.image = #imageLiteral(resourceName: "LHDeselect")
                lblLH.textColor = GRAY
            }
        }
    }
    
    @IBAction func ActionDiscounts(_ sender: UIButton) {
        
        if isOfferDiscount{
            isOfferDiscount = false
            imgOffer.image = #imageLiteral(resourceName: "black_discount")
            lblOffer.textColor = GRAY
        }
        else{
            isOfferDiscount = true
            imgOffer.image = #imageLiteral(resourceName: "green_discount")
            lblOffer.textColor = COLOR_TURQUOISE
        }
    }
    
    @IBAction func ActionVeg(_ sender: UIButton) {
        
        if isPureVegitatian{
            isPureVegitatian = false
            imgPureVeg.image = #imageLiteral(resourceName: "black_leaf")
            lblPureVeg.textColor = GRAY
        }
        else{
            isPureVegitatian = true
            imgPureVeg.image = #imageLiteral(resourceName: "green_leaf")
            lblPureVeg.textColor = COLOR_TURQUOISE
        }
    }
    
    @IBAction func ActionApply(_ sender: Any) {
        
        let DictFilter = NSMutableDictionary()
        if self.ArraySelectedFilter.count != 0 {
            let strCuisine = self.ArraySelectedFilter.componentsJoined(by: ",")
            DictFilter.setValue(strCuisine, forKey: "Cuisine")
        }
        
        if isRate {
            DictFilter.setValue("1", forKey: "Rate")
        }
        if isTime {
            DictFilter.setValue("1", forKey: "Time")
        }
        if isLH {
            DictFilter.setValue("1", forKey: "LH")
        }
        if isHL {
            DictFilter.setValue("1", forKey: "HL")
        }
        if isVeg {
            DictFilter.setValue("1", forKey: "Veg")
        }
        
        

        if let block = self.Filter {
            block(DictFilter)
            self.dismiss(animated: true, completion: nil)
        }
        
}
}

// MARK: - TableView

extension FilterViewController: UITableViewDelegate, SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: FilterTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(FilterTableViewCell.self)
        cell.selectionStyle = .none

        if let Dict = ArrayFilter.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
            cell.img.image = #imageLiteral(resourceName: "uncheck")
            if let str = Dict.value(forKey: "cuisine_id") as? String, ArraySelectedFilter.contains(str){
                cell.img.image = #imageLiteral(resourceName: "checked")
            }
            
            cell.img.setImageColor(color: GRAY!)
        }
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = ArrayFilter.object(at: indexPath.row) as? NSDictionary{
            if let str = Dict.value(forKey: "cuisine_id") as? String{
                if ArraySelectedFilter.contains(str){
                    self.ArraySelectedFilter.remove(str)
                }
                else{
                    self.ArraySelectedFilter.add(str)
                }
                self.tblView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
}

//MARK:- --- [API CALLING] ---

extension FilterViewController{
    
    func FetchCuisines(){
        if !GlobalMethod.internet(){
            
        }else{
            
            //GlobalMethod.StartProgress(self.view)
            self.view.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.CUISINES_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                //GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let Data = CuisineListModel.init(dictionary: dataDict){
                        for Dict in Data.cuisine!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayFilter.add(dictTemp)
                                }
                            }
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                }

                self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                self.tblView.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))

                self.tblView.reloadData()
            }
        }
    }
}

//
//  RestaurantDetailsVC.swift
//  FoodBoss
//
//  Created by Apple on 01/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class RestaurantDetailsVC: UIViewController {

    @IBOutlet weak var segmentController: UISegmentedControl!
    var restaurntReview = RestaurantReview.getEmptyReview()
    var restaurantInfo = RestaurantInfo.getEmptyInfo()
    var imageList = [String]()
    var comeFromScreen = ""
    
    private lazy var restaurnReviewVC: RestaurantReviewVC = {
        let viewController = RestaurantReviewVC.initiateFromStoryBoard()
        viewController.restaurntReview = self.restaurntReview
        viewController.restaurantName = self.restaurantInfo.restaurantName
        viewController.imageList = self.imageList
        //self.add(asChildViewController: viewController)
        return viewController
    }()
    
    private lazy var restaurantInfoVc: RestaurantInfoVC = {
        let viewController = RestaurantInfoVC.initiateFromStoryBoard()
        viewController.restaurantInfo = self.restaurantInfo
        viewController.imageList = self.imageList
        //self.add(asChildViewController: viewController)
        return viewController
    }()
    
    static func initiateFromStoryBoard() -> RestaurantDetailsVC {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            let vc = mainStoryBoard.instantiateViewController(identifier: String(describing: RestaurantDetailsVC.self)) as RestaurantDetailsVC
            return vc
        } else {
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: RestaurantDetailsVC.self)) as! RestaurantDetailsVC
            return vc
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func setupViews() {
        
        if self.comeFromScreen == "InfoClick"
        {
            setupSegmentControllerInfo()
        }
        else
        {
            setupSegmentController()
        }
        updateChildViewController()
    }
    
    func setupSegmentController() {
        self.segmentController.addTarget(self, action: #selector(segmentController(didChangeSelection:)), for: .valueChanged)
        self.segmentController.selectedSegmentIndex = 0
    }
    
    func setupSegmentControllerInfo() {
        self.segmentController.addTarget(self, action: #selector(segmentController(didChangeSelection:)), for: .valueChanged)
        self.segmentController.selectedSegmentIndex = 1
    }
    
    @objc func segmentController(didChangeSelection sender: UISegmentedControl) {
        updateChildViewController()
    }
    
    func updateChildViewController() {
        if self.segmentController.selectedSegmentIndex == 0 {
            self.add(asChildViewController: restaurnReviewVC)
            self.remove(asChildViewController: restaurantInfoVc)
        } else {
            self.add(asChildViewController: restaurantInfoVc)
            self.remove(asChildViewController: restaurnReviewVC)
        }
    }
    
    func add(asChildViewController viewController: UIViewController) {
        self.addChild(viewController)
        self.view.addSubview(viewController.view)
        //let segmentBottom = self.segmentController.frame.maxY
        let segmentBottom = CGFloat(0.0)
        viewController.view.frame = CGRect(x: 0, y: segmentBottom, width: self.view.bounds.width, height: self.view.bounds.height - segmentBottom)
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    func selectController(index: Int) {
        self.segmentController.selectedSegmentIndex = index
        updateChildViewController()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

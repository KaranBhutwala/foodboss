//
//  ViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 06/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class ViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    private var ArrayList = NSMutableArray()
    private var SelectedLanguage = ""
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var viewLanguage: UIView!
    
    var defaultLanguage = ""
    var fromProfile = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        
        
        if let appLaunch = UserDefaults.standard.string(forKey: INITIAL_LAUNCH)
        {
            if appLaunch == "true"
            {
                print("APP ALREADY LAUNCH ==>>")
                
                ISLOGIN = "NO"
                if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
                {
                    print("USER ALREADY LOGGED IN ==>>")
                    
                    if GlobalsVariable.sharedInstance.isComeFromMenuAddress == "true"
                    {
                        print("APP COME FROM MENU ==>>")
                    }
                    else
                    {
                        print("THIS SCENARIO RUN EVERY TIME ==>>")
                        
                        ISLOGIN = "YES"
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressPickerVC") as? AddressPickerVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
                else
                {
                    print("USER NOT LOGGED IN ==>>")
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                    self.navigationController?.pushViewController(vc!, animated: true)

                   // let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                   // self.navigationController?.pushViewController(vc!, animated: true)
                    
                }
            }
            else
            {
                print("APP LAUNCH FIRST TIME ==>>")
            }
        }
        
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.defaultLanguage = GlobalMethod.selectedLanguageForUser()
        
        self.btnContinue.layoutIfNeeded()
        self.btnContinue.roundCorners([.topLeft, .topRight], radius: 15)
        
        self.viewLanguage.layoutIfNeeded()
        self.viewLanguage.roundCorners([.topLeft, .topRight], radius: 15)
        
        self.btnContinue.clipsToBounds = true
        self.tblView.registerNib(LanguageSelectionTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 50
        self.FetchLanguage()
    }
    
   
    
    @IBAction func ActionContinue(_ sender: Any) {
        if self.defaultLanguage == self.SelectedLanguage {
            updateDeviceLanguage()
            moveToLoginViewController()
        } else {
            updateDeviceLanguage()
            if fromProfile {
                let dialog = UIAlertController(title: "", message: "Please restart the app to reflact changes", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (vc) in
                    self.moveToLoginViewController()
                }
                dialog.addAction(okAction)
                self.present(dialog, animated: true, completion: nil)
            } else {
                GlobalMethod.popDialog(controller: self, title: "", message: "Please restart the app to reflact changes")
            }
        }
    }
    
    func updateDeviceLanguage() {
        var languageList = UserDefaults.standard.array(forKey: "AppleLanguages") as? [String] ?? []
        let langCode = GlobalMethod.getLanguageCodeFrom(language: self.SelectedLanguage)
        var isFound = false
        var index = 0
        for (i, language) in languageList.enumerated() {
            let subLang = language.components(separatedBy: "-")
            if subLang.first == langCode {
                isFound = true
                index = i
                break
            }
        }
        if !isFound {
            languageList.insert(langCode, at: 0)
        } else {
            languageList.swapAt(0, index)
        }
        UserDefaults.standard.set(languageList, forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
    }
    
    func moveToLoginViewController() {
        
        if GlobalsVariable.sharedInstance.isComeFromMenuAddress == "true"
        {
            print("APP COME FROM MENU SO MOVE TO DASHBOARD ==>>")
        }
        else
        {
            print("APP INITIAL LAUNCH SO MOVE TO DETECT LOCATION ==>>")

            NSUSERDEFAULT.set("true", forKey: INITIAL_LAUNCH)
            NSUSERDEFAULT.set("", forKey: SESSION_USER_ID)                           
            NSUSERDEFAULT.set(UUID().uuidString, forKey: SAVED_CART_SESSION)

            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            self.navigationController?.pushViewController(vc!, animated: true)
            
          //  let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
          //  self.navigationController?.pushViewController(vc!, animated: true)

        }

    }
}

extension ViewController{
    func FetchLanguage(){
        if !GlobalMethod.internet(){
            
        }else{
            
            self.view.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP, "apiId": API_ID]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.LANGUAGE_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    
                    if let Data = LanguageListModel.init(dictionary: dataDict){
                        for (index,Dict) in Data.languages!.enumerated(){
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    if let str = dictTemp.value(forKey: "language") as? String {
                                        if self.defaultLanguage == str {
                                            self.SelectedLanguage = str
                                        }
                                    }
                                    self.ArrayList.add(dictTemp)
                                }
                            }
                        }
                    }
                    self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()
                }
            }
        }
    }
}

// MARK: - TableView

extension ViewController: UITableViewDelegate,SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: LanguageSelectionTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(LanguageSelectionTableViewCell.self)
        if let Dict = ArrayList.object(at: indexPath.row) as? NSDictionary{
            cell.vWObjCustomize.isHidden = true
            cell.configure(with: Dict)
            cell.imgRadio.image = #imageLiteral(resourceName: "radioUnselect")
            if let str = Dict.value(forKey: "language") as? String, self.SelectedLanguage == str{
                cell.imgRadio.image = #imageLiteral(resourceName: "radioSelect")
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = ArrayList.object(at: indexPath.row) as? NSDictionary{
            if let str = Dict.value(forKey: "language") as? String{
                self.SelectedLanguage = str
            }
        }
        self.tblView.reloadData()
    }
}

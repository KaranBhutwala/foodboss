//
//  RestaurantReviewVC.swift
//  FoodBoss
//
//  Created by Apple on 01/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class RestaurantReviewVC: UIViewController {
    let tableHeaderHeight: CGFloat = 250.0
    static func initiateFromStoryBoard() -> RestaurantReviewVC {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            let vc = mainStoryBoard.instantiateViewController(identifier: String(describing: RestaurantReviewVC.self)) as RestaurantReviewVC
            return vc
        } else {
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: RestaurantReviewVC.self)) as! RestaurantReviewVC
            return vc
        }
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var vcTitleLabel: UILabel!
    var collectionView: UICollectionView!
    
    var restaurntReview = RestaurantReview.getEmptyReview()
    var restaurantName = ""
    var imageList = [String]()
    
    var timer: Timer? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.setImage(#imageLiteral(resourceName: "Back_White"), for: .normal)
        self.backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: .touchUpInside)
        self.navigationView.backgroundColor = .clear
        self.vcTitleLabel.text = ""
        self.view.bringSubviewToFront(self.navigationView)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: tableHeaderHeight),
                                               collectionViewLayout: flowLayout)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isPagingEnabled = true
        self.collectionView.bounces = false
        self.collectionView.registerNib(ImageCollectionViewCell.self)
        
        self.tableView.estimatedRowHeight = 75
        self.tableView.estimatedSectionHeaderHeight = 75
        
        let tvHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: tableHeaderHeight))
        tvHeaderView.addSubview(self.collectionView)
        self.tableView.tableHeaderView = tvHeaderView
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tableView.reloadData()
        self.collectionView.reloadData()
        self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollCollectionView), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
    }
    
    @objc func scrollCollectionView() {
        if self.imageList.count > 1 {        
            let offSet = self.collectionView.contentOffset.x
            if offSet == 0.0 {
                self.collectionView.setContentOffset(CGPoint(x: offSet + self.collectionView.frame.width, y: 0), animated: true)
            } else if offSet >= self.collectionView.frame.width {
                let page = offSet / self.collectionView.frame.width
                let pageInt = Int(page)
                if pageInt == self.imageList.count - 1 {
                    self.collectionView.setContentOffset(CGPoint.zero, animated: true)
                } else {
                    self.collectionView.setContentOffset(CGPoint(x: offSet + self.collectionView.frame.width, y: 0), animated: true)
                }
            }
        }
    }

    @objc func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func changeController(_ sender: UISegmentedControl) {
        guard let masterVC = self.parent as? RestaurantDetailsVC else { return }
        masterVC.selectController(index: 1)
    }
}

extension RestaurantReviewVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "RestaurantReviewHeader") as? RestaurantReviewCell
        headerView?.ratingLabel.text = self.restaurntReview.finalReview
        headerView?.reviewLabel.text = "(\(self.restaurntReview.finalReviewCount))"
        headerView?.userNameLabel.text = "User Reviews".localized
        (headerView?.viewWithTag(5) as? UISegmentedControl)?.setTitle("Reviews".localized, forSegmentAt: 0)
        (headerView?.viewWithTag(5) as? UISegmentedControl)?.setTitle("Info".localized, forSegmentAt: 1)
        (headerView?.viewWithTag(5) as? UISegmentedControl)?.selectedSegmentIndex = 0
        (headerView?.viewWithTag(5) as? UISegmentedControl)?.addTarget(self, action: #selector(changeController(_:)), for: .valueChanged)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restaurntReview.reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RestaurantReviewCell.self)
        cell.userNameLabel.text = self.restaurntReview.reviews[indexPath.row].userName
        cell.reviewLabel.text = self.restaurntReview.reviews[indexPath.row].review
        cell.ratingLabel.text = self.restaurntReview.reviews[indexPath.row].rating
        GlobalMethod.applyShadowCardView(cell.containerView, andCorner: 10)
        return cell
    }
    
}

extension RestaurantReviewVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            if self.tableView.contentOffset.y > tableHeaderHeight && self.vcTitleLabel.text?.isEmpty == true {
                self.backButton.setImage(#imageLiteral(resourceName: "Back_White_gray"), for: .normal)
                self.vcTitleLabel.text = restaurantName
                self.navigationView.backgroundColor = .white
                self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
            } else if self.tableView.contentOffset.y <= 0 && self.vcTitleLabel.text?.isEmpty == false {
                self.backButton.setImage(#imageLiteral(resourceName: "Back_White"), for: .normal)
                self.vcTitleLabel.text = ""
                self.navigationView.backgroundColor = .clear
                self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
    }
    
}
extension RestaurantReviewVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return max(1, imageList.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(ImageCollectionViewCell.self, indexPath: indexPath)
        if self.imageList.count == 0 {
            cell.img.image = UIImage (named: "RestaurantPlaceholder")
        } else {
            let str = imageList[indexPath.item]
            let availableWidth = collectionView.frame.width
            var imgUrl:String = str + "&w=\(availableWidth)"
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            cell.img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: UIImage (named: "RestaurantPlaceholder"))
        }
        
        return cell
    }
    
    
}


extension RestaurantReviewVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: tableHeaderHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

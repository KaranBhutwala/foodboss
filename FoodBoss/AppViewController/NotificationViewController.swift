//
//  NotificationViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 07/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class NotificationViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    private var notificationList = [[String: Any]]()
    private var ArrayNotification = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 77
        self.FetchNotification()
    }
    
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Notifications".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- --- [TABLEVIEW CALLING] ---

extension NotificationViewController: UITableViewDelegate, SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: NotificationTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationTableViewCell.self),
                                                 for: indexPath) as! NotificationTableViewCell
        if cell.isSkeletonActive {
            cell.hideSkeleton()
        }
        cell.notificationTitleLabel.text = (self.notificationList[indexPath.row]["message"] as? String) ?? ""
        cell.notificationDateLabel.text = (self.notificationList[indexPath.row]["time"] as? String) ?? ""
        cell.notificationImageView.layer.cornerRadius = 12.5
        cell.notificationImageView.layer.borderColor = UIColor.black.cgColor
        cell.notificationImageView.layer.borderWidth = 0.5
        return cell
        /*if let Dict = self.ArrayNotification.object(at: indexPath.section) as? NSDictionary{
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationTableViewCell.self),
                                                     for: indexPath) as! NotificationTableViewCell

            cell.notificationImageView.layer.cornerRadius = 12.5
            cell.notificationImageView.layer.borderColor = UIColor.black.cgColor
            cell.notificationImageView.layer.borderWidth = 0.5
            if let str = Dict.value(forKey: "message") as? String{
                cell.notificationTitleLabel.text = str
            }
            if let str = Dict.value(forKey: "time") as? String{
                cell.notificationDateLabel.text = str
            }
            
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- --- [API CALLING] ---

extension NotificationViewController{
    func FetchNotification(){
        if !GlobalMethod.internet(){
            
        } else {
            self.view.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.NOTIFICATION_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                if isSuccess {
                    if let list = dataDict["notifications"] as? [[String: Any]] {
                        self.notificationList = list
                    }
                    /*if let Data = NotificationModel.init(dictionary: dataDict){
                        for Dict in Data.notifications!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayNotification.add(dictTemp)
                                }
                            }
                        }
                    }*/
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                }
                self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                self.tblView.reloadData()
            }
        }
    }
}

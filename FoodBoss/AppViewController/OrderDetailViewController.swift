//
//  OrderDetailViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 04/07/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Lottie
class OrderDetailViewController: UIViewController {
    var isPushed = false
    var LottieJson = "Burger"
    
    private var ArrayCartItems = NSMutableArray()
    private var DictRestaurant = NSMutableDictionary()
    private var DictPrice = NSMutableDictionary()
    private var RestaurantRequest = "Any Restaurant Request?"
    private var CouponCode = ""
    
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet var viewRestaurant: UIView!
    @IBOutlet weak var imgRestaurant: UIImageView!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblRestaurantArea: UILabel!
    @IBOutlet weak var lblOrderPlaced: UILabel!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var stackBottom: UIStackView!
    @IBOutlet weak var tblView: UITableView!
    
    private var DictOrder = NSDictionary()
    var OrderID = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
        //NSUSERDEFAULT.set("true", forKey: COME_FROM_REPEAT_ORDER)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
//        self.SetData()
        GlobalMethod.applyShadowCardProjectView(imgRestaurant, andCorner: 5)
        
        tblView.tableHeaderView = viewRestaurant
        tblView.registerNib(OrderSummaryTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        self.FetchDetails()
    }
    
   
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Order Summary".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }

    func SetData(){
        print(DictOrder)
        lblOrderPlaced.layer.cornerRadius = 5
        self.ArrayCartItems = NSMutableArray()
        if let str = self.DictOrder.value(forKey: "logo") as? String{
            var imgUrl:String = str + "&w=\(70)"
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            imgRestaurant.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
        }
        imgStar.isHidden = true
        lblRate.isHidden = true
        if let Rate = self.DictOrder.value(forKey: "restaurant_rate") as? String,!Rate.isEmpty{
            lblRate.text = Rate
            imgStar.isHidden = false
            lblRate.isHidden = false
            self.stackBottom.removeArrangedSubview(self.btnReview)
            self.btnReview.isHidden = true
            self.btnRepeat.layoutIfNeeded()
            self.stackBottom.layoutIfNeeded()
        }
        
        self.btnRepeat.layoutIfNeeded()
        self.btnReview.layoutIfNeeded()
        self.btnRepeat.roundCorners([.topLeft, .topRight], radius: 15)
        self.btnReview.roundCorners([.topLeft, .topRight], radius: 15)
        
        // Manthan 4-9-2020
        // Dynamic text sieze
        self.btnRepeat.titleLabel?.minimumScaleFactor = 0.5
        self.btnRepeat.titleLabel?.numberOfLines = 1
        self.btnRepeat.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.btnReview.titleLabel?.minimumScaleFactor = 0.5
        self.btnReview.titleLabel?.numberOfLines = 1
        self.btnReview.titleLabel?.adjustsFontSizeToFitWidth = true
        
        if let str = self.DictOrder.value(forKey: "restaurant_name") as? String{
            self.lblRestaurantName.text = str
        }
        
        if let str = self.DictOrder.value(forKey: "location") as? String{
            self.lblRestaurantArea.text = str
        }
        
        if let str = self.DictOrder.value(forKey: "status") as? String{
            lblOrderPlaced.text = "  " + str.capitalized + "  "
        }
        
        if let ArrItems = self.DictOrder.value(forKey: "order_items") as? NSArray{
            for Dict in ArrItems{
                if let dic = Dict as? NSDictionary{
                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                        self.ArrayCartItems.add(dictTemp)
                    }
                }
            }
        }
        
        self.tblView.reloadData()
    }
    
    
    func ActionDeliveryLocation() {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                View.FromCart = true
                View.DeliveryLocation = { (Dict : NSDictionary)in
                    print(Dict)
                    NSUSERDEFAULT.set(Dict, forKey: DELIVERY_LOCATION)
                }
                self.navigationController?.pushViewController(View, animated: true)
//                self.tblView.reloadSections(1, with: .automatic)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                View.FromCart = true
                View.DeliveryLocation = { (Dict : NSDictionary)in
                    print(Dict)
                    NSUSERDEFAULT.set(Dict, forKey: DELIVERY_LOCATION)
//                    self.tblView.reloadSections(1, with: .automatic)
                }
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }//DELIVERY_LOCATION
    
    func ActionCoupon() {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "PromoCodeViewController") as? PromoCodeViewController{
                View.Coupon = { (Promo : String)in
                    print(Promo)
                    self.CouponCode = Promo
                }
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "PromoCodeViewController") as? PromoCodeViewController{
                View.Coupon = { (Promo : String)in
                    print(Promo)
                    self.CouponCode = Promo
                }
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func ActionPay(_ sender: Any) {
        let DictParameter = NSMutableDictionary()
        if RestaurantRequest != "Any Restaurant Request?" && !RestaurantRequest.isEmpty{
            DictParameter.setValue(RestaurantRequest, forKey: "delivery_note")
        }
        DictParameter.setValue(UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!, forKey: "cart_session")
        DictParameter.setValue("", forKey: "payment_method")
        DictParameter.setValue(self.CouponCode, forKey: "coupon_code")
        DictParameter.setValue(USERID, forKey: "user_id")
        DictParameter.setValue(API_ID, forKey: "apiId")
        DictParameter.setValue(FROMAPP, forKey: "from_app")
        
        if let DictLocation = NSUSERDEFAULT.value(forKey: DELIVERY_LOCATION) as? NSDictionary
        {
            if let str = DictLocation.value(forKey: "address_id") as? String{
                DictParameter.setValue(str, forKey: "address_id")
            }
        }
    }
    
    @IBAction func ActionRepeatOrder(_ sender: Any) {
        print(DictOrder)

        if let str = DictOrder.value(forKey: "order_id") as? String{
            self.ReOrder(OrderID: str)
        }
        if let str = DictOrder.value(forKey: "order_id") as? Int{
            self.ReOrder(OrderID: String (describing: str))
        }
    }
    
    @IBAction func ActionReview(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "ReviewOrderViewController") as? ReviewOrderViewController{
                View.ArrayItems = ArrayCartItems
                View.OrderID = self.OrderID
                View.Review = { (Yey : Bool)in
                    if Yey{self.FetchDetails()}
                }
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "ReviewOrderViewController") as? ReviewOrderViewController{
                View.ArrayItems = ArrayCartItems
                View.OrderID = self.OrderID
                View.Review = { (Yey : Bool)in
                    if Yey{self.FetchDetails()}
                }
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
}

// MARK: - TableView

extension OrderDetailViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.ArrayCartItems.count
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeue(OrderSummaryTableViewCell.self)
            if let Dict = ArrayCartItems.object(at: indexPath.row) as? NSDictionary{
                cell.configure(with: Dict)
            }
            cell.selectionStyle = .none
            return cell
        }
        else{
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellOrderDetails")
                let lblOrderNumber = cell?.viewWithTag(1) as! UILabel
                let lblPayment = cell?.viewWithTag(2) as! UILabel
                let lblDate = cell?.viewWithTag(3) as! UILabel
                let lblDeliveryNew = cell?.viewWithTag(4) as! UILabel
                
                if let str = self.DictOrder.value(forKey: "order_number") as? String{
                    lblOrderNumber.text = ": " + str
                }
                if let paymentType = self.DictOrder.value(forKey: "payment_type_name") as? String {
                    lblPayment.text = ": \(paymentType)"
                }
               
                // Manthan 28-9-2020
                // Do not show address if Order Type is Takeaway
                /*if let DictAddress = self.DictOrder.value(forKey: "address") as? NSDictionary{
                    if let complete_location = DictAddress.value(forKey: "complete_location") as? String{
                        if let current_location = DictAddress.value(forKey: "current_location") as? String{
                            lblDeliveryNew.text = ": " + complete_location + ", " + current_location
                        }
                    }
                }*/
                if let orderType = self.DictOrder["order_type"] as? String {
                    if orderType == "1"{
                        //Takeaway
                        lblDeliveryNew.text = ": " + "Takeaway".localized
                    } else {
                        //Delivery
                        let addressDetails = (self.DictOrder["address"] as? [String: Any]) ?? [String: Any]()
                        let completeLocation = addressDetails["complete_location"] as? String ?? ""
                        let currentLocation = addressDetails["current_location"] as? String ?? ""
                        lblDeliveryNew.text = ": \(completeLocation), \(currentLocation)"
                    }
                }
                //
                if let str = self.DictOrder.value(forKey: "order_date") as? String{
                    lblDate.text = ": " + str
                }
                
                cell?.selectionStyle = .none
                return cell!
            }
            else if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellBillDetails")
                let lblItemTotal = cell?.viewWithTag(1) as! UILabel
                let lblAddonTotal = cell?.viewWithTag(2) as! UILabel
                let lblTaxes = cell?.viewWithTag(3) as! UILabel
                let lblDeliveryCharge = cell?.viewWithTag(4) as! UILabel
//                let lblDiscount = cell?.viewWithTag(5) as! UILabel
                let lblTotal = cell?.viewWithTag(6) as! UILabel
                let tipAmountLabel = cell?.viewWithTag(12) as! UILabel
                
                tipAmountLabel.text = CURRANCY + " " + (self.DictOrder["tip"] as? String ?? "0")
                
                if let str = self.DictOrder.value(forKey: "sub_total") as? String{
                    lblItemTotal.text = CURRANCY + " " + str
                }
                if let str = self.DictOrder.value(forKey: "addon_total") as? String{
                    lblAddonTotal.text = CURRANCY + " " + str
                }
                if let str = self.DictOrder.value(forKey: "tax") as? String{
                    lblTaxes.text = CURRANCY + " " + str
                }
                if let str = self.DictOrder.value(forKey: "delivery_charge") as? String, str != "0"{
                    lblDeliveryCharge.text = CURRANCY + " " + str
                    lblDeliveryCharge.textColor = UIColor.black
                }
                else{
                    lblDeliveryCharge.text = "Free".localized
                    lblDeliveryCharge.textColor = COLOR_GREEN
                }
//                if let str = self.DictOrder.value(forKey: "discount_amount") as? String{
//                    lblDiscount.text = CURRANCY + " " + str
//                }
                if let str = self.DictOrder.value(forKey: "grand_total") as? String{
                    lblTotal.text = CURRANCY + " " + str
                }
                
                cell?.selectionStyle = .none
                return cell!
            }
            return UITableViewCell()
        }
        
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                //return 250
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}



//MARK:- --- [API CALLING] ---

extension OrderDetailViewController{
    
    func FetchDetails(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                        "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                        "order_id":OrderID,
                                        "apiId":API_ID,
                                        "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ORDER_DETAILS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let Dict = dataDict.value(forKey: "order") as? NSDictionary{
                        self.DictOrder = Dict
                        self.SetData()
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    
//
    func ReOrder(OrderID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                        "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                        "order_id":OrderID,
                                        "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                        "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.REORDER, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if #available(iOS 13.0, *) {
                        if let View = self.storyboard?.instantiateViewController(identifier: "CartViewController") as? CartViewController{
                            View.isPushed = true
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    } else {
                        if let View = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController{
                            View.isPushed = true
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

//
//  LoginViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 07/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

enum LoginType: String {
    case phone = "Phone Number", facebook = "Facebook", google = "Google"
}

class LoginViewController: UIViewController {
    var alertVC: CPAlertVC!

    @IBOutlet weak var viewNumber: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewGoogle: UIView!
    @IBOutlet weak var viewFacebook: UIView!
    var receivedUserID :String?

    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var txtNumber: UITextField!
    private var selectedLoginType: LoginType = .phone
    
    @IBOutlet weak var bottomView: UIView!
    private var strCountryCode = "+91"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        
        let backgroundImage = UIImage.init(named: "Background.png")
        let backgroundImageView = UIImageView.init(frame: self.view.frame)
        backgroundImageView.image = backgroundImage
        //backgroundImageView.contentMode = .scaleAspectFill
        //backgroundImageView.alpha = 0.1
        self.view.insertSubview(backgroundImageView, at: 0)
        
       // bottomView.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTappedGoogleSignIn(tapGesture:)))
        self.viewGoogle.addGestureRecognizer(tapGesture)
        
        let fbTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTappedFaceBookSignIn(tapGesture:)))
        self.viewFacebook.addGestureRecognizer(fbTapGesture)
        
        GlobalMethod.applyWhiteShadowCardView(viewNumber, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(viewGoogle, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(viewFacebook, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(viewEmail, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(viewLogin, andCorner: 10)

        
        btnSubmit.layer.cornerRadius = btnSubmit.frame.height/2
        btnSubmit.layer.borderWidth = 1
        btnSubmit.layer.borderColor = UIColor.white.cgColor
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        tabBarController?.tabBar.isHidden = true

    }
    
    @IBAction func btnSkip(_ sender: UIButton) {
        
        
        if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
        {
            if #available(iOS 13.0, *) {
                let keyWindow = UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .map({$0 as? UIWindowScene})
                    .compactMap({$0})
                    .first?.windows
                    .filter({$0.isKeyWindow}).first
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                
                UIView.transition(with: (keyWindow)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                    keyWindow?.rootViewController = initialViewController
                }, completion: nil)
            } else {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                APP!.window?.rootViewController = initialViewController
            }
        }
        else
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
            self.navigationController?.pushViewController(vc!, animated: true)

        }
        
        
    }
    
    @IBAction func CloseBottomView(_ sender: UIButton)
    {
        bottomView.isHidden = true
    }
    @IBAction func LoginWithEmail(_ sender: UIButton)
    {
        bottomView.isHidden = true
           
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailLoginVC") as? EmailLoginVC
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    
    @IBAction func SignupWithEmail(_ sender: UIButton)
    {
        bottomView.isHidden = true
         let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupVC") as? SignupVC
               self.navigationController?.pushViewController(vc!, animated: true)
    }
    
   
    
    @IBAction func ActionLogin(_ sender: Any) {
        self.view.endEditing(true)
        
        if (txtNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtNumber.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter mobile number.", comment: ""))
            alertVC.show(into: self)

        }
        else if txtNumber.text!.count < 10 {
            self.txtNumber.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter valid mobile.", comment: ""))
            alertVC.show(into: self)

        }
        else{
            self.selectedLoginType = .phone
            let parameter : [String : Any] = ["mobile_no":self.txtNumber.text!,
                                              "language_id":"1",
                                              "phonecode":self.strCountryCode,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            self.SignIn(with: parameter)
        }
    }
    
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         let touch = touches.first
         if touch?.view == self.view {
            bottomView.resignFirstResponder()
        }
    }
    
    @IBAction func EmailLogin(_ sender: UIButton)
    {
        addBottomSheetView()
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
                bottomView.isHidden = true
    }
    
    
    func addBottomSheetView() {
        let bottomSheetVC = LoginPopup()
        
        self.addChild(bottomSheetVC)
        self.view.addSubview(bottomSheetVC.view)
        bottomSheetVC.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC.view.frame = CGRect(x: 0, y: view.frame.maxY, width: width, height: height)
    }
    
    @IBAction func ActionCountry(_ sender: UIButton) {
        if let View = self.storyboard?.instantiateViewController(withIdentifier: "CountryPickerViewController") as? CountryPickerViewController{
            View.SelectedCountry = { (Dict : NSDictionary)in
                if let strDialCode = Dict.value(forKey: "phonecode") as? String{
                    if let strname = Dict.value(forKey: "name") as? String{
                        self.strCountryCode = "+" + strDialCode
                        sender.setTitle("+" + strDialCode, for: .normal)
                    }
                }
            }
            let navigationController = UINavigationController(rootViewController: View)
            navigationController.modalPresentationStyle = .formSheet
            self.present(navigationController, animated: true){
            }
        }
    }
    
    @objc func didTappedGoogleSignIn(tapGesture: UITapGestureRecognizer) {
        self.selectedLoginType = .google
        GIDSignIn.sharedInstance()?.signIn()
        
      //  let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmailLoginVC") as? EmailLoginVC
      //  self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @objc func didTappedFaceBookSignIn(tapGesture: UITapGestureRecognizer) {
        
       // let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupVC") as? SignupVC
      //  self.navigationController?.pushViewController(vc!, animated: true)

        self.selectedLoginType = .facebook
        let fbLoginManager = LoginManager()
        fbLoginManager.logIn(
            permissions: ["public_profile"],
            viewController: self)
        { (loginResult) in
            switch loginResult {
            case .success(let granted, _, _):
                if granted.contains(.publicProfile) {
                    self.getFacebookProfile()
                }
            case .cancelled:
                print("\nFacebook SignIn Cancelled\n")
            case .failed(let error):
                print("\nFacebook SignIn Error:\t\(error.localizedDescription)\n")
                if #available(iOS 13.0, *) {
                    GlobalMethod.ShowToast(Text: error.localizedDescription, View: (SCENE?.window)!)
                } else {
                    GlobalMethod.ShowToast(Text: error.localizedDescription, View: (APP?.window)!)
                }
            }
        }
    }
    
    func getFacebookProfile() {
        GraphRequest(graphPath: "me").start { (connection, result, error) in
            if let _ = error {
                print("\nFacebook Graph Error:\t\(error!.localizedDescription)\n")
                if #available(iOS 13.0, *) {
                    GlobalMethod.ShowToast(Text: error!.localizedDescription, View: (SCENE?.window)!)
                } else {
                    GlobalMethod.ShowToast(Text: error!.localizedDescription, View: (APP?.window)!)
                }
            } else {
                if let userData = result as? [String: Any] {
                    print("\nFacebook Graph Data:\n\(userData)\n")
                } else {
                    print("\nFacebook Graph UserData nil\n")
                }
            }
        }
    }
    
}

//MARK:- Google Sign In
extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let _ = error {
            if (error as NSError).code == -5 {
                print("\nUser pressed Cancell button\n")
            } else {
//                if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {}
                print("\nGoogle SignIn Error:\t\(error.localizedDescription)\n")
                if #available(iOS 13.0, *) {
                    GlobalMethod.ShowToast(Text: error.localizedDescription, View: (SCENE?.window)!)
                } else {
                    GlobalMethod.ShowToast(Text: error.localizedDescription, View: (APP?.window)!)
                }
            }
        } else {
            var loginParams = [String: Any]()
            loginParams["login_type"] = self.selectedLoginType.rawValue
            loginParams["first_name"] = user.profile.givenName
            loginParams["last_name"] = user.profile.familyName
            loginParams["email"] = user.profile.email
            loginParams["language_id"] = "1"
            loginParams["apiId"] = API_ID
            loginParams["from_app"] = FROMAPP
            loginParams["profile_pic"] = user.profile.imageURL(withDimension: 640)
            
            self.socialSignIn(with: loginParams)
        }
    }
}

//MARK:- --- [API CALLING] ---

extension LoginViewController{
    
    func socialSignIn(with parameter: [String: Any]) {
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            
            print("\nSocial SignIn Parameter:\n\(parameter)\n")
            
            ServManager.viewController = self
            ServManager.callPostAPI(self.selectedLoginType == .phone ? constant.LOG_IN : constant.SOCIAL_LOG_IN,
                                    isLoaderRequired: true,
                                    paramters: parameter)
            { (isSuccess, status, msg, dataDict) in
                
                GlobalMethod.StopProgress(self.view)
                
                if isSuccess {
                    if let strMessage = dataDict["message"] as? String {
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        } else {
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        
                        if let userData = dataDict["user"] as? [String: Any] {
                            if let userId = userData["user_id"] as? String {
                                USERID = userId
                                NSUSERDEFAULT.set(USERID, forKey: USER_ID)
                                NSUSERDEFAULT.set(USERID, forKey: SESSION_USER_ID)
                                
                            }
                            
                            if let cartSession = userData["cart_session"] as? String {
                                CART_SESSION = cartSession
                                NSUSERDEFAULT.set(cartSession, forKey: SAVED_CART_SESSION)

                            }
                            
                            ISLOGIN = "Yes"
                            NSUSERDEFAULT.set(ISLOGIN, forKey: LOGIN)
                            
                            if #available(iOS 13.0, *) {
                                if let locationVc = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                                    locationVc.fromLoginVc = true
                                    self.navigationController?.pushViewController(locationVc, animated: true)
                                }
                            } else {
                                if let locationVc = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                                    locationVc.fromLoginVc = true
                                    self.navigationController?.pushViewController(locationVc, animated: true)
                                }
                            }
                            
                        }
                    }
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func SignIn(with parameter: [String: Any]) {
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            print("MOBILE LOGIN PARAMETER ==>>",parameter)
            
            ServManager.viewController = self
            ServManager.callPostAPI(self.selectedLoginType == .phone ? constant.LOG_IN : constant.SOCIAL_LOG_IN,
                                    isLoaderRequired: true,
                                    paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                
                print("MOBILE LOGIN RESPONSE ==>>",dataDict)

                if isSuccess{
                    
                    ISLOGIN = "Yes"
                    NSUSERDEFAULT.set(ISLOGIN, forKey: LOGIN)

                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        
                    }
                    
                    if let parentId = dataDict.value(forKey: "user_id") as? Int
                    {
                        USERID = String(parentId)
                        NSUSERDEFAULT.set(USERID, forKey: USER_ID)
                        NSUSERDEFAULT.set(USERID, forKey: SESSION_USER_ID)

                    }
                    else if let parentId = dataDict.value(forKey: "user_id") as? String
                    {
                        USERID = parentId
                        NSUSERDEFAULT.set(USERID, forKey: USER_ID)
                        NSUSERDEFAULT.set(USERID, forKey: SESSION_USER_ID)
                    }
                    
                   
                    if let str = dataDict.value(forKey: "cart_session") as? String, str.count != 0
                    {
                        CART_SESSION = str
                        NSUSERDEFAULT.set(str, forKey: SAVED_CART_SESSION)
                        
                    }
                    if #available(iOS 13.0, *) {
                        if let View = self.storyboard?.instantiateViewController(identifier: "OTPVerificationViewController") as? OTPVerificationViewController{
                            View.strCode = self.strCountryCode
                            View.strNumber = self.txtNumber.text!
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    } else {
                        if let View = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationViewController") as? OTPVerificationViewController{
                            View.strCode = self.strCountryCode
                            View.strNumber = self.txtNumber.text!
                            self.navigationController?.pushViewController(View, animated: true)
                        }
                    }
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

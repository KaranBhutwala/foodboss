//
//  OTPVerificationViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 17/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class OTPVerificationViewController: UIViewController {
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var txtOTP: InsetTextField!
    @IBOutlet weak var constraintOTPBottom: NSLayoutConstraint!
    @IBOutlet weak var lblOTP: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    
    @IBOutlet weak var btn: UIButton!
    private var seconds = 30 //This variable will hold a starting value of
    private var timer = Timer()
    private var isTimerRunning = false
    var strNumber = ""
    var strCode = ""
    private var arrayAddress = NSMutableArray()
    var comeFrom = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        txtOTP.layer.borderColor = COLOR_TURQUOISE!.cgColor
        txtOTP.layer.borderWidth = 0.5
        txtOTP.layer.cornerRadius = 10
        
        txtOTP.textColor = COLOR_TURQUOISE!
        
        txtOTP.textContentType = .oneTimeCode
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        
        lblNumber.text = strCode + " " + strNumber
        
        lblOTP.isHidden = true
        btnResend.isHidden = true
        
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Select Your Location".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        if seconds == 0{
            lblTimer.text = ""
            self.timer.invalidate()
            lblOTP.isHidden = false
            btnResend.isHidden = false
            constraintOTPBottom.constant = 180
        }
        else{
            lblTimer.text = String(format: "00 : %02d", seconds)
        }
    }
    
    @IBAction func ActionVerify(_ sender: Any) {
        if (txtOTP.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtOTP.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: "Please enter OTP.".localized, View: self)
        }
        else{
            if txtOTP.text == "1212"
            {
                ISLOGIN = "Yes"
                NSUSERDEFAULT.set(ISLOGIN, forKey: LOGIN)
                NSUSERDEFAULT.set(strNumber, forKey: SAVED_MOBILE_NO)

                if comeFrom == "EmptyMobile"
                {
                    VerifyOtpPlaceOrder()
                }
                else
                {
                    FetchAddresses()
                }
               
               /* if #available(iOS 13.0, *) {
                    if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }*/
                
            }
            
//            self.VerifyOtp()
        }
    }
    
}

//MARK:- --- [API CALLING] ---

extension OTPVerificationViewController{
    
    func FetchAddresses()
    {
        if !GlobalMethod.internet(){
            
        }
        else
        {
            
            let parameter : [String : Any] = ["user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!, "from_app":FROMAPP,"apiId":API_ID,"language": GlobalMethod.selectedLanguageForUser()]
            print("FETCH ADDRESS PARAMETER ==>>",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADDRESS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                print("FETCH ADDRESS RESPONSE ==>>",dataDict)
                
                if status == 1
                {
                    if isSuccess
                    {
                        self.arrayAddress.removeAllObjects()
                        if let Data = AddressModel.init(dictionary: dataDict){
                            for Dict in Data.user_addresses!{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.arrayAddress.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if self.arrayAddress.count > 0
                        {
                            
                            
                            if let Dict = self.arrayAddress.object(at: 0) as? NSDictionary{
                                if let strLat = Dict.value(forKey: "latitude") as? String{
                                    if let strLon = Dict.value(forKey: "longitude") as? String{
                                        LATTITUDE = strLat
                                        LONGITUDE = strLon
                                        
                                        let strOne = Dict.value(forKey: "area") as? String
                                        let strOne1 = Dict.value(forKey: "street") as? String
                                        
                                        ADDRESS = "\(strOne ?? "") \(strOne1 ?? "")"
                                    }
                                }
                            }
                            
                            NSUSERDEFAULT.set("true", forKey: IS_ADDRESS_ADDED)
                            NSUSERDEFAULT.set(LATTITUDE, forKey: LAT)
                            NSUSERDEFAULT.set(LONGITUDE, forKey: LONG)
                            NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS_SELECTED)
                            NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS)
                            NSUSERDEFAULT.set(ADDRESS, forKey: SAVED_ADDRESS)
                            
                            
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressPickerVC") as? AddressPickerVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                            
                        }
                        else
                        {
                            NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                        
                    }
                    else
                    {
                        GlobalMethod.ShowToast(Text: msg, View: self.view)
                        GlobalMethod.StopProgress(self.view)
                        NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                }
                else
                {
                    print("NEW USER HAVE NO ADDRESS SO MOVE TO ADD ADDRESS -->>>>")
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                    self.navigationController?.pushViewController(vc!, animated: true)

                }
            }
        }
    }
    
    func VerifyOtp(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!, "otp":"1", "from_app":FROMAPP, "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.VERIFY_OTP, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    
                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func VerifyOtpPlaceOrder(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["mobile_no": strNumber, "otp":"1212", "from_app":FROMAPP, "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.VERIFY_OTP_PLACEORDER, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                
                
                if status == 1
                {
                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                    }
                    
                    if #available(iOS 13.0, *) {
                         if let View = self.storyboard?.instantiateViewController(identifier: "CartViewController") as? CartViewController{
                            View.isPushed = false
                             self.navigationController?.pushViewController(View, animated: true)
                         }
                     } else {
                         if let View = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController{
                            View.isPushed = false
                             self.navigationController?.pushViewController(View, animated: true)
                         }
                     }
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func SendOtp(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["mobile_no":self.strNumber,
                                              "language_id":"1",
                                              "phonecode":self.strCode,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.LOG_IN, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    
                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        self.lblTimer.text = "00 : 30"
                        self.seconds = 30
                        self.timer.invalidate()
                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

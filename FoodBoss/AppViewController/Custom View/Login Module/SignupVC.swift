//
//  SignupVC.swift
//  FoodBoss
//
//  Created by IOS Development on 28/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    var iconClick = true
    var alertVC: CPAlertVC!

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtFirstNameView: UIView!
    @IBOutlet weak var txtFirstNameLine: UIView!
    
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtLastNameView: UIView!
    @IBOutlet weak var txtLastNameLine: UIView!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtEmailView: UIView!
    @IBOutlet weak var txtEmailLine: UIView!
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPasswordView: UIView!
    @IBOutlet weak var txtPasswordLine: UIView!
    
    @IBOutlet weak var btnPassword: UIButton!
    
    @IBOutlet weak var btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn.roundCorners([.topLeft, .topRight], radius: 15)
        GlobalMethod.applyWhiteShadowCardView(txtFirstNameView, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(txtLastNameView, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(txtEmailView, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(txtPasswordView, andCorner: 10)
        
    }
    
    @IBAction func btnSignup(_ sender: UIButton) {
        
        
        if (txtFirstName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtFirstName.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter first name.", comment: ""))
            alertVC.show(into: self)

        }
        else if (txtLastName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtLastName.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter last name.", comment: ""))
            alertVC.show(into: self)
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtEmail.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter email id.", comment: ""))
            alertVC.show(into: self)
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtPassword.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter Password.", comment: ""))
            alertVC.show(into: self)
        }
        else
        {
            if txtEmail.text!.isValidEmail()
            {
                if isValidPassword(txtPassword.text!)
                {
                  self.Signup()

                }
                else
                {
                    alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Enter valid password.", comment: ""))
                    alertVC.show(into: self)

                }
            }
            else
            {
                alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Enter valid email.", comment: ""))
                alertVC.show(into: self)

            }
        }
    }
    
    @IBAction func btnPasswordAction(_ sender: UIButton)
    {
        if(iconClick == true) {
            txtPassword.isSecureTextEntry = false
            let image = UIImage(named: "PlainEyes.png") as UIImage?
            btnPassword.setImage(image, for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            let image = UIImage(named: "CrossEyes.png") as UIImage?
            btnPassword.setImage(image, for: .normal)
            
        }
        
        iconClick = !iconClick
    }
    
    func isValidPassword(_ password:String) -> Bool {
       if(password.count > 7 && password.count < 17) {
       } else {
           return false
       }
       let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
       let letters = password.components(separatedBy: nonUpperCase)
       let strUpper: String = letters.joined()

       let smallLetterRegEx  = ".*[a-z]+.*"
       let samlltest = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
       let smallresult = samlltest.evaluate(with: password)

       let numberRegEx  = ".*[0-9]+.*"
       let numbertest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
       let numberresult = numbertest.evaluate(with: password)

       let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
       var isSpecial :Bool = false
       if regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, password.count)) != nil {
        print("could not handle special characters")
           isSpecial = true
       }else{
           isSpecial = false
       }
       return (strUpper.count >= 1) && smallresult && numberresult && isSpecial
    }
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
    func Signup(){
        if !GlobalMethod.internet(){
            
        }else{
            
          //  GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["email": txtEmail.text!,
                                              "password": txtPassword.text!,
                                              "first_name": txtFirstName.text!,
                                              "last_name": txtLastName.text!,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            print("sign WITH EMAIL PARAMETER",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.EMAIL_SIGN_UP, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                print(dataDict)

                if isSuccess {
                    if let strMessage = dataDict["message"] as? String {
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        } else {
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        self.navigationController?.popViewController(animated: true)

                        
                    }
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }

}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}

//
//  ForgotPasswordVC.swift
//  FoodBoss
//
//  Created by IOS Development on 28/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtEmailView: UIView!
    @IBOutlet weak var txtEmailLine: UIView!
    @IBOutlet weak var btn: UIButton!
    var alertVC: CPAlertVC!

    override func viewDidLoad() {
        super.viewDidLoad()

        btn.roundCorners([.topLeft, .topRight], radius: 15)
        GlobalMethod.applyWhiteShadowCardView(txtEmailView, andCorner: 10)
        
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
              self.txtEmail.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter email id.", comment: ""))
            alertVC.show(into: self)

        }
        
          else{
           
              self.ForgotPassword()
          }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func ForgotPassword(){
        if !GlobalMethod.internet(){
            
        }else{
            
          //  GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["email": txtEmail.text!,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            print("FORGOT PASSWORD PARAMETER",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.FORGOT_PASSWORD, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                print(dataDict)

                if isSuccess {
                    if let strMessage = dataDict["message"] as? String {
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        } else {
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        self.navigationController?.popViewController(animated: true)

                        
                    }
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }

}

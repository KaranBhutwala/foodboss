//
//  EmailLoginVC.swift
//  FoodBoss
//
//  Created by IOS Development on 28/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class EmailLoginVC: UIViewController  {
    var iconClick = true
    private var arrayAddress = NSMutableArray()
    var alertVC: CPAlertVC!

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtEmailView: UIView!
    @IBOutlet weak var txtEmailLine: UIView!
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPasswordView: UIView!
    @IBOutlet weak var txtPasswordLine: UIView!
    
    @IBOutlet weak var btnPassword: UIButton!
    
    @IBOutlet weak var btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        btn.roundCorners([.topLeft, .topRight], radius: 15)
        GlobalMethod.applyWhiteShadowCardView(txtEmailView, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(txtPasswordView, andCorner: 10)

       // txtEmail.text = "jiten.foodboss@mailinator.com"
       // txtPassword.text = "craft@2852"
    }
    
    @IBAction func btnLogin(_ sender: UIButton)
    {
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtEmail.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter email id.", comment: ""))
            alertVC.show(into: self)
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtPassword.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter password.", comment: ""))
            alertVC.show(into: self)
            
        }
        
        else{
            
            self.SignIn()
        }
    }
    
    @IBAction func btnForgotPassword(_ sender: UIButton)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func btnPasswordAction(_ sender: UIButton)
    {
        if(iconClick == true) {
            txtPassword.isSecureTextEntry = false
            let image = UIImage(named: "PlainEyes.png") as UIImage?
            btnPassword.setImage(image, for: .normal)
        } else {
            txtPassword.isSecureTextEntry = true
            let image = UIImage(named: "CrossEyes.png") as UIImage?
            btnPassword.setImage(image, for: .normal)

        }

        iconClick = !iconClick
    }
   
    func SignIn()
    {
        if !GlobalMethod.internet()
        {
            
        }
        else
        {
            
            let parameter : [String : Any] = ["email": txtEmail.text!,
                                              "password": txtPassword.text!,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]

            print("EMAIL LOGIN PARAMETER ==>>",parameter)

            ServManager.viewController = self
            ServManager.callPostAPI(constant.EMAIL_SIGN_IN, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                
                if isSuccess {
                    
                    print("EMAIL LOGIN RESPONSE ==>>",dataDict)

                    ISLOGIN = "Yes"
                    NSUSERDEFAULT.set(ISLOGIN, forKey: LOGIN)

                    
                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        
                    }
                    
                    if let json = dataDict["user"] as? [String: Any?]
                    {
                        let result = json.compactMapValues { $0}
                        print("RESULT===>>",result)
                        
                        if let DictUser = result as? NSDictionary
                        {
                            NSUSERDEFAULT.set(DictUser, forKey: USER_DATA)
                            
                            
                            if let str = DictUser.value(forKey: "mobile") as? String
                            {
                                NSUSERDEFAULT.set(str, forKey: SAVED_MOBILE_NO)
                            }
                            
                            if let parentId = DictUser.value(forKey: "user_id") as? Int
                            {
                                USERID = String(parentId)
                                NSUSERDEFAULT.set(USERID, forKey: USER_ID)
                                NSUSERDEFAULT.set(USERID, forKey: SESSION_USER_ID)
                                
                            }
                            else if let parentId = DictUser.value(forKey: "user_id") as? String
                            {
                                USERID = parentId
                                NSUSERDEFAULT.set(USERID, forKey: USER_ID)
                                NSUSERDEFAULT.set(USERID, forKey: SESSION_USER_ID)
                            }
                            
                           
                            if let str = DictUser.value(forKey: "cart_session") as? String, str.count != 0
                            {
                                CART_SESSION = str
                                NSUSERDEFAULT.set(str, forKey: SAVED_CART_SESSION)
                            }
                        }
                    }
                    
                    self.FetchAddresses()
                    
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func FetchAddresses(){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!, "from_app":FROMAPP,"apiId":API_ID,"language": GlobalMethod.selectedLanguageForUser()]
            print("FETCH ADDRESS PARAMETER ==>>",parameter)

            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADDRESS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                print("FETCH ADDRESS RESPONSE ==>>",dataDict)

                if isSuccess{
                    
                    self.arrayAddress.removeAllObjects()
                    if let Data = AddressModel.init(dictionary: dataDict){
                        for Dict in Data.user_addresses!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.arrayAddress.add(dictTemp)
                                }
                            }
                        }
                    }
                 
                    if self.arrayAddress.count > 0
                    {
                        
                        NSUSERDEFAULT.set("true", forKey: IS_ADDRESS_ADDED)
                        
                        if let Dict = self.arrayAddress.object(at: 0) as? NSDictionary{
                            if let strLat = Dict.value(forKey: "latitude") as? String{
                                if let strLon = Dict.value(forKey: "longitude") as? String{
                                    LATTITUDE = strLat
                                    LONGITUDE = strLon
                                    
                                    let street = Dict.value(forKey: "street") as? String
                                    let number = Dict.value(forKey: "number") as? String
                                    let area = Dict.value(forKey: "area") as? String
                                    let city = Dict.value(forKey: "city") as? String

                                    if (street!.isEmpty) && (number!.isEmpty)
                                    {
                                        ADDRESS = "\(area ?? "") , \(city ?? "")"

                                    }
                                    else
                                    {
                                        ADDRESS = "\(street ?? "") , \(number ?? "") , \(area ?? "") , \(city ?? "")"
                                    }
                                }
                            }
                        }
                        
                        NSUSERDEFAULT.set(LATTITUDE, forKey: LAT)
                        NSUSERDEFAULT.set(LONGITUDE, forKey: LONG)
                        NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS_SELECTED)
                        NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS)
                        NSUSERDEFAULT.set(ADDRESS, forKey: SAVED_ADDRESS)

                        
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressPickerVC") as? AddressPickerVC
                        self.navigationController?.pushViewController(vc!, animated: true)

                    }
                    else
                    {
                        NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
        }
    }
}

//
//  EmptyMobileVC.swift
//  FoodBoss
//
//  Created by IOS Development on 10/11/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class EmptyMobileVC: UIViewController {

    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var btnPickCountryOutlet: UIButton!
    
    @IBOutlet weak var lblFullAddress: UILabel!
    
    @IBOutlet weak var txtFloorName: UITextField!
    @IBOutlet weak var txtDoorbell: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtDeliveryInstruction: UITextField!
    
    @IBOutlet weak var ViewFloor: UIView!
    @IBOutlet weak var ViewDoorbell: UIView!
    @IBOutlet weak var ViewMobileNo: UIView!
    @IBOutlet weak var ViewInstruction: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addNavigation()
        btn.roundCorners([.topLeft, .topRight], radius: 15)
        GlobalMethod.applyWhiteShadowCardView(ViewFloor, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(ViewDoorbell, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(ViewMobileNo, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(ViewInstruction, andCorner: 10)
        
        let addressSelected = UserDefaults.standard.string(forKey: ADDRESS_SELECTED)!
        lblFullAddress.numberOfLines = 0
        lblFullAddress.text = addressSelected.capitalized
        lblFullAddress.font = FONT_BOLD_CUSTOM(xx: 15)
        lblFullAddress.textColor = UIColor.darkGray

    }
    
    func addNavigation()
    {
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true

        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = NSLocalizedString("Address Detail", comment: "")
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDONE(_ sender: UIButton)
    {
        if (txtFloorName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtFloorName.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: NSLocalizedString("Please enter floor name.", comment: ""), View: self)
        }
        else if (txtDoorbell.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtDoorbell.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: NSLocalizedString("Please enter doorbell.", comment: ""), View: self)
        }
        else if (txtMobileNo.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtMobileNo.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: NSLocalizedString("Please enter mobile no.", comment: ""), View: self)
        }
        else
        {
            
            let parameter : [String : Any] = ["mobile_no":self.txtMobileNo.text!,
                                              "phonecode":btnPickCountryOutlet.currentTitle!,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            self.SignIn(with: parameter)
        }
    }
    
    @IBAction func btnPickCountry(_ sender: UIButton)
    {
        
    }
    
    func SignIn(with parameter: [String: Any]) {
        if !GlobalMethod.internet(){
            
        }else{
            
           // GlobalMethod.StartProgress(self.view)
            print("REQUEST OTP PARAMETER ==>>",parameter)
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.REQUEST_OTP_PLACEORDER , isLoaderRequired: true,
                                    paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                
                print("REQUEST OTP RESPONSE ==>>",dataDict)

                if status == 1
                {
                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        
                        if #available(iOS 13.0, *) {
                            if let nextVC = self.storyboard?.instantiateViewController(identifier: "OTPVerificationViewController") as? OTPVerificationViewController{
                                nextVC.comeFrom = "EmptyMobile"
                                nextVC.strNumber = self.txtMobileNo.text!
                                self.navigationController?.pushViewController(nextVC, animated: true)
                            }
                        } else {
                            if let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationViewController") as? OTPVerificationViewController{
                                nextVC.comeFrom = "EmptyMobile"
                                nextVC.strNumber = self.txtMobileNo.text!

                                self.navigationController?.pushViewController(nextVC, animated: true)
                            }
                        }
                    }
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

//
//  AddressPickerVC.swift
//  FoodBoss
//
//  Created by IOS Development on 05/11/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class AddressPickerVC: UIViewController {
    var alertVC: CPAlertVC!

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var addPickView: UIView!
    @IBOutlet weak var btnorderOutlet: UIButton!
    @IBOutlet weak var viewLanguage: UIView!
    private var arrayAddress = NSMutableArray()

    @IBOutlet weak var txtDeliveryAddress: UITextField!
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.setNavigationBarHidden(true, animated: true)
        tabBarController?.tabBar.isHidden = true
        FetchAddresses()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //txtDeliveryAddress.text = ""
        btnorderOutlet.layer.cornerRadius = 10
        GlobalMethod.applyWhiteShadowCardView(addPickView, andCorner: 10)
        
       // FetchAddresses()
        
        
        if  UserDefaults.standard.string(forKey: IS_ADDRESS_AVAILABLE) == "true"
        {
            self.txtDeliveryAddress.text = UserDefaults.standard.string(forKey: SAVED_ADDRESS)!
        }
    }

    @IBAction func btnOrderNow(_ sender: UIButton)
    {
        if (txtDeliveryAddress.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtDeliveryAddress.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: "Please select address")
            alertVC.show(into: self)

        }
        else
        {
            if #available(iOS 13.0, *) {
                let keyWindow = UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .map({$0 as? UIWindowScene})
                    .compactMap({$0})
                    .first?.windows
                    .filter({$0.isKeyWindow}).first
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                
                UIView.transition(with: (keyWindow)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                    keyWindow?.rootViewController = initialViewController
                }, completion: nil)
            } else {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                APP!.window?.rootViewController = initialViewController
            }
        }
    }
    
    @IBAction func btnAddressPickerr(_ sender: UIButton)
    {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destVC = storyboard.instantiateViewController(withIdentifier: "ViewAddressVC") as! ViewAddressVC
       // self.present(destVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(destVC, animated: true)
    }
    
 
    func FetchAddresses(){
        if !GlobalMethod.internet(){
            
        }else{
            
           // self.view.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,"from_app":FROMAPP,"apiId":API_ID,"language": GlobalMethod.selectedLanguageForUser()]
            print("GET ADDRESS PARAMETER ==>>",parameter)
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADDRESS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                print("GET ADDRESS REPONSE ==>>",parameter)
                print("ADDRESS STATUS ==>>",status)

                if status == 1
                {
                    if  UserDefaults.standard.string(forKey: SAVED_ADDRESS) != ""
                    {
                        self.txtDeliveryAddress.text = UserDefaults.standard.string(forKey: SAVED_ADDRESS)!
                    }
                    
                    if isSuccess{
                        if let Data = AddressModel.init(dictionary: dataDict){
                            for Dict in Data.user_addresses!{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.arrayAddress.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                    }else{
                        GlobalMethod.ShowToast(Text: msg, View: self.view)
                        GlobalMethod.StopProgress(self.view)
                    }
                }
                else
                {
                    NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                    self.navigationController?.pushViewController(vc!, animated: true)

                }
                
                
               
            }
        }
    }
    
    @IBAction func btnAddAddress(_ sender: UIButton)
    {
        
    }
    
}
extension AddressPickerVC: UITableViewDelegate,SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: LocationListTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.arrayAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(LocationListTableViewCell.self)
        //cell.delegate = self
        cell.index = indexPath
        if let Dict = arrayAddress.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
            
            cell.lblAddressType.textColor = UIColor.black
            cell.imgAddressType.setImageColor(color: UIColor.black)
            
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


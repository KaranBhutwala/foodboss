//
//  AddLocationViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 13/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddLocationViewController: UIViewController {

    @IBOutlet weak var btn: UIButton!

    @IBOutlet weak var vwStreet: UIView!
    @IBOutlet weak var vwNumber: UIView!
    @IBOutlet weak var vwFloorNo: UIView!
    @IBOutlet weak var vwDoorNo: UIView!
    @IBOutlet weak var vwCity: UIView!
    @IBOutlet weak var vwArea: UIView!
    @IBOutlet weak var vwZip: UIView!

    
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var floorNumberTextField: UITextField!
    @IBOutlet weak var doorBellNameTextField: UITextField!

    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtZip: UITextField!
    
    @IBOutlet weak var imgWork: UIImageView!
    @IBOutlet weak var lblWork: UILabel!
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblHome: UILabel!
    
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var lblOther: UILabel!
    
    private var isWork = false
    private var isHome = false
    private var isOther = false
    private var address_id = ""
    
    let locationManager = CLLocationManager() 
    var latitude = 0.0
    var longitude = 0.0
    private var isDefault = false
    var isEdit = false
    var DictEdit = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTextFields()
        self.addNavigation()
        configureUI()
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()

        let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        
        if isEdit{
            self.floorNumberTextField.text = self.DictEdit["floor_number"] as? String
            self.doorBellNameTextField.text = self.DictEdit["name_on_door_bell"] as? String
            
            if let str = DictEdit.value(forKey: "latitude") as? String{
                
                if str != ""
                {
                    latitude = Double(str)!
                }
            }
            if let str = DictEdit.value(forKey: "longitude") as? String{
                if str != ""
                {
                    longitude = Double(str)!
                }
            }
            
            if let str = DictEdit.value(forKey: "address_id") as? String {
                address_id = str
            }
            
            if let str = DictEdit.value(forKey: "address_type") as? String, str == "home" {
                self.ActionHome(_:UIButton())
            }
            else if let str = DictEdit.value(forKey: "address_type") as? String, str == "work" {
                self.ActionWork(_:UIButton())
            }
            else{
                self.ActionOther(_:UIButton())
            }
            
            if let str = DictEdit.value(forKey: "street") as? String{
                txtStreet.text = str
            }
            
            if let str = DictEdit.value(forKey: "number") as? String{
                txtNumber.text = str
            }
            
            if let str = DictEdit.value(forKey: "city") as? String{
                txtCity.text = str
            }
            
            if let str = DictEdit.value(forKey: "area") as? String{
                txtArea.text = str
            }
            
            if let str = DictEdit.value(forKey: "zipcode") as? String{
                txtZip.text = str
            }
        }
    }
 
    func configureUI()
    {
        GlobalMethod.applyWhiteShadowCardView(vwStreet, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwNumber, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwFloorNo, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwDoorNo, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwCity, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwArea, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwZip, andCorner: 10)


    }
    
    @IBAction func btnRemoveAddress(_ sender: UIButton)
    {
        removeAddress()
    }
    
    func removeAddress (){
        if !GlobalMethod.internet(){
            
        }else{
            
          //  GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["address_id": address_id ,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            print("FORGOT PASSWORD PARAMETER",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.REMOVE_LOCATION, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                print(dataDict)

                if isSuccess {
                    if let strMessage = dataDict["message"] as? String {
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        } else {
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
    
        
        if isEdit
        {
            lblHeader.text = NSLocalizedString("Update address", comment: "")
        }
        else
        {
            lblHeader.text = NSLocalizedString("Add new address", comment: "")
        }
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    func setupTextFields() {
        txtStreet.placeholder = NSLocalizedString("Street", comment: "")
        txtNumber.placeholder = NSLocalizedString("Number", comment: "")
        txtCity.placeholder = NSLocalizedString("City", comment: "")
        txtArea.placeholder = NSLocalizedString("Area", comment: "")
        txtZip.placeholder = NSLocalizedString("Zip", comment: "")
        floorNumberTextField.placeholder = "Floor Number".localized
        doorBellNameTextField.placeholder = "Doorbell Name".localized
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ActionLiveLocation(_ sender: UIButton) {
        print("AddLocation")
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    
    @IBAction func ActionSwitchChange(_ sender: UISwitch) {
        if sender.isOn{
            isDefault = true
        }
        else{
            isDefault = false
        }
    }
    
    @IBAction func ActionWork(_ sender: Any) {
        if !isWork{
            isWork = true
            isHome = false
            isOther = false
            imgWork.image = #imageLiteral(resourceName: "OfficeSelected")
            imgHome.image = #imageLiteral(resourceName: "HomeUnSelected")
            imgOther.image = #imageLiteral(resourceName: "OtherUnSelected")
            lblWork.textColor = COLOR_TURQUOISE
            lblHome.textColor = UIColor.black
            lblOther.textColor = UIColor.black
        }
    }
    
    @IBAction func ActionHome(_ sender: Any) {
        if !isHome{
            isWork = false
            isHome = true
            isOther = false
            imgWork.image = #imageLiteral(resourceName: "OfficeUnSelected")
            imgHome.image = #imageLiteral(resourceName: "HomeSelected")
            imgOther.image = #imageLiteral(resourceName: "OtherUnSelected")
            lblWork.textColor = UIColor.black
            lblHome.textColor = COLOR_TURQUOISE
            lblOther.textColor = UIColor.black
        }
    }
    
    @IBAction func ActionOther(_ sender: Any) {
        if !isOther{
            isWork = false
            isHome = false
            isOther = true
            imgWork.image = #imageLiteral(resourceName: "OfficeUnSelected")
            imgHome.image = #imageLiteral(resourceName: "HomeUnSelected")
            imgOther.image = #imageLiteral(resourceName: "OtherSelected")
            lblWork.textColor = UIColor.black
            lblHome.textColor = UIColor.black
            lblOther.textColor = COLOR_TURQUOISE
        }
    }
    
    
    @IBAction func ActionSave(_ sender: Any) {
        self.AddLocation()
    }
}

extension AddLocationViewController : CLLocationManagerDelegate{
    // Add this function to your program
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        // set the value of lat and long
        latitude = location.latitude
        longitude = location.longitude
        
        self.getAddressFromLatLon(pdblLatitude: String (describing: latitude), withLongitude: String (describing: longitude))
        locationManager.stopUpdatingLocation()
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = latitude
        //21.228124
        let lon: Double = longitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                  
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country!
                    }


                    print(addressString)
                    if !self.isEdit{
                        //self.txtCurrentLocation.text = addressString
                    }
                    
              }
        })

    }
}


//MARK:- --- [API CALLING] ---

extension AddLocationViewController{
    
    func AddLocation(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            var address_type = ""
            if isWork{
                address_type = "work"
            }
            if isHome{
                address_type = "home"
            }
            if isOther{
                address_type = "other"
            }
            
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          //"current_location":txtCurrentLocation.text!,
                                         // "complete_location":txtCompleteAddress.text!,
                                          "street":txtStreet.text!,
                                          "number":txtNumber.text!,
                                          "city":txtCity.text!,
                                          "area":txtArea.text!,
                                          "zipcode":txtZip.text!,
                                          "latitude":latitude,
                                          "longitude":longitude,
                                          "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "address_id":address_id,
                                          "address_type":address_type,
                                          "is_default":isDefault,
                                          "name_on_door_bell": self.doorBellNameTextField.text!,
                                          "floor_number": self.floorNumberTextField.text!]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADD_LOCATION, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    
                    if let strMessage = dataDict.value(forKey: "message") as? String{
                        if #available(iOS 13.0, *) {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

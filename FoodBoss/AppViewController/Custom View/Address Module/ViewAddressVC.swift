//
//  ViewAddressVC.swift
//  FoodBoss
//
//  Created by IOS Development on 05/11/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class addressCell : UITableViewCell
{
    @IBOutlet weak var imgAddressType: UIImageView!
    @IBOutlet weak var lblAddressType: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var viewInner: UIView!
}

class ViewAddressVC: UIViewController {

    private var arrayAddress = NSMutableArray()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    var FromDashboard = false
    var FromCart = false
    var fromLoginVc = false

    var DeliveryLocation: ((_ DictLocation: NSDictionary) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
        tblView.showAnimatedGradientSkeleton()

        addNavigation()
        self.btnAdd.layoutIfNeeded()
        self.btnAdd.roundCorners([.topLeft, .topRight], radius: 15)
        self.FetchAddresses()
        
    }
    
    @IBAction func btnAdd(_ sender: Any)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    func FetchAddresses(){
        if !GlobalMethod.internet(){
            
        }else{
            
            //self.view.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,"from_app":FROMAPP,"apiId":API_ID,"language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADDRESS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                if isSuccess{
                    if let Data = AddressModel.init(dictionary: dataDict){
                        for Dict in Data.user_addresses!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.arrayAddress.add(dictTemp)
                                }
                            }
                        }
                    }
                 
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
                self.tblView.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                self.tblView.reloadData()
            }
        }
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = NSLocalizedString("Select Address", comment: "")
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
     
        if self.fromLoginVc || self.navigationController?.viewControllers.count == 1 {
            if let list = self.navigationController?.viewControllers {
                self.navigationController?.viewControllers = [list.last!]
            }
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem (customView: viewHeader)]
        } else {
            self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        }
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ViewAddressVC: UITableViewDelegate,SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "addressCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.arrayAddress.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "addressCell") as! addressCell
        
        cell.viewInner.layer.cornerRadius = 10
        cell.viewInner.layer.borderColor = UIColor.lightGray.cgColor
        cell.viewInner.layer.borderWidth = 0.5

        if let Dict = arrayAddress.object(at: indexPath.row) as? NSDictionary{
            
            print(Dict)
            cell.lblAddressType.textColor = UIColor.black
            cell.imgAddressType.setImageColor(color: UIColor.black)

            if let str = Dict.value(forKey: "address_type") as? String{
                cell.lblAddressType.text = str.capitalized
                
                if str == "other"{
                    cell.imgAddressType.image = #imageLiteral(resourceName: "OtherSelected")
                }
                else if str == "home"{
                    cell.imgAddressType.image = #imageLiteral(resourceName: "HomeSelected")
                }
                else{
                    cell.imgAddressType.image = #imageLiteral(resourceName: "OfficeSelected")
                }
            }
            
            let street = Dict.value(forKey: "street") as? String
            let area = Dict.value(forKey: "area") as? String
            
            cell.lblAddress.numberOfLines = 2
            cell.lblAddress.text = "\(street ?? "") \n\(area ?? "")"
            
            let is_default = Dict.value(forKey: "is_default") as? String

            if is_default == "1"
            {
                cell.lblAddressType.textColor = COLOR_ORANGE!
                cell.imgAddressType.setImageColor(color: COLOR_ORANGE!)

            }
           
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        GlobalsVariable.sharedInstance.isComeFromMenuAddress = "false"
        if let Dict = arrayAddress.object(at: indexPath.row) as? NSDictionary{
            print(Dict)

            if let strLat = Dict.value(forKey: "latitude") as? String{
                if let strLon = Dict.value(forKey: "longitude") as? String{
                    LATTITUDE = strLat
                    LONGITUDE = strLon
                    
                    let street = Dict.value(forKey: "street") as? String
                    let number = Dict.value(forKey: "number") as? String
                    let area = Dict.value(forKey: "area") as? String
                    let city = Dict.value(forKey: "city") as? String

                    
                    
                    ADDRESS = "\(street ?? "") , \(number ?? ""), \(area ?? ""), \(city ?? "")"
                    
                    
                    NSUSERDEFAULT.set(LATTITUDE, forKey: LAT)
                    NSUSERDEFAULT.set(LONGITUDE, forKey: LONG)
                    NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS_SELECTED)
                    NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS)
                    NSUSERDEFAULT.set(ADDRESS, forKey: SAVED_ADDRESS)

                    
                    self.navigationController?.popViewController(animated: true)

                }
            }
        }
    }
}
extension UIView {

  
  
}

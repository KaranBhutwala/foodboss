//
//  InitialLocationVC.swift
//  FoodBoss
//
//  Created by IOS Development on 29/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class InitialLocationVC: UIViewController , CLLocationManagerDelegate {

    var strCountry = String()
    var strPostalCode = String()
    var strCityName = String()
    var strStateName = String()
    var strLocality = String()

    
    @IBOutlet weak var btnAdd: UIButton!
    var strLat = ""
    var strLongitude = ""

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblAddress: UILabel!
    let locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
        
        addNavigation()
        
        self.btnAdd.roundCorners([.topLeft, .topRight], radius: 15)
        self.locationManager.requestWhenInUseAuthorization()


        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()

            mapView.showsUserLocation = true
        }
        
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = NSLocalizedString("Select location", comment: "")
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func locationManager(_ locationManager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        print("locations = \(userLocation.coordinate.latitude) \(userLocation.coordinate.longitude)")
        
        strLat = "\(userLocation.coordinate.latitude)"
        strLongitude = "\(userLocation.coordinate.longitude)"

        locationManager.stopUpdatingLocation()

       // let cooridinate = CLLocationCoordinate2D(latitude: 23.0071 , longitude: 72.5055)
        let cooridinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude , longitude: userLocation.coordinate.longitude)
        let  spanDegree = MKCoordinateSpan(latitudeDelta: 0.2,longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: cooridinate , span: spanDegree)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = cooridinate
        //annotation.title = "Raleigh"
       // annotation.subtitle = "North Carolina"
        mapView.addAnnotation(annotation)
        let cityCoords = CLLocation(latitude: 23.0071, longitude: 72.5055)
        getAdressName(coords: cityCoords)

    }
    func getAdressName(coords: CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    if place.thoroughfare != nil {
                        adressString = adressString + place.thoroughfare! + ", "
                    }
                    if place.subThoroughfare != nil {
                        adressString = adressString + place.subThoroughfare! + "\n"
                        self.strLocality = place.country!

                    }
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                        self.strCityName = place.locality!
                    }
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + "\n"
                        self.strPostalCode = place.postalCode!

                    }
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                        self.strStateName = place.subAdministrativeArea!

                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                        self.strCountry = place.country!

                    }
                    
                    self.lblAddress.text = adressString
                }
            }
        }
    }
    
    @IBAction func SaveLocation(_ sender: UIButton)
    {
        
        NSUSERDEFAULT.set(self.strLat, forKey: searchLat)
        NSUSERDEFAULT.set(self.strLongitude, forKey: searchLong)
        NSUSERDEFAULT.set(self.strLocality, forKey: searchArea)
        NSUSERDEFAULT.set(self.strPostalCode, forKey: searchZip)
        NSUSERDEFAULT.set(self.strCityName, forKey: searchCitys)
        NSUSERDEFAULT.set(self.strStateName, forKey: searchStates)
        NSUSERDEFAULT.set(self.strCountry, forKey: searchCountrys)

        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "AddAddressVC") as? AddAddressVC{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as? AddAddressVC{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
        
    }
}

//
//  LocationListViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 13/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class LocationListViewController: UIViewController {

    private var arrayAddress = NSMutableArray()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    var FromDashboard = false
    var FromCart = false
    var fromLoginVc = false
    
    var DeliveryLocation: ((_ DictLocation: NSDictionary) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addNavigation()
        self.tblView.registerNib(LocationListTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 110
        self.btnAdd.layoutIfNeeded()
        self.btnAdd.roundCorners([.topLeft, .topRight], radius: 15)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        self.FetchAddresses()
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = NSLocalizedString("Manage Addresses", comment: "")
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
     
        if self.fromLoginVc || self.navigationController?.viewControllers.count == 1 {
            if let list = self.navigationController?.viewControllers {
                self.navigationController?.viewControllers = [list.last!]
            }
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem (customView: viewHeader)]
        } else {
            self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        }
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ActionAddAddress(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "DetectLocationVC") as? DetectLocationVC{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    func ClearCart(){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["cart_session": UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                              "from_app":FROMAPP,
                                              "apiId":API_ID]
            
            
            print("CLEAR CART PARAMETER  ==>>",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.CLEAR_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                print("CLEAR CART RESPONSE  ==>>",dataDict)
               
                if isSuccess
                {

                    if GlobalsVariable.sharedInstance.isComeFromCart == "true"
                    {
                        
                        GlobalsVariable.sharedInstance.isComeFromCart = "false"
                        if #available(iOS 13.0, *) {
                            if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? CartViewController{
                                self.navigationController?.pushViewController(View, animated: true)
                            }
                        } else {
                            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? CartViewController{
                                self.navigationController?.pushViewController(View, animated: true)
                            }
                        }
                    }
                    else
                    {
                        if #available(iOS 13.0, *) {
                            let keyWindow = UIApplication.shared.connectedScenes
                                .filter({$0.activationState == .foregroundActive})
                                .map({$0 as? UIWindowScene})
                                .compactMap({$0})
                                .first?.windows
                                .filter({$0.isKeyWindow}).first
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                            
                            UIView.transition(with: (keyWindow)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                                keyWindow?.rootViewController = initialViewController
                            }, completion: nil)
                        } else {
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                            APP!.window?.rootViewController = initialViewController
                        }
                    }
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
   
}

// MARK: - TableView

extension LocationListViewController: UITableViewDelegate,SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return String(describing: LocationListTableViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.arrayAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(LocationListTableViewCell.self)
        cell.delegate = self
        cell.index = indexPath
        if let Dict = arrayAddress.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
            
            cell.lblAddressType.textColor = UIColor.black
            cell.imgAddressType.setImageColor(color: UIColor.black)
            if FromCart{
                
            }
            else{
                if let str = Dict.value(forKey: "complete_location") as? String, ADDRESS == str{
                    cell.lblAddressType.textColor = COLOR_ORANGE!
                    cell.imgAddressType.setImageColor(color: COLOR_ORANGE!)
                }
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        GlobalsVariable.sharedInstance.isComeFromMenuAddress = "false"
        
        if let Dict = arrayAddress.object(at: indexPath.row) as? NSDictionary{
            if let strLat = Dict.value(forKey: "latitude") as? String{
                if let strLon = Dict.value(forKey: "longitude") as? String{
                    LATTITUDE = strLat
                    LONGITUDE = strLon
                    
                    let street = Dict.value(forKey: "street") as? String
                    let number = Dict.value(forKey: "number") as? String
                    let area = Dict.value(forKey: "area") as? String
                    let city = Dict.value(forKey: "city") as? String

                    if (street!.isEmpty) && (number!.isEmpty)
                    {
                        ADDRESS = "\(area ?? "") , \(city ?? "")"

                    }
                    else
                    {
                        ADDRESS = "\(street ?? "") , \(number ?? "") , \(area ?? "") , \(city ?? "")"
                    }
                    
                   // let mobile = Dict.value(forKey: "mobile") as? String

                    if FromCart{
                        if let block = self.DeliveryLocation {
                            block(Dict)
                            self.navigationController?.popViewController(animated: true)
                        }
                        return
                    }
                    NSUSERDEFAULT.set(LATTITUDE, forKey: LAT)
                    NSUSERDEFAULT.set(LONGITUDE, forKey: LONG)
                    NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS_SELECTED)
                    NSUSERDEFAULT.set(ADDRESS, forKey: ADDRESS)

                    NSUSERDEFAULT.set(ADDRESS, forKey: SAVED_ADDRESS)
                    //NSUSERDEFAULT.set(mobile, forKey: SAVED_MOBILE_NO)

                    self.ClearCart()
                   
                }
            }
        }
    }
}


// MARK: - ActionDelegate
extension LocationListViewController: AddressActionDelegate{
    
    func EditButtonPressed(_ index: IndexPath) {
        print(index.row)
        if let Dict = arrayAddress.object(at: index.row) as? NSDictionary{
            
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "AddLocationViewController") as? AddLocationViewController{
                    View.isEdit = true
                    View.DictEdit = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "AddLocationViewController") as? AddLocationViewController{
                    View.isEdit = true
                    View.DictEdit = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
}


extension LocationListViewController{
    
    func FetchAddresses(){
        if !GlobalMethod.internet(){
            
        }else{
            
            self.view.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!, "from_app":FROMAPP,"apiId":API_ID,"language": GlobalMethod.selectedLanguageForUser()]

            print("FETCH ADDRESS PARAMETER ==>>",parameter)

            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADDRESS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                print("FETCH ADDRESS RESPONSE ==>>",dataDict)
                
                if status == 1
                {
                    self.arrayAddress.removeAllObjects()
                    if let Data = AddressModel.init(dictionary: dataDict){
                        for Dict in Data.user_addresses!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.arrayAddress.add(dictTemp)
                                }
                            }
                        }
                    }
                 
                    if self.arrayAddress.count > 0
                    {
                        
                        NSUSERDEFAULT.set("true", forKey: IS_ADDRESS_AVAILABLE)
                        NSUSERDEFAULT.set("true", forKey: IS_ADDRESS_ADDED)
                        self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()

                    }
                    else
                    {
                        NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_AVAILABLE)
                        NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                        self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                        self.navigationController?.pushViewController(vc!, animated: true)

                    }
                }
                else
                {
                    self.view.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()

                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    NSUSERDEFAULT.set("false", forKey: IS_ADDRESS_ADDED)
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetectLocationVC") as? DetectLocationVC
                    self.navigationController?.pushViewController(vc!, animated: true)


                }
                
                
            }
        }
    }
}


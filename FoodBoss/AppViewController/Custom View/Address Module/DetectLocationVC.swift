//
//  DetectLocationVC.swift
//  FoodBoss
//
//  Created by IOS Development on 04/11/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import GooglePlaces

class DetectLocationVC: UIViewController {

    @IBOutlet weak var txtSearchView: UIView!
    @IBOutlet weak var DetectView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    let locationManager = CLLocationManager() // create Location Manager object
    var latitude = 0.0
    var longitude = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true

        addNavigation()

        GlobalMethod.applyWhiteShadowCardView(txtSearchView, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(DetectView, andCorner: 10)

        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    @IBAction func SearchTapped(_ sender: UIButton)
    {
        txtSearch.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)

    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = NSLocalizedString("Enter your address", comment: "")
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDetectAvtion(_ sender: UIButton)
    {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "InitialLocationVC") as? InitialLocationVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}
extension DetectLocationVC : GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
   // print("Address components: \(place.addressComponents)")

    let lat = "\(place.coordinate.latitude)"
    let lon = "\(place.coordinate.longitude)"

    
    var strCountry = String()
    var strPostalCode = String()
    var strCityName = String()
    var strStateName = String()
    var strLocality = String()

    for addressComponent in (place.addressComponents)! {
        for type in (addressComponent.types){
            
            switch(type){
            case "country":
                strCountry = addressComponent.name
                
            case "postal_code":
                strPostalCode = addressComponent.name

                
            case "administrative_area_level_2":
                strCityName = addressComponent.name

                
            case "administrative_area_level_1":
                strStateName = addressComponent.name

                
            case "sublocality":
                strLocality = addressComponent.name

            default:
                break
            }
            
        }
    }
    NSUSERDEFAULT.set(lat, forKey: searchLat)
    NSUSERDEFAULT.set(lon, forKey: searchLong)
    NSUSERDEFAULT.set(strLocality, forKey: searchArea)
    NSUSERDEFAULT.set(strPostalCode, forKey: searchZip)
    NSUSERDEFAULT.set(strCityName, forKey: searchCitys)
    NSUSERDEFAULT.set(strStateName, forKey: searchStates)
    NSUSERDEFAULT.set(strCountry, forKey: searchCountrys)

    
    
    txtSearch.text = "\(strCityName), \(strStateName), \(strCountry), \(strPostalCode)"
    
    dismiss(animated: true, completion: nil)
    
    if #available(iOS 13.0, *) {
        if let View = self.storyboard?.instantiateViewController(identifier: "AddAddressVC") as? AddAddressVC{
            self.navigationController?.pushViewController(View, animated: true)
        }
    } else {
        if let View = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as? AddAddressVC{
            self.navigationController?.pushViewController(View, animated: true)
        }
    }
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }
}
extension DetectLocationVC : CLLocationManagerDelegate{
    // Add this function to your program
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        // set the value of lat and long
        latitude = location.latitude
        longitude = location.longitude
        
        self.getAddressFromLatLon(pdblLatitude: String (describing: latitude), withLongitude: String (describing: longitude))
        locationManager.stopUpdatingLocation()
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = latitude
        //21.228124
        let lon: Double = longitude
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                  
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country!
                    }
                    
                    NSUSERDEFAULT.set(lat, forKey: searchLat)
                    NSUSERDEFAULT.set(lon, forKey: searchLong)
                   // NSUSERDEFAULT.set(strLocality, forKey: searchArea)
                   // NSUSERDEFAULT.set(strPostalCode, forKey: searchZip)
                    NSUSERDEFAULT.set(pm.locality, forKey: searchCitys)
                    NSUSERDEFAULT.set(pm.thoroughfare, forKey: searchStates)
                    NSUSERDEFAULT.set(pm.country, forKey: searchCountrys)

                    self.txtSearch.text = "\(addressString)"

              }
        })

        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "AddAddressVC") as? AddAddressVC{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as? AddAddressVC{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
}

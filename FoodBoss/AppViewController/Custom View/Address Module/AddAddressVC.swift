//
//  AddAddressVC.swift
//  FoodBoss
//
//  Created by IOS Development on 04/11/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {

    @IBOutlet weak var vwStreet: UIView!
    @IBOutlet weak var vwNumber: UIView!
    @IBOutlet weak var vwCity: UIView!
    @IBOutlet weak var vwArea: UIView!
    @IBOutlet weak var vwZip: UIView!
    @IBOutlet weak var btn: UIButton!
    var latitude = 0.0
    var longitude = 0.0
    var alertVC: CPAlertVC!

    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtZip: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
        
        addNavigation()

        GlobalMethod.applyWhiteShadowCardView(vwStreet, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwNumber, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwCity, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwArea, andCorner: 10)
        GlobalMethod.applyWhiteShadowCardView(vwZip, andCorner: 10)
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)

        
        txtCity.text = UserDefaults.standard.string(forKey: searchCitys)
        txtArea.text = UserDefaults.standard.string(forKey: searchArea)
        txtZip.text = UserDefaults.standard.string(forKey: searchZip)
        
    }
    
    @IBAction func btnContinue(_ sender: UIButton)
    {
        if (txtStreet.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtStreet.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter street name.", comment: ""))
            alertVC.show(into: self)

        }
        else if (txtNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtNumber.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter street number.", comment: ""))
            alertVC.show(into: self)

        }
        else if (txtCity.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtCity.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter city name.", comment: ""))
            alertVC.show(into: self)

        }
        else if (txtArea.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtArea.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter area.", comment: ""))
            alertVC.show(into: self)

        }
        else if (txtZip.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtZip.becomeFirstResponder()
            alertVC = CPAlertVC(title: GlobalsVariable.sharedInstance.CustomAlert, message: NSLocalizedString("Please enter zipcode.", comment: ""))
            alertVC.show(into: self)

        }
        else
        {
            let Login = UserDefaults.standard.string(forKey: LOGIN)

            if Login == "Yes"
            {
                AddLocation()
            }
            else
            {
                let completeAddress = "\(self.txtStreet.text!), \(self.txtNumber.text!),\(self.txtArea.text!), \(self.txtCity.text!), \(self.txtZip.text!)"
                NSUSERDEFAULT.set(UserDefaults.standard.string(forKey: searchLat)!, forKey: LAT)
                NSUSERDEFAULT.set(UserDefaults.standard.string(forKey: searchLong)!, forKey: LONG)
                NSUSERDEFAULT.set(completeAddress, forKey: ADDRESS_SELECTED)
                NSUSERDEFAULT.set(completeAddress, forKey: SAVED_ADDRESS)
                
                if #available(iOS 13.0, *) {
                    let keyWindow = UIApplication.shared.connectedScenes
                        .filter({$0.activationState == .foregroundActive})
                        .map({$0 as? UIWindowScene})
                        .compactMap({$0})
                        .first?.windows
                        .filter({$0.isKeyWindow}).first
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                    
                    UIView.transition(with: (keyWindow)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                        keyWindow?.rootViewController = initialViewController
                    }, completion: nil)
                } else {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                    APP!.window?.rootViewController = initialViewController
                }
               
            }
            
        }
    }
    
    func AddLocation(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
           
            let parameter : [String : Any] = ["from_app": FROMAPP,
                                              "apiId": API_ID,
                                              "current_location": "",
                                              "complete_location": "",
                                              "street":txtStreet.text!,
                                              "number":txtNumber.text!,
                                              "city":txtCity.text!,
                                              "area":txtArea.text!,
                                              "zipcode":txtZip.text!,
                                              "latitude":UserDefaults.standard.string(forKey: searchLat)!,
                                              "longitude":UserDefaults.standard.string(forKey: searchLong)!,
                                              "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "address_id":"",
                                              "address_type":"",
                                              "is_default":"1",
                                              "name_on_door_bell": "",
                                              "floor_number": ""]
            print("ADD NEW LOCATION PARAMETER==>>",parameter)
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADD_LOCATION, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    
                    let completeAddress = "\(self.txtStreet.text!), \(self.txtNumber.text!),\(self.txtArea.text!), \(self.txtCity.text!), \(self.txtZip.text!)"
                    NSUSERDEFAULT.set(UserDefaults.standard.string(forKey: searchLat)!, forKey: LAT)
                    NSUSERDEFAULT.set(UserDefaults.standard.string(forKey: searchLong)!, forKey: LONG)
                    NSUSERDEFAULT.set(completeAddress, forKey: ADDRESS_SELECTED)
                    NSUSERDEFAULT.set(completeAddress, forKey: SAVED_ADDRESS)
                    NSUSERDEFAULT.set("true", forKey: IS_ADDRESS_AVAILABLE)

                    if let strMessage = dataDict.value(forKey: "message") as? String
                    {
                        if #available(iOS 13.0, *)
                        {
                            GlobalMethod.ShowToast(Text: strMessage, View: (SCENE?.window)!)
                        }
                        else{
                            GlobalMethod.ShowToast(Text: strMessage, View: (APP?.window)!)
                        }
                        
                        if #available(iOS 13.0, *) {
                            let keyWindow = UIApplication.shared.connectedScenes
                                .filter({$0.activationState == .foregroundActive})
                                .map({$0 as? UIWindowScene})
                                .compactMap({$0})
                                .first?.windows
                                .filter({$0.isKeyWindow}).first
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                            
                            UIView.transition(with: (keyWindow)!, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                                keyWindow?.rootViewController = initialViewController
                            }, completion: nil)
                        } else {
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavTab")
                            APP!.window?.rootViewController = initialViewController
                        }
                    }
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = NSLocalizedString("Add new address", comment: "")
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

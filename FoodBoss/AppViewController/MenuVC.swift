//
//  MenuVC.swift
//  FoodBoss
//
//  Created by IOS Development on 09/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import BLTNBoard
import GoogleSignIn
import Alamofire

class menuCell : UITableViewCell
{
    @IBOutlet weak var lblMenuName: UILabel!
}

class MenuVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    lazy var bulletinManager: BLTNItemManager = {
        let rootItem: BLTNItem = self.makeLogoutPage()
        return BLTNItemManager(rootItem: rootItem)
    }()
    var arrMenu = ["Manage Address","My Order","Favourite","Payment","Login"]
    var arrMenuLoggedInUser = ["Manage Address","My Order","Favourite","Payment","App Language","Logout"]

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgProfileView: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMyAccountText: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        tabBarController?.tabBar.isHidden = true
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
       // view.backgroundColor = UIColor.white.withAlphaComponent(0.3)
       // view.backgroundColor = UIColor.black
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "Background")
        backgroundImage.contentMode = .scaleToFill
        view.insertSubview(backgroundImage, at: 0)

        setupHeaderView()
        configureUI()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    func setupHeaderView() {
        
        let userData = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary
        let firstName = (userData?["first_name"] as? String) ?? ""
        let lastName = (userData?["last_name"] as? String) ?? ""
        let userName = "\(firstName) \(lastName)"
        let userEmail = (userData?["email"] as? String) ?? ""
        let userPhone = (userData?["mobile"] as? String) ?? ""
        var imageUrl = (userData?["profile_pic"] as? String) ?? ""
        imageUrl = imageUrl.isEmpty ? "" : (imageUrl + "&w=260")
        
        imgProfile.contentMode = .scaleAspectFill

        imgProfile.sd_setImage(with: URL(string: imageUrl as String), placeholderImage: #imageLiteral(resourceName: "UserPlaceHolder"))
        
        lblUserName.text = "\(userName)\n\(userEmail)"
    }
    
    func configureUI()
    {
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        imgProfile.layer.borderWidth = 1
        imgProfile.layer.borderColor = UIColor.white.cgColor
        
        imgProfileView.layer.cornerRadius = imgProfileView.frame.width/2
        imgProfileView.layer.borderWidth = 1
        imgProfileView.layer.borderColor = UIColor.white.cgColor

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if UserDefaults.standard.string(forKey: LOGIN) == "Yes" {
             return arrMenuLoggedInUser.count
        }
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! menuCell
        cell.selectionStyle = .none
        
        if UserDefaults.standard.string(forKey: LOGIN) == "Yes" {
            cell.lblMenuName.text = arrMenuLoggedInUser[indexPath.row]
            return cell

        }
        else
        {
            cell.lblMenuName.text = arrMenu[indexPath.row]
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        GlobalsVariable.sharedInstance.isComeFromMenuAddress = "true"
        GlobalsVariable.sharedInstance.isComeFromMenu = "true"
        
        if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
        {
            GlobalsVariable.sharedInstance.isComeFromMenuAddress = "true"
            
            switch indexPath.row {
            case 0:
                
                if #available(iOS 13.0, *) {
                    
                    if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
            case 1:
                if #available(iOS 13.0, *) {
                    if let View = self.storyboard?.instantiateViewController(identifier: "OrderListViewController") as? OrderListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "OrderListViewController") as? OrderListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
            case 2:
                if #available(iOS 13.0, *) {
                    if let View = self.storyboard?.instantiateViewController(identifier: "FavRestaurantListViewController") as? FavRestaurantListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "FavRestaurantListViewController") as? FavRestaurantListViewController{
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
            case 3:
                if #available(iOS 13.0, *) {
                    if let View = self.storyboard?.instantiateViewController(identifier: "SavedPaymentMethodsViewController") as? SavedPaymentMethodsViewController {
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "SavedPaymentMethodsViewController") as? SavedPaymentMethodsViewController {
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
            case 4:
                
                if let View = self.storyboard?.instantiateViewController(identifier: "ViewController") as? ViewController {
                    View.fromProfile = true
                    self.navigationController?.pushViewController(View, animated: true)
                }
               /* var viewController = UIViewController()
                
                if #available(iOS 13.0, *) {
                    if let View = self.storyboard?.instantiateViewController(identifier: "LanguageViewController") as? ViewController {
                        viewController = View
                        View.fromProfile = true
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as? ViewController {
                        viewController = View
                        View.fromProfile = true
                    }
                }
                
                viewController.modalPresentationStyle = .popover
                
                let navigationController = UINavigationController(rootViewController: viewController)
                navigationController.modalPresentationStyle = .formSheet
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.tabBarController?.present(navigationController, animated: true, completion: nil)
                }*/
            case 5:
                bulletinManager.backgroundViewStyle = .dimmed
                bulletinManager.statusBarAppearance = .darkContent
                bulletinManager.showBulletin(above: self)
            default:
                print("\nDefault\n")
            }
        }
        else
        {
            MoveToLogin()
        }
    }
    
    func MoveToLogin()
    {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as? LoginViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    func makeLogoutPage() -> BLTNItem {
        let page = BLTNPageItem(title: "Logout".localized)
        page.image = UIImage(named: "LogoutLogo")
        
        page.descriptionText = "Are you sure you want to Logout?".localized
        page.actionButtonTitle = "Yes".localized
        page.alternativeButtonTitle = "Not now".localized
        page.isDismissable = true
        page.actionHandler = { (item: BLTNActionItem) in
            print("Action button tapped")
            
            item.manager?.dismissBulletin(animated: true)
            if let userData = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary {
                let strLoginType = userData["login_type"] as? String
                if let loginType = LoginType(rawValue: strLoginType ?? "") {
                    if loginType == .google {
                        GIDSignIn.sharedInstance()?.signOut()
                    } else if loginType == .facebook {
                        
                    } else {
                        print("\nLogin Type:\t\(loginType.rawValue)\n")
                    }
                }
            }
            
            self.clearUserData()
            
            ISLOGIN = "NO"
            self.MoveToLogin()
            
            
        }
        
        page.alternativeHandler = { (item: BLTNActionItem) in
            print("Alternative button tapped")
            item.manager?.dismissBulletin(animated: true)
        }
        
        return page
        
    }
    
    func clearUserData() {
        NSUSERDEFAULT.removeObject(forKey: LOGIN)
        NSUSERDEFAULT.removeObject(forKey: USER_DATA)
        NSUSERDEFAULT.removeObject(forKey: LAT)
        NSUSERDEFAULT.removeObject(forKey: LONG)
        NSUSERDEFAULT.removeObject(forKey: ADDRESS_SELECTED)
        NSUSERDEFAULT.removeObject(forKey: DELIVERY_LOCATION)
        NSUSERDEFAULT.removeObject(forKey: SAVED_ADDRESS)
        NSUSERDEFAULT.removeObject(forKey: SAVED_CART_SESSION)

        LATTITUDE = ""
        LONGITUDE = ""
        ADDRESS = ""
        USERID = ""
    }
    @IBAction func btnImagePicker(_ sender: UIButton)
    {
        
        if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
        {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "EditProfileViewController") as? EditProfileViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
        else
        {
            MoveToLogin()
        }
        
    }
    @IBAction func btnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
extension MenuVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc func ActionEditProfile(_ sender: UITapGestureRecognizer)
    {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "EditProfileViewController") as? EditProfileViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    @objc func ActionSeletProfile(_ sender: UITapGestureRecognizer)
    {
        if !GlobalMethod.internet()
        {
            GlobalMethod.popDialog(controller: self, title: "No internet connection", message: "Please check your internet connection.")
            return
        }
        self.openImagePicker(forImageView: self.imgProfile)
    }
    
    func openImagePicker(forImageView imageView: UIImageView) {
        CustomImagePickerController.shared.showImagePickerFrom(vc: self, withOption: .askForOption, andCompletion: { (isSuccess, image) in
            if isSuccess {
                
                imageView.image = image
                self.updateProfilPicture()
            }
        })
    }
    
    func updateProfilPicture()
    {
        if !GlobalMethod.internet(){
            
        }else{
            GlobalMethod.StartProgress(self.view)
            
            ServManager.viewController = self
            
            let urlAlert = URL(string: constant.UPDATE_PROFILE_PICTURE)!
            let headerS: HTTPHeaders = ["Authorization": "Bearer (HelperGlobalVars.shared.access_token)","Accept": "application/json"]
            let parameterS: Parameters = ["from_app":FROMAPP,"user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,"apiId":API_ID]
            
            print(parameterS)
            
            
            AF.upload(
                multipartFormData: { multipartFormData in
                    for (key, value) in parameterS {
                        if let temp = value as? String {
                            multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                        }
                    }
                    
                    
                    multipartFormData.append(self.imgProfile.image!.jpegData(compressionQuality: 0.5)!, withName: "profile_pic" , fileName: "file.jpeg", mimeType: "image/jpeg")
            },
                to: urlAlert, method: .post , headers: nil)
                .response { resp in
                    print(resp)
                    //                GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    switch resp.result{
                    case .failure(let error):
                        print(error)
                    case.success( _):
                        print("🥶🥶Response after upload Img: (resp.result)")
                    }
            }
            
        }
    }
    
}

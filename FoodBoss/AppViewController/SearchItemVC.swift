//
//  SearchItemVC.swift
//  FoodBoss
//
//  Created by IOS Development on 14/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Alamofire
import PopupKit


struct SearchContainer
{
    var restaurant_menu_id:String!
    var restaurant_menu_name:String!
    var menu_items : [MenuItemsContainer]
}

struct MenuItemsContainer
{
    var item_id:String!
    var item_name:String!
    var item_description:String!
    var image:String!
    var sale_price:String!
    var is_offer_available:String!
    var total_quantity:String!

}

class SearchCell : UITableViewCell
{
    
    @IBOutlet weak var lblItemName: UILabel!
}

class SearchItemVC: UIViewController,UITextFieldDelegate, FoodDetailsActionDelegateSearch, QuantityActionDelegateSearch {
    
    
    
    private var SearchString = ""
    private var ArrayCuisine = NSMutableArray()
    private var ArrayRestaurant = NSMutableArray()
    
    @IBOutlet weak var txtSearch: InsetTextField!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var RestaurantID = ""
    
    var restaurantDetailsRequest: DataRequest? = nil
    private var ArrayMenuCategory = NSMutableArray()
    private var DictItemCart = NSDictionary()
    
    var arrayofItem = [MenuItemsContainer]()
    var arrayofMenu = [SearchContainer]()
    var filterArray = [MenuItemsContainer]()
    var arrMenuData = [[String:AnyObject]]()
    var arrItemData = [[String:AnyObject]]()
    private var selectedIndex = 0
    private var selectedSection = 0
    private var total_quantity1 = 0
    var shouldShowSearchResults = false
    var CheckSameRestaurant = Bool()
    var getRestaurantName = ""
    var getPreviousRestaurantName = ""
    @IBOutlet var viewWarning: UIView!
    @IBOutlet weak var viewInnerCart: UIView!
    private var popup : PopupView!

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        GlobalsVariable.sharedInstance.ArrayOfSearchMenu.removeAll()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.registerNib(FoodItemTableViewCell1.self)
        tblView.isHidden = true
        tblView.separatorStyle = .none
        txtSearch.autocorrectionType = .no
        txtSearch.autocapitalizationType = .none
        txtSearch.spellCheckingType = .no
        txtSearch.isEnabled = false
        
        btnClear.isHidden = true
        addNavigation()
        FetchDetails()
    }
    
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Search Food Item".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = textField.text!.count + string.count - range.length
        
        
        if newLength == 0 {
            btnClear.isHidden = true
        }
        else{
            btnClear.isHidden = false
        }
        print(String(newLength))
        if newLength > 2 {
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                SearchString = updatedText
                
            }
        }
        return true
    }
    
    @IBAction func ActionClear(_ sender: Any) {
        tblView.isHidden = true
        btnClear.isHidden = true
        txtSearch.text = ""
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.reloadData()
    }
    
    @IBAction func ActionSearch(_ sender: Any) {
        if SearchString.isEmpty{
            return
        }
        shouldShowSearchResults = true
        filterArray.removeAll()
        filterArray = self.arrayofItem.filter(){ (MenuItemsContainer) -> Bool in
            let range = MenuItemsContainer.item_name!.range(of: SearchString, options: NSString.CompareOptions.caseInsensitive)
            return range != nil
        }
        self.tblView.isHidden = false
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.reloadData()
    }
    
    func PlusButtonPressedSearch(_ index: IndexPath) {
        selectedIndex = index.row
        selectedSection = index.section
        
        print("PLUS CLICK")
        
    }
    
    func MinusButtonPressedSearch(_ index: IndexPath) {
        
        print("MINUS CLICK")

    }

}

extension SearchItemVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
             return filterArray.count
         }
        return self.arrayofItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeue(FoodItemTableViewCell1.self)
        
        if shouldShowSearchResults
        {
            cell.delegate = self
            cell.delegateQty = self
            cell.index = indexPath
            
            let ObjModel = self.filterArray[indexPath.row]
            cell.lblName.text = ObjModel.item_name
            cell.lblPrice.text = CURRANCY + " " + ObjModel.sale_price
            if let str = ObjModel.image {
                var imgUrl:String = str
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                cell.imgItem.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
            }
            
            if let str = ObjModel.total_quantity, str == "0"{
                cell.lblQty.text = ""
                cell.viewAdd.isHidden = false
                cell.viewQty.isHidden = true
            }
            else if let str = ObjModel.total_quantity {
                cell.lblQty.text = str
                cell.viewAdd.isHidden = true
                cell.viewQty.isHidden = false
            }
            
            let num = cell.lblDesc.numberOfLines
            if num == 0 {
                cell.lblDesc.text = ObjModel.item_description
                cell.lblDesc.lineBreakMode = .byTruncatingTail
                cell.lblDesc.numberOfLines = 2
            } else {
                cell.lblDesc.text = ObjModel.item_description
                cell.lblDesc.numberOfLines = 0
            }
            
            cell.selectionStyle = .none
        }
        else
        {
            cell.delegate = self
            cell.delegateQty = self
            cell.index = indexPath

            let ObjModel = self.arrayofItem[indexPath.row]
            cell.lblName.text = ObjModel.item_name
            cell.lblPrice.text = CURRANCY + " " + ObjModel.sale_price
            if let str = ObjModel.image {
                var imgUrl:String = str
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                cell.imgItem.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
            }
        
            if let str = ObjModel.total_quantity, str == "0"{
                cell.lblQty.text = ""
                cell.viewAdd.isHidden = false
                cell.viewQty.isHidden = true
            }
            else if let str = ObjModel.total_quantity {
                cell.lblQty.text = str
                cell.viewAdd.isHidden = true
                cell.viewQty.isHidden = false
            }
            
            let num = cell.lblDesc.numberOfLines
            if num == 0 {
                cell.lblDesc.text = ObjModel.item_description
                cell.lblDesc.lineBreakMode = .byTruncatingTail
                cell.lblDesc.numberOfLines = 2
            } else {
                cell.lblDesc.text = ObjModel.item_description
                cell.lblDesc.numberOfLines = 0
            }

            cell.selectionStyle = .none
        }
    
        return cell
    }
    
    func AddMoreButtonPressed(_ index: IndexPath) {
        
        if shouldShowSearchResults
        {
            var Controller = UIViewController()
            let obj = self.filterArray[index.row]
            
            if let View = self.storyboard?.instantiateViewController(identifier: "ItemMenuViewController") as? ItemMenuViewController{
                Controller = View
                
                GlobalsVariable.sharedInstance.ItemAddComeFrom = "SearchItem"
                //View.DictItem = Dict
                //View.isVeg = self.isVeg
                View.SearchItemID = obj.item_id
                View.HeaderTittle = obj.item_name
                View.SearchItemName = obj.item_name
                View.SearchItemDesc = obj.item_description
                View.SearchItemPrice = obj.sale_price
                View.SearchItemPrice = obj.sale_price
                View.SearchQtyValue = obj.total_quantity
                
                View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                    
                    print("RETURN BACK FROM SERCH VIEW CONTROLLLER")
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
            Controller.modalPresentationStyle = .formSheet
            
            let navigationController = UINavigationController(rootViewController: Controller)
            navigationController.modalPresentationStyle = .formSheet
            
            tabBarController?.present(navigationController, animated: true, completion: nil)
        }
        else
        {
            var Controller = UIViewController()
            let obj = self.arrayofItem[index.row]
            
            if let View = self.storyboard?.instantiateViewController(identifier: "ItemMenuViewController") as? ItemMenuViewController{
                Controller = View
                
                GlobalsVariable.sharedInstance.ItemAddComeFrom = "SearchItem"
                //View.DictItem = Dict
                //View.isVeg = self.isVeg
                View.SearchItemID = obj.item_id
                View.HeaderTittle = obj.item_name
                View.SearchItemName = obj.item_name
                View.SearchItemDesc = obj.item_description
                View.SearchItemPrice = obj.sale_price
                View.SearchItemPrice = obj.sale_price
                View.SearchQtyValue = obj.total_quantity
                
                View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                    
                    print("RETURN BACK FROM SERCH VIEW CONTROLLLER")
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
            Controller.modalPresentationStyle = .formSheet
            
            let navigationController = UINavigationController(rootViewController: Controller)
            navigationController.modalPresentationStyle = .formSheet
            
            tabBarController?.present(navigationController, animated: true, completion: nil)
        }
       
    }

    
    func AddItem()
    {
        if let Dictionary = self.ArrayMenuCategory.object(at: selectedIndex) as? NSMutableDictionary{
            if let Arr = Dictionary.value(forKey: "menu_items") as? NSMutableArray{
                if let Dict = Arr.object(at: selectedIndex) as? NSMutableDictionary{
                    if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                        let Qty = Int(String (describing: Quantity))! + 1
                        Dict.setValue(String (describing: Qty), forKey: "quantity")
                    }
                }
                
            }
        }
    }
}

//MARK:- --- [CollectionView] ---

extension SearchItemVC{
    
    func FetchDetails(){
        if !GlobalMethod.internet(){
            
        }
        else{
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "restaurant_id":RestaurantID,
                                              "pure_veg":"",
                                              "apiId":API_ID,
                                              "language": GlobalMethod.selectedLanguageForUser()]
            // print("FOOD DETAIL PARAMETER =====>>",parameter)
            ServManager.viewController = self
            self.restaurantDetailsRequest = ServManager.callPostAPI(constant.DETAILS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in

                if isSuccess{
                    if let dictRestaurant = dataDict.value(forKey: "restaurant") as? NSDictionary{
                        //print("FOOD DETAIL RESPONSE =====>>",dataDict)
                        
                        self.ArrayMenuCategory = NSMutableArray()
                        let arr = dictRestaurant["menu_category"] as? Array<AnyObject>
                        self.arrMenuData = arr as! [[String:AnyObject]]

                        for index in 0...self.arrMenuData.count - 1
                        {
                            let obj = self.arrMenuData[index] as! NSDictionary
                            let tempMenuArray = obj["menu_items"] as! Array<AnyObject>
                            self.arrItemData = tempMenuArray as! [[String:AnyObject]]

                            if self.arrItemData.count > 0
                            {
                                for indexex in 0...self.arrItemData.count - 1
                                {
                                    let objData = self.arrItemData[indexex] as NSDictionary
                                    var modelOBJ = MenuItemsContainer()
                                    let item_id1 = objData["item_id"] as! String
                                    let item_name1 = objData["item_name"] as! String
                                    let item_description1 = objData["item_description"] as! String
                                    let image1 = objData["image"] as! String
                                    let is_offer_available1 = objData["is_offer_available"] as! Int
                                    let sale_price1 = objData["sale_price"] as! String
                                    
                                    let cart_items = objData["cart_items"] as! Array<AnyObject>

                                    if cart_items.count > 0
                                    {
                                        let tempTotal_quantity = objData["total_quantity"] as! String
                                        self.total_quantity1 = Int(tempTotal_quantity)!

                                    }
                                    else
                                    {
                                        self.total_quantity1 = 0
                                    }
                                    
                                    
                                    
                                    
                                    modelOBJ.item_id = item_id1
                                    modelOBJ.item_name = item_name1
                                    modelOBJ.item_description = item_description1
                                    modelOBJ.image = image1
                                    modelOBJ.is_offer_available = String(is_offer_available1)
                                    modelOBJ.sale_price = sale_price1
                                    modelOBJ.total_quantity = String(self.total_quantity1)
                                    self.arrayofItem.append(modelOBJ)
                                }
                                
                            }
                        }
                        
                       // self.tblView.isHidden = false
                       // self.tblView.reloadData()
                        self.txtSearch.isEnabled = true
                    }
                }
                else
                {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }

   
}


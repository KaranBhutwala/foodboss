//
//  PromoCodeViewController.swift
//  FoodBoss
//
//  Created by Jay on 21/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class PromoCodeViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var txtPromo: InsetTextField!
    private var ArrayCoupon = NSMutableArray()
    
    var Coupon: ((_ Coupon: String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.FetchCoupon()
        self.addNavigation()
        self.tblView.registerNib(CouponListTableViewCell.self)
        self.tblView.tableFooterView = UIView()
    }
    
   
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = "Apply Promo Code".localized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
    
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ActionApply(_ sender: Any) {
        if !txtPromo.text!.isEmpty{
            if let block = self.Coupon {
                block(txtPromo.text!)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

// MARK: - TableView

extension PromoCodeViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayCoupon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(CouponListTableViewCell.self)
        if let Dict = ArrayCoupon.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = ArrayCoupon.object(at: indexPath.row) as? NSDictionary{
            if let str = Dict.value(forKey: "coupon_code") as? String{
                if let block = self.Coupon {
                    block(str)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

//MARK:- --- [API CALLING] ---

extension PromoCodeViewController{
    func FetchCoupon(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                      "apiId":API_ID,
                                      "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
//            print(constant.MENU_ITEM_DETAILS)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.COUPON_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let Data = CouponListModel.init(dictionary: dataDict){
                        for Dict in Data.coupons!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayCoupon.add(dictTemp)
                                }
                            }
                        }
                        self.tblView.reloadData()
                    }

                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

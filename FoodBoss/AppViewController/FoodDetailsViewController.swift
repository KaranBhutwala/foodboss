//
//  FoodDetailsViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 06/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import PopupKit
import SkeletonView
import Alamofire

class FoodDetailsViewController: UIViewController {
    
    private var popup : PopupView!
    private var SameRestaurant = true
    private var PreviousRestaurntName = ""
    @IBOutlet var viewWarning: UIView!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet var viewFooter: UIView!
    @IBOutlet weak var switchHeaderVeg: UISwitch!
    @IBOutlet weak var collectionMenu: UICollectionView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblClosed: UILabel!
    @IBOutlet weak var constraintClosed: NSLayoutConstraint!
    @IBOutlet weak var viewFood: UIView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var tblCart: UITableView!
//    @IBOutlet weak var viewRate: HCSStarRatingView!
    
    @IBOutlet weak var viewInnerCart: UIView!
    

    @IBOutlet weak var collectioncuisine: UICollectionView!
    private (set) var collectionParallaxView: UICollectionView!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var lblCart: UILabel!
    @IBOutlet weak var constraintViewCartHeight: NSLayoutConstraint!
    
    @IBOutlet var viewCartFooter: UIView!
    
    @IBOutlet weak var lblRates: UILabel!
    @IBOutlet weak var lblReview: UILabel!
    
    
    @IBOutlet weak var constraintsViewCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCollection: UIView!
    var RestaurantName = ""
    
    
    private var ArrayBannerImages = NSMutableArray()
    private var Arraycuisines = NSMutableArray()
    private var ArrayMenuCategory = NSMutableArray()
    private var ArrayMenuImages = NSMutableArray()

    private var ArrayMenu = NSMutableArray()
    private var ArraySelectedMenu = NSMutableArray()
    private var ArrayItemCart = NSMutableArray()
    private var MenuOpen = false
    private var isVeg = false
    private var isItemsinCart = false
    private var isFav = false
    var DictRestaurant = NSDictionary()
    private var DictItemCart = NSDictionary()
    var RestaurantID = ""
    private var selectedIndex = 0
    private var selectedSection = 0
    private var SelectedMenuIndex = 0
    private var DictMainRestaurant = NSMutableDictionary()
    
    @IBOutlet weak var imgRestaurant: UIImageView!
    private var scolledPositionIndexpath : IndexPath!
    private var isRequiredHardReload: Bool = true
    private var ScrollableSection = 0
    
    var restaurantReview = RestaurantReview.getEmptyReview()
    var restaurantInfo = RestaurantInfo.getEmptyInfo()
    var restaurantDetailsRequest: DataRequest? = nil
    var cartDetailsRequest: DataRequest? = nil
    var timer: Timer? = nil
    @IBOutlet weak var imgFav: UIImageView!
    
    
    private var ArrayFullMenu = NSMutableArray()

    override func viewWillAppear(_ animated: Bool) {
    
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        GlobalsVariable.sharedInstance.ArrayOfSearchMenu.removeAll()
        
        self.FetchDetails()
        self.FetchCartList()
        self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollCollectionView), userInfo: nil, repeats: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scolledPositionIndexpath = IndexPath(row: 0, section: 0)
        self.constraintsViewCollectionHeight.constant = 00
        self.viewCollection.isHidden = true
        if let str = DictRestaurant.value(forKey: "final_review") as? String{
            self.lblRates.text = str
        }
        
        if let str = DictRestaurant.value(forKey: "restaurant_id") as? String{
            RestaurantID = str
            SELECTED_RESTAURANT_ID = RestaurantID
        }
        self.viewCart.layoutIfNeeded()
        self.viewCart.roundCorners([.topLeft, .topRight], radius: 15)
        self.viewInnerCart.layoutIfNeeded()
        self.viewInnerCart.roundCorners([.topLeft, .topRight], radius: 15)

        self.tblCart.registerNib(CartItemTableViewCell.self)
        self.tblCart.registerNib(FoodItemTableViewCell.self)
        self.addNavigation()
        self.setupParallaxHeader()
//        self.FetchDetails()
        
        self.tblView.registerNib(FoodItemTableViewCell.self)
//        viewFooter.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.view.frame.height - 190)
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 103
        
        self.tblCart.tableFooterView = viewCartFooter
       // self.lblClosed.layer.cornerRadius = self.lblClosed.frame.height/2
        self.viewCart.isHidden = true
        self.constraintViewCartHeight.constant = 0
//        self.FetchCartList()
    }
    
    

    
    @IBAction func btnSearchAction(_ sender: UIButton)
    {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "SearchItemVC") as? SearchItemVC{
                //View.DictRestaurant = Dict
                View.RestaurantID = RestaurantID
                View.CheckSameRestaurant = SameRestaurant
                View.getRestaurantName = self.lblRestaurantName.text!
                View.getPreviousRestaurantName = PreviousRestaurntName
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "SearchItemVC") as? SearchItemVC{
                //View.DictRestaurant = Dict
                View.RestaurantID = RestaurantID
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func btnInfo(_ sender: UIButton)
    {
        let restaurantDetalVC = RestaurantDetailsVC.initiateFromStoryBoard()
        restaurantDetalVC.comeFromScreen = "InfoClick"
        restaurantDetalVC.restaurntReview = self.restaurantReview
        restaurantDetalVC.restaurantInfo = self.restaurantInfo
        restaurantDetalVC.imageList = self.ArrayBannerImages.compactMap({ (element) -> String? in
            let imageMap = element as? [String: String]
            return imageMap?["image"]
        })
        self.navigationController?.pushViewController(restaurantDetalVC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.restaurantDetailsRequest?.cancel()
        self.cartDetailsRequest?.cancel()
        self.timer?.invalidate()
    }
    
    @objc func scrollCollectionView() {
        let imageList = self.ArrayBannerImages.compactMap({ (element) -> String? in
            let imageMap = element as? [String: String]
            return imageMap?["image"]
        })
        if imageList.count > 1 {
            let offSet = self.collectionParallaxView.contentOffset.x
            if offSet == 0.0 {
                self.collectionParallaxView.setContentOffset(CGPoint(x: offSet + self.collectionParallaxView.frame.width, y: 0), animated: true)
            } else if offSet >= self.collectionParallaxView.frame.width {
                let page = offSet / self.collectionParallaxView.frame.width
                let pageInt = Int(page)
                if pageInt == imageList.count - 1 {
                    self.collectionParallaxView.setContentOffset(CGPoint.zero, animated: true)
                } else {
                    self.collectionParallaxView.setContentOffset(CGPoint(x: offSet + self.collectionParallaxView.frame.width, y: 0), animated: true)
                }
            }
        }
    }

    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white

        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "Back_White"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = ""
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.white
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        
    }
    
    @IBAction func ActionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func ActionBackView()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func GetSelectedMenu(Dict : NSDictionary){
        if let Arr = Dict.value(forKey: "menu_items") as? NSArray{
            self.ArrayMenu = NSMutableArray()
            for Dict in Arr{
                if let dic = Dict as? NSDictionary{
                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                        self.ArrayMenu.add(dictTemp)
                    }
                }
            }
        }
      
        self.tblView.reloadData()
    }
    
    private func setupParallaxHeader() {
        let parallaxHeight: CGFloat = 250
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionParallaxView = UICollectionView(
            frame: CGRect(x: 0, y: 0, width: view.frame.width, height: parallaxHeight),
            collectionViewLayout: layout
        )
        
        viewHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: viewHeader.frame.height)
        
        collectionParallaxView.registerNib(ImageCollectionViewCell.self)
        collectionParallaxView.isPagingEnabled = true
        collectionParallaxView.showsHorizontalScrollIndicator = false
        collectionParallaxView.backgroundColor = UIColor.white
        collectionParallaxView.delegate = self
        collectionParallaxView.dataSource = self
        collectionParallaxView.bounces = false
        
        self.viewCollection.dropShadow(color: .black,
                                       opacity: 0.07,
                                       offSet: CGSize(width: -1, height: 3),
                                       radius: 1,
                                       scale: true)
        
        //adding view as parallax header to table view is straightforward
        tblView.parallaxHeader.view = viewHeader
        tblView.parallaxHeader.height = viewHeader.frame.height + parallaxHeight - 250
        tblView.parallaxHeader.minimumHeight = 0
        tblView.parallaxHeader.mode = .centerFill
        tblView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
//            print(parallaxHeader.progress)
            
            if parallaxHeader.progress < 0.25{
                //self.viewNavigation.backgroundColor = UIColor.white
                self.btnBack.setImage(#imageLiteral(resourceName: "Back_White_gray"), for: .normal)
               // self.btnSearch.setImage(#imageLiteral(resourceName: "search_menu_gray"), for: .normal)
                self.lblTop.text = self.lblRestaurantName.text
                self.constraintsViewCollectionHeight.constant = 50
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                    self.viewCollection.alpha = 1.0
                    self.viewCollection.isHidden = false
                    self.viewNavigation.backgroundColor = UIColor.white
                }, completion: nil)
            }
            else{
                //self.viewNavigation.backgroundColor = UIColor.clear
                self.btnBack.setImage(#imageLiteral(resourceName: "Back_White"), for: .normal)
               // self.btnSearch.setImage(#imageLiteral(resourceName: "search_menu"), for: .normal)
                self.lblTop.text = ""
                self.constraintsViewCollectionHeight.constant = 00
                UIView.animate(withDuration: 0.3, delay: 0, options: .transitionCurlUp, animations: {
                    self.viewCollection.alpha = 0.0
                    self.viewCollection.isHidden = true
                    self.viewNavigation.backgroundColor = UIColor.clear
                }, completion: {_ in
                })
            }
            
        }
        self.viewHeader.addSubview(collectionParallaxView)
        
      
        
        
        
        self.viewHeader.bringSubviewToFront(self.viewFood)
        
        self.viewFood.layer.cornerRadius = 15
        self.viewFood.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        print("FULL MENU CLICK ==>>")
        
        
        if let View = self.storyboard?.instantiateViewController(identifier: "ShowImagesVC") as? ShowImagesVC{
        View.comeFrom = "seeFullMenu"
        View.ArrayMenuImages = self.ArrayMenuImages
        View.RestaurantName = RestaurantName

        self.navigationController?.pushViewController(View, animated: true)
        }
    }
    
    @IBAction func ActionSwitch(_ sender: UISwitch) {
        if sender.isOn{
            print("Yey")
            self.isVeg = true
        }
        else{
            print("Nay")
            self.isVeg = false
        }
        self.FetchDetails()
    }
    
    @IBAction func ActionGoToCart(_ sender: Any) {
        
       // NSUSERDEFAULT.set("true", forKey: COME_FROM_REPEAT_ORDER)

        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "CartViewController") as? CartViewController{
                View.isPushed = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController{
                View.isPushed = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }

    
    @IBAction func ActionFavUnFav(_ sender: Any) {
        
        if isFav{
            self.isFav = false
           // self.btnFav.setImage(#imageLiteral(resourceName: "UnFav"), for: .normal)
            self.imgFav.image = UIImage.init(named: "plain_heart.png")
        }
        else{
            self.isFav = true
           // self.btnFav.setImage(#imageLiteral(resourceName: "Fav"), for: .normal)
            self.imgFav.image = UIImage.init(named: "fill_heart.png")

        }
        self.FavUnfav()
    }
    
    
    @IBAction func ActionNewCustomization(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
        self.PushtoItemsMenu(Dict: DictItemCart)
    }
    
    
    @IBAction func ActionCANCELWarning(_ sender: Any) {
        self.popup.dismiss(animated: true)
    }
    
    @IBAction func ActionOKWarning(_ sender: Any) {
        self.popup.dismiss(animated: true)
        // Manthan 23-9-2020
        // Set `SameRestaurant` true if user adds item after warning
        self.SameRestaurant = true
        self.AddItem()
    }
    
    
    func ShowWarning (){
        
        let string_to_color = self.lblRestaurantName.text!
        let string_to_color1 = PreviousRestaurntName
        let str1 = "The Cart contains items from ".localized
        let preItem = string_to_color1
        let str2 = ", by adding item from ".localized
        let newItem = string_to_color
        let str3 = " your previous items will be cleared, click ".localized
        let strOk = "OK".localized
        let str4 = " to proceed.".localized

        //let main_string = "The Cart contains items from \(string_to_color1), by adding item from \(string_to_color) your previous items will be cleared, click OK to proceed."
        let main_string = str1 + preItem + str2 + newItem + str3 + strOk + str4
        

        let range = (main_string as NSString).range(of: string_to_color)
        let range1 = (main_string as NSString).range(of: string_to_color1)

        let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: COLOR_ORANGE! , range: range)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: COLOR_ORANGE! , range: range1)

        self.lblWarning.attributedText = attribute
        let popUpWidth = self.view.frame.size.width - 20
        var lblHeight = main_string.height(constraintedWidth: popUpWidth, font: self.lblWarning.font)
        lblHeight += (15 + 30 + 35 + 20)
        
        
        //self.viewWarning.frame = CGRect(x: 10, y: 0, width: self.view.frame.size.width - 20, height: 135)
        self.viewWarning.frame = CGRect(x: 10, y: 0, width: popUpWidth, height: lblHeight)
        self.viewWarning.layer.cornerRadius = 10
        self.viewWarning.layer.masksToBounds = true

        self.viewInnerCart.setNeedsLayout()

        let layout = PopupView.Layout(horizontal: PopupView.HorizontalLayout.center, vertical: PopupView.VerticalLayout.center)

        self.popup = PopupView(contentView: self.viewWarning, showType: PopupView.ShowType.slideInFromBottom, dismissType: PopupView.DismissType.slideOutToBottom, maskType: PopupView.MaskType.dimmed, shouldDismissOnBackgroundTouch: true, shouldDismissOnContentTouch: false)
        self.popup.show(with: layout)
        self.viewInnerCart.layoutIfNeeded()
    }
    
    @IBAction func moveToRestaurantDetails(_ sender: Any) {
        let restaurantDetalVC = RestaurantDetailsVC.initiateFromStoryBoard()
        restaurantDetalVC.comeFromScreen = "ReviewClick"
        restaurantDetalVC.restaurntReview = self.restaurantReview
        restaurantDetalVC.restaurantInfo = self.restaurantInfo
        restaurantDetalVC.imageList = self.ArrayBannerImages.compactMap({ (element) -> String? in
            let imageMap = element as? [String: String]
            return imageMap?["image"]
        })
        self.navigationController?.pushViewController(restaurantDetalVC, animated: true)
    }
    
}

//MARK:- --- [TABLEVIEW CALLING] ---

extension FoodDetailsViewController: SkeletonTableViewDelegate, SkeletonTableViewDataSource {

    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        if (skeletonView == tblView) {
            return "FoodItemTableViewCell"
        } else {
            if indexPath.row == 0 {
                return "FoodItemTableViewCell"
            } else {
                return "CartItemTableViewCell"
            }
        }

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        ArrayMenuCategory
        if tableView == tblCart{
            return 1
        }
        return ArrayMenuCategory.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tblCart {
            return 0
        }
        return 40
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if tableView == tblMenuBottom{
//            return 10
//        }
//        return 0
//    }
    
    func collectionSkeletonView(_ skeletonView: UITableView,
                                identifierForHeaderInSection section: Int) -> ReusableHeaderFooterIdentifier? {
        if skeletonView == self.tblCart {
            return nil
        } else {
            return "headerDetailNew"
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == tblCart{
            return nil
        }
        var headerView = tableView.headerView(forSection: section)
        if let _ = headerView {
            let label = headerView!.viewWithTag(101) as! UILabel
            if let Dict = ArrayMenuCategory.object(at: section) as? NSDictionary{
                if let str = Dict.value(forKey: "restaurant_menu_name") as? String{
                    label.text = str
                }
            }
        } else {
            headerView = UITableViewHeaderFooterView(frame: tableView.rectForHeader(inSection: section))
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width - 40, height: 0))
            label.tag = 101
            label.translatesAutoresizingMaskIntoConstraints = false
            if let Dict = ArrayMenuCategory.object(at: section) as? NSDictionary{
                if let str = Dict.value(forKey: "restaurant_menu_name") as? String{
                    label.text = str
                    
                    let text = label.text
                    let textRange = NSRange(location: 0, length: (text?.count)!)
                    let attributedText = NSMutableAttributedString(string: text!)
                    attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
                    label.attributedText = attributedText

                }
            }
            
            label.font = UIFont(name: "Rubik-Medium", size: 17.0)
            label.sizeToFit()
            headerView!.addSubview(label)
            label.centerYAnchor.constraint(equalTo: headerView!.centerYAnchor).isActive = true
            NSLayoutConstraint(item: label, attribute: .leading, relatedBy: .equal, toItem: headerView!, attribute: .leading, multiplier: 1.0, constant: 20).isActive = true
        
        
        }
        
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == tblCart{
            if ArrayItemCart.count != 0 {
                return ArrayItemCart.count + 1
            }
            return 0
        }
        else{
            if ArrayMenuCategory.count != 0{
                if let Dict = self.ArrayMenuCategory.object(at: section) as? NSDictionary{
                    if let Arr = Dict.value(forKey: "menu_items") as? NSArray{
                        return Arr.count
                    }
                }
            }
        }
        return ArrayMenu.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCart{
            if indexPath.row == 0{
                let cell = tableView.dequeue(FoodItemTableViewCell.self)
                cell.configure(with: DictItemCart)
                cell.viewAdd.isHidden = true
                cell.viewQty.isHidden = true
                cell.lblPrice.isHidden = true
                return cell
            }
            else{
                let cell = tableView.dequeue(CartItemTableViewCell.self)
                cell.delegateDetailQty = self
                cell.index = indexPath
                cell.IsUsedinDetail = true
                if let Dict = ArrayItemCart.object(at: indexPath.row - 1) as? NSDictionary{
                    //                    print(Dict)
                    cell.configure(with: Dict)
                }
                if let str = DictItemCart.value(forKey: "item_name") as? String{
                    cell.lblItemName.text = str
                }
                cell.ConstraintOne.constant = 15
                cell.ConstraintTwo.constant = 15
                cell.ConstraintThree.constant = 15
                cell.imgView.isHidden = true
                cell.imgVegNonveg.isHidden = true
                cell.lblQtywithPrice.isHidden = true
                cell.lblOtherPrice.isHidden = true
                cell.selectionStyle = .none
                return cell
            }
        }
        else{
            let cell = tableView.dequeue(FoodItemTableViewCell.self)
            cell.delegate = self
            cell.delegateQty = self
            cell.index = indexPath
            cell.viewAdd.isHidden = true
            
            //            if let Dict = self.ArrayMenuCategory.object(at: indexPath.section) as? NSDictionary{
            //                if let Arr = Dict.value(forKey: "menu_items") as? NSArray{
            //                    if let Dictionary = Arr.object(at: indexPath.section) as? NSDictionary{
            //
            //                    }
            //                }
            //            }
            
            if ArrayMenuCategory.count != 0{
                if let Dictionary = self.ArrayMenuCategory.object(at: indexPath.section) as? NSDictionary{
                    if let Arr = Dictionary.value(forKey: "menu_items") as? NSArray{
                        if let Dict = Arr.object(at: indexPath.row) as? NSDictionary{
                            //                        print(Dict)
                            cell.configure(with: Dict)
                            cell.viewQty.isHidden = true
                            cell.viewAdd.isHidden = false
                            if let str = Dict.value(forKey: "SentToCart") as? String, str == "Yes"{
                                cell.viewAdd.isHidden = true
                                cell.viewQty.isHidden = false
                            }
                            
                            if let str = Dict.value(forKey: "total_quantity") as? String, str == "0"{
                                cell.lblQty.text = ""
                                cell.viewAdd.isHidden = false
                                cell.viewQty.isHidden = true
                            }
                            else if let str = Dict.value(forKey: "total_quantity") as? String{
                                cell.lblQty.text = str
                                cell.viewAdd.isHidden = true
                                cell.viewQty.isHidden = false
                            }
                            else{
                                if let str = Dict.value(forKey: "quantity") as? String, str == "0"{
                                    cell.lblQty.text = ""
                                    cell.viewAdd.isHidden = false
                                    cell.viewQty.isHidden = true
                                }
                                else if let str = Dict.value(forKey: "quantity") as? String{
                                    cell.lblQty.text = str
                                    cell.viewAdd.isHidden = true
                                    cell.viewQty.isHidden = false
                                }
                                else{}
                            }
                            
                        }
                    }
                }
            }
            if cell.lblDesc.maxNumberOfLines > 2 {
                cell.lblDesc.attributedText = expandLabel(flag: false, description: self.getDesriptionFor(itemAt: indexPath))
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func expandLabel(flag: Bool, description: String) -> NSAttributedString {
        let readMore = " Read More".localized
        let readLess = " Read Less".localized
        let finalString = NSMutableAttributedString(string: description)
        if flag {
            let attLess = NSMutableAttributedString(string: readLess, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "Red") ?? UIColor.red])
            finalString.append(attLess)
        } else {
            let attMore = NSMutableAttributedString(string: readMore, attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "Red") ?? UIColor.red])
            finalString.append(attMore)
        }
        
        return finalString
    }
    
    func getDesriptionFor(itemAt indexPath: IndexPath) -> String {
        let masterMap = self.ArrayMenuCategory[indexPath.section] as? [String: Any] ?? [String : Any]()
        let outerMap = masterMap["menu_items"] as? [[String: Any]] ?? []
        let innerMap = outerMap[indexPath.row]
        return innerMap["item_description"] as? String ?? ""
    }
    
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblView{
            guard let cell = tableView.cellForRow(at: indexPath) as? FoodItemTableViewCell else { return }
            if cell.lblDesc.maxNumberOfLines > 2 {
                let num = cell.lblDesc.numberOfLines
                tableView.beginUpdates()
                if num == 0 {
                    cell.lblDesc.attributedText = expandLabel(flag: false, description: self.getDesriptionFor(itemAt: indexPath))
                    cell.lblDesc.numberOfLines = 2
                } else {
                    cell.lblDesc.attributedText = expandLabel(flag: true, description: self.getDesriptionFor(itemAt: indexPath))
                    cell.lblDesc.numberOfLines = 0
                }
                tableView.endUpdates()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView,
                   willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == self.tblView {
            let cell = tableView.dequeue(FoodItemTableViewCell.self)
        }
    }
    
}


// MARK: - ActionDelegate
extension FoodDetailsViewController: QuantityDetailCartActionDelegate{
    
    func PlusDetailButtonPressed(_ index: IndexPath) {
        if let Quantity = DictItemCart.value(forKey: "total_quantity") as? String,!Quantity.isEmpty{
            let Qty = Int(String (describing: Quantity))! + 1
            DictItemCart.setValue(String (describing: Qty), forKey: "total_quantity")
        }
        else if let Quantity = DictItemCart.value(forKey: "quantity") as? String,!Quantity.isEmpty{
            let Qty = Int(String (describing: Quantity))! + 1
            DictItemCart.setValue(String (describing: Qty), forKey: "quantity")
        }
        if let Dict = ArrayItemCart.object(at: index.row - 1) as? NSMutableDictionary{
            if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                let Qty = Int(String (describing: Quantity))! + 1
                
                if let CartID = Dict.value(forKey: "cart_id") as? String{
                    self.UpdateCart(cart_id: CartID, Qty: String (describing: Qty))
                }
                
                Dict.setValue(String (describing: Qty), forKey: "quantity")
                self.tblCart.reloadData()
            }
        }
        self.tblView.reloadData()
    }
    
    func MinusDetailButtonPressed(_ index: IndexPath) {
        if let Quantity = DictItemCart.value(forKey: "total_quantity") as? String,!Quantity.isEmpty{
            let Qty = Int(String (describing: Quantity))! - 1
            DictItemCart.setValue(String (describing: Qty), forKey: "total_quantity")
        }
        else if let Quantity = DictItemCart.value(forKey: "quantity") as? String,!Quantity.isEmpty{
            let Qty = Int(String (describing: Quantity))! - 1
            DictItemCart.setValue(String (describing: Qty), forKey: "quantity")
        }
        
        if let Dict = ArrayItemCart.object(at: index.row - 1) as? NSMutableDictionary{
            if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                
                if Quantity == "1"{
                    self.popup.dismiss(animated: true)
                    self.ArrayItemCart.removeObject(at: index.row - 1)
                    if let ArrCARTITEM = DictItemCart.value(forKey: "cart_items")as? NSArray{
                        let ARRAY = ArrCARTITEM.mutableCopy() as? NSMutableArray
                        ARRAY!.remove(Dict)
                        DictItemCart.setValue(ARRAY, forKey: "cart_items")
                    }
                    if let strID = Dict.value(forKey: "cart_id") as? String{
                        self.DeleteFromCart(cart_id: strID)
                    }
                    self.RefreshCartTable()
                }
                else{
                    let Qty = Int(String (describing: Quantity))! - 1
                    
                    if let CartID = Dict.value(forKey: "cart_id") as? String{
                        self.UpdateCart(cart_id: CartID, Qty: String (describing: Qty))
                    }
                    
                    Dict.setValue(String (describing: Qty), forKey: "quantity")
                }
                
                self.tblCart.reloadData()
            }
        }
        self.tblView.reloadData()
    }
    
    func RefreshCartTable(){
        
        self.tblCart.reloadData()
        var Height = CGFloat()
        self.tblCart.layoutIfNeeded()
        Height =  tblCart.contentSize.height
        if Height > tblView.frame.height - 35{
            Height = tblView.frame.height - 35
        }
        
        self.viewInnerCart.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: Height + 15)
        self.viewInnerCart.roundCorners([.topLeft, .topRight], radius: 15)
        self.viewInnerCart.layer.masksToBounds = true

        self.viewInnerCart.setNeedsLayout()

        let layout = PopupView.Layout(horizontal: PopupView.HorizontalLayout.center, vertical: PopupView.VerticalLayout.bottom)

        self.popup = PopupView(contentView: self.viewInnerCart, showType: PopupView.ShowType.slideInFromBottom, dismissType: PopupView.DismissType.slideOutToBottom, maskType: PopupView.MaskType.dimmed, shouldDismissOnBackgroundTouch: true, shouldDismissOnContentTouch: false)
        self.popup.show(with: layout)
        self.viewInnerCart.layoutIfNeeded()
    }
    
}



extension FoodDetailsViewController: FoodDetailsActionDelegate, QuantityActionDelegate{
    func PlusButtonPressed(_ index: IndexPath) {
        selectedIndex = index.row
        selectedSection = index.section
        
        if let Dictionary = self.ArrayMenuCategory.object(at: index.section) as? NSMutableDictionary{
            var dictSend : [String : AnyObject] = [:]
            if let Arr = Dictionary.value(forKey: "menu_items") as? [NSDictionary]{
                if let Dict = Arr[selectedIndex] as? NSDictionary{
                    print(Dict)
                    dictSend = Dict as! [String : AnyObject]
                    if let Arr = Dict.value(forKey: "variations") as? NSArray,Arr.count == 0{
                        if let Quantity = Dict.value(forKey: "total_quantity") as? String,!Quantity.isEmpty{
                            if let ArrCart = Dict.value(forKey: "cart_items") as? NSArray,ArrCart.count != 0{
                                if let DictCart = ArrCart.object(at: 0) as? NSDictionary{
                                    if let CartID = DictCart.value(forKey: "cart_id") as? String{
                                        let Qty = Int(String (describing: Quantity))! + 1
                                        dictSend["total_quantity"] = Qty as! AnyObject
                                        //Dict.setValue(Qty, forKey: "total_quantity")
                                        self.UpdateCart(cart_id: CartID, Qty: String (describing: Qty))
                                        self.FetchDetails()
                                        self.FetchCartList()
                                    }
                                }
                            }
                        }
                        else if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                            let Qty = Int(String (describing: Quantity))! + 1
                            Dict.setValue(String (describing: Qty), forKey: "quantity")
                        }
                    }
                    else{
                        self.SetupCartPopup(Dict: dictSend as NSDictionary)
                    }
                    self.tblView.reloadData()
                }
            }
        }
        return
        
    }
    
    func MinusButtonPressed(_ index: IndexPath) {
        selectedIndex = index.row
        selectedSection = index.section
        
        if let Dictionary = self.ArrayMenuCategory.object(at: index.section) as? NSMutableDictionary{
            if let Arr = Dictionary.value(forKey: "menu_items") as? [NSDictionary]{
                if let Dict = Arr[selectedIndex] as? NSDictionary{
                    print(Dict)
                    if let Arr = Dict.value(forKey: "variations") as? NSArray,Arr.count == 0{
                        if let Quantity = Dict.value(forKey: "total_quantity") as? String,!Quantity.isEmpty{
                            if let ArrCart = Dict.value(forKey: "cart_items") as? NSArray,ArrCart.count != 0{
                                if let DictCart = ArrCart.object(at: 0) as? NSDictionary{
                                    if let CartID = DictCart.value(forKey: "cart_id") as? String{
                                        let Qty = Int(String (describing: Quantity))! - 1
                                        print("TOTAL ITEM QTY-->>>",Qty)
                                       // Dict.setValue(String (describing: Qty), forKey: "total_quantity")
                                        self.UpdateCart(cart_id: CartID, Qty: String (describing: Qty))
                                        if Quantity == "1"{
                                            
                                          self.DeleteFromCart(cart_id: CartID)

                                        }else{
                                            self.FetchDetails()
                                            self.FetchCartList()
                                        }
                                        
                                    }
                                }
                            }
                            
                           
                        }
                        else if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                            let Qty = Int(String (describing: Quantity))! - 1
                            Dict.setValue(String (describing: Qty), forKey: "quantity")
                        }
                    }
                    else{
                        self.SetupCartPopup(Dict: Dict)
                    }
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
            }
        }
        return
        
    }
    
    func AddMoreButtonPressed(_ index: IndexPath) {
        selectedIndex = index.row
        selectedSection = index.section
        if !self.SameRestaurant{
            self.ShowWarning()
        }
        else{
            AddItem()
        }
    }
    
    
    
    func AddItem(){
        if let Dictionary = self.ArrayMenuCategory.object(at: selectedSection) as? NSMutableDictionary{
            if let Arr = Dictionary.value(forKey: "menu_items") as? [NSDictionary]{
                print(Arr)
                if let Dict = Arr[selectedIndex] as? NSDictionary{
                    
                    let strIMAGE = Dict.value(forKey: "image") as? String
                    print("SELECTED ITEM IMAGE ==>>",strIMAGE)

                    if strIMAGE != ""
                    {
                        print("ADSADDASDSADASFD ==>>",strIMAGE)

                        NSUSERDEFAULT.set(strIMAGE!, forKey: SELECTED_ITEM_IMAGE)

                    }
                    else
                    {
                        NSUSERDEFAULT.set("", forKey: SELECTED_ITEM_IMAGE)
                    }

                    if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                        let Qty = Int(String (describing: Quantity))! + 1
                        Dict.setValue(String (describing: Qty), forKey: "quantity")
                    }
                    else{
                        //Dict.setValue("1", forKey: "quantity")
                    }
                    
                    let canAddComment = Dict["comments_for_item"] as? String ?? "0"
                    
                    if let Arr = Dict.value(forKey: "variations") as? NSArray,Arr.count == 0 && canAddComment == "0" {
                        if let Quantity = Dict.value(forKey: "quantity") as? String{
                            if let ItemID = Dict.value(forKey: "item_id") as? String{
                                self.AddToCart(Qty: Quantity, ItemID: ItemID)
                                return
                            }
                        }
                    }
                    if let cust = Dict.value(forKey: "is_customization") as? String, cust == "0"{
                        if let combo = Dict.value(forKey: "is_combo_product") as? String, combo == "0"
                        {
                            if let ItemID = Dict.value(forKey: "item_id") as? String{
                                self.AddToCart(Qty: "1", ItemID: ItemID)
                                return
                            }
                        }
                    }
                    var Controller = UIViewController()
                    
                    if let combo = Dict.value(forKey: "is_combo_product") as? String, combo == "1"
                    {
                        if let View = self.storyboard?.instantiateViewController(withIdentifier: "ComboProductsViewController") as? ComboProductsViewController{
                            //GlobalsVariable.sharedInstance.ItemAddComeFrom = "FoodController"
                            Controller = View
                            View.DictItem = Dict
                            View.isVeg = self.isVeg
                            View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                                print(Items)
                                
                               // Dict.setValue(String (describing: Qty), forKey: "quantity")
                                self.FetchDetails()
                                self.FetchCartList()
                                self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollCollectionView), userInfo: nil, repeats: true)
                                
                               /* self.FetchCartList()
                                Dict.setValue(String (describing: Qty), forKey: "quantity")
                                let FinalCart = NSMutableArray()
                                let ItemID = Dict.value(forKey: "item_id") as! String
                                for Dict in Items{
                                    if let strID = (Dict as! NSDictionary).value(forKey: "item_id") as? String, strID == ItemID{
                                        FinalCart.add(Dict)
                                    }
                                }
                                Dict.setValue(FinalCart, forKey: "cart_items")
                                self.tblView.reloadData()*/
                            }
                        }
                        Controller.modalPresentationStyle = .formSheet
                        
                        let navigationController = UINavigationController(rootViewController: Controller)
                        navigationController.modalPresentationStyle = .formSheet

                        tabBarController?.present(navigationController, animated: true, completion: nil)
                        return
                    }
                    
                    if #available(iOS 13.0, *) {
                        if let View = self.storyboard?.instantiateViewController(identifier: "ItemMenuViewController") as? ItemMenuViewController{
                            GlobalsVariable.sharedInstance.ItemAddComeFrom = "FoodController"
                            Controller = View
                            View.DictItem = Dict
                            View.isVeg = self.isVeg
                            View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                              
                
                                self.FetchDetails()
                                self.FetchCartList()
                                self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollCollectionView), userInfo: nil, repeats: true)
                            }
                        }
                    } else {
                        if let View = self.storyboard?.instantiateViewController(withIdentifier: "ItemMenuViewController") as? ItemMenuViewController{
                            GlobalsVariable.sharedInstance.ItemAddComeFrom = "FoodController"
                            Controller = View
                            View.DictItem = Dict
                            View.isVeg = self.isVeg
                            View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                                print(Items)
                                
                                Dict.setValue(String (describing: Qty), forKey: "quantity")
                                self.FetchDetails()
                                self.FetchCartList()
                                self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.scrollCollectionView), userInfo: nil, repeats: true)
                                
                               /* self.FetchCartList()
                                Dict.setValue(String (describing: Qty), forKey: "quantity")
                                let FinalCart = NSMutableArray()
                                let ItemID = Dict.value(forKey: "item_id") as! String
                                for Dict in Items{
                                    if let strID = (Dict as! NSDictionary).value(forKey: "item_id") as? String, strID == ItemID{
                                        FinalCart.add(Dict)
                                    }
                                }
                                Dict.setValue(FinalCart, forKey: "cart_items")
                                self.tblView.reloadData()*/
                            }
                        }
                    }
                    
                    Controller.modalPresentationStyle = .formSheet
                    
                    let navigationController = UINavigationController(rootViewController: Controller)
                    navigationController.modalPresentationStyle = .formSheet

                    tabBarController?.present(navigationController, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    func SetupCartPopup(Dict : NSDictionary){
        print(Dict)
        let ItemID = Dict.value(forKey: "item_id") as? String
        ArrayItemCart = NSMutableArray()
        if let ArrCart = Dict.value(forKey: "cart_items") as? NSArray,ArrCart.count != 0{
            for Dict in ArrCart{
                if let dic = Dict as? NSDictionary{
                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                        self.ArrayItemCart.add(dictTemp)
                    }
                }
            }
        }
        //print(ArrayItemCart)
       // print(ArrayItemCart.count)
        self.DictItemCart = Dict
        self.tblCart.reloadData()
        var Height = CGFloat()
        self.tblCart.layoutIfNeeded()
        Height =  tblCart.contentSize.height
        if Height > tblView.frame.height - 35{
            Height = tblView.frame.height - 35
        }
        
        self.viewInnerCart.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: Height + 15)
        self.viewInnerCart.roundCorners([.topLeft, .topRight], radius: 15)
        self.viewInnerCart.layer.masksToBounds = true

        self.viewInnerCart.setNeedsLayout()

        let layout = PopupView.Layout(horizontal: PopupView.HorizontalLayout.center, vertical: PopupView.VerticalLayout.bottom)

        self.popup = PopupView(contentView: self.viewInnerCart, showType: PopupView.ShowType.slideInFromBottom, dismissType: PopupView.DismissType.slideOutToBottom, maskType: PopupView.MaskType.dimmed, shouldDismissOnBackgroundTouch: true, shouldDismissOnContentTouch: false)
        self.popup.show(with: layout)
        self.viewInnerCart.layoutIfNeeded()
    }
    
    func PushtoItemsMenu(Dict : NSDictionary){
        print(Dict)
        self.popup.dismiss(animated: true)
        var Controller = UIViewController()
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "ItemMenuViewController") as? ItemMenuViewController{
                Controller = View
                View.DictItem = Dict
                View.isVeg = self.isVeg
                View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                    
                    print(Items)
                    print(Dict)
                    self.FetchCartList()
                    if let Quantity = Dict.value(forKey: "total_quantity") as? String,!Quantity.isEmpty{
                        let Qty = Int(String (describing: Quantity))! + 1
                        Dict.setValue(String (describing: Qty), forKey: "total_quantity")
                    }
                    if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                        let Qty = Int(String (describing: Quantity))! + 1
                        Dict.setValue(String (describing: Qty), forKey: "quantity")
                    }
                    let FinalCart = NSMutableArray()
                    let ItemID = Dict.value(forKey: "item_id") as! String
                    for Dict in Items{
                        if let strID = (Dict as! NSDictionary).value(forKey: "item_id") as? String, strID == ItemID{
                            FinalCart.add(Dict)
                        }
                    }
                    print(Dict)
                    Dict.setValue(FinalCart, forKey: "cart_items")
                    print(Dict)
                    self.SetupCartPopup(Dict : Dict)
                    self.tblView.reloadData()
                }
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "ItemMenuViewController") as? ItemMenuViewController{
                Controller = View
                View.DictItem = Dict
                View.isVeg = self.isVeg
                View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                    
                    print(Items)
                    print(Dict)
                    self.FetchCartList()
                    if let Quantity = Dict.value(forKey: "total_quantity") as? String,!Quantity.isEmpty{
                        let Qty = Int(String (describing: Quantity))! + 1
                        Dict.setValue(String (describing: Qty), forKey: "total_quantity")
                    }
                    if let Quantity = Dict.value(forKey: "quantity") as? String,!Quantity.isEmpty{
                        let Qty = Int(String (describing: Quantity))! + 1
                        Dict.setValue(String (describing: Qty), forKey: "quantity")
                    }
                    let FinalCart = NSMutableArray()
                    let ItemID = Dict.value(forKey: "item_id") as! String
                    for Dict in Items{
                        if let strID = (Dict as! NSDictionary).value(forKey: "item_id") as? String, strID == ItemID{
                            FinalCart.add(Dict)
                        }
                    }
                    print(Dict)
                    Dict.setValue(FinalCart, forKey: "cart_items")
                    print(Dict)
                    self.SetupCartPopup(Dict : Dict)
                    self.tblView.reloadData()
                }
            }
        }
        Controller.modalPresentationStyle = .formSheet
        
        let navigationController = UINavigationController(rootViewController: Controller)
        navigationController.modalPresentationStyle = .formSheet

        tabBarController?.present(navigationController, animated: true, completion: nil)
    }
}

//MARK:- --- [CollectionView] ---

extension FoodDetailsViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionParallaxView{
            if ArrayBannerImages.count == 0{ return 1 }
            return ArrayBannerImages.count
        }
        else if collectionView == collectionMenu{
            return ArrayMenuCategory.count
        }
        else{
            return 0
        }
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionParallaxView{
            let cell = collectionView.dequeue(ImageCollectionViewCell.self, indexPath: indexPath)
            if ArrayBannerImages.count == 0{
                cell.img.image = UIImage (named: "RestaurantPlaceholder")
            }
            else{
                if let Dict = ArrayBannerImages.object(at: indexPath.row) as? NSDictionary{
                    if let str = Dict.value(forKey: "image") as? String, !str.isEmpty{
                        let availableWidth = collectionView.frame.width
                        var imgUrl:String = str + "&w=\(availableWidth)"
                        imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        cell.img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: UIImage (named: "RestaurantPlaceholder"))
                    }
                }
            }
            
            return cell
        }
        else if collectionView == collectionMenu{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMenu", for: indexPath)
            
            cell.contentView.layer.cornerRadius = 10
            let lblTag = cell.viewWithTag(2) as! UILabel
            
            
            if let Dict = ArrayMenuCategory.object(at: indexPath.item) as? NSDictionary{
//                print(Dict)
                if let str = Dict.value(forKey: "restaurant_menu_name") as? String{
                    lblTag.font = UIFont(name: "Rubik-Regular", size: 14.0)
                    lblTag.text = str
                }
            }
            
            if self.scolledPositionIndexpath != nil && self.scolledPositionIndexpath.section == indexPath.item {
                lblTag.textColor = UIColor.white
                cell.contentView.backgroundColor = COLOR_TURQUOISE!

            } else {
                lblTag.textColor = UIColor.black
                cell.contentView.backgroundColor = UIColor.clear
            }
            return cell
        }
        else{
            let cell = UICollectionViewCell()
            return cell
        }
        
    }
    
    /*func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionMenu{
//            SelectedMenuIndex = indexPath.item
//            if let Dict = self.ArrayMenuCategory.object(at: indexPath.item) as? NSDictionary{
//                if !self.ArraySelectedMenu.contains(Dict){
//                    self.ArraySelectedMenu = NSMutableArray()
//                    self.ArraySelectedMenu.add(Dict)
//                    self.collectionMenu.reloadData()
//                    self.collectionMenu.scrollToItem(at:IndexPath(item: indexPath.item, section: 0), at: .centeredHorizontally, animated: true)
//                    self.GetSelectedMenu(Dict: Dict)
//                    print(self.ArrayMenu)
//                }
//            }
            var direction: UITableView.ScrollPosition = .none
            if indexPath.item > self.scolledPositionIndexpath.section {
                //direction = .bottom
            } else {
                //direction = .top
            }
            self.isRequiredHardReload = false
            print(indexPath)
            print("The selected index path row: \(self.scolledPositionIndexpath)")
            self.collectionMenu.reloadData()
            self.scolledPositionIndexpath = IndexPath(row: 0, section: indexPath.item)
//            self.tblView.scrollToRow(at: self.scolledPositionIndexpath, at: .top, animated: true)
            let rect = self.tblView.rectForRow(at: self.scolledPositionIndexpath)
//            let newRect = self.view.convert(rect, to: self.tblView)
            self.tblView.scrollRectToVisible(rect, animated: true)
            //self.tblView.scrollToRow(at: self.scolledPositionIndexpath, at: direction, animated: true)
        }
//        self.CheckBottomMenu()
    }*/
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
            if collectionView == collectionParallaxView
            {
                
                if let View = self.storyboard?.instantiateViewController(identifier: "ShowImagesVC") as? ShowImagesVC{
                View.comeFrom = "bannerImages"
                View.RestaurantID = RestaurantID
                View.RestaurantName = RestaurantName
                self.navigationController?.pushViewController(View, animated: true)
                }
            }
           else if collectionView == collectionMenu {
                /*var direction: UITableView.ScrollPosition = .none
                if indexPath.item > self.scolledPositionIndexpath.section {
                    direction = .bottom
                } else {
                    direction = .top
                }*/
                self.isRequiredHardReload = false
                self.scolledPositionIndexpath = IndexPath(row: 0, section: indexPath.item)
                self.collectionMenu.reloadData()
                
                /* Insets = 646, OffSet = -646, Menu = maxY (158) */
                var y = ((self.tblView.contentInset.top * 0.25) * -1) + 1
                let rowFrame = self.tblView.rectForRow(at: IndexPath(row: 0, section: self.scolledPositionIndexpath.section))
                let sectionFrame = self.tblView.rect(forSection: indexPath.item)
                if y < 0 {
                    y = y + rowFrame.minY
                } else {
                    y = y - rowFrame.minY
                }
                
                // Section Height
                y -= rowFrame.minY - sectionFrame.minY
                
                // Do not scroll more than Content size
                if self.tblView.frame.height  > self.tblView.contentSize.height - y {
                    y = self.tblView.contentSize.height - self.tblView.frame.height
                }
                
                if y == self.tblView.contentOffset.y {
                    self.isRequiredHardReload = true
                } else {
                    self.tblView.setContentOffset(CGPoint(x: self.tblView.contentOffset.x, y: y), animated: true)
                }
                
    //            self.tblView.scrollToRow(at: self.scolledPositionIndexpath, at: .top, animated: true)
                /*if self.scolledPositionIndexpath.section == 0 {
                    /* Insets = 646, OffSet = -646, Menu = maxY (158) */
                    let y = ((self.tblView.contentInset.top * 0.25) * -1) + 1
                    self.tblView.setContentOffset(CGPoint(x: self.tblView.contentOffset.x, y: y), animated: true)
                    return
                } else if self.scolledPositionIndexpath.section == 2 {
                    var y = ((self.tblView.contentInset.top * 0.25) * -1) + 1
                    let rowFrame = self.tblView.rectForRow(at: IndexPath(row: 0, section: self.scolledPositionIndexpath.section))
                    let sectionFrame = self.tblView.rect(forSection: self.scolledPositionIndexpath.section)
                    y = y + sectionFrame.height + rowFrame.minY
                    self.tblView.setContentOffset(CGPoint(x: self.tblView.contentOffset.x, y: y), animated: true)
                    return
                } else {
                    self.tblView.rect(forSection: indexPath.item)
                    
                    let rect = self.tblView.rectForRow(at: self.scolledPositionIndexpath)
                    let newRect = self.tblView.convert(rect, to: self.view)
                    let selectedSection = indexPath.row
                    let rowCount = self.tblView.numberOfRows(inSection: selectedSection)
                    
                    /*If all row visible {
                       Do not scroll
                    } else {
                       scrol
                    }*/
                    if let firstVisibleIndexPath = self.tblView.indexPathsForVisibleRows?.first,
                        let lastVisibleIndexPath = self.tblView.indexPathsForVisibleRows?.last
                    {
                        if selectedSection >= firstVisibleIndexPath.section && selectedSection <= lastVisibleIndexPath.section {
                            return
                        } else {
                            self.tblView.scrollToRow(at: self.scolledPositionIndexpath, at: direction, animated: true)
                        }
                    }
                }*/
                //self.tblView.scrollRectToVisible(newRect, animated: true)
                //self.tblView.scrollToRow(at: self.scolledPositionIndexpath, at: direction, animated: true)
            }
        }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView == self.tblView {
                let newCentre = self.view.convert(self.view.center, to: self.tblView)
                if let cellScrollIndexpath = self.tblView.indexPathForRow(at: newCentre) {
                    if self.scolledPositionIndexpath.section != cellScrollIndexpath.section {
                        self.scolledPositionIndexpath = cellScrollIndexpath
                        
                        if self.isRequiredHardReload {
                            self.collectionMenu.reloadData()
                        }
                        
                        DispatchQueue.main.async {
                            /*if let cell = self.collectionMenu.cellForItem(at: IndexPath(item: self.scolledPositionIndexpath.section, section: 0)) {
                                //let visibleWidth = self.collectionMenu.contentSize.width - self.collectionMenu.contentOffset.x
                                let visibleWidth = self.collectionMenu.frame.width
                                //let trailingWidth = visibleWidth - self.collectionMenu.frame.maxX
                                if cell.frame.minX < self.collectionMenu.contentOffset.x {
                                    /*
                                     OffSet - Cell center to CV center
                                     */
                                    let cvCenter = visibleWidth / 2
                                    let desireX = cvCenter - (cell.frame.width / 2)
                                    let newContentX = self.collectionMenu.contentOffset.x - desireX
                                    self.collectionMenu.setContentOffset(CGPoint(x: newContentX, y: self.collectionMenu.contentOffset.y), animated: true)
                                } else {
                                    let cvCenter = visibleWidth / 2
                                    let desireX = cvCenter - (cell.frame.width / 2)
                                    let newContentX = self.collectionMenu.contentOffset.x + desireX
                                    self.collectionMenu.setContentOffset(CGPoint(x: newContentX, y: self.collectionMenu.contentOffset.y), animated: true)
                                }
                                /*let x = cell.center.x - self.collectionMenu.frame.height / 2
                                if x >= 0 {

                                    let offSet = CGPoint(x: cell.center.x - self.collectionMenu.frame.height / 2,
                                                         y: self.collectionMenu.contentOffset.y)
                                    self.collectionMenu.setContentOffset(offSet, animated: true)
                                }*/
                            }*/
                            
                            
                            self.collectionMenu.scrollToItem(
                                at: IndexPath(item: self.scolledPositionIndexpath.section, section: 0),
                                at: .centeredHorizontally,
                                animated: true)
                        }
                    }

                }
            }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == self.tblView && !decelerate {
            checkTableViewDidScrolledToBottom()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.tblView  {
            checkTableViewDidScrolledToBottom()
        }
    }
    
    func checkTableViewDidScrolledToBottom() {
        if let lastIndexPath = self.tblView.indexPathsForVisibleRows?.last {
            if lastIndexPath.section == self.tblView.numberOfSections - 1 {
                let rowCount = self.tblView.numberOfRows(inSection: lastIndexPath.section)
                if lastIndexPath.row == rowCount - 1 &&
                    self.scolledPositionIndexpath.section != lastIndexPath.section {
                    self.collectionView(self.collectionMenu, didSelectItemAt: IndexPath(item: lastIndexPath.section, section: 0))
                    self.isRequiredHardReload = true
                }
            }
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView == self.tblView  {
            self.isRequiredHardReload = true
        }
    }
}


extension FoodDetailsViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionParallaxView{
            return CGSize(width: self.view.frame.width, height: 250)
        }
      
        else if collectionView == collectionMenu{
            if let Dict = ArrayMenuCategory.object(at: indexPath.item) as? NSDictionary{
                if let str = Dict.value(forKey: "restaurant_menu_name") as? String{
                    let labelSize = UILabel.estimatedSize("   " + str, targetSize: CGSize(width: view.frame.size.width / 2 - 10, height: 0))
                    let cellSize = CGSize(width: labelSize.width, height: 40)
                    return cellSize
                }
            }
            return CGSize(width: 0 , height: 0)
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if collectionView == collectionParallaxView{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == collectionParallaxView{
            return 0
        }
        else{
            return 0
        }

    }
}
//MARK:- --- [API CALLING] ---

extension FoodDetailsViewController{
    
    func FetchDetails(){
        if !GlobalMethod.internet(){
            
        }else{
            var Veg = ""
            if self.isVeg{
                Veg = "0"
            }
            self.tblView.showAnimatedGradientSkeleton()
            self.viewHeader.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                      "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                      "restaurant_id":RestaurantID,
                                      "pure_veg":Veg,
                                      "apiId":API_ID,
                                      "language": GlobalMethod.selectedLanguageForUser()]
            print("FOOD DETAIL PARAMETER =====>>",parameter)
            ServManager.viewController = self
            self.restaurantDetailsRequest = ServManager.callPostAPI(constant.DETAILS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                self.viewHeader.hideSkeleton()
                
                if isSuccess{
                    if let dictRestaurant = dataDict.value(forKey: "restaurant") as? NSDictionary{
                        print("FOOD DETAIL RESPONSE =====>>",dataDict)

                        self.ArrayBannerImages = NSMutableArray()
                        self.Arraycuisines = NSMutableArray()
                        self.ArrayMenuCategory = NSMutableArray()
                        self.ArraySelectedMenu = NSMutableArray()
                        self.ArrayMenuImages = NSMutableArray()

                        
                        
                        if let str = dictRestaurant.value(forKey: "name") as? String{
                            self.lblRestaurantName.text = str
                            self.RestaurantName = str
                        }
                        if let str = dictRestaurant.value(forKey: "address") as? String{
                            self.lblAddress.text = str
                        }
                        if let str = dictRestaurant.value(forKey: "delivery_time") as? String{
                            self.lblTime.text = str
                        }
                        
                        if let str = dictRestaurant.value(forKey: "min_order_amount") as? String{
                            self.lblOther.text = "Min : " + CURRANCY + " " + str
                        }
                        
                        if let str = dictRestaurant.value(forKey: "logo") as? String{
                            DispatchQueue.main.async {
                                self.imgRestaurant.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "RestaurantPlaceholder"))
                            }

                        }
                        
                        
                        
                        if let Arr = dictRestaurant.value(forKey: "images") as? NSArray, Arr.count != 0{
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayBannerImages.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if let Arr = dictRestaurant.value(forKey: "cuisines") as? NSArray, Arr.count != 0{
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.Arraycuisines.add(dictTemp)
                                    }
                                }
                            }
                        }

                        if let Arr = dictRestaurant.value(forKey: "menu_category") as? NSArray, Arr.count != 0{
                            print(Arr)
                            print(Arr.count)
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary
                                    {
                                        dictTemp.setValue(dictTemp, forKey: "user")
//                                        if let ArrCat = dic.value(forKey: "user") as? NSArray,ArrCat.count != 0
//                                        {
//                                            if let ArrCatTemp = (ArrCat.mutableCopy()) as? NSMutableArray
//                                            {
//                                                //dictTemp.setValue(ArrCatTemp, forKey: "user")
//                                                let ARRAY = NSMutableArray()
//                                                for (index,Dictionary) in ArrCatTemp.enumerated()
//                                                {
//                                                    if let dictionary = Dictionary as? NSDictionary{
//                                                        if let dictTempInner = (dictionary.mutableCopy()) as? NSMutableDictionary{
//                                                            ArrCatTemp.replaceObject(at: index, with: dictTempInner)
//                                                        }
//                                                    }
//                                                }
//                                            }
////
//                                            self.ArrayMenuCategory.add(dictTemp)
//                                        }
                                        self.ArrayMenuCategory.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        
                        if let Arr = dictRestaurant.value(forKey: "menu_images") as? NSArray, Arr.count != 0{
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayMenuImages.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if self.ArrayMenuImages.count != 0 {
                            
                            var blackSquare: UIView!
                            blackSquare = UIView(frame: CGRect(x: self.view.bounds.maxX - 130, y: 170, width: 120, height: 35))
                            blackSquare.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
                            blackSquare.backgroundColor = UIColor.white
                            blackSquare.layer.cornerRadius = 15
                            blackSquare.layer.borderWidth = 0.5
                            blackSquare.layer.borderColor = UIColor.gray.cgColor
                            
                            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 35))
                            label.textAlignment = .center
                            label.text = "See Full Menu"
                            label.font = UIFont.boldSystemFont(ofSize: 13)
                            label.textColor = UIColor.darkGray
                            blackSquare.addSubview(label)
                            
                            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
                            label.isUserInteractionEnabled = true
                            label.addGestureRecognizer(tap)
                            
                            self.viewHeader.addSubview(blackSquare)
                        }
                        
                        print("FULL IMAGE COUNT==>>",self.ArrayMenuImages.count)
                      /*  if let str = dictRestaurant.value(forKey: "price_for_one") as? String, !str.isEmpty{
                            self.lblPrice.text = CURRANCY + " " + str + " per person".localized
                            if let strTwo = dictRestaurant.value(forKey: "price_for_two") as? String{
                                self.lblPrice.text = self.lblPrice.text! + " | " + CURRANCY + " " + strTwo + " for two".localized
                            }
                        }
                        else if let str = dictRestaurant.value(forKey: "price_for_two") as? String, !str.isEmpty{
                            self.lblPrice.text = CURRANCY + " " + str + " for two"
                        }
                        else{
                            self.lblPrice.text = ""
                        }
                        */
                        if self.ArrayMenuCategory.count != 0 {
                            if let Dict = self.ArrayMenuCategory.object(at: 0) as? NSDictionary{
                                self.GetSelectedMenu(Dict: Dict)
                                self.ArraySelectedMenu.add(Dict)
                            }
                        }
                        if let Fav = dictRestaurant.value(forKey: "is_favourite") as? Int, Fav == 1{
                            self.isFav = true
                           // self.btnFav.setImage(#imageLiteral(resourceName: "Fav"), for: .normal)
                            self.imgFav.image = UIImage.init(named: "fill_heart.png")

                        }
                        
                        // Manthan 1-10-2020
                        // RestaurantView Model
                        // RestaurantInfo Model
                        let finalReview = dictRestaurant["final_review"] as? String ?? "0"
                        let finalReviewCount = dictRestaurant["final_review_count"] as? String ?? "0"
                        var userReviews = [UserReview]()
                        let reviewJson = dictRestaurant["restaurant_reviews"] as? [[String: Any]] ?? []
                        if let jsonData = try? JSONSerialization.data(withJSONObject: reviewJson, options: .prettyPrinted) {
                            userReviews = (try? JSONDecoder().decode([UserReview].self, from: jsonData)) ?? []
                        }
                        
                        self.restaurantReview = RestaurantReview(reviews: userReviews, finalReview: finalReview, finalReviewCount: finalReviewCount)
                        print("\nRestaurant Review Count:\t\(self.restaurantReview.reviews.count)\n")
                        
                        self.lblReview.text = "(\(self.restaurantReview.reviews.count))"
                        let timeJson = dictRestaurant["timing"] as? [[String: Any]] ?? []
                        let address = dictRestaurant["address"] as? String ?? ""
                        let description = dictRestaurant["description"] as? String ?? ""
                        let name = dictRestaurant["name"] as? String ?? ""
                        var timingModel = [RestaurantTime]()
                        if let jsonData = try? JSONSerialization.data(withJSONObject: timeJson, options: .prettyPrinted) {
                            timingModel = (try? JSONDecoder().decode([RestaurantTime].self, from: jsonData)) ?? []
                        }
                        
                        self.restaurantInfo = RestaurantInfo(timing: timingModel, restaurantName: name, address: address, description: description)
                        print("\nRestaurant Time Count:\t\(self.restaurantInfo.timing.count)\n")
                        //
                        
//                        self.collectionParallaxView.reloadData()
//                        self.collectioncuisine.reloadData()
//                        self.collectionMenu.reloadData()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                            self.collectionParallaxView.reloadData()
                           // self.collectioncuisine.reloadData()
                            self.collectionMenu.reloadData()
                            self.tblView.reloadData()
                        }
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }
            }
        }
    }
    
    func FetchCartList(){
            if !GlobalMethod.internet(){
                
            }else{
                
                let parameter : [String : Any] = ["from_app":FROMAPP,
                                                  "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                                  "apiId":API_ID,
                                                  "language": GlobalMethod.selectedLanguageForUser()]
                print(parameter)
                ServManager.viewController = self
                self.cartDetailsRequest = ServManager.callPostAPI(constant.CART_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                   // print(dataDict)
                    if isSuccess{
                        
                        if let Data = CartListModel.init(dictionary: dataDict){
                            
                            self.viewCart.isHidden = false
                            self.constraintViewCartHeight.constant = 50
                            if let str = Data.restaurant!.value(forKey: "restaurant_id") as? String, str == self.RestaurantID{
                                self.SameRestaurant = true
                            }
                            else{
                                self.SameRestaurant = false
                            }
                            if let str = Data.restaurant!.value(forKey: "name") as? String{
                                self.PreviousRestaurntName = str
                            }
                            if let str = Data.cart!.value(forKey: "total_quantity") as? String{
                                if let strTotal = Data.cart!.value(forKey: "grand_total") as? String{
                                    if str == "0"{
                                        self.viewCart.isHidden = true
                                        self.constraintViewCartHeight.constant = 0
                                    }
                                    else if str == "1"{
                                        self.lblCart.text = str + " item | " + CURRANCY + " " + strTotal
                                    }
                                    else{
                                        self.lblCart.text = str + " items | " + CURRANCY + " " + strTotal
                                    }
                                }
                            }
                            
                            if self.SameRestaurant
                            {  
                                self.viewCart.isHidden = false
                                self.constraintViewCartHeight.constant = 50
                            }
                            else
                            {
                                self.viewCart.isHidden = true
                                self.constraintViewCartHeight.constant = 0
                            }
                            
                        }
                    }else{
                        self.viewCart.isHidden = true
                        self.constraintViewCartHeight.constant = 0
//                        GlobalMethod.ShowToast(Text: msg, View: self.view)
                    }
                }
            }
        }
    
    func FavUnfav(){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,"restaurant_id":RestaurantID,"user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,"apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.RESTAURANT_FAV, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    print(dataDict)
                }else{
                    self.viewCart.isHidden = true
                    self.constraintViewCartHeight.constant = 0
//                        GlobalMethod.ShowToast(Text: msg, View: self.view)
                }
            }
        }
    }
    
    func DeleteFromCart(cart_id : String){
        if !GlobalMethod.internet(){
            
        }else{
//            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,"apiId":API_ID, "cart_id":cart_id]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.DELETE_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    print(dataDict)
                    self.FetchDetails()
                    self.FetchCartList()
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func FetchItemDetails(ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                      "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                      "item_id":ItemID,
                                      "apiId":API_ID,
                                      "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.MENU_ITEM_DETAILS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let dictRestaurant = dataDict.value(forKey: "restaurant") as? NSDictionary{
                        
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func AddToCart(Qty : String, ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
//            GlobalMethod.StartProgress(self.view)
            
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                      "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                      "item_id":ItemID,
                                      "selected_variation":"",
                                      "addons":"",
                                      "quantity": Qty,
                                      "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                      "is_customization": "0",
                                      "variation_id":"",
                                      "apiId":API_ID,
                                      "language": GlobalMethod.selectedLanguageForUser()]
           // print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADD_TO_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                   // print(dataDict)
                    var Array = NSMutableArray()
                    if let Dict = dataDict.value(forKey: "cart") as? NSDictionary{
                        if let str = Dict.value(forKey: "total_quantity") as? String{
                            if let strTotal = Dict.value(forKey: "grand_total") as? String{
                                if str == "0"{
                                    self.viewCart.isHidden = true
                                    self.constraintViewCartHeight.constant = 0
                                }
                                else if str == "1"{
                                    self.lblCart.text = str + " item | " + CURRANCY + " " + strTotal
                                    self.viewCart.isHidden = false
                                    self.constraintViewCartHeight.constant = 50
                                }
                                else{
                                    self.lblCart.text = str + " items | " + CURRANCY + " " + strTotal
                                    self.viewCart.isHidden = false
                                    self.constraintViewCartHeight.constant = 50
                                }
                            }
                        }
                        
                        

                        if let Arr = Dict.value(forKey: "cart_items") as? NSArray{
                            if let DictCartItems = Arr.object(at: Arr.count - 1) as? NSDictionary{
                                Array.add(DictCartItems)
                            }
                            
                        }
                    }
                    
                    if let Dictionary = self.ArrayMenuCategory.object(at: self.selectedSection) as? NSMutableDictionary{
                        if let Arr = Dictionary.value(forKey: "menu_items") as? NSMutableArray{
                            if let Dict = Arr.object(at: self.selectedIndex) as? NSMutableDictionary{
                                Dict.setValue(Array, forKey: "cart_items")
                                Dict.setValue("1", forKey: "total_quantity")
                            }
                        }
                    }
                    self.self.FetchDetails()
                    self.FetchCartList()
                    //self.tblView.reloadData()
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func UpdateCart(cart_id : String, Qty : String){
        if !GlobalMethod.internet(){
            
        }else{
//            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "quantity":Qty,
                                              "apiId":API_ID,
                                              "cart_id":cart_id,
                                              "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                              "language": GlobalMethod.selectedLanguageForUser()]
            print("UPDATE CART PARAMETER =====>>>>",parameter)
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.UPDATE_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                
                if isSuccess{
                    //print(self.DictItemCart)
                    print(dataDict)
                    print("UPDATE CART REPONSE COUNT =====>>>>",dataDict.count)

                    GlobalMethod.StopProgress(self.view)
                    
                    var Array = NSMutableArray()
                    if let Dict = dataDict.value(forKey: "cart") as? NSDictionary{
                        // print(Dict)
                        if let str = Dict.value(forKey: "total_quantity") as? String{
                            if let strTotal = Dict.value(forKey: "grand_total") as? String{
                                if str == "0"{

                                    self.viewCart.isHidden = true
                                    self.constraintViewCartHeight.constant = 0
                                }
                                else if str == "1"{
                                   if let strID = Dict.value(forKey: "cart_id") as? String{
                                        self.DeleteFromCart(cart_id: strID)
                                    }
                                }
                                else{
                                    self.lblCart.text = str + " items | " + CURRANCY + " " + strTotal
                                    self.viewCart.isHidden = false
                                    self.constraintViewCartHeight.constant = 50
                                }
                            }
                        }
                        
                        // Manthan 10-9-2020
                        // Update Cart items in Global variable `DictItemCart` from api response
                        /*let ItemID = self.DictItemCart.value(forKey: "item_id") as! String
                        if let Arr = Dict.value(forKey: "cart_items") as? NSArray{
                            for Dict in Arr{
                                if let strID = (Dict as! NSDictionary).value(forKey: "item_id") as? String, strID == ItemID{
                                    Array.add(Dict)
                                }
                            }
                        }
                        self.DictItemCart.setValue(Array, forKey: "cart_items")*/
                        
                        if let cartItems = Dict.value(forKey: "cart_items") as? NSArray {
                            print("cartItems count",cartItems.count)
                            
                            let qty = cartItems.value(forKey: "quantity") as? String
                            print("cartItems qty",qty)

                            self.DictItemCart = NSDictionary.init(dictionary: [cartItems: "cart_items"])
                            
                        }
                        
                       
                        //
                    }
                    self.tblView.reloadData()
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}
extension UIColor {
   class var separatorColor: UIColor {
     return UIColor(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1.0)
   }
}

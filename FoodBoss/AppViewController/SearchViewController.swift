//
//  SearchViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 19/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITextFieldDelegate {
    
    private var SearchString = ""
    private var ArrayCuisine = NSMutableArray()
    private var ArrayRestaurant = NSMutableArray()
    
    @IBOutlet weak var lblCuisines: UILabel!
    
    @IBOutlet weak var lblRestaurant: UILabel!
    @IBOutlet weak var constraintviewCuisineHeight: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: InsetTextField!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var viewCuisines: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collectionCuisines: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.registerNib(SearchRestaurantTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        
        viewCuisines.isHidden = true
        tblView.isHidden = true
        btnClear.isHidden = true
        lblRestaurant.isHidden = true
        CustomisedNavigationBar()
        
    }
    
    func CustomisedNavigationBar()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = textField.text!.count + string.count - range.length
        
        
        if newLength == 0 {
            btnClear.isHidden = true
        }
        else{
            btnClear.isHidden = false
        }

        if newLength > 2 {
            if let text = textField.text, let textRange = Range(range, in: text) {
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                SearchString = updatedText
                
            }
        }
        return true
    }
    
    @IBAction func ActionClear(_ sender: Any) {
        self.ArrayCuisine = NSMutableArray()
        self.ArrayRestaurant = NSMutableArray()
        viewCuisines.isHidden = true
        tblView.isHidden = true
        btnClear.isHidden = true
        txtSearch.text = ""
        lblRestaurant.isHidden = true
    }
    
    @IBAction func ActionSearch(_ sender: Any)
    {
        if SearchString.isEmpty{
            return
        }
        self.Search()
    }
}

extension SearchViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayRestaurant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SearchRestaurantTableViewCell.self)
        if let Dict = ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = self.ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
}

extension SearchViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.ArrayCuisine.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategory", for: indexPath)

        let img = cell.viewWithTag(2) as! UIImageView
        let lbl = cell.viewWithTag(3) as! UILabel
        
        if let Dict = self.ArrayCuisine.object(at: indexPath.row) as? NSDictionary{
            if let str = Dict.value(forKey: "icon") as? String{
                var imgUrl:String = str
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
            }
            if let str = Dict.value(forKey: "name") as? String{
                lbl.text = str.capitalized
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
}

extension SearchViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100, height: 100)
    }
}

extension SearchViewController{
    
    func Search(){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "search":SearchString,
                                        "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                        "latitude":LATTITUDE,
                                        "longitude":LONGITUDE,]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.SEARCH_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    if let Data = SearchModel.init(dictionary: dataDict){
                        self.ArrayCuisine = NSMutableArray()
                        self.ArrayRestaurant = NSMutableArray()
                        
                        if let Arr = Data.search!.value(forKey: "cuisines") as? NSArray{
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayCuisine.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if let Arr = Data.search!.value(forKey: "restaurant") as? NSArray{
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayRestaurant.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if self.ArrayCuisine.count == 0 {
                            self.viewCuisines.isHidden = true
                            self.constraintviewCuisineHeight.constant = 0
                        }
                        else{
                            self.viewCuisines.isHidden = false
                            self.constraintviewCuisineHeight.constant = 153
                        }
                        self.collectionCuisines.reloadData()
                        self.tblView.reloadData()
                        self.viewCuisines.isHidden = false
                        self.tblView.isHidden = false
                        self.lblRestaurant.isHidden = false
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

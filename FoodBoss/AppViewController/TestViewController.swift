//
//  TestViewController.swift
//  FoodBoss
//
//  Created by Apple on 17/09/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    var coupons = [NSMutableDictionary]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        FetchCouponList()
    }
    
    func FetchCouponList(){
            if !GlobalMethod.internet(){
                
            }else{
                let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID, "language":GlobalMethod.selectedLanguageForUser()]
                print(parameter)
                ServManager.viewController = self
                ServManager.callPostAPI(constant.COUPON_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                    if isSuccess{
                        self.coupons = []
                        if let Data = CouponListModel.init(dictionary: dataDict){
                            for Dict in Data.coupons!{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.coupons.append(dictTemp)
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }else{
                        GlobalMethod.ShowToast(Text: msg, View: self.view)
                        GlobalMethod.StopProgress(self.view)
                    }
                }
            }
        }
}

extension TestViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.coupons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coupon", for: indexPath)

        let img = cell.viewWithTag(1) as! UIImageView
        
        if let Dict = self.coupons[indexPath.row] as? NSDictionary{
            if let str = Dict.value(forKey: "coupon_image") as? String{
                //let availableWidth = collectionView.frame.width
                let availableWidth = img.frame.width
                var imgUrl:String = str + "&w=\(availableWidth)"
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
            }
        }
        return cell
    }
    
}

extension TestViewController: UICollectionViewDelegate {
    
}

extension TestViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - collectionView.contentInset.right - collectionView.contentInset.left
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

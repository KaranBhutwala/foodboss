//
//  SavedPaymentMethodsViewController.swift
//  FoodBoss
//
//  Created by Apple on 24/09/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class SavedPaymentMethodsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var paymentMethodList = [PayPalPaymentMethod]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        self.tableView.registerNib(PaypalPaymentMethodTableViewCell.self)
        self.tableView.tableFooterView = UIView()
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true

        self.fetchPaymentMethods()
    }
    
    func addNavigation() {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = "Saved Payment Method".localized
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        
        let buttonFilter = UIButton(type: .custom)
        let image = UIImage(named: "Filter")?.withRenderingMode(.alwaysTemplate)
        buttonFilter.setImage(image, for: .normal)
        buttonFilter.imageEdgeInsets = UIEdgeInsets (top: 5, left: 5, bottom: 5, right: 5)
        buttonFilter.tintColor = UIColor.black
    }
    
   
    
    
    @objc func ActionBack() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func updateDefaultPaymentMethod(sender: UIButton) {
        let paymentMethod = self.paymentMethodList[sender.tag]
        if !paymentMethod.isDefault {
            self.setDefaultPaymentMethod(token: paymentMethod.token) { (isSuccess, message) in
                DispatchQueue.main.async {
                    if isSuccess {
                        if let oldIndex = self.paymentMethodList.lastIndex(where: { $0.isDefault == true }) {
                            self.paymentMethodList[oldIndex].isDefault = false
                            self.paymentMethodList[sender.tag].isDefault = true
                            self.tableView.reloadRows(at: [IndexPath(row: oldIndex, section: 0), IndexPath(row: sender.tag, section: 0)],
                                                      with: .automatic)
                        } else {
                            self.fetchPaymentMethods()
                        }
                    } else {
                        GlobalMethod.ShowToast(Text: message, View: self.view)
                    }
                }
            }
        }
    }
    
    func proceedToDeletePatmentMethod(at indexPath: IndexPath) {
        self.deletePaymentMethod(token: self.paymentMethodList[indexPath.row].token) { (isSuccess, message) in
            if isSuccess {
                DispatchQueue.main.async {
                    self.paymentMethodList.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .left)
                }
            }
        }
    }

}

extension SavedPaymentMethodsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let paymentMethod = paymentMethodList[indexPath.row]
        let cell = tableView.dequeue(PaypalPaymentMethodTableViewCell.self)
        cell.paymentMethodImageView.sd_setImage(with: URL(string: paymentMethod.imageUrl))
        
        if paymentMethod.isDefault {
            cell.defaultStateImageView.image = UIImage(named: "checked")
        } else {
            cell.defaultStateImageView.image = UIImage(named: "uncheck")
        }
        
        cell.defaultStateImageView.setImageColor(color: GRAY!)
        
        if paymentMethod.paymentType == .card {
            cell.paymentMethodTitle.text = paymentMethod.cardType ?? ""
            cell.paymentMethodDetails.text = paymentMethod.maskedNumber ?? ""
        } else {
            cell.paymentMethodTitle.text = "PayPal"
            cell.paymentMethodDetails.text = paymentMethod.email ?? ""
        }
        
        cell.setDefaultButton.tag = indexPath.row
        
        cell.setDefaultButton.addTarget(self, action: #selector(updateDefaultPaymentMethod(sender:)), for: .touchUpInside)
        
        GlobalMethod.applyShadowCardView(cell.viewWithTag(4)!, andCorner: 10)
        return cell
    }
    
}

extension SavedPaymentMethodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt
        indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (_, _, _) in
            self.proceedToDeletePatmentMethod(at: indexPath)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
}

extension SavedPaymentMethodsViewController {
    func fetchPaymentMethods() {
        if !GlobalMethod.internet() {
            
        } else {
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID,
                                              "language": GlobalMethod.selectedLanguageForUser(),
                                              "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.SAVED_PAYMENT_METHOD,
                                    isLoaderRequired: true,
                                    paramters: parameter) { (isSuccess, status, msg, dataDict) in
                self.paymentMethodList = []
                if isSuccess {
                    let creditCardList = dataDict["caredit_cards"] as? [Dictionary<String, Any>] ?? []
                    let paypalAccounts = dataDict["paypalAccounts"] as? [Dictionary<String, Any>] ?? []
                    self.paymentMethodList.append(contentsOf: creditCardList.compactMap({ PayPalPaymentMethod(data: $0) }))
                    self.paymentMethodList.append(contentsOf: paypalAccounts.compactMap({ PayPalPaymentMethod(data: $0) }))
                    GlobalMethod.StopProgress(self.view)
                    //GlobalMethod.StopProgress(self.view)
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
                                        
                self.tableView.reloadData()
            }
        }
        
    }
    
    func setDefaultPaymentMethod(token: String, completion: @escaping (_ isSuccess: Bool, _ message: String) -> Void) {
        if !GlobalMethod.internet() {
            
        } else {
//            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID,
                                              "language": GlobalMethod.selectedLanguageForUser(),
                                              "token": token]
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.SET_DEFAULT_PAYMENT_METHOD,
                                    isLoaderRequired: true,
                                    paramters: parameter) { (isSuccess, status, msg, dataDict) in
                /*if isSuccess {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    self.fetchPaymentMethods()
                } else {
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }*/
                completion(isSuccess, msg)
            }
        }
        
    }
    
    func deletePaymentMethod(token:String, completion: @escaping (_ isSuccess: Bool, _ message: String) -> Void) {
        if !GlobalMethod.internet() {
            
        } else {
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID,
                                              "language": GlobalMethod.selectedLanguageForUser(),
                                              "token": token]
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.DELETE_PAYMENT_METHOD,
                                    isLoaderRequired: true,
                                    paramters: parameter) { (isSuccess, status, msg, dataDict) in
                completion(true, msg)
            }
        }
    }
}

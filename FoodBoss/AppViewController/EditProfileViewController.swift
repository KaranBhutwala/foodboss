//
//  EditProfileViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 07/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Alamofire

protocol UpdateProfileActionDelegate : class {
    func UpdateButtonPressed(FirstName: String, LastName: String , Mobile: String, Email: String)
}

class EditProfileCustomTableViewCell: UITableViewCell {
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var txtFirstName: MyTextField!
    @IBOutlet weak var txtLastName: MyTextField!
    @IBOutlet weak var txtMobile: MyTextField!
    @IBOutlet weak var txtEmail: MyTextField!
    @IBOutlet weak var viewBackground: UIView!
    
    
    var delegate:UpdateProfileActionDelegate!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        self.setupTextFields()
//        self.btn.layoutIfNeeded()
//        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
//    self.txtEmail.isEnabled = false
    self.btn.layer.cornerRadius = 15
    self.btn.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
    
    self.viewBackground.layer.cornerRadius = 10
    self.viewBackground.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
    
    
    if let dicUser = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary{
        if let str = dicUser.value(forKey: "first_name") as? String{
            txtFirstName.text = str
        }
        if let str = dicUser.value(forKey: "last_name") as? String{
            txtLastName.text = str
        }
        if let str = dicUser.value(forKey: "mobile") as? String{
            txtMobile.text = str
        }
        if let str = dicUser.value(forKey: "email") as? String,!str.isEmpty{
            txtEmail.text = str
        }
        else{
            self.txtEmail.isEnabled = true
        }
    }
//        layerMaxXMaxYCorner – lower right corner
//        layerMaxXMinYCorner – top right corner
//        layerMinXMaxYCorner – lower left corner
//        layerMinXMinYCorner – top left corner

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    @IBAction func ActionSave(_ sender: Any) {
        var ViewController = UIViewController()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            if let View = mainStoryboard.instantiateViewController(identifier: "EditProfileViewController") as? EditProfileViewController{
                ViewController = View
            }
        } else {
            if let View = mainStoryboard.instantiateViewController(withIdentifier: "AddLocationViewController") as? EditProfileViewController{
                ViewController = View
            }
        }
        
        if (txtFirstName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtFirstName.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: "Please enter First Name.".localized, View: ViewController)
        }
        else if (txtLastName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtLastName.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: "Please enter Last Name.".localized, View: ViewController)
        }
        else if (txtMobile.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtMobile.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: "Please enter Mobile Number.".localized, View: ViewController)
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            self.txtEmail.becomeFirstResponder()
            GlobalMethod.ShowPopAlert(Message: "Please enter Email.".localized, View: ViewController)
        }
        else{
            delegate.UpdateButtonPressed(FirstName: txtFirstName.text!, LastName: txtLastName.text!, Mobile: txtMobile.text!, Email: txtEmail.text!)
        }
    }
    
    func setupTextFields() {
        self.txtFirstName.placeholder = "First Name".localized
        self.txtLastName.placeholder = "Last Name".localized
        self.txtMobile.placeholder = "Mobile Number".localized
        self.txtEmail.placeholder = "Email".localized
    }
    
}

class EditProfileViewController: UIViewController {

    private let viewImage = UIView()
    private let UserImage = UIImageView()
    private let viewProfile = UIView()
    private let PosterImageView = UIImageView()
    private let btnEditImage = UIButton()
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        // Do any additional setup after loading the view.
        
        self.CreateData()
    }
        
    func CreateData(){
        self.tblView.contentInset = UIEdgeInsets(top: 250, left: 0, bottom: 0, right: 0)
        self.viewProfile.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 250)
        
        self.PosterImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 250)
        self.PosterImageView.image = UIImage.init(named: "ProfilePoster")
        self.PosterImageView.contentMode = .scaleAspectFill
        self.PosterImageView.clipsToBounds = true
        
        self.viewProfile.clipsToBounds = true
        
        view.addSubview(viewProfile)
        self.viewProfile.addSubview(PosterImageView)
        self.viewProfile.addSubview(viewImage)
        
        self.viewImage.frame = CGRect(x: 0, y: 0, width: 130, height: 130)
        self.UserImage.frame = CGRect(x: 0, y: 0, width: 130, height: 130)
        
        self.UserImage.image = UIImage.init(named: "UserPlaceHolder")
        self.UserImage.layer.cornerRadius = UserImage.frame.height/2
        self.UserImage.layer.borderColor = UIColor.white.cgColor
        self.UserImage.layer.borderWidth = 5
        self.UserImage.contentMode = .scaleAspectFill
        self.UserImage.clipsToBounds = true
        self.viewImage.addSubview(UserImage)
        self.viewImage.center = viewProfile.center
        
        self.btnEditImage.frame = CGRect(x: 95, y: 95, width: 30, height: 30)
        self.btnEditImage.backgroundColor = UIColor.white
        self.btnEditImage.layer.cornerRadius = btnEditImage.frame.height/2
        self.btnEditImage.setImage(UIImage(named: "EditTerquoise"), for: .normal)
        self.btnEditImage.imageEdgeInsets = UIEdgeInsets (top: 8, left: 8, bottom: 8, right: 8)
        self.btnEditImage.addTarget(self, action: #selector(self.ActionSeletProfile(_:)), for: UIControl.Event.touchUpInside)
        self.viewImage.addSubview(btnEditImage)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ActionSeletProfile(_:)))
        self.UserImage.addGestureRecognizer(tapGesture)
        self.UserImage.isUserInteractionEnabled = true
        
        if let dicUser = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary
        {
            if let str = dicUser.value(forKey: "profile_pic") as? String, !str.isEmpty{
                var imgUrl:String = str + "&w=260"
                imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                UserImage.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
            }
            print(dicUser)
        }
    }
   
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
      
    
        self.navigationItem.leftBarButtonItems = [barBack]
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    /*func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "Back_White"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = "Select Your Location"
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.white
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack]
    }*/
    
   
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.Check(){
            return self.view.frame.height - 280
        }
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileCustomTableViewCell", for: indexPath) as! EditProfileCustomTableViewCell
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     let y = 250 - (scrollView.contentOffset.y + 250)
        let height = min(max(y, 0), 400)
        viewProfile.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        viewImage.frame = CGRect(x: 0, y: 0, width: 130, height: 130)
        viewImage.center = viewProfile.center
        PosterImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
    }
    
    func Check () -> Bool {
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136:
                return false
                print("iPhone 5 or 5S or 5C")

            case 1334:
                return false
                print("iPhone 6/6S/7/8")

            case 1920, 2208:
                return true
                print("iPhone 6+/6S+/7+/8+")

            case 2436:
                return true
                print("iPhone X/XS/11 Pro")

            case 2688:
                return true
                print("iPhone XS Max/11 Pro Max")

            case 1792:
                return true
                print("iPhone XR/ 11 ")

            default:
                print("Unknown")
            
            }
        }
        return false
    }
}

// MARK: - ActionDelegate
extension EditProfileViewController: UpdateProfileActionDelegate{
    func UpdateButtonPressed(FirstName: String, LastName: String, Mobile: String, Email: String) {
        let DictParameters = NSMutableDictionary()
        DictParameters.setValue(FROMAPP, forKey: "from_app")
        DictParameters.setValue(USERID, forKey: "user_id")
        DictParameters.setValue(API_ID, forKey: "apiId")
        DictParameters.setValue(FirstName, forKey: "first_name")
        DictParameters.setValue(LastName, forKey: "last_name")
        DictParameters.setValue(Mobile, forKey: "mobile")
        DictParameters.setValue(Email, forKey: "email")
//        DictParameters.setValue("+91", forKey: "phonecode")
        self.UpdateProfile(parameter: DictParameters)
    }
}

//MARK:- ****** Image Picker ********
extension EditProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc func ActionSeletProfile(_ sender: UITapGestureRecognizer)
    {
        if !GlobalMethod.internet()
        {
            GlobalMethod.popDialog(controller: self, title: "No internet connection", message: "Please check your internet connection.")
            return
        }
        self.openImagePicker(forImageView: self.UserImage)
    }
    
    func openImagePicker(forImageView imageView: UIImageView) {
        CustomImagePickerController.shared.showImagePickerFrom(vc: self, withOption: .askForOption, andCompletion: { (isSuccess, image) in
            if isSuccess {
                
                imageView.image = image
                self.updateProfilPicture()
            }
        })
    }
}

//MARK:- --- [API CALLING] ---

extension EditProfileViewController{
    
    func FetchProfile(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,"user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!, "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.PROFILE, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func UpdateProfile(parameter : NSMutableDictionary){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.UPDATE_PROFILE, isLoaderRequired: true, paramters: (parameter as! [String : Any])) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RELOAD_AFTER_PROFILE_UPDATE), object: nil)
                    self.navigationController?.popViewController(animated: true)

                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func updateProfilPicture()
    {
        if !GlobalMethod.internet(){
            
        }else{
            GlobalMethod.StartProgress(self.view)
            
            ServManager.viewController = self
            
            let urlAlert = URL(string: constant.UPDATE_PROFILE_PICTURE)!
            let headerS: HTTPHeaders = ["Authorization": "Bearer (HelperGlobalVars.shared.access_token)","Accept": "application/json"]
            let parameterS: Parameters = ["from_app":FROMAPP, "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,"apiId":API_ID]
            
            print(parameterS)


            AF.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in parameterS {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                }
                
                
                multipartFormData.append(self.UserImage.image!.jpegData(compressionQuality: 0.5)!, withName: "profile_pic" , fileName: "file.jpeg", mimeType: "image/jpeg")
            },
            to: urlAlert, method: .post , headers: nil)
            .response { resp in
                print(resp)
//                GlobalMethod.ShowToast(Text: msg, View: self.view)
                GlobalMethod.StopProgress(self.view)
                switch resp.result{
                case .failure(let error):
                print(error)
                case.success( _):
                print("🥶🥶Response after upload Img: (resp.result)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: RELOAD_AFTER_PROFILE_UPDATE), object: nil)
                }
            }
            
        }
    }
}

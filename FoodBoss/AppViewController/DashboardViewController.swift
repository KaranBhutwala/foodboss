//
//  DashboardViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 19/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SkeletonView
import SideMenu
import AwesomeSpotlightView

class DashboardViewController: UIViewController , AwesomeSpotlightViewDelegate {
    
    var menu : SideMenuNavigationController?
    
    @IBOutlet weak var collectionCategory: UICollectionView!
    @IBOutlet weak var collectionBanner: UICollectionView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var lblViewAll: UILabel!
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 5, left: 7.5, bottom: 5, right: 7.5)
    private var DictFilter = NSMutableDictionary()
    private var ArrayCoupon = NSMutableArray()
    private var ArrayOffer = NSMutableArray()
    private var ArrayCatagory = NSMutableArray()
    private var ArrayRestaurant = NSMutableArray()
    
    private let locationManager = CLLocationManager() // create Location Manager object
    private var latitude : Double!
    private var longitude : Double!
    
    private var coupnFetched = false
    private var categoryFetched = false
    private var restaurantFetched = false
    private var profileFetched = false
    
    @IBOutlet weak var viewBanner: UIView!
    
    @IBOutlet weak var tableViewChildView: UIView!
    @IBOutlet weak var lblRestaurantsCount: UILabel!
    
    @IBOutlet weak var OfferCV: UIView!
    @IBOutlet weak var CollectionOffer: UICollectionView!
    @IBOutlet weak var SegmentHolder: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var SelectedRestaurantType = ""
    
    @IBOutlet weak var viewCategoryShow: UIView!
    @IBOutlet weak var lblCategorySelected: UILabel!

    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = false

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        showSportAlert()
        GlobalsVariable.sharedInstance.FilterScreenComeFrom = "Dashboard"
        self.AddLeftBarButton()
        self.tabBarController?.tabBar.tintColor = COLOR_ORANGE!
        self.tabBarController?.tabBar.backgroundColor = UIColor.white
        self.SegmentHolder.showAnimatedGradientSkeleton()
       // self.viewCategoryShow.isHidden = true
        self.showSkeleton()
        self.tblView.estimatedRowHeight = 138
        self.tblView.registerNib(RestaurantTableViewCell.self)
        self.tblView.tableFooterView = UIView()

        segmentedControl.tintColor = .clear
        segmentedControl.setTitle("Delivery".localized, forSegmentAt: 0)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.orange], for: .selected)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)

        self.navigationController?.delegate = self
        viewBanner.layer.cornerRadius = 10
        self.CollectionOffer.register(UINib(nibName: "offercell", bundle: nil), forCellWithReuseIdentifier: "offercell")

         if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
          {
            print("REGISTER USER VIEW ==>>")

             self.FetchCouponList()
             self.FetchOffer()
             self.FetchCategory()
             self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)
             self.FetchProfile()
            }
            else
            {
                print("GUEST USER VIEW ==>>")

                NSUSERDEFAULT.set("", forKey: USERID)
                USERID = ""
                
                self.FetchCouponList()
                self.FetchOffer()
                self.FetchCategory()
                self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)
            }


        
        
    }
    
    func showSportAlert()
    {
        let hasLaunchedKey = "HasLaunched"
        let defaults = UserDefaults.standard
        let hasLaunched = defaults.bool(forKey: hasLaunchedKey)

        if !hasLaunched {
            defaults.set(true, forKey: hasLaunchedKey)
            let spotlight1 = AwesomeSpotlight(withRect: CGRect(x: self.view.layer.frame.width - 80, y: 20, width: 100, height: 100), shape: .circle, text: "You can sort your restaurant by ratings, time , price etc from here", isAllowPassTouchesThroughSpotlight: true)
            let spotlightView = AwesomeSpotlightView(frame: view.frame, spotlight: [spotlight1])
            spotlightView.cutoutRadius = 8
            spotlightView.delegate = self
            view.addSubview(spotlightView)
            spotlightView.start()
        }
    }
    
    @IBAction func btnCloseCategory(_ sender: UIButton)
    {
        
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            GlobalsVariable.sharedInstance.SelectedOptionForFilterType = "home_delivery"
            GlobalsVariable.sharedInstance.SelectedOptionForFilter = "1"
            segmentedControl.setTitle("Delivery".localized, forSegmentAt: 0)
            GlobalsVariable.sharedInstance.SelectedRestaurantType = "home_delivery"
            self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)

        case 1:
            GlobalsVariable.sharedInstance.SelectedOptionForFilterType = "self_service"
            GlobalsVariable.sharedInstance.SelectedOptionForFilter = "2"
            segmentedControl.setTitle("Self Pickup".localized, forSegmentAt: 1)
            GlobalsVariable.sharedInstance.SelectedRestaurantType = "self_service"
            self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)

        default:
            break;
        }
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {

        if let View = self.storyboard?.instantiateViewController(identifier: "ExploreCategoryVC") as? ExploreCategoryVC{
        self.navigationController?.pushViewController(View, animated: true)
        }
    }

    func AddLeftBarButton(){
        self.navigationItem.leftBarButtonItems = []
        self.navigationController?.navigationBar.tintColor = COLOR_TURQUOISE
        self.navigationController?.navigationBar.barTintColor = COLOR_TURQUOISE

        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "blackMenui.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionSendToADDRESS(_:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        button.tintColor = UIColor.darkGray
        let barButton = UIBarButtonItem(customView: button)
        
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        
        let addressSelected = UserDefaults.standard.string(forKey: ADDRESS_SELECTED)!
        lblHeader.text = addressSelected.capitalized
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.darkGray
        viewHeader.addSubview(lblHeader)
        
        let buttonPlace = UIButton.init(type: .custom)
        buttonPlace.setImage(UIImage(named: "imgBlack.png"), for: UIControl.State.normal)
        buttonPlace.addTarget(self, action: #selector(self.ActionSendToPLACE(_:)), for: UIControl.Event.touchUpInside)
        buttonPlace.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonPlace.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        buttonPlace.tintColor = UIColor.darkGray
        let barButtonPlace = UIBarButtonItem(customView: buttonPlace)
        self.navigationItem.leftBarButtonItems = [barButton, barButtonPlace, UIBarButtonItem (customView: viewHeader)]
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ActionSendToPLACE(_:)))
        viewHeader.addGestureRecognizer(tapGesture1)
        viewHeader.isUserInteractionEnabled = true
        
        let buttonNotification = UIButton(type: .custom)
        let image = UIImage(named: "Notification")?.withRenderingMode(.alwaysTemplate)
        buttonNotification.setImage(image, for: .normal)
        buttonNotification.frame = CGRect(x: 0, y: 0, width: 30, height: 30)

        buttonNotification.imageEdgeInsets = UIEdgeInsets (top: 5, left: 5, bottom: 5, right: 5)
        buttonNotification.addTarget(self, action: #selector(self.ActionNotification(_:)), for: UIControl.Event.touchUpInside)
        buttonNotification.tintColor = UIColor.darkGray
        
        let buttonFilter = UIButton(type: .custom)
        let image1 = UIImage(named: "Filter")?.withRenderingMode(.alwaysTemplate)
        buttonFilter.setImage(image1, for: .normal)
        buttonFilter.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonFilter.addTarget(self, action: #selector(self.ActionFilter(_:)), for: UIControl.Event.touchUpInside)
        buttonFilter.imageEdgeInsets = UIEdgeInsets (top: 5, left: 5, bottom: 5, right: 5)
        buttonFilter.tintColor = UIColor.darkGray
            
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem (customView: buttonFilter), UIBarButtonItem (customView: buttonNotification)]
    }
    
    @IBAction func ActionFilter(_ sender: Any) {
        var Controller = UIViewController()
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "FilterViewController") as? FilterViewController{
                Controller = View
                View.DictFilter = self.DictFilter
                View.Filter = { (Dict : NSMutableDictionary)in
                    self.DictFilter = NSMutableDictionary()
                    self.DictFilter = Dict
                    self.ArrayRestaurant = NSMutableArray()
                    self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)
                }
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController{
                Controller = View
                View.DictFilter = self.DictFilter
                View.Filter = { (Dict : NSMutableDictionary)in
                    print(Dict)
                    self.DictFilter = NSMutableDictionary()
                    self.DictFilter = Dict
                    self.ArrayRestaurant = NSMutableArray()
                    self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)
                }
            }
        }
        
        Controller.modalPresentationStyle = .formSheet
        
        let navigationController = UINavigationController(rootViewController: Controller)
        navigationController.modalPresentationStyle = .formSheet

        tabBarController?.present(navigationController, animated: true, completion: nil)

    }
    
    @IBAction func ActionNotification(_ sender: Any) {
        
        
        if UserDefaults.standard.string(forKey: LOGIN) == "Yes"
        {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "NotificationViewController") as? NotificationViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
        else
        {
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as? LoginViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
       
    }
    
    @IBAction func ActionSendToPLACE(_ sender: Any) {
        
        GlobalsVariable.sharedInstance.isComeFromMenuAddress = "true"
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                View.FromDashboard = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                View.FromDashboard = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func ActionSendToADDRESS(_ sender: Any) {
       
        
        if let View = self.storyboard?.instantiateViewController(identifier: "MenuVC") as? MenuVC{
            self.navigationController?.pushViewController(View, animated: true)
        }
    }
    
    
}

// MARK: - Pop Gesture Methods
// need to copy self.navigationController?.delegate = self
extension DashboardViewController:UIGestureRecognizerDelegate,UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if (navigationController.viewControllers.count > 1)
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationController.interactivePopGestureRecognizer?.isEnabled = true;
        }
        else
        {
            self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
            navigationController.interactivePopGestureRecognizer?.isEnabled = false;
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - TableView

extension DashboardViewController: UITableViewDelegate, SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "RestaurantTableViewCell"
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayRestaurant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RestaurantTableViewCell.self)
//        cell.delegate = self
//        cell.index = indexPath
        if let Dict = ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = tableView.dequeue(RestaurantTableViewCell.self)
        if cell.isSkeletonActive {
            cell.hideSkeleton()
        }
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                //return 250
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = self.ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
}

//MARK:- --- [CollectionView] ---

extension DashboardViewController : UICollectionViewDelegate, SkeletonCollectionViewDataSource
{
    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        if skeletonView == collectionBanner {
            return "cellBanner"
        }
            else if skeletonView == CollectionOffer {
                return "offercell"
            }
        else {
            return "cellTagSearch"
        }
    }
    
   
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionBanner{
            return self.ArrayCoupon.count
        }
        else if collectionView == CollectionOffer
        {
            return self.ArrayOffer.count
        }
        else{
            return self.ArrayCatagory.count
        }
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionBanner{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellBanner", for: indexPath)

            let img = cell.viewWithTag(1) as! UIImageView
            
            if let Dict = self.ArrayCoupon.object(at: indexPath.row) as? NSDictionary{
                if let str = Dict.value(forKey: "coupon_image") as? String{
                    let availableWidth = collectionView.frame.width
                    var imgUrl:String = str + "&w=\(availableWidth)"
                    imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
                }
            }
            return cell
        }
        else if collectionView == CollectionOffer {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offercell", for: indexPath)
            
            let img = cell.viewWithTag(1) as! UIImageView
            
            if let Dict = self.ArrayOffer.object(at: indexPath.row) as? NSDictionary{
                if let str = Dict.value(forKey: "coupon_image") as? String{
                    let availableWidth = collectionView.frame.width
                    var imgUrl:String = str + "&w=\(availableWidth)"
                    imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    img.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: nil)
                }
            }
            return cell
        }
        else if collectionView == collectionCategory {
            
            
            let cell = collectionCategory.dequeueReusableCell(withReuseIdentifier: "cellTagSearch", for: indexPath)
            cell.contentView.layer.cornerRadius = cell.frame.height/2
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.borderWidth = 0.3
            
            let lblTag = cell.viewWithTag(2) as! UILabel
            
            if let Dict = self.ArrayCatagory.object(at: indexPath.row) as? NSDictionary{
                if let str = Dict.value(forKey: "category") as? String{
                    lblTag.textColor = UIColor.orange
                    lblTag.layer.cornerRadius = 10
                    //lblTag.font = FONT_BOLD_CUSTOM(xx: 17)
                    lblTag.text = str.capitalized
                }
            }

           // GlobalMethod.applyShadowCardView(cell, andCorner: 10)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionBanner{
            if let Dict = self.ArrayCoupon.object(at: indexPath.row) as? NSDictionary{
                print(Dict)
                
                let resID = Dict.value(forKey: "restaurant_id") as! String
                if let View = self.storyboard?.instantiateViewController(identifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    
                    View.RestaurantID = resID
                    
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
        else  if collectionView == CollectionOffer{
            
            GlobalsVariable.sharedInstance.FilterScreenComeFrom = "OfferClick"
            if let Dict = self.ArrayOffer.object(at: indexPath.row) as? NSDictionary{
                if #available(iOS 13.0, *) {
                    if let View = self.storyboard?.instantiateViewController(identifier: "RestaurantListViewController") as? RestaurantListViewController{
                        
                        View.comeFrom = "OfferView"
                        if let str = Dict.value(forKey: "offer_type") as? String{
                            View.strHeaderTittle = str
                        }
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                } else {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListViewController") as? RestaurantListViewController{
                        View.comeFrom = "OfferView"
                        if let str = Dict.value(forKey: "offer_type") as? String{
                            View.strHeaderTittle = str
                        }
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
            }
        }
        else{
            if let Dict = self.ArrayCatagory.object(at: indexPath.row) as? NSDictionary{
                
                if #available(iOS 13.0, *)
                {
                    if let View = self.storyboard?.instantiateViewController(identifier: "RestaurantListViewController") as? RestaurantListViewController{
                        if let str = Dict.value(forKey: "category_id") as? String{
                            
                            View.CatID = str
                            View.CatType = Dict.value(forKey: "category") as? String ?? "Category"
                        }
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
                else
                {
                    if let View = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListViewController") as? RestaurantListViewController{
                        if let str = Dict.value(forKey: "category_id") as? String{
                            View.CatID = str
                            View.CatType = Dict.value(forKey: "category") as? String ?? "All Restaurant"

                        }
                        self.navigationController?.pushViewController(View, animated: true)
                    }
                }
            }
        }
    }
    
    func scrollToNextCell(){
        
        collectionBanner.layoutIfNeeded()
        //get cell size
        let cellSize = collectionBanner.frame.size

        //get current content Offset of the Collection view
        let contentOffset = collectionBanner.contentOffset

        if collectionBanner.contentSize.width <= collectionBanner.contentOffset.x + cellSize.width
        {
            let r = CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            collectionBanner.scrollRectToVisible(r, animated: true)

        } else {
            let r = CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            collectionBanner.scrollRectToVisible(r, animated: true)
        }

    }
}

extension DashboardViewController : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionBanner{
            let availableWidth = collectionView.frame.width
            let availableHeight = collectionView.frame.height - collectionView.contentInset.top - collectionView.contentInset.bottom
            return CGSize(width: availableWidth, height: availableHeight)
        }
        else if collectionView == CollectionOffer{
            return CGSize(width: 100, height: 120)
        }
        if collectionView == collectionCategory{
          if let Dict = ArrayCatagory.object(at: indexPath.item) as? NSDictionary{
               if let str = Dict.value(forKey: "category") as? String{
                   let labelSize = UILabel.estimatedSize("   " + str + "   ", targetSize: CGSize(width: collectionCategory.frame.size.width / 2 - 25, height: 40))
                   let cellSize = CGSize(width: labelSize.width, height: 40)
                   return cellSize
               }
           }
           return CGSize(width: 0 , height: 0)
        }
        else{
            return CGSize(width: 90, height: 90)
        }
    }
}

extension DashboardViewController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        latitude = location.latitude
        longitude = location.longitude
        self.FetchRestaurant(type: GlobalsVariable.sharedInstance.SelectedRestaurantType)
        locationManager.stopUpdatingLocation()
    }
}

extension DashboardViewController{
    
    func updateSkeletonView() {
        if coupnFetched && categoryFetched && restaurantFetched {
            hideSkeleton()
        }
    }
    
    func showSkeleton() {
        self.view.showAnimatedGradientSkeleton()
        self.tblView.showAnimatedGradientSkeleton()
        self.collectionBanner.showAnimatedGradientSkeleton()
        self.CollectionOffer.showAnimatedGradientSkeleton()
     //  self.collectionCategory.showAnimatedGradientSkeleton()
    }
    
    func hideSkeleton() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.view.hideSkeleton()
            self.tblView.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
           // self.collectionCategory.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
            //self.collectionBanner.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
            self.tblView.reloadData()
           // self.collectionBanner.reloadData()
           // self.collectionCategory.reloadData()
        }
    }
    
    func FetchOffer(){
        if !GlobalMethod.internet(){
            self.coupnFetched = true
            self.updateSkeletonView()
        }else{
            
            let parameter : [String : Any] = ["apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.OFFER_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                if isSuccess{
                    print(dataDict)
                    if let Data = CouponListModel.init(dictionary: dataDict){
                        for Dict in Data.coupons!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayOffer.add(dictTemp)
                                }
                            }
                        }
                        self.CollectionOffer.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        self.CollectionOffer.reloadData()
                        
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
               // self.coupnFetched = true
                //self.updateSkeletonView()
            }
        }
    }
    
    func FetchCouponList(){
        if !GlobalMethod.internet(){
            self.coupnFetched = true
            self.updateSkeletonView()
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "language":GlobalMethod.selectedLanguageForUser()]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.COUPON_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in

                if isSuccess{

                    if let Data = CouponListModel.init(dictionary: dataDict){
                        for Dict in Data.coupons!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayCoupon.add(dictTemp)
                                }
                            }
                        }
                        self.collectionBanner.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        self.collectionBanner.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
                self.coupnFetched = true
                self.updateSkeletonView()
            }
        }
    }
    
    func FetchCategory(){
        if !GlobalMethod.internet(){
            self.categoryFetched = true
            self.updateSkeletonView()
        }else{
            
//            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "language": GlobalMethod.selectedLanguageForUser()]
            ServManager.viewController = self
            ServManager.callPostAPI(constant.CATEGORY_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in

                if isSuccess{
                    print("CATEGORY RESPONSE ==>>",dataDict)
                    if let Data = CategoriesModel.init(dictionary: dataDict){
                        for Dict in Data.categories!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayCatagory.add(dictTemp)
                                }
                            }
                        }
                       
                        self.collectionCategory.reloadData()
                        
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
                self.categoryFetched = true
                self.updateSkeletonView()
            }
        }
    }
    
    func FetchRestaurant(type: String){
        if !GlobalMethod.internet(){
            self.restaurantFetched = true
            self.updateSkeletonView()
        }else{
            
            
            var Cuisine = ""
            var Rate = ""
            var Time = ""
            var LH = ""
            var HL = ""
            var Veg = ""
            
            
            if let str = self.DictFilter.value(forKey: "Cuisine") as? String{
                Cuisine = str
            }
            if let str = self.DictFilter.value(forKey: "Rate") as? String{
                Rate = str
            }
            if let str = self.DictFilter.value(forKey: "Time") as? String{
                Time = str
            }
            if let str = self.DictFilter.value(forKey: "LH") as? String{
                LH = str
            }
            if let str = self.DictFilter.value(forKey: "HL") as? String{
                HL = str
            }
            if let str = self.DictFilter.value(forKey: "Veg") as? String{
                Veg = str
            }
            
            
            
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "latitude":LATTITUDE,
                                          "longitude":LONGITUDE,
                                          "cuisine_id":Cuisine,
                                          "is_pureveg":Veg,
                                          "is_rating":Rate,
                                          "delivery_time":Time,
                                          "price_low_to_high":LH,
                                          "price_high_to_low":HL,
                                          "type":type,
                                          "language":GlobalMethod.selectedLanguageForUser()]
            print("RESTURANT FETCH PARAMETER ==>",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.RESTAURANT_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
//                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                   print(dataDict)
                    self.ArrayRestaurant.removeAllObjects()
                    if let Data = RestaurantModel.init(dictionary: dataDict){
                        for Dict in Data.restaurants!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayRestaurant.add(dictTemp)
                                }
                            }
                        }
//                        self.tblView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
                
                self.SegmentHolder.hideSkeleton()
                let lbl = "All Restaurant".localized
                self.lblRestaurantsCount.text = "\(lbl) (\(self.ArrayRestaurant.count))"
                self.restaurantFetched = true
                
                self.updateSkeletonView()
            }
        }
    }
    
    func FetchProfile(){
        if !GlobalMethod.internet(){
            self.profileFetched = true
            self.updateSkeletonView()
        }else{
            
//            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id": UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "apiId":API_ID,
                                          "language":GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.PROFILE, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
//                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print("fetch profile",dataDict)
                    
                    if let json = dataDict["user"] as? [String: Any?] {
                        let result = json.compactMapValues { $0 }
//                        print(result)
                        
                        if let DictUser = result as? NSDictionary{
                            NSUSERDEFAULT.set(DictUser, forKey: USER_DATA)
                            if let str = DictUser.value(forKey: "cart_session") as? String, str.count != 0
                            {
                                CART_SESSION = str
                                NSUSERDEFAULT.set(str, forKey: SAVED_CART_SESSION)
                                self.fetchCartDetails(cartSession: str)
                            }
                        }
                        
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
                self.profileFetched = true
                self.updateSkeletonView()
            }
        }
    }
    
    func fetchCartDetails(cartSession: String) {
        let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                          "apiId":API_ID,
                                          "language": GlobalMethod.selectedLanguageForUser(),
                                          "coupon_code": ""]
        
        ServManager.callPostAPI(constant.CART_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
            
            if isSuccess{
                let cartMap = dataDict["cart"] as? [String: Any] ?? [String: Any]()
                let cartCountStr = cartMap["total_quantity"] as? String ?? "0"
                guard let cartCount = Int(cartCountStr) else { return }
                if cartCount > 0 {
                    DispatchQueue.main.async {
                        let tabItem = self.tabBarController!.tabBar.items![2]
                        tabItem.badgeValue = cartCountStr
                    }
                }
            } else {
                
            }
        }
    }
}

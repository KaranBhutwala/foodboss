//
//  ProfileViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 19/05/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import BLTNBoard
import Alamofire
import GoogleSignIn

class ProfileCustomTableViewCell: UITableViewCell {
    @IBOutlet weak var viewMyAccount: UIView!
    @IBOutlet weak var viewHelp: UIView!
    @IBOutlet weak var viewOrders: UIView!
    @IBOutlet weak var viewReviews: UIView!
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var viewAppLanguage: UIView!
    @IBOutlet weak var viewBackground: UIView!
    weak var parallaxHeaderView: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        GlobalMethod.applyShadowCardView(viewMyAccount, andCorner: 10)
        GlobalMethod.applyShadowCardView(viewHelp, andCorner: 10)
        GlobalMethod.applyShadowCardView(viewOrders, andCorner: 10)
        GlobalMethod.applyShadowCardView(viewReviews, andCorner: 10)
        GlobalMethod.applyShadowCardView(viewLogout, andCorner: 10)
        
        self.viewBackground.layer.cornerRadius = 10
        self.viewBackground.layer.maskedCorners = [.layerMaxXMinYCorner , .layerMinXMinYCorner]
        
//        layerMaxXMaxYCorner – lower right corner
//        layerMaxXMinYCorner – top right corner
//        layerMinXMaxYCorner – lower left corner
//        layerMinXMinYCorner – top left corner

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

class ProfileViewController: UIViewController {
    
    let kDescription = "keyDescription"
    let kTitle = "kTitle"
    
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var tblView: UITableView!
    
//    private let viewProfile = UIView()
//    private let viewImage = UIView()
//    private let PosterImageView = UIImageView()
//    private let UserImage = UIImageView()
//    private let NumberLabel = UILabel()
//    private let UserLabel = UILabel()
//    private let UserEmail = UILabel()
//    weak var parallaxHeaderView: UIView?
    private var profileImageView = UIImageView()
    var dataSet = [[String: String]]()
    
    
    lazy var bulletinManager: BLTNItemManager = {
        let rootItem: BLTNItem = self.makeLogoutPage()
        return BLTNItemManager(rootItem: rootItem)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureText()
        self.tblView.registerNib(ProfileTableViewCell.self)
        self.setupHeaderView()
        //self.CreateData()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(Reload),
                                               name: NSNotification.Name(rawValue: RELOAD_AFTER_PROFILE_UPDATE),
                                               object: nil)
        self.AddLeftBarButton()
    }
    
    func configureText() {
        //self.dataSet.append([kTitle:"My Account",kDescription:"Address, Payments, Favourite & Offers"])
        self.dataSet.append([kTitle:"Manage Address".localized,kDescription:"Work, Home & Other...".localized])
        self.dataSet.append([kTitle:"Payments".localized,kDescription:"Wallet, Saved Cards...".localized])
        self.dataSet.append([kTitle:"Favourites".localized,kDescription:"Your top liked restaurant & food".localized])
        self.dataSet.append([kTitle:"Offers".localized,kDescription:"Best deals for you...".localized])
        
        self.dataSet.append([kTitle:"Past Orders".localized,kDescription:"View your past orders".localized])
        self.dataSet.append([kTitle:"Help".localized,kDescription:"FAQs & Links".localized])
        self.dataSet.append([kTitle:"App Language".localized,kDescription:"Change selected language".localized])
        self.dataSet.append([kTitle:"Logout".localized,kDescription:""])
        /*self.dataSet.append([kTitle:"Demo1",kDescription:""])
        self.dataSet.append([kTitle:"Demo2",kDescription:""])
        self.dataSet.append([kTitle:"Demo3",kDescription:""])
        self.dataSet.append([kTitle:"Demo4",kDescription:""])*/
    }
        
    func AddLeftBarButton(){
        self.navigationItem.leftBarButtonItems = []

        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "LocationWhite"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionSendToADDRESS(_:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barButton = UIBarButtonItem(customView: button)
        
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        lblHeader.text = ADDRESS.capitalized
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.white
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barButton, UIBarButtonItem (customView: viewHeader)]
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ActionSendToADDRESS(_:)))
        viewHeader.addGestureRecognizer(tapGesture1)
        viewHeader.isUserInteractionEnabled = true
     
        let buttonFilter = UIButton(type: .custom)
        let image = UIImage(named: "Notification")?.withRenderingMode(.alwaysTemplate)
        buttonFilter.setImage(image, for: .normal)
        buttonFilter.imageEdgeInsets = UIEdgeInsets (top: 5, left: 5, bottom: 5, right: 5)
        buttonFilter.addTarget(self, action: #selector(self.ActionNotification(_:)), for: UIControl.Event.touchUpInside)
        buttonFilter.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem (customView: buttonFilter)]
    }
    
    @IBAction func ActionNotification(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "NotificationViewController") as? NotificationViewController{
//                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController{
//                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func ActionSendToADDRESS(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                View.FromDashboard = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                View.FromDashboard = true
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func actionChangeLanguage(_ sender: Any) {
        var viewController = UIViewController()
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "LanguageViewController") as? ViewController {
                viewController = View
                View.fromProfile = true
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as? ViewController {
                viewController = View
                View.fromProfile = true
            }
        }
        
        viewController.modalPresentationStyle = .popover
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .formSheet

        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.tabBarController?.present(navigationController, animated: true, completion: nil)
        }
    }
    
    @objc func Reload(){
        self.FetchProfile()
    }
    
    func setupHeaderView() {
        let profileView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 250))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 250))
        imageView.image = UIImage.init(named: "ProfilePoster")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        profileView.addSubview(imageView)
        
        self.profileImageView = UIImageView(frame: CGRect(x: (UIScreen.main.bounds.size.width/2) - 65, y: 40, width: 130, height: 130))
        self.profileImageView.image = UIImage.init(named: "UserPlaceHolder")
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
        self.profileImageView.layer.borderColor = UIColor.white.cgColor
        self.profileImageView.layer.borderWidth = 5
        self.profileImageView.clipsToBounds = true
        
        profileView.addSubview(self.profileImageView)
        
        let userData = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary
        let firstName = (userData?["first_name"] as? String) ?? ""
        let lastName = (userData?["last_name"] as? String) ?? ""
        let userName = "\(firstName) \(lastName)"
        let userEmail = (userData?["email"] as? String) ?? ""
        let userPhone = (userData?["mobile"] as? String) ?? ""
        var imageUrl = (userData?["profile_pic"] as? String) ?? ""
        imageUrl = imageUrl.isEmpty ? "" : (imageUrl + "&w=260")
        
        self.profileImageView.sd_setImage(with: URL(string: imageUrl as String), placeholderImage: #imageLiteral(resourceName: "UserPlaceHolder"))
        
        let nameHeight = userName.height(constraintedWidth: UIScreen.main.bounds.size.width - 20,
                                                 font: FONT_BOLD_CUSTOM(xx: 15))
        let emailHeight = userEmail.height(constraintedWidth: UIScreen.main.bounds.size.width - 20,
                                           font: FONT_NORAML_CUSTOM(xx: 15))
        let phoneHeight = userPhone.height(constraintedWidth: UIScreen.main.bounds.size.width - 20,
                                           font: FONT_NORAML_CUSTOM(xx: 15))
        
        let userNameLabel = UILabel(frame: CGRect(x: 10,
                                                  y: profileImageView.frame.maxY + 10,
                                                  width: UIScreen.main.bounds.size.width - 20,
                                                  height: nameHeight))
        
        
        userNameLabel.numberOfLines = 0
        userNameLabel.font = FONT_BOLD_CUSTOM(xx: 15)
        userNameLabel.textColor = UIColor.white
        userNameLabel.text = userName
        userNameLabel.textAlignment = .center
        
        let userEmailLabel = UILabel(frame: CGRect(x: 10,
                                                   y: userNameLabel.frame.maxY + 5,
                                                   width: UIScreen.main.bounds.size.width - 20,
                                                   height: emailHeight))
        userEmailLabel.font = FONT_NORAML_CUSTOM(xx: 15)
        userEmailLabel.textColor = UIColor.white
        userEmailLabel.text = userEmail
        userEmailLabel.textAlignment = .center
        
        let userPhoneLabel = UILabel(frame: CGRect(x: 10,
                                                   y: userEmailLabel.frame.maxY + 5,
                                                   width: UIScreen.main.bounds.size.width - 20,
                                                   height: phoneHeight))
        userPhoneLabel.font = FONT_NORAML_CUSTOM(xx: 15)
        userPhoneLabel.textColor = UIColor.white
        userPhoneLabel.text = userPhone
        userPhoneLabel.textAlignment = .center
        
        profileView.addSubview(userNameLabel)
        profileView.addSubview(userEmailLabel)
        profileView.addSubview(userPhoneLabel)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ActionEditProfile(_:)))
        profileView.addGestureRecognizer(tapGesture)
        profileView.isUserInteractionEnabled = true
        
        self.tblView.tableHeaderView = profileView
        
        self.tblView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        print("Profile")
    }
    
    func makeLogoutPage() -> BLTNItem {
        let page = BLTNPageItem(title: "Logout".localized)
        page.image = UIImage(named: "LogoutLogo")
        
        page.descriptionText = "Are you sure you want to Logout?".localized
        page.actionButtonTitle = "Yes".localized
        page.alternativeButtonTitle = "Not now".localized
        page.isDismissable = true
        page.actionHandler = { (item: BLTNActionItem) in
            print("Action button tapped")
            
            item.manager?.dismissBulletin(animated: true)
            if let userData = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary {
                let strLoginType = userData["login_type"] as? String
                if let loginType = LoginType(rawValue: strLoginType ?? "") {
                    if loginType == .google {
                        GIDSignIn.sharedInstance()?.signOut()
                    } else if loginType == .facebook {
                        
                    } else {
                        print("\nLogin Type:\t\(loginType.rawValue)\n")
                    }
                }
            }
            
            self.clearUserData()

            ISLOGIN = "NO"
            if #available(iOS 13.0, *) {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NavLogin") as! UINavigationController
                let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
                
                keyWindow?.rootViewController = viewController
            }
            else{
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavLogin")
                APP!.window?.rootViewController = initialViewController
                APP!.window?.makeKeyAndVisible()
            }
            
            
        }
        
        page.alternativeHandler = { (item: BLTNActionItem) in
            print("Alternative button tapped")
            item.manager?.dismissBulletin(animated: true)
        }
        
        return page
        
    }
    
    func clearUserData() {
        NSUSERDEFAULT.removeObject(forKey: LOGIN)
        NSUSERDEFAULT.removeObject(forKey: USER_DATA)
        NSUSERDEFAULT.removeObject(forKey: LAT)
        NSUSERDEFAULT.removeObject(forKey: LONG)
        NSUSERDEFAULT.removeObject(forKey: ADDRESS_SELECTED)
        NSUSERDEFAULT.removeObject(forKey: DELIVERY_LOCATION)
        
        LATTITUDE = ""
        LONGITUDE = ""
        ADDRESS = ""
        USERID = ""
    }
    
    @IBAction func ActionMyAccount(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "MyAccountViewController") as? MyAccountViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
        /*if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "TestViewController") as? TestViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "TestViewController") as? TestViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }*/
        
    }
    
    @IBAction func ActionPastOrders(_ sender: Any) {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "OrderListViewController") as? OrderListViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "OrderListViewController") as? OrderListViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func ActionLogout(_ sender: Any) {
        
        bulletinManager.backgroundViewStyle = .dimmed
        bulletinManager.statusBarAppearance = .darkContent
        bulletinManager.showBulletin(above: self)
        
    }
    
}

//MARK:- --- [TABLEVIEW CALLING] ---

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSet.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        return 790
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCustomTableViewCell", for: indexPath) as! ProfileCustomTableViewCell
        cell.selectionStyle = .none
        return cell*/
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileLogoutCell", for: indexPath)
            GlobalMethod.applyShadowCardView(cell.viewWithTag(1)!, andCorner: 10)
            (cell.viewWithTag(2) as! UILabel).text = self.dataSet[indexPath.row][kTitle]
            return cell
        } else {
            let cell = tableView.dequeue(ProfileTableViewCell.self)
            cell.profileTitleLabel.text = self.dataSet[indexPath.row][kTitle]
            cell.profileDetailsLabel.text = self.dataSet[indexPath.row][kDescription]
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "LocationListViewController") as? LocationListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "LocationListViewController") as? LocationListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
            /*if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "MyAccountViewController") as? MyAccountViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }*/
            
            /*if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "TestViewController") as? TestViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "TestViewController") as? TestViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }*/
        case 1:
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "SavedPaymentMethodsViewController") as? SavedPaymentMethodsViewController {
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "SavedPaymentMethodsViewController") as? SavedPaymentMethodsViewController {
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        case 2:
            print("\nFavourites\n")
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "FavRestaurantListViewController") as? FavRestaurantListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "FavRestaurantListViewController") as? FavRestaurantListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        case 3:
            print("\nOffers\n")
        case 4:
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "OrderListViewController") as? OrderListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "OrderListViewController") as? OrderListViewController{
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        case 5:
            print("\nHelp\n")
        case 6:
            print("\nLanguage\n")
            self.actionChangeLanguage(AnyObject.self)
        case 7:
            print("\nLogout\n")
            bulletinManager.backgroundViewStyle = .dimmed
            bulletinManager.statusBarAppearance = .darkContent
            bulletinManager.showBulletin(above: self)
            
        default:
            print("\nDefault\n")
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*let y = 250 - (scrollView.contentOffset.y + 250)
        let height = min(max(y, 0), 400)
        viewProfile.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        PosterImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        viewImage.frame = CGRect(x: 0, y: 0, width: 150, height: 190)
        viewImage.center = viewProfile.center*/
    }
}

//MARK:- ****** Image Picker ********
extension ProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc func ActionEditProfile(_ sender: UITapGestureRecognizer)
    {
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "EditProfileViewController") as? EditProfileViewController{
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    @objc func ActionSeletProfile(_ sender: UITapGestureRecognizer)
    {
        if !GlobalMethod.internet()
        {
            GlobalMethod.popDialog(controller: self, title: "No internet connection", message: "Please check your internet connection.")
            return
        }
        self.openImagePicker(forImageView: self.profileImageView)
    }
    
    func openImagePicker(forImageView imageView: UIImageView) {
        CustomImagePickerController.shared.showImagePickerFrom(vc: self, withOption: .askForOption, andCompletion: { (isSuccess, image) in
            if isSuccess {
                
                imageView.image = image
                self.updateProfilPicture()
            }
        })
    }
}
//MARK:- --- [API CALLING] ---

extension ProfileViewController{
    
    func FetchProfile(){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.PROFILE, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let json = dataDict["user"] as? [String: Any?] {
                        let result = json.compactMapValues { $0 }
                        print(result)
                        
                        if let DictUser = result as? NSDictionary{
                            NSUSERDEFAULT.set(DictUser, forKey: USER_DATA)
                            self.setupHeaderView()
                        }
                    }
                }
                else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func updateProfilPicture()
    {
        if !GlobalMethod.internet(){
            
        }else{
            GlobalMethod.StartProgress(self.view)
            
            ServManager.viewController = self
            
            let urlAlert = URL(string: constant.UPDATE_PROFILE_PICTURE)!
            let headerS: HTTPHeaders = ["Authorization": "Bearer (HelperGlobalVars.shared.access_token)","Accept": "application/json"]
            let parameterS: Parameters = ["from_app":FROMAPP,"user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,"apiId":API_ID]
            
            print(parameterS)


            AF.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in parameterS {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                }
                
                
                multipartFormData.append(self.profileImageView.image!.jpegData(compressionQuality: 0.5)!, withName: "profile_pic" , fileName: "file.jpeg", mimeType: "image/jpeg")
            },
            to: urlAlert, method: .post , headers: nil)
            .response { resp in
                print(resp)
//                GlobalMethod.ShowToast(Text: msg, View: self.view)
                GlobalMethod.StopProgress(self.view)
                switch resp.result{
                case .failure(let error):
                print(error)
                case.success( _):
                print("🥶🥶Response after upload Img: (resp.result)")
                }
            }
            
        }
    }
}

/*
 func CreateData(){
 //        tblView.contentInset = UIEdgeInsets(top: 250, left: 0, bottom: 0, right: 0)
         
         viewProfile.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 250)
         
         PosterImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 250)
         PosterImageView.image = UIImage.init(named: "ProfilePoster")
         PosterImageView.contentMode = .scaleAspectFill
         PosterImageView.clipsToBounds = true
         viewProfile.clipsToBounds = true
         
         viewImage.frame = CGRect(x: 0, y: 0, width: 150, height: 170)
         viewImage.backgroundColor = .red
         
 //        view.addSubview(viewProfile)
         self.viewProfile.addSubview(PosterImageView)
         self.viewProfile.addSubview(viewImage)
         
         self.UserImage.frame = CGRect(x: 10, y: 0, width: 130, height: 130)
         self.UserImage.image = UIImage.init(named: "UserPlaceHolder")
         self.UserImage.contentMode = .scaleAspectFill
         self.UserImage.layer.cornerRadius = UserImage.frame.height/2
         self.UserImage.layer.borderColor = UIColor.white.cgColor
         self.UserImage.layer.borderWidth = 5
         self.UserImage.clipsToBounds = true
         self.viewImage.addSubview(UserImage)
         
         
         UserLabel.frame = CGRect(x: 0, y: 140, width: 150, height: 25)
         UserLabel.font = FONT_BOLD_CUSTOM(xx: 15)
         UserLabel.textColor = UIColor.white
         UserLabel.text = "Guest User".localized
         UserLabel.textAlignment = .center
         viewImage.addSubview(UserLabel)
         
         viewImage.center = viewProfile.center
         
         // Do any additional setup after loading the view.
         
         UserEmail.frame = CGRect(x: 0, y: 250-30 - 25, width: UIScreen.main.bounds.size.width, height: 25)
         UserEmail.font = FONT_NORAML_CUSTOM(xx: 15)
         UserEmail.textColor = UIColor.white
         UserEmail.text = ""
         UserEmail.textAlignment = .center
         viewProfile.addSubview(UserEmail)
         
         NumberLabel.frame = CGRect(x: 0, y: 250-30, width: UIScreen.main.bounds.size.width, height: 25)
         NumberLabel.font = FONT_NORAML_CUSTOM(xx: 15)
         NumberLabel.textColor = UIColor.white
         NumberLabel.text = ""
         NumberLabel.textAlignment = .center
         viewProfile.addSubview(NumberLabel)
         
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ActionEditProfile(_:)))
         UserImage.addGestureRecognizer(tapGesture)
         UserImage.isUserInteractionEnabled = true
         
         let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ActionEditProfile(_:)))
         viewProfile.addGestureRecognizer(tapGesture1)
         viewProfile.isUserInteractionEnabled = true
         
         self.SetData()
         
         self.tblView.tableHeaderView = self.viewProfile
     }
     
     func SetData(){
         if let dicUser = NSUSERDEFAULT.value(forKey: USER_DATA) as? NSDictionary
         {
             if let strfirst_name = dicUser.value(forKey: "first_name") as? String{
                 if let strlast_name = dicUser.value(forKey: "last_name") as? String{
                     UserLabel.text = strfirst_name + " " + strlast_name
                 }
             }
             
             if let strEmail = dicUser.value(forKey: "email") as? String{
                 UserEmail.text = strEmail
             }
             
             UserEmail.text = "manthan@gmail.com"
             
             if let str = dicUser.value(forKey: "mobile") as? String{
                 NumberLabel.text = str
             }
             
             if let str = dicUser.value(forKey: "profile_pic") as? String, !str.isEmpty{
                 var imgUrl:String = str + "&w=150"
                 imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                 UserImage.sd_setImage(with: URL(string: imgUrl as String), placeholderImage: #imageLiteral(resourceName: "UserPlaceHolder"))
             }
         }
     }
 */

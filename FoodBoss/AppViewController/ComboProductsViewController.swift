//
//  ItemMenuViewController.swift
//  FoodBoss
//
//  Created by Jay on 14/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Lottie

class ComboProductsViewController: UIViewController {
    
    
    var ingredientDetails: [String: Any] = [String: Any]()
    var itemComments: String = ""
    
    @IBOutlet weak var btnCustomize: EdgeRoundRectButton!
    @IBOutlet var viewCart: UIView!
    @IBOutlet weak var tblCart: UITableView!
    
    
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var viewLottie: UIView!
    
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var constraintLoaderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewIngrediants: UIView!
    @IBOutlet weak var lblIngrediants: UILabel!
    //@IBOutlet weak var constraintsIngrediantsHeight: NSLayoutConstraint!
    var DictItem = NSDictionary()
    var arrayOption = NSMutableArray()
    var arrayAddOn = NSMutableArray()
    var arrayData = NSMutableArray()
    var indextobeExpand = -1
    var arrayVariations : [NSDictionary] = []
    var arrayFullAddon : [NSDictionary] = []
    var arrayIngredients = NSMutableArray()
    var arraySelectedIngredients = NSMutableArray()
    var arrComboBaseSelectedOption : [NSArray] = []
    var arrSendingredientDetails: [String: Any] = [String: Any]()
    
    var itemID = ""
    var comboID = ""
    
    private var CustomizeTapped = false
    var isVeg = false
    var Qty = 1
    private var VariationID = ""
    var SearchItemID = ""
    var comeFrom = ""
    var HeaderTittle = ""
    @IBOutlet weak var HeaderImageHC: NSLayoutConstraint!
    var SearchItemName = ""
    var SearchItemDesc = ""
    var SearchItemPrice = ""
    var SearchQtyValue = ""
    var isCustomize : Bool = false
    var originalPrice : Float = Float()
    var finalPrice : Float = Float()
    var salePrice : Float = Float()
    var selectedSection: Int = 0
    var arrCombo : [NSDictionary] = []
    var basePrice : Float = Float()
    var addOnPrice : Float = Float()
    // var arrCustomizeTitle : [String] = []
    var arrCustomizeTitle: [(uniqId: String, title: String, price : String)] = []
    var arrComboSelection : [NSDictionary] = []
    var isChange : Bool = false
    var arrSendData = NSMutableArray()
    var arraySendVariations : [NSDictionary] = []
    var arraySendFullAddon : [NSDictionary] = []
    var arrSendCustomizeTitle: [(uniqId: String, title: String, price : String, select : Int)] = []
    var isSendCustomize : Bool = Bool()
    var arraySubselection : [NSArray] = []
    var arrSelection = NSMutableArray()
    var selectedCustomizeJson = ""
    var selectedIndex = 0
    var indexPath : IndexPath = [-1,-1]
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemDesc: UILabel!
    @IBOutlet weak var lblItemDescHC: NSLayoutConstraint!
    @IBOutlet weak var HeaderImageView: UIView!
    
    @IBOutlet weak var HeaderImage: UIImageView!
    var AddToCart: ((_ SentToCart: Bool, _ Qty : Int,_ Item : NSArray) -> Void)?
    //    var AddToCart: ((_ SentToCart: Bool, _ Qty : Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnCustomize.isHidden = true
        self.btnCustomize.layer.borderColor = COLOR_ORANGE?.cgColor
        self.btnCustomize.layer.borderWidth = 0.3
        
        let imageHeader = UserDefaults.standard.string(forKey: SELECTED_ITEM_IMAGE)
        print("IMAGE URL IN ITEM ==>>",imageHeader)
        if  imageHeader == ""
        {
            print("ITEM NOT HAVE IMAGE")
            HeaderImageHC.constant = 0
            HeaderImageView.isHidden = true
            
        }
        else
        {
            print("ITEM HAVE IMAGE")
            HeaderImageHC.constant = 250
            HeaderImageView.isHidden = false
            // HeaderImage.contentMode = .scaleAspectFill
            // var imgUrl:String = imageHeader!
            // imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            HeaderImage.contentMode = .scaleAspectFit
            HeaderImage.sd_setImage(with: URL(string: imageHeader!), placeholderImage: nil)
        }
        self.viewLottie.layoutIfNeeded()
        self.viewLottie.isHidden = false
        self.constraintLoaderHeight.constant = 2
        
        let animationView = AnimationView(name: "horizontal_loader")
        animationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 2)
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.loopMode = .loop
        
        viewLottie.addSubview(animationView)
        
        
        animationView.play()
        
        if GlobalsVariable.sharedInstance.ItemAddComeFrom == "SearchItem"
        {
            self.FetchItemDetails(ItemID: SearchItemID)
            lblItemDesc.text = SearchItemDesc
            lblPrice.text = CURRANCY + " " + SearchItemPrice
            Qty = Int(SearchQtyValue)!
            
            if Qty == 0
            {
                Qty = 1
            }
            self.lblQty.text = String (describing: Qty)
        }
        else
        {
            if let str = self.DictItem.value(forKey: "sale_price") as? String{
                lblPrice.text = CURRANCY + " " + str
                originalPrice = Float(str)!
                finalPrice = Float(str)!
            }
            self.lblQty.text = String (describing: Qty)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if let ItemID = self.DictItem.value(forKey: "item_id") as? String{
                    self.FetchItemDetails(ItemID: ItemID)
                }
                if let ItemID = self.DictItem.value(forKey: "item_id") as? Int{
                    self.FetchItemDetails(ItemID: String (describing: ItemID))
                }
            }
            
            if let str = DictItem.value(forKey: "item_name") as? String{
                HeaderTittle = str
            }
            if let str = DictItem.value(forKey: "item_description") as? String{
                lblItemDesc.text = str
            }
            
            if let Arr = DictItem.value(forKey: "ingredients") as? NSArray,Arr.count == 0{
                self.viewIngrediants.isHidden = true
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdateData(notification:)), name: NSNotification.Name(rawValue: "getNewData"), object: nil)
        
        // self.addNavigation()
        
        self.tblView.registerNib(HeaderTableViewCell.self)
        self.tblView.registerNib(LanguageSelectionTableViewCell.self)
        self.tblView.registerNib(FilterTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
        lblItemName.text = self.HeaderTittle
        
    }
    
    @objc func UpdateData(notification: NSNotification) {
        if let data = notification.userInfo as? [String : Any]{
            let receivedArr1 = data["arrData"]
            let receivedArr2 = data["arrVar"]
            let receivedArr3 = data["arrfullAdd"]
            let receivedArr4 = data["arrcustom"]
            let receivedArr5 = data["arrSendOption"]
            let received6 = data["price"] as! String
            let receive7 = data["itemID"] as! String
            let receive8 = data["arrSelect"]
            let receive9 = data["SendJsonBase"] as! String
            let receive10 = data["arrSendSelection"]
            let receive11 = data["arrInGrediants"]
            
            arrSendData = receivedArr1 as! NSMutableArray
            arraySendVariations = receivedArr2 as! [NSDictionary]
            arraySendFullAddon = receivedArr3 as! [NSDictionary]
            arrSendCustomizeTitle = receivedArr4 as! [(uniqId: String, title: String, price: String, select : Int)]
            arraySubselection = receivedArr5 as! [NSArray]
            lblPrice.text = CURRANCY + " " + received6
            originalPrice = Float(received6)!
            self.itemID = receive7
            arrComboBaseSelectedOption = receive8 as! [NSArray]
            selectedCustomizeJson = receive9
            arrSelection = receive10 as! NSMutableArray
            isChange = true
            arrSendingredientDetails = receive11 as! [String : Any]
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let Image = UIImageView()
        Image.image = UIImage(named: "NonVeg")
        
        if isVeg{
            Image.image = UIImage(named: "Veg")
        }
        Image.contentMode = .scaleAspectFit
        Image.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        let barBack = UIBarButtonItem(customView: Image)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        
        lblHeader.text = self.HeaderTittle
        
        lblHeader.font = FONT_NORAML_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white
    }
    
    @IBAction func ActionPlus(_ sender: Any) {
        if Qty == 0{
            let roundedVal = String(format:"%.2f", originalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
        }else{
            Qty += 1
            self.lblQty.text = String (describing: Qty)
            finalPrice = finalPrice + originalPrice
            let roundedVal = String(format:"%.2f", finalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
            for (index,DictData) in arrayData.enumerated(){
                if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
                    for DictInner in Arr{
                        if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                            if let Value = (DictInner as! NSMutableDictionary).value(forKey: "value_id") as? String{
                                if let Key = (DictData as! NSMutableDictionary).value(forKey: "item_variation_id") as? String{
                                    print(DictInner)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func ActionMinus(_ sender: Any) {
        if Qty == 1{
            let roundedVal = String(format:"%.2f", originalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
            return
        }else{
            Qty -= 1
            self.lblQty.text = String (describing: Qty)
            finalPrice = finalPrice - originalPrice
            let roundedVal = String(format:"%.2f", finalPrice)
            lblPrice.text = CURRANCY + " \(roundedVal)"
        }
        
    }
    
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    
    @IBAction func ActionAddToCart(_ sender: Any) {
        
        let Json = NSMutableDictionary()
        let ArrayAddon = NSMutableArray()
        for (index,DictData) in arrSendData.enumerated(){
//            if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
//                print(Arr)
//                var ISSelected = false
//                for DictInner in Arr{
//                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
//                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "value_id") as? String{
//                            if let Key = (DictData as! NSMutableDictionary).value(forKey: "item_variation_id") as? String{
//                                Json.setValue(Value, forKey: Key)
//                                ISSelected = true
//                            }
//                        }
//                    }
//                }
//
//                if !ISSelected{
//                    self.indextobeExpand = index
//                    if var str = (DictData as! NSDictionary).value(forKey: "title") as? String{
//                        str = "Select option from".localized + " \(str.uppercased()) " + "to continue!".localized
//                        GlobalMethod.ShowToast(Text: str, View: self.view)
//                    }
//                    self.tblView.reloadData()
//                    return
//                }
//            }
            
            
            if let Arr = (DictData as! NSDictionary).value(forKey: "add_on_options") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "id") as? String{
                            ArrayAddon.add(Value)
                        }
                    }
                }
            }
        }
        let JsonAddOn = NSMutableDictionary()
        for (index, ID) in ArrayAddon.enumerated(){
            let Key = String (describing: index)
            JsonAddOn.setValue(ID, forKey: Key)
        }
        
        
        
        var baseVariant : [String] = []
        var indexBase = 0
        if arrSelection.count != 0{
            for i in 0...arrSelection.count - 1{
                let dict = arrSelection[i] as! NSDictionary
                let id = dict.value(forKey: "item_variation_id") as! String
                if id == VariationID{
                    print(dict)
                    if let ArrValue = dict.value(forKey: "value") as? NSArray{
                        for DictInner in ArrValue{
                            if let arrItems = arraySubselection[i] as? [NSDictionary]{
                                print(arrItems)
                                for i in 0...arrItems.count - 1{
                                    if let dict = arrItems[i] as? NSDictionary{
                                        if let Selected = (dict as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                                            if let Value = (dict as! NSMutableDictionary).value(forKey: "value_id") as? String{
                                                baseVariant.append("\(Value)")
                                                indexBase = indexBase + 1
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                if let arrItems = arraySubselection[i] as? NSArray{
                                    print(arrItems)
                                    for index in 0...arrItems.count - 1{
                                        if let arrsubContent = arrItems[index] as? [NSDictionary]{
                                            for item in arrsubContent{
                                                // let dictnew = item[index] as? NSDictionary
                                                if let Selected = item.value(forKey: "selected") as? Int, Selected == 1{
                                                    if let Value = item.value(forKey: "value_id") as? String{
                                                        baseVariant.append("\(Value)")
                                                        indexBase = indexBase + 1
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        let newVal = uniq(source: baseVariant)
        let jsonBase = NSMutableDictionary()
        for (index, ID) in newVal.enumerated(){
            let Key = String (describing: index)
            jsonBase.setValue(ID, forKey: Key)
        }
        
        
        
        print("----------")
        print(Json)
        print("----------")
        print(JsonAddOn)
        
        var VariationString = GlobalMethod.ConvertInJsonToString(from: Json)
        if Json.allKeys.count == 0{
            VariationString = ""
        }
        
        var comboID = ""
        var itemID = ""
        let jsonForComboSelectedComboItems = NSMutableDictionary()
        let jsonForComboVariation = NSMutableDictionary()
        let tempJson = NSMutableDictionary()
        let JsonForComboValues = NSMutableDictionary()
        
        let tempCombovalJson2 = NSMutableDictionary()
        let tempCombovalJson1 = NSMutableDictionary()
        
      //  for item in arrayData{
           // if let dict = item as? NSDictionary{
        for i  in 0...arrayData.count - 1{
            if let dictNewItem = arrayData[i] as? NSDictionary{
                if let arrNewcombo = dictNewItem.value(forKey: "combo_options") as? [NSDictionary]{
                    for dictItem in arrNewcombo{
                        if let Selected = (dictItem as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                            if let newID = dictItem.value(forKey: "item_id") as? String{
                                itemID = newID
                                comboID = dictNewItem.value(forKey: "combo_id") as! String
                                jsonForComboSelectedComboItems.setValue(itemID, forKey: comboID)
                                for item in arrSendCustomizeTitle{
                                    if item.select == 1{
                                        let newComboID = dictNewItem.value(forKey: "combo_id") as! String
                                        let newID = item.uniqId
                                        if let arrVar = dictItem.value(forKey: "variations") as? [NSDictionary]{
                                            for itemVar in arrVar{
                                                if let itemVarID = itemVar.value(forKey: "item_variation_id") as? String{
                                                    if itemVarID == newID{
                                                        tempJson.setValue(itemVarID, forKey: "selected_variation")
                                                        jsonForComboVariation.setValue(tempJson, forKey: newComboID)
                                                        tempCombovalJson2.setValue(selectedCustomizeJson, forKey: itemVarID)
                                                        tempCombovalJson1.setValue(tempCombovalJson2, forKey: "selected_value")
                                                        JsonForComboValues.setValue(tempCombovalJson1, forKey: comboID)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                break
                            }
                        }
                    }
                }
            }
        }
           // }
    //    }
        
        
        print(JsonForComboValues)
        print(jsonForComboVariation)
        print(jsonForComboSelectedComboItems)
        
       // let selectedComboItem = GlobalMethod.ConvertInJsonToString(from: jsonForComboSelectedComboItems)
        
        //let comboVar = GlobalMethod.ConvertInJsonToString(from: jsonForComboVariation)
        
       // let finalCombo = GlobalMethod.ConvertInJsonToString(from: JsonForComboValues)
        
        if GlobalsVariable.sharedInstance.ItemAddComeFrom == "SearchItem"
        {
            self.AddToCart(SelectedComboVariation: jsonForComboSelectedComboItems, AddOn: ArrayAddon, ItemID: SearchItemID, comboItem: jsonForComboVariation, comboValues : JsonForComboValues)
        }
        else
        {
            if let ItemNewID = self.DictItem.value(forKey: "item_id") as? String{
                self.AddToCart(SelectedComboVariation: jsonForComboSelectedComboItems, AddOn: ArrayAddon, ItemID: ItemNewID, comboItem: jsonForComboVariation, comboValues : JsonForComboValues)
            }
            if let ItemNewID = self.DictItem.value(forKey: "item_id") as? Int{
                self.AddToCart(SelectedComboVariation: jsonForComboSelectedComboItems, AddOn: ArrayAddon, ItemID: "\(ItemNewID)", comboItem: jsonForComboVariation, comboValues : JsonForComboValues)
            }
           
        }
    }
    
    func GetPrice(){
        let Json = NSMutableDictionary()
        
        let ArrayAddon = NSMutableArray()
        for DictData in arrayData{
            if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "value_id") as? String{
                            if let Key = (DictData as! NSMutableDictionary).value(forKey: "id") as? String{
                                Json.setValue(Value, forKey: Key)
                            }
                        }
                    }
                }
            }
            
            if let Arr = (DictData as! NSDictionary).value(forKey: "add_on_options") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "id") as? String{
                            ArrayAddon.add(Value)
                        }
                    }
                }
            }
        }
        let JsonAddOn = NSMutableDictionary()
        for (index, ID) in ArrayAddon.enumerated(){
            let Key = String (describing: index)
            JsonAddOn.setValue(ID, forKey: Key)
        }
        
        var VariationString = GlobalMethod.ConvertInJsonToString(from: Json)
        if Json.allKeys.count == 0{
            VariationString = ""
        }
        
        if GlobalsVariable.sharedInstance.ItemAddComeFrom == "SearchItem"
        {
            self.GetPrice(Variation: VariationString!, AddOn: ArrayAddon, ItemID: SearchItemID)
        }
        else
        {
            if let ItemID = self.DictItem.value(forKey: "item_id") as? String{
                // self.GetPrice(Variation: VariationString!, AddOn: ArrayAddon, ItemID: ItemID)
            }
            if let ItemID = self.DictItem.value(forKey: "item_id") as? Int{
                //self.GetPrice(Variation: VariationString!, AddOn: ArrayAddon, ItemID: String (describing: ItemID))
            }
        }
    }
    
    @IBAction func ActionCustomize(_ sender: Any) {
        
        if CustomizeTapped{
            CustomizeTapped = false
            self.btnCustomize.backgroundColor = COLOR_ORANGE
            self.btnCustomize.setTitleColor(.white, for: .normal)
        }
        else{
            CustomizeTapped = true
            self.btnCustomize.backgroundColor = .white
            self.btnCustomize.setTitleColor(COLOR_ORANGE, for: .normal)
        }
        self.tblView.reloadData()
    }
    
    @IBAction func btnActionCustomize(_ sender: UIButton) {
        print(sender.tag)
      //  var Controller = UIViewController()
        if let View = self.storyboard?.instantiateViewController(identifier: "ItemMenuViewController") as? ItemMenuViewController{
            
            if let Dict = self.arrayData[selectedSection] as? NSDictionary{
                if let arrnew = Dict.value(forKey: "combo_options") as? [NSDictionary]{
                    let dictInner = arrnew[sender.tag]
                    View.DictItem = dictInner
                }
            }
            //  GlobalsVariable.sharedInstance.ItemAddComeFrom = "FoodController"
            //  Controller = View
            View.isFrom = (title: "Back", price : "\(originalPrice)")
            
            if isChange == true{
                View.isVeg = self.isVeg
                //     View.arrayData = arrSendData
                //View.arrayVariations = arraySendVariations
                //View.arrayFullAddon = arraySendFullAddon
                View.arrCustomizeTitle = arrSendCustomizeTitle
                // View.arrBaseSelectedOption = arrComboBaseSelectedOption
                //  View.originalPrice = self.originalPrice
                
                
                
                //            View.DictItem = self.DictItem
                View.ingredientDetails = arrSendingredientDetails
                View.arrayOption = self.arrayOption
                View.arrayAddOn = self.arrayAddOn
                View.arrayData = self.arrSendData
                View.arrayVariations = self.arraySendVariations
                View.arrayFullAddon = self.arrayFullAddon
                View.arrayIngredients = self.arrayAddOn
                View.arraySelectedIngredients = self.arrayAddOn
                // View.arrCustomizeTitle = self.arrCustomizeTitle
                View.arrayIngredients = self.arrayIngredients
                View.arrayOption = self.arrayOption
                View.isCustomize = self.isCustomize
                View.arrBaseSelectedOption = arrComboBaseSelectedOption
                View.arrSelection = self.arrSelection
                View.arrSubSelection = self.arraySubselection
                View.isUpdate = self.isChange
                
                View.AddToCart = { (Success : Bool, Qty : Int, Items : NSArray )in
                }
            }else{
                View.isUpdate = false
            }
            
            
            View.modalPresentationStyle = .overCurrentContext
            self.present(View, animated: true, completion: nil)
            return
        }
        
//        let navigationController = UINavigationController(rootViewController: Controller)
//        navigationController.modalPresentationStyle = .formSheet
//
//        tabBarController?.present(navigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func ActionExpand(_ sender: UIButton) {
        if CustomizeTapped{
            return
        }
        print(sender.tag)
        if self.indextobeExpand == sender.tag && self.indextobeExpand != -1{
            self.indextobeExpand = -1
        }
        else{
            self.indextobeExpand = sender.tag
        }
        self.tblView.reloadData()
    }
    
    func checkForIngredients() -> Bool {
        let ingredients = self.ingredientDetails[kItemIngredients] as? [[String: Any]]
        return ingredients != nil && !ingredients!.isEmpty
    }
    
    func getSelectedIngredients() -> [String] {
        let ingredients = (self.ingredientDetails[kItemIngredients] as? [[String: Any]]) ?? []
        let selectedIngredients = ingredients.compactMap({ (ingredient) -> String? in
            if let flag = ingredient[kIsSelected] as? Bool, flag == true {
                return ingredient[kIngredientName] as? String
            } else {
                return nil
            }
        })
        
        return selectedIngredients
    }
}

//MARK:- --- [TABLEVIEW CALLING] ---

extension ComboProductsViewController: UITableViewDelegate, UITableViewDataSource {
    //return UITableView.automaticDimension Header
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return self.arrayData.count
//        if self.arrayVariations.count != 0{
//            if let dictTemp = self.arrayVariations[selectionIndex] as? NSDictionary{
//                if let ArrInner = dictTemp.value(forKey: "other_data") as? NSArray, ArrInner.count > 1{
//                    return self.arrayVariations.count + ArrInner.count - 1 + self.arrayFullAddon.count
//                }else{
//                    print(self.arrayVariations.count + self.arrayFullAddon.count)
//                    return self.arrayVariations.count + self.arrayFullAddon.count
//                }
//            }
//        }
        return self.arrCombo.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let ingredients = self.ingredientDetails[kItemIngredients] as? [[String: Any]]
        
        if section == 0 && self.checkForIngredients() {
            let ingredientHeader = tableView.dequeue(IngredientHeader.self)
            var ingredientStr = ""
            for (index, element) in ingredients!.enumerated() {
                if index == 0 {
                    ingredientStr = element[kIngredientName] as! String
                } else {
                    ingredientStr += ", \(element[kIngredientName] as! String)"
                }
            }
            
            if (self.ingredientDetails[kIsCustomisable] as! Bool) {
                ingredientHeader.customiseButton.isHidden = false
            } else {
                ingredientHeader.customiseButton.isHidden = true
            }
            
            ingredientHeader.expandButton.tag = section
            ingredientHeader.expandButton.addTarget(self, action: #selector(ActionExpand(_:)), for: .touchUpInside)
            
            ingredientHeader.headerDiscriptionLabel.text = ingredientStr
            return ingredientHeader
        } else if section == self.arrayData.count - 1 &&
                    (self.arrayData[section] as! [String: Any]).keys.contains("comment") {
            return nil
        } else {
            let header = tableView.dequeue(HeaderItem.self)
            
            //header.lblDesc.isHidden = true
            header.btn.tag = section
            header.lblTitle.text = "Select Item"
            header.lblTitle.textColor = COLOR_TURQUOISE

            if self.indextobeExpand == section{
                header.imgRight.transform = header.imgRight.transform.rotated(by: .pi / 2)
            }
            
            header.backgroundColor = COLOR_BACKGROUND
            return header
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       60 // UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if section == self.arrayData.count - 1 &&
            (self.arrayData[section] as! [String: Any]).keys.contains("comment") {
            return 0
        } else {
            return 70
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isCustomisable = (self.ingredientDetails[kIsCustomisable] as? Bool) ?? false
        let ingredients = (self.ingredientDetails[kItemIngredients] as? [[String: Any]]) ?? []
        if section == 0 && self.checkForIngredients() {
            if isCustomisable && indextobeExpand == section {
                return ingredients.count
            } else {
                return 0
            }
        } else if section == self.arrayData.count - 1 && (self.arrayData[section] as! [String: Any]).keys.contains("comment") {
            return 1
        } else {
            if indextobeExpand == section{
                if let Dict = self.arrCombo[section] as? NSDictionary{
//                    if isCustomize == true {
//                        if section == 0{
//                            return arrayVariations.count
//                        }
//                        if section == 1{
//                            let dict1 = self.arrayVariations[selectionIndex]
//                            if let ArrInner = dict1["other_data"] as? NSArray, ArrInner.count != 0{
//                                if let ArrItems = ArrInner.value(forKey: "items") as? NSArray, ArrItems.count != 0{
//                                    return (ArrItems[0] as! NSArray).count
//                                }
//                            }
//
//                        }
//                        //                        if selectionIndex != 0{
//                        //                            if section == 2{
//                        //                                let dict1 = self.arrayVariations[selectionIndex]
//                        //                                if let ArrInner = dict1["other_data"] as? NSArray, ArrInner.count != 0{
//                        //                                    if let ArrItems = ArrInner.value(forKey: "items") as? NSArray, ArrItems.count != 0{
//                        //                                        return (ArrItems[selectionIndex] as! NSArray).count
//                        //                                    }
//                        //                                }
//                        //
//                        //                            }
//                        //                        }
//
//                    }
                    
                    if let Arr = Dict.value(forKey: "items") as? [NSDictionary]{
                        return Arr.count
                    }
                    if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                        return Arr.count
                    }
                }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && self.checkForIngredients() {
            let cell = tableView.dequeue(FilterTableViewCell.self)
            cell.img.image = #imageLiteral(resourceName: "uncheck")
            cell.img.setImageColor(color: GRAY!)
            let allIngredients = self.ingredientDetails[kItemIngredients] as! [[String: Any]]
            let ingredientInfo = allIngredients[indexPath.row]
            cell.lbl.text = (ingredientInfo[kIngredientName] as! String).capitalized
            if (ingredientInfo[kIsSelected] as! Bool) == true {
                cell.img.image = #imageLiteral(resourceName: "checked")
                cell.img.setImageColor(color: COLOR_TURQUOISE!)
            }
            
            cell.lbl.textColor = .black
            cell.lblPrice.text = ""
            cell.selectionStyle = .none
            return cell
        } else {
            if let Dict = self.arrayData[indexPath.section] as? NSDictionary{
                if let arrnew = Dict.value(forKey: "combo_options") as? [NSDictionary]{
                    let dictInner = arrnew[indexPath.row]
                    let cell = tableView.dequeue(LanguageSelectionTableViewCell.self)
                    cell.lbl.text = dictInner["item_name"] as? String
                    cell.vWObjCustomize.isHidden = true
                    cell.imgRadio.image = #imageLiteral(resourceName: "radioUnselect")
                    cell.imgRadio.setImageColor(color: GRAY!)
                    if let strSelected = dictInner["selected"] as? Int, strSelected == 1{
                        cell.imgRadio.image = #imageLiteral(resourceName: "radioSelect")
                        cell.imgRadio.setImageColor(color: COLOR_TURQUOISE!)
                        
                        if let customize = dictInner["is_customization"] as? String, customize == "1"{
                            cell.btnObjCustomize.tag = indexPath.row
                            selectedSection = indexPath.section
                            cell.btnObjCustomize.addTarget(self, action: #selector(btnActionCustomize(_:)), for: .touchUpInside)
                            cell.vWObjCustomize.isHidden = false
                        }else{
                            cell.vWObjCustomize.isHidden = true
                        }
                    }
                    cell.selectionStyle = .none
                    
                    return cell
                }
                
                if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                    if let DictInner = Arr.object(at: indexPath.row) as? NSDictionary{
                        let cell = tableView.dequeue(FilterTableViewCell.self)
                        cell.configure(with: DictInner)
                        cell.img.image = #imageLiteral(resourceName: "uncheck")
                        cell.img.setImageColor(color: GRAY!)
                        if let strSelected = DictInner.value(forKey: "selected") as? Int, strSelected == 1{
                            cell.img.image = #imageLiteral(resourceName: "checked")
                            cell.img.setImageColor(color: COLOR_TURQUOISE!)
                        }
                        cell.selectionStyle = .none
                        return cell
                    }
                }
                if let _ = Dict["comment"] as? String {
                    let cell = tableView.dequeue(ItemCommentCell.self)
                    cell.commentTextView.text = self.itemComments
                    cell.textChanged { [weak tableView] (newText: String) in
                        DispatchQueue.main.async {
                            self.itemComments = newText
                            tableView?.beginUpdates()
                            tableView?.endUpdates()
                        }
                    }
                    cell.selectionStyle = .none
                    return cell
                }
            }
        }
        
        let cell = UITableViewCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && self.checkForIngredients() {
            var allIngredients = self.ingredientDetails[kItemIngredients] as! [[String: Any]]
            var ingredientInfo = allIngredients[indexPath.row]
            ingredientInfo[kIsSelected] = !(ingredientInfo[kIsSelected] as! Bool)
            allIngredients[indexPath.row] = ingredientInfo
            self.ingredientDetails[kItemIngredients] = allIngredients
        } else {
            
            if indexPath == self.indexPath{
                isChange = true
            }else{
                isChange = false
            }
            
            if let Dict = self.arrayData[indexPath.section] as? NSDictionary{
                if let arrnew = Dict.value(forKey: "combo_options") as? [NSDictionary]{
                    let dictInner = arrnew[indexPath.row]
                    self.SetArrayUnSelect(Array: arrnew as NSArray)
                    dictInner.setValue(1, forKey: "selected")
                }
            }
            
            if let Dict = self.arrayData.object(at: indexPath.section) as? NSDictionary{
                if let Arr = Dict.value(forKey: "value") as? NSArray{
                    if let DictInner = Arr.object(at: indexPath.row) as? NSMutableDictionary{
                        print(DictInner)
                        self.SetArrayUnSelect(Array: Arr)
                        DictInner.setValue(1, forKey: "selected")
                    }
                }
                if let Arr = Dict.value(forKey: "add_on_options") as? NSArray{
                    if let DictInner = Arr.object(at: indexPath.row) as? NSMutableDictionary{
                        print(DictInner)
                        if let Selected = DictInner.value(forKey: "selected") as? Int, Selected == 0{
                            DictInner.setValue(1, forKey: "selected")
                        }
                        else{
                            DictInner.setValue(0, forKey: "selected")
                        }
                    }
                }
            }

            selectedSection = indexPath.section
            selectedIndex = indexPath.row
            self.indexPath = [selectedSection, selectedIndex]
            self.tblView.reloadData()
            //self.GetPrice()
          //  self.getLatestPrice()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func SetArrayUnSelect(Array : NSArray){
        for DictInner in Array{
            if let dicInner = DictInner as? NSMutableDictionary{
                dicInner.setValue(0, forKey: "selected")
            }
        }
    }
    
    func getLatestPrice(){
        let Json = NSMutableDictionary()
        let ArrayselectedAddon = NSMutableArray()
        
        var baseP : Float = Float()
        var addonP : Float = Float()
        
        for DictData in arrayData{
            if let Arr = (DictData as! NSDictionary).value(forKey: "value") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "addon_price") as? NSNumber{
                            let price : Float = Float(truncating: Value)
                            baseP = baseP + price
                            print(baseP)
                        }
                    }
                }
            }
            
            
            
            if let Arr = (DictData as! NSDictionary).value(forKey: "add_on_options") as? NSArray{
                for DictInner in Arr{
                    if let Selected = (DictInner as! NSMutableDictionary).value(forKey: "selected") as? Int, Selected == 1{
                        if let Value = (DictInner as! NSMutableDictionary).value(forKey: "addon_price") as? String{
                            let price : Float = Float(Value)!
                            addonP = addonP + price
                            print(addonP)
                            //ArrayselectedAddon.add(Value)
                        }
                    }
                }
            }
        }
        
//        let price = arrCustomizeTitle[selectionIndex].price
//
//        let finalVal = baseP + addonP
//        if finalVal == 0{
//            originalPrice = Float(price)!
//        }else{
//            originalPrice = finalVal + Float(price)!
//        }
        
        let roundedVal = String(format:"%.2f", originalPrice)
        lblPrice.text = CURRANCY + " \(roundedVal)"
        print(Json)
        print(ArrayselectedAddon)
    }
    
}

//MARK:- --- [API CALLING] ---

extension ComboProductsViewController
{
    func FetchItemDetails(ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "item_id":ItemID,
                                              "apiId":API_ID]
            print("ADD ITEM PARAMETER ==>> ",parameter)
            
            self.ingredientDetails.removeAll()
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.MENU_ITEM_DETAILS, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                self.viewLottie.isHidden = true
                self.constraintLoaderHeight.constant = 0
                if isSuccess{
                    
                    if let dictItems = dataDict.value(forKey: "items") as? NSDictionary{
                        
                        if let customisable = dictItems["custom_ingredients"] as? String, customisable == "1" {
                            self.ingredientDetails[kIsCustomisable] = true
                        } else {
                            self.ingredientDetails[kIsCustomisable] = false
                        }
                        
                        if let ingredients = dictItems["ingredients"] as? [String] {
                            self.ingredientDetails[kItemIngredients] = ingredients.map({ [kIngredientName: $0, kIsSelected: false] })
                        }
                        
                        if let ingredients = self.ingredientDetails[kItemIngredients] as? [[String: Any]], !ingredients.isEmpty {
                            self.arrayData.add(self.ingredientDetails)
                        }
                        
                        if let str = dictItems.value(forKey: "is_customization") as? String, str == "1"{
                            self.btnCustomize.isHidden = false
                            self.isCustomize = true
                        }
                        
                        if let ID = dictItems.value(forKey: "variation_id") as? String{
                            self.VariationID = ID
                        }
                        if let ID = dictItems.value(forKey: "variation_id") as? Int{
                            self.VariationID = String (describing: ID)
                        }
                        
                        if let Arr = dictItems.value(forKey: "ingredients") as? NSArray, Arr.count != 0{
                            for str in Arr{
                                self.arrayIngredients.add(str)
                            }
                        }
                        
                        if let Arr = dictItems.value(forKey: "variations") as? NSArray, Arr.count != 0{
                            self.arrayVariations = Arr as! [NSDictionary]
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        if let str = dictTemp.value(forKey: "option_name") as? String{
                                            let id = dictTemp.value(forKey: "item_variation_id") as? String
                                            if let str1 = dictTemp.value(forKey: "sale_price") as? NSNumber, str1 != 0{
                                                self.arrCustomizeTitle.append((uniqId: id!, title: str, price : "\(str1)"))
                                            }else if let str1 = dictTemp.value(forKey: "sale_price") as? String, str1 != "0"{
                                                self.arrCustomizeTitle.append((uniqId: id!, title: str, price : str1))
                                            }else{
                                                self.arrCustomizeTitle.append((uniqId: id!, title: str, price : ""))
                                            }
                                            
                                            // self.arrayOption.add(str)
                                        }
                                        
                                        let ARR = NSMutableArray()
                                        if let ArrInner = dictTemp.value(forKey: "other_data") as? NSArray, Arr.count != 0{
                                            if let ArrItems = ArrInner.value(forKey: "items") as? NSArray, Arr.count != 0{
                                                var arrnew : [NSDictionary] = []
                                                if ArrItems.count == 1{
                                                    arrnew = ArrItems[0] as! [NSDictionary]
                                                }else{
                                                    for i in 0...ArrItems.count - 1{
                                                        if let dict = ArrItems[i] as? [NSDictionary]{
                                                            arrnew.append(contentsOf: dict)
                                                        }
                                                        
                                                    }
                                                    // arrnew = ArrItems[0] as! [NSDictionary]
                                                }
                                                
                                                var index = 0
                                                for DictInner in arrnew{
                                                    if let dicInner = DictInner as? NSDictionary{
                                                        if let dictInnerTemp = (dicInner.mutableCopy()) as? NSMutableDictionary{
                                                            if index == 0{
                                                                dictInnerTemp.setValue(true, forKey: "selected")
                                                                self.VariationID = self.arrCustomizeTitle[0].uniqId
                                                            }else{
                                                                dictInnerTemp.setValue(false, forKey: "selected")
                                                            }
                                                            self.arrayOption.add(dictInnerTemp)
                                                            ARR.add(dictInnerTemp)
                                                        }
                                                    }
                                                    index = index + 1
                                                }
                                            }
                                        }
                                        dictTemp.setValue(ARR, forKey: "value")
                                       // self.arrSelection.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if let Arr = dictItems.value(forKey: "add_ons") as? NSArray, Arr.count != 0{
                            self.arrayFullAddon = Arr as! [NSDictionary]
                            for Dict in Arr{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        if let str = dictTemp.value(forKey: "addon_for") as? String{
                                            self.arrayAddOn.add(str)
                                        }
                                        
                                        let ARR = NSMutableArray()
                                        if let ArrInner = dictTemp.value(forKey: "add_on_options") as? NSArray, Arr.count != 0{
                                            for DictInner in ArrInner{
                                                if let dicInner = DictInner as? NSDictionary{
                                                    if let dictInnerTemp = (dicInner.mutableCopy()) as? NSMutableDictionary{
                                                        dictInnerTemp.setValue(false, forKey: "selected")
                                                        self.arrayAddOn.add(dictInnerTemp)
                                                        ARR.add(dictInnerTemp)
                                                    }
                                                }
                                            }
                                        }
                                        dictTemp.setValue(ARR, forKey: "add_on_options")
                                        self.arrayData.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        if let ArrCom = dictItems.value(forKey: "combos") as? [NSDictionary], ArrCom.count != 0{
                            self.arrCombo = ArrCom
                           
                            for Dict in ArrCom{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
//                                        if let str = dictTemp.value(forKey: "addon_for") as? String{
//                                            self.arrayAddOn.add(str)
//                                        }
                                        
                                        let ARR = NSMutableArray()
                                        if let ArrInner = dictTemp.value(forKey: "items") as? NSArray, ArrCom.count != 0{
                                            var index = 0
                                            for DictInner in ArrInner{
                                                if let dicInner = DictInner as? NSDictionary{
                                                    if let dictInnerTemp = (dicInner.mutableCopy()) as? NSMutableDictionary{
                                                        if index == 0{
                                                            dictInnerTemp.setValue(true, forKey: "selected")
                                                        }else{
                                                            dictInnerTemp.setValue(false, forKey: "selected")
                                                        }
                                                        self.arrComboSelection.append(dictInnerTemp)
                                                        ARR.add(dictInnerTemp)
                                                    }
                                                }
                                                index = index + 1
                                            }
                                        }
                                        dictTemp.setValue(ARR, forKey: "combo_options")
                                        self.arrayData.add(dictTemp)
                                    }
                                }
                            }
                        }
                        
                        //   print(self.arrayData)
                        print("\nNumber of Sections:\t\(self.arrayData.count)\n")
                        // Manthan 30-9-2020
                        // Check if user can add comments for selected item
                        let canAddComment = dictItems["comments_for_item"] as? String ?? "0"
                        if canAddComment == "1" {
                            self.arrayData.add(["comment": "1"])
                            self.arrayFullAddon.append(["comment": "1"])
                        }
                        self.tblView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func UpdateCart(cart_id : String, Qty : String){
        //        if !GlobalMethod.internet(){
        //
        //        }else{
        ////            GlobalMethod.StartProgress(self.view)
        //
        //            let parameter : [String : Any] = ["from_app":FROMAPP,"quantity":Qty,"apiId":API_ID, "cart_id":cart_id,"cart_session":CART_SESSION]
        //            print(parameter)
        //            ServManager.viewController = self
        //            ServManager.callPostAPI(constant.UPDATE_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
        //
        //                if isSuccess{
        //                    print(dataDict)
        //                    GlobalMethod.StopProgress(self.view)
        //
        //                    var Array = NSArray()
        //                    if let Dict = dataDict.value(forKey: "cart") as? NSDictionary{
        //                        if let str = Dict.value(forKey: "total_quantity") as? String{
        //                            if let strTotal = Dict.value(forKey: "grand_total") as? String{
        //                                if str == "0"{
        //
        //                                    self.viewCart.isHidden = true
        //                                    self.constraintViewCartHeight.constant = 0
        //                                }
        //                                else if str == "1"{
        //                                    self.lblCart.text = str + " item | " + CURRANCY + " " + strTotal
        //                                }
        //                                else{
        //                                    self.lblCart.text = str + " items | " + CURRANCY + " " + strTotal
        //                                }
        //                            }
        //                        }
        //
        //
        //                        if let Arr = Dict.value(forKey: "cart_items") as? NSArray{
        //                            Array = Arr
        //                        }
        //                    }
        //
        //                    if let Dictionary = self.ArrayMenuCategory.object(at: self.selectedSection) as? NSMutableDictionary{
        //                        if let Arr = Dictionary.value(forKey: "menu_items") as? NSMutableArray{
        //                            if let Dict = Arr.object(at: self.selectedIndex) as? NSMutableDictionary{
        //                                Dict.setValue(Array, forKey: "cart_items")
        //                            }
        //                        }
        //                    }
        //
        //                    self.viewCart.isHidden = false
        //                    self.constraintViewCartHeight.constant = 50
        //                    self.tblView.reloadData()
        //
        //                }else{
        //                    GlobalMethod.ShowToast(Text: msg, View: self.view)
        //                    GlobalMethod.StopProgress(self.view)
        //                }
        //            }
        //        }
    }
    
    func AddToCart(SelectedComboVariation : NSDictionary, AddOn : NSMutableArray, ItemID : String, comboItem : NSDictionary, comboValues : NSDictionary){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              //"user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "combo_values" : GlobalMethod.ConvertInJsonToString(from: comboValues)!,
                                              "combo_variation" : GlobalMethod.ConvertInJsonToString(from: comboItem)!,
                                              "selected_combo_item" : GlobalMethod.ConvertInJsonToString(from: SelectedComboVariation)!,
                                              "item_id":ItemID,
                                              //"variation_options":comboItem,
                                              "auto_added_with":"",
                                              "is_auto_added":"",
                                              "selected_ingredients":GlobalMethod.ConvertInJsonToString(from: self.getSelectedIngredients())!,
                                              "addons":GlobalMethod.ConvertInJsonToString(from: AddOn)!,
                                              "quantity":String (describing: Qty),
                                              "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                              "is_customization": 0,
                                              "variation_id":"",
                                              "apiId":API_ID,
                                              "comments": self.itemComments.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines),
                                              "final_price": originalPrice,
                                              "language" : GlobalMethod.selectedLanguageForUser(),
                                              "selected_variation" : ""]
            print("ADD TO CART PARAMETER ==>> ",parameter)
            
            
            ServManager.viewController = self
            ServManager.callPostAPI(constant.ADD_TO_CART, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print("ADD TO CART RESPONSE ==>> ",dataDict)
                    if let DictCart = dataDict.value(forKey: "cart") as? NSDictionary{
                        if let Arr = DictCart.value(forKey: "cart_items") as? NSArray{
                            if let block = self.AddToCart {
                                block(true, self.Qty, Arr)
                                GlobalsVariable.sharedInstance.ItemAddComeFrom = ""
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                        }
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func GetPrice(Variation : String, AddOn : NSMutableArray, ItemID : String){
        if !GlobalMethod.internet(){
            
        }else{
            
            //            GlobalMethod.StartProgress(self.view)
            
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "item_id":ItemID,
                                              "selected_variation":Variation,
                                              "addons":GlobalMethod.ConvertInJsonToString(from: AddOn)!,
                                              //                                      "quantity":String (describing: Qty),
                                              //                                      "cart_session":CART_SESSION,
                                              "is_customization": "1",
                                              "variation_id":VariationID,
                                              "apiId":API_ID]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.GET_ITEM_PRICE, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    print(dataDict)
                    if let DictCart = dataDict.value(forKey: "item_price") as? NSDictionary{
                        if let ID = DictCart.value(forKey: "variation_id") as? String{
                            self.VariationID = ID
                        }
                        if let ID = DictCart.value(forKey: "variation_id") as? Int{
                            self.VariationID = String (describing: ID)
                        }
                        if let str = DictCart.value(forKey: "price") as? String{
                            self.lblPrice.text = CURRANCY + " " + str
                        }
                    }
                    
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}


//
//  RestaurantListViewController.swift
//  FoodBoss
//
//  Created by Raj Shukla on 07/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import SkeletonView

class RestaurantListViewController: UIViewController {
    
    private var ArrayRestaurant = NSMutableArray()
    @IBOutlet weak var tblView: UITableView!
    var CatID = ""
    var CatType = ""

    var strHeaderTittle = ""
    var comeFrom = ""

    private var DictFilter = NSMutableDictionary()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.addNavigation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.registerNib(RestaurantTableViewCell.self)
        self.tblView.tableFooterView = UIView()
        self.tblView.estimatedRowHeight = 104
        
        if comeFrom == "OfferView"
        {
            self.FetchRestaurant()
        }
        else
        {
            self.FetchRestauran1()
        }
    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.white

        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
        let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
        
        if comeFrom == "OfferView"
        {
            lblHeader.text = strHeaderTittle
        }
        else
        {
            lblHeader.text = "\(CatType) Category".localized
        }
        
        lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
        lblHeader.textColor = UIColor.black
        viewHeader.addSubview(lblHeader)
        
        self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
        
        let buttonFilter = UIButton(type: .custom)
        let image = UIImage(named: "Filter")?.withRenderingMode(.alwaysTemplate)
        buttonFilter.addTarget(self, action: #selector(self.ActionFilter(_:)), for: UIControl.Event.touchUpInside)
        buttonFilter.setImage(image, for: .normal)
        buttonFilter.imageEdgeInsets = UIEdgeInsets (top: 5, left: 5, bottom: 5, right: 5)
        buttonFilter.tintColor = UIColor.black
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem (customView: buttonFilter)]
//        imgEvent.setImageColor(color: COLOR_GRAY_ACTIVE!)
    }
    
  
    
    
    @IBAction func ActionFilter(_ sender: Any) {
        var Controller = UIViewController()
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "FilterViewController") as? FilterViewController{
                Controller = View
                View.DictFilter = self.DictFilter
                View.Filter = { (Dict : NSMutableDictionary)in
                    self.DictFilter = NSMutableDictionary()
                    self.DictFilter = Dict
                    self.ArrayRestaurant = NSMutableArray()
                    self.FetchRestaurantFilter()
                }
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController{
                Controller = View
                View.DictFilter = self.DictFilter
                View.Filter = { (Dict : NSMutableDictionary)in
                    self.DictFilter = NSMutableDictionary()
                    self.DictFilter = Dict
                    self.ArrayRestaurant = NSMutableArray()
                    self.self.FetchRestaurantFilter()
                }
            }
        }
        
        Controller.modalPresentationStyle = .formSheet
        
        let navigationController = UINavigationController(rootViewController: Controller)
        navigationController.modalPresentationStyle = .formSheet
        
        tabBarController?.present(navigationController, animated: true, completion: nil)
        
    }
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

// MARK: - TableView

extension RestaurantListViewController: SkeletonTableViewDelegate, SkeletonTableViewDataSource {
    
    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "RestaurantTableViewCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection _: Int) -> Int {
        return self.ArrayRestaurant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RestaurantTableViewCell.self)
        if let Dict = ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let Dict = self.ArrayRestaurant.object(at: indexPath.row) as? NSDictionary{
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "FoodDetailsViewController") as? FoodDetailsViewController{
                    View.DictRestaurant = Dict
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
}
//MARK:- --- [API CALLING] ---

extension RestaurantListViewController{
    func FetchRestaurant()
    {
        if !GlobalMethod.internet(){
            
        }else{
            
            self.tblView.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "latitude":LATTITUDE,
                                          "longitude":LONGITUDE,
                                          "category_id":"",
                                          "type": GlobalsVariable.sharedInstance.SelectedRestaurantType,
                                          "offer_type" : self.strHeaderTittle,
                                          "language": GlobalMethod.selectedLanguageForUser()]
            print("RESUTAURANT LIST PARAMETER ==>>",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.RESTAURANT_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
//                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    if let Data = RestaurantModel.init(dictionary: dataDict){
                        for Dict in Data.restaurants!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayRestaurant.add(dictTemp)
                                }
                            }
                        }
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    func FetchRestauran1()
    {
        if !GlobalMethod.internet(){
            
        }else{
            
            self.tblView.showAnimatedGradientSkeleton()
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                          "apiId":API_ID,
                                          "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                          "latitude":LATTITUDE,
                                          "longitude":LONGITUDE,
                                          "category_id":CatID,
                                          "type": "",
                                          "offer_type" : self.strHeaderTittle,
                                          "language": GlobalMethod.selectedLanguageForUser()]
            //print("RESUTAURANT LIST PARAMETER ==>>",parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.RESTAURANT_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
//                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    if let Data = RestaurantModel.init(dictionary: dataDict){
                        for Dict in Data.restaurants!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayRestaurant.add(dictTemp)
                                }
                            }
                        }
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                    self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    
    func FetchRestaurantFilter()
        {
            if !GlobalMethod.internet(){
                
            }
            else{
                var Cuisine = ""
                var Rate = ""
                var Time = ""
                var LH = ""
                var HL = ""
                var Veg = ""
                
                
                if let str = self.DictFilter.value(forKey: "Cuisine") as? String{
                    Cuisine = str
                }
                if let str = self.DictFilter.value(forKey: "Rate") as? String{
                    Rate = str
                }
                if let str = self.DictFilter.value(forKey: "Time") as? String{
                    Time = str
                }
                if let str = self.DictFilter.value(forKey: "LH") as? String{
                    LH = str
                }
                if let str = self.DictFilter.value(forKey: "HL") as? String{
                    HL = str
                }
                if let str = self.DictFilter.value(forKey: "Veg") as? String{
                    Veg = str
                }
                
                self.tblView.showAnimatedGradientSkeleton()
                let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "latitude":LATTITUDE,
                                              "longitude":LONGITUDE,
                                              "category_id":CatID,
                                              "type": GlobalsVariable.sharedInstance.SelectedRestaurantType,
                                              "language": GlobalMethod.selectedLanguageForUser(),
                                              "cuisine_id": Cuisine,
                                              "is_pureveg":Veg,
                                              "is_rating":Rate,
                                              "delivery_time":Time,
                                              "price_low_to_high":LH,
                                              "price_high_to_low":HL]
                print("RESUTAURANT LIST PARAMETER FILTER ==>>",parameter)
                ServManager.viewController = self
                ServManager.callPostAPI(constant.RESTAURANT_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                    if isSuccess{
                        print(dataDict)
                        if let Data = RestaurantModel.init(dictionary: dataDict){
                            for Dict in Data.restaurants!{
                                if let dic = Dict as? NSDictionary{
                                    if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                        self.ArrayRestaurant.add(dictTemp)
                                    }
                                }
                            }
                            self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                            self.tblView.reloadData()
                        }
                    }else{
                        GlobalMethod.ShowToast(Text: msg, View: self.view)
                        GlobalMethod.StopProgress(self.view)
                        self.tblView.hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.25))
                        self.tblView.reloadData()
                    }
                }
            }
        }
}

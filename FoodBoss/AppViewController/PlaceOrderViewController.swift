//
//  PlaceOrderViewController.swift
//  FoodBoss
//
//  Created by Jay on 21/06/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Lottie

class PlaceOrderViewController: UIViewController, CAAnimationDelegate {

    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var viewAnimation: UIView!
    var DictOrder = NSDictionary()
    var deliveryTime = ""
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    
    @IBOutlet weak var img1: UIButton!
    @IBOutlet weak var img2: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        View1.layer.cornerRadius = 15
        View2.layer.cornerRadius = 15

        if let str = DictOrder.value(forKey: "order_number") as? String{
            lblOrderId.text = "Order #" + str
        }
        
        if let str1 = DictOrder.value(forKey: "delivery_time") as? String{
            lblDeliveryTime.text = str1
        }
        
        
        self.addNavigation()
        self.addAnimatedTick()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        tabBarController?.tabBar.isHidden = true

    }
    
    func addNavigation()
    {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
        let barBack = UIBarButtonItem(customView: button)
        
        
        self.navigationItem.leftBarButtonItems = [barBack]
    }
    
    
    @IBAction func CallAction(_ sender: UIButton)
    {
        if let str = DictOrder.value(forKey: "restaurant_number") as? String, !str.isEmpty{
            if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    
    @objc func ActionBack()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func ActionTrackOrder(_ sender: Any) {
        var orderId = ""
        if let orderInt = DictOrder["order_id"] as? Int {
            orderId = String(orderInt)
        } else {
            orderId = DictOrder["order_id"] as? String ?? ""
        }
        if #available(iOS 13.0, *) {
            if let View = self.storyboard?.instantiateViewController(identifier: "OrderDetailViewController") as? OrderDetailViewController{
                /*if let str = DictOrder.value(forKey: "order_id") as? Int{
                    View.OrderID = String (describing: str)
                }*/
                View.OrderID = orderId
                self.navigationController?.pushViewController(View, animated: true)
            }
        } else {
            if let View = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as? OrderDetailViewController{
                /*if let str = DictOrder.value(forKey: "order_id") as? Int{
                    View.OrderID = String (describing: str)
                }*/
                View.OrderID = orderId
                self.navigationController?.pushViewController(View, animated: true)
            }
        }
    }
    
    @IBAction func ActionCallRestaurant(_ sender: Any) {
       
    }
    
    func addAnimatedTick() {
        let layer = CAShapeLayer()
        let bounds = CGRect(x: 0, y: 0, width: 150, height: 150)
        layer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 75, height: 75)).cgPath
        let color = UIColor(red: 42.0/255.0, green: 179.0/255.0, blue: 75.0/255.0, alpha: 1.0)
        layer.strokeColor = color.cgColor
        layer.fillColor = nil
        layer.lineWidth = 12
        self.viewAnimation.layer.addSublayer(layer)
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 1.5
        animation.delegate = self
        layer.add(animation, forKey: "line")
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        imageView.image = UIImage(named: "only_tick")
        UIView.animate(withDuration: 0.3, delay: 0, options: .showHideTransitionViews, animations: {
            self.viewAnimation.addSubview(imageView)
            var trans = CGAffineTransform(scaleX: 0.5, y: 0.5)
            imageView.transform = trans
            trans = CGAffineTransform(scaleX: 1.3, y: 1.3)
            imageView.transform = trans
        }, completion: nil)
    }
    
}

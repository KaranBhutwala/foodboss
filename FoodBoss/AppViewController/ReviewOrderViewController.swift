//
//  ReviewOrderViewController.swift
//  FoodBoss
//
//  Created by Apple on 18/08/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit

class ReviewOrderViewController: UIViewController,UITextViewDelegate {
    var ArrayItems = NSMutableArray()
    var OrderID = ""
    private let ArrayRate = NSMutableArray()
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var viewFooter: UIView!
    @IBOutlet weak var viewRating: HCSStarRatingView!
    @IBOutlet weak var txtReview: UITextView!
    private var Placeholder = "Describe your review*".localized
    var Review: ((_ Yey: Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNavigation()
        tblView.registerNib(RateTableViewCell.self)
        self.tblView.tableFooterView = viewFooter
        
        for _ in ArrayItems{
            ArrayRate.add(5.0)
        }
        
        self.txtReview.layer.cornerRadius = 5
        self.txtReview.layer.borderColor = UIColor.darkGray.cgColor
        self.txtReview.layer.borderWidth = 0.3
        
        txtReview.text = Placeholder
        txtReview.textColor = UIColor.lightGray
        
        self.btn.layoutIfNeeded()
        self.btn.roundCorners([.topLeft, .topRight], radius: 15)
    }
    
   
    func addNavigation()
       {
           self.navigationController?.navigationBar.topItem?.title = ""
           self.navigationController?.navigationBar.topItem?.title = ""
           self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
           self.navigationController?.navigationBar.shadowImage = UIImage()
           self.navigationController?.navigationBar.isTranslucent = true
           self.navigationController?.view.backgroundColor = UIColor.white
           
           let button = UIButton.init(type: .custom)
           button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
           button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
           let barBack = UIBarButtonItem(customView: button)
           
           let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
           let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
           
           
           lblHeader.text = "Rate restaurant".localized
           lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
           lblHeader.textColor = UIColor.black
           viewHeader.addSubview(lblHeader)
       
           self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
       }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Placeholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    @objc func ActionBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ActionRestaurantRating(_ sender: HCSStarRatingView) {
        print(sender.value)
    }
    
    @IBAction func ActionRate(_ sender: Any) {
        if txtReview.text == Placeholder{
            txtReview.becomeFirstResponder()
            GlobalMethod.ShowToast(Text: "Please Write Review", View: self.view)
            return
        }
        if (txtReview.text.trimmingCharacters(in: .whitespaces).isEmpty){
            txtReview.becomeFirstResponder()
            GlobalMethod.ShowToast(Text: "Please Write Review", View: self.view)
            return
        }
        let ARRAY = NSMutableArray()
        for (index, Dict) in ArrayItems.enumerated(){
            let Dictionary = NSMutableDictionary()
            if let Star = self.ArrayRate.object(at: index) as? CGFloat{
                Dictionary.setValue(Int(Star), forKey: "rate")
            }
            if let str = (Dict as! NSDictionary).value(forKey: "order_item_id") as? String{
                Dictionary.setValue(str, forKey: "order_item_id")
            }
            Dictionary.setValue("", forKey: "comments")
            ARRAY.add(Dictionary)
        }
//        print(ARRAY)
        
        let parameters : [String : Any] = ["from_app":FROMAPP,
                                            "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                            "items":GlobalMethod.ConvertInJsonToString(from: ARRAY)!,
                                            "restaurant_rate":Int(viewRating.value),
                                            "review":txtReview.text!,
                                            "order_id":OrderID,
                                            "cart_session":UserDefaults.standard.string(forKey: SAVED_CART_SESSION)!,
                                            "apiId":API_ID]
        print(parameters)
        self.Review(parameter: parameters)
        
    }
    
}

// MARK: - TableView

extension ReviewOrderViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ArrayItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(RateTableViewCell.self)
        cell.delegateRate = self
        cell.index = indexPath
        if let Dict = ArrayItems.object(at: indexPath.row) as? NSDictionary{
            cell.configure(with: Dict)
        }
        if let Star = self.ArrayRate.object(at: indexPath.row) as? CGFloat{
            cell.viewRate.value = Star
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension ReviewOrderViewController: RateDelegate{
    func Rate(_ index: IndexPath, Rate: CGFloat) {
        print(Rate)
        self.ArrayRate.replaceObject(at: index.row, with: Rate)
        self.tblView.reloadData()
    }
    
    
}

//MARK:- --- [API CALLING] ---

extension ReviewOrderViewController{
    func Review(parameter : [String : Any]){
        if !GlobalMethod.internet(){
            
        }else{
            
            GlobalMethod.StartProgress(self.view)
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.REVIEW, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                    if let block = self.Review {
                        block(true)
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
}

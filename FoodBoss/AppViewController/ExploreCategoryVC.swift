//
//  ExploreCategoryVC.swift
//  FoodBoss
//
//  Created by IOS Development on 09/10/20.
//  Copyright © 2020 Raj Shukla. All rights reserved.
//

import UIKit
import Alamofire

class ExploreCategoryVC: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {

    var estimateWidth = 160.0
    var cellMarginSize = 16.0
    var arrPartners = [[String:AnyObject]]()
    var ArrayCatagory = NSMutableArray()
    var restaurantDetailsRequest: DataRequest? = nil

    @IBOutlet weak var CollectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           navigationController?.setNavigationBarHidden(false, animated: true)
           tabBarController?.tabBar.isHidden = true

       }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tabBarController?.tabBar.isHidden = true
        addNavigation()
        FetchCategory()
        self.CollectionView.delegate = self
        self.CollectionView.dataSource = self
        self.CollectionView.register(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setupGridView()
    }
    
    func FetchCategory(){
        if !GlobalMethod.internet(){
        }else{
            
            let parameter : [String : Any] = ["from_app":FROMAPP,
                                              "apiId":API_ID,
                                              "user_id":UserDefaults.standard.string(forKey: SESSION_USER_ID)!,
                                              "language": GlobalMethod.selectedLanguageForUser()]
            print(parameter)
            ServManager.viewController = self
            ServManager.callPostAPI(constant.CATEGORY_LIST, isLoaderRequired: true, paramters: parameter) { (isSuccess, status, msg, dataDict) in
                //                GlobalMethod.StopProgress(self.view)
                if isSuccess{
                                    print(dataDict)
                    if let Data = CategoriesModel.init(dictionary: dataDict){
                        for Dict in Data.categories!{
                            if let dic = Dict as? NSDictionary{
                                if let dictTemp = (dic.mutableCopy()) as? NSMutableDictionary{
                                    self.ArrayCatagory.add(dictTemp)
                                }
                            }
                        }
                    self.CollectionView.reloadData()
                    }
                }else{
                    GlobalMethod.ShowToast(Text: msg, View: self.view)
                    GlobalMethod.StopProgress(self.view)
                }
            }
        }
    }
    
    func setupGridView() {
        let flow = CollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ArrayCatagory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CollectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as! ItemCell
        
      
        if let Dict = self.ArrayCatagory.object(at: indexPath.row) as? NSDictionary{
         
            let str = Dict.value(forKey: "icon") as? String
            var imgUrl:String = str!
            imgUrl = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let str1 = Dict.value(forKey: "category") as? String
            cell.setData(text: str1!, img: imgUrl)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let Dict = self.ArrayCatagory.object(at: indexPath.row) as? NSDictionary{
            print(Dict)
            if #available(iOS 13.0, *) {
                if let View = self.storyboard?.instantiateViewController(identifier: "RestaurantListViewController") as? RestaurantListViewController{
                    if let str = Dict.value(forKey: "category_id") as? String{
                        View.CatID = str
                    }
                    self.navigationController?.pushViewController(View, animated: true)
                }
            } else {
                if let View = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListViewController") as? RestaurantListViewController{
                    if let str = Dict.value(forKey: "category_id") as? String{
                        View.CatID = str
                    }
                    self.navigationController?.pushViewController(View, animated: true)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: width)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 3)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        
        return width
    }
    
   
    func addNavigation()
       {
           self.navigationController?.navigationBar.topItem?.title = ""
           self.navigationController?.navigationBar.topItem?.title = ""
           self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
           self.navigationController?.navigationBar.shadowImage = UIImage()
           self.navigationController?.navigationBar.isTranslucent = true
           self.navigationController?.view.backgroundColor = UIColor.white
           
           let button = UIButton.init(type: .custom)
           button.setImage(UIImage(named: "backIconB.png"), for: UIControl.State.normal)
           button.addTarget(self, action: #selector(self.ActionBack), for: UIControl.Event.touchUpInside)
           button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           button.imageEdgeInsets = UIEdgeInsets (top: 3, left: 3, bottom: 3, right: 3)
           let barBack = UIBarButtonItem(customView: button)
           
           let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 44))
           let lblHeader = UILabel(frame: CGRect(x: 5, y: 0, width: viewHeader.frame.size.width - 15, height: viewHeader.frame.size.height))
           
           
           lblHeader.text = "Categories".localized
           lblHeader.font = FONT_BOLD_CUSTOM(xx: 15)
           lblHeader.textColor = UIColor.black
           viewHeader.addSubview(lblHeader)
       
           self.navigationItem.leftBarButtonItems = [barBack, UIBarButtonItem (customView: viewHeader)]
       }

    @objc func ActionBack()
       {
           self.navigationController?.popViewController(animated: true)
       }
}
